module rojff_to_quaff
    use erloff, only: error_list_t, fatal_t, module_t, procedure_t
    use quaff
    use rojff, only: &
            fallible_json_value_t, json_object_t, json_string_t, json_value_t

    implicit none
    private
    public :: &
            fallible_acceleration_t, &
            fallible_acceleration_unit_t, &
            fallible_amount_t, &
            fallible_amount_unit_t, &
            fallible_amount_rate_t, &
            fallible_amount_rate_unit_t, &
            fallible_amount_temperature_t, &
            fallible_amount_temperature_unit_t, &
            fallible_amount_temperature_rate_t, &
            fallible_amount_temperature_rate_unit_t, &
            fallible_angle_t, &
            fallible_angle_unit_t, &
            fallible_area_t, &
            fallible_area_unit_t, &
            fallible_conductance_t, &
            fallible_conductance_unit_t, &
            fallible_delta_temperature_t, &
            fallible_delta_temperature_unit_t, &
            fallible_density_t, &
            fallible_density_unit_t, &
            fallible_diffusivity_t, &
            fallible_diffusivity_unit_t, &
            fallible_dynamic_viscosity_t, &
            fallible_dynamic_viscosity_unit_t, &
            fallible_energy_t, &
            fallible_energy_unit_t, &
            fallible_enthalpy_t, &
            fallible_enthalpy_unit_t, &
            fallible_fluence_t, &
            fallible_fluence_unit_t, &
            fallible_force_t, &
            fallible_force_unit_t, &
            fallible_fracture_toughness_t, &
            fallible_fracture_toughness_unit_t, &
            fallible_frequency_t, &
            fallible_frequency_unit_t, &
            fallible_heat_capacity_t, &
            fallible_heat_capacity_unit_t, &
            fallible_heat_transfer_coefficient_t, &
            fallible_heat_transfer_coefficient_unit_t, &
            fallible_insulance_t, &
            fallible_insulance_unit_t, &
            fallible_intensity_t, &
            fallible_intensity_unit_t, &
            fallible_inverse_molar_mass_t, &
            fallible_inverse_molar_mass_unit_t, &
            fallible_length_t, &
            fallible_length_unit_t, &
            fallible_mass_t, &
            fallible_mass_unit_t, &
            fallible_mass_flux_t, &
            fallible_mass_flux_unit_t, &
            fallible_mass_rate_t, &
            fallible_mass_rate_unit_t, &
            fallible_molar_density_t, &
            fallible_molar_density_unit_t, &
            fallible_molar_enthalpy_t, &
            fallible_molar_enthalpy_unit_t, &
            fallible_molar_mass_t, &
            fallible_molar_mass_unit_t, &
            fallible_molar_specific_heat_t, &
            fallible_molar_specific_heat_unit_t, &
            fallible_power_t, &
            fallible_power_unit_t, &
            fallible_pressure_t, &
            fallible_pressure_unit_t, &
            fallible_specific_heat_t, &
            fallible_specific_heat_unit_t, &
            fallible_speed_t, &
            fallible_speed_unit_t, &
            fallible_temperature_gradient_t, &
            fallible_temperature_gradient_unit_t, &
            fallible_temperature_t, &
            fallible_temperature_unit_t, &
            fallible_thermal_conductivity_t, &
            fallible_thermal_conductivity_unit_t, &
            fallible_thermal_expansion_coefficient_t, &
            fallible_thermal_expansion_coefficient_unit_t, &
            fallible_time_t, &
            fallible_time_unit_t, &
            fallible_volume_t, &
            fallible_volume_unit_t, &
            fallible_volume_rate_t, &
            fallible_volume_rate_unit_t, &
            fallible_volumetric_heat_capacity_t, &
            fallible_volumetric_heat_capacity_unit_t, &
            fallible_yank_t, &
            fallible_yank_unit_t

    interface fallible_acceleration_t
            module procedure fallible_acceleration_from_object_and_key
            module procedure fallible_acceleration_from_fallible_json
            module procedure fallible_acceleration_from_json_value
    end interface

    interface fallible_acceleration_unit_t
            module procedure fallible_acceleration_unit_from_object_and_key
            module procedure fallible_acceleration_unit_from_fallible_json
            module procedure fallible_acceleration_unit_from_json_value
    end interface

    interface fallible_amount_t
            module procedure fallible_amount_from_object_and_key
            module procedure fallible_amount_from_fallible_json
            module procedure fallible_amount_from_json_value
    end interface

    interface fallible_amount_unit_t
            module procedure fallible_amount_unit_from_object_and_key
            module procedure fallible_amount_unit_from_fallible_json
            module procedure fallible_amount_unit_from_json_value
    end interface

    interface fallible_amount_rate_t
            module procedure fallible_amount_rate_from_object_and_key
            module procedure fallible_amount_rate_from_fallible_json
            module procedure fallible_amount_rate_from_json_value
    end interface

    interface fallible_amount_rate_unit_t
            module procedure fallible_amount_rate_unit_from_object_and_key
            module procedure fallible_amount_rate_unit_from_fallible_json
            module procedure fallible_amount_rate_unit_from_json_value
    end interface

    interface fallible_amount_temperature_t
            module procedure fallible_amount_temperature_from_object_and_key
            module procedure fallible_amount_temperature_from_fallible_json
            module procedure fallible_amount_temperature_from_json_value
    end interface

    interface fallible_amount_temperature_unit_t
            module procedure fallible_amount_temperature_unit_from_object_and_key
            module procedure fallible_amount_temperature_unit_from_fallible_json
            module procedure fallible_amount_temperature_unit_from_json_value
    end interface

    interface fallible_amount_temperature_rate_t
            module procedure fallible_amount_temperature_rate_from_object_and_key
            module procedure fallible_amount_temperature_rate_from_fallible_json
            module procedure fallible_amount_temperature_rate_from_json_value
    end interface

    interface fallible_amount_temperature_rate_unit_t
            module procedure fallible_amount_temperature_rate_unit_from_object_and_key
            module procedure fallible_amount_temperature_rate_unit_from_fallible_json
            module procedure fallible_amount_temperature_rate_unit_from_json_value
    end interface

    interface fallible_angle_t
            module procedure fallible_angle_from_object_and_key
            module procedure fallible_angle_from_fallible_json
            module procedure fallible_angle_from_json_value
    end interface

    interface fallible_angle_unit_t
            module procedure fallible_angle_unit_from_object_and_key
            module procedure fallible_angle_unit_from_fallible_json
            module procedure fallible_angle_unit_from_json_value
    end interface

    interface fallible_area_t
            module procedure fallible_area_from_object_and_key
            module procedure fallible_area_from_fallible_json
            module procedure fallible_area_from_json_value
    end interface

    interface fallible_area_unit_t
            module procedure fallible_area_unit_from_object_and_key
            module procedure fallible_area_unit_from_fallible_json
            module procedure fallible_area_unit_from_json_value
    end interface

    interface fallible_conductance_t
            module procedure fallible_conductance_from_object_and_key
            module procedure fallible_conductance_from_fallible_json
            module procedure fallible_conductance_from_json_value
    end interface

    interface fallible_conductance_unit_t
            module procedure fallible_conductance_unit_from_object_and_key
            module procedure fallible_conductance_unit_from_fallible_json
            module procedure fallible_conductance_unit_from_json_value
    end interface

    interface fallible_delta_temperature_t
            module procedure fallible_delta_temperature_from_object_and_key
            module procedure fallible_delta_temperature_from_fallible_json
            module procedure fallible_delta_temperature_from_json_value
    end interface

    interface fallible_delta_temperature_unit_t
            module procedure fallible_delta_temperature_unit_from_object_and_key
            module procedure fallible_delta_temperature_unit_from_fallible_json
            module procedure fallible_delta_temperature_unit_from_json_value
    end interface

    interface fallible_density_t
            module procedure fallible_density_from_object_and_key
            module procedure fallible_density_from_fallible_json
            module procedure fallible_density_from_json_value
    end interface

    interface fallible_density_unit_t
            module procedure fallible_density_unit_from_object_and_key
            module procedure fallible_density_unit_from_fallible_json
            module procedure fallible_density_unit_from_json_value
    end interface

    interface fallible_diffusivity_t
            module procedure fallible_diffusivity_from_object_and_key
            module procedure fallible_diffusivity_from_fallible_json
            module procedure fallible_diffusivity_from_json_value
    end interface

    interface fallible_diffusivity_unit_t
            module procedure fallible_diffusivity_unit_from_object_and_key
            module procedure fallible_diffusivity_unit_from_fallible_json
            module procedure fallible_diffusivity_unit_from_json_value
    end interface

    interface fallible_dynamic_viscosity_t
            module procedure fallible_dynamic_viscosity_from_object_and_key
            module procedure fallible_dynamic_viscosity_from_fallible_json
            module procedure fallible_dynamic_viscosity_from_json_value
    end interface

    interface fallible_dynamic_viscosity_unit_t
            module procedure fallible_dynamic_viscosity_unit_from_object_and_key
            module procedure fallible_dynamic_viscosity_unit_from_fallible_json
            module procedure fallible_dynamic_viscosity_unit_from_json_value
    end interface

    interface fallible_energy_t
            module procedure fallible_energy_from_object_and_key
            module procedure fallible_energy_from_fallible_json
            module procedure fallible_energy_from_json_value
    end interface

    interface fallible_energy_unit_t
            module procedure fallible_energy_unit_from_object_and_key
            module procedure fallible_energy_unit_from_fallible_json
            module procedure fallible_energy_unit_from_json_value
    end interface

    interface fallible_enthalpy_t
            module procedure fallible_enthalpy_from_object_and_key
            module procedure fallible_enthalpy_from_fallible_json
            module procedure fallible_enthalpy_from_json_value
    end interface

    interface fallible_enthalpy_unit_t
            module procedure fallible_enthalpy_unit_from_object_and_key
            module procedure fallible_enthalpy_unit_from_fallible_json
            module procedure fallible_enthalpy_unit_from_json_value
    end interface

    interface fallible_fluence_t
            module procedure fallible_fluence_from_object_and_key
            module procedure fallible_fluence_from_fallible_json
            module procedure fallible_fluence_from_json_value
    end interface

    interface fallible_fluence_unit_t
            module procedure fallible_fluence_unit_from_object_and_key
            module procedure fallible_fluence_unit_from_fallible_json
            module procedure fallible_fluence_unit_from_json_value
    end interface

    interface fallible_force_t
            module procedure fallible_force_from_object_and_key
            module procedure fallible_force_from_fallible_json
            module procedure fallible_force_from_json_value
    end interface

    interface fallible_force_unit_t
            module procedure fallible_force_unit_from_object_and_key
            module procedure fallible_force_unit_from_fallible_json
            module procedure fallible_force_unit_from_json_value
    end interface

    interface fallible_fracture_toughness_t
            module procedure fallible_fracture_toughness_from_object_and_key
            module procedure fallible_fracture_toughness_from_fallible_json
            module procedure fallible_fracture_toughness_from_json_value
    end interface

    interface fallible_fracture_toughness_unit_t
            module procedure fallible_fracture_toughness_unit_from_object_and_key
            module procedure fallible_fracture_toughness_unit_from_fallible_json
            module procedure fallible_fracture_toughness_unit_from_json_value
    end interface

    interface fallible_frequency_t
            module procedure fallible_frequency_from_object_and_key
            module procedure fallible_frequency_from_fallible_json
            module procedure fallible_frequency_from_json_value
    end interface

    interface fallible_frequency_unit_t
            module procedure fallible_frequency_unit_from_object_and_key
            module procedure fallible_frequency_unit_from_fallible_json
            module procedure fallible_frequency_unit_from_json_value
    end interface

    interface fallible_heat_capacity_t
            module procedure fallible_heat_capacity_from_object_and_key
            module procedure fallible_heat_capacity_from_fallible_json
            module procedure fallible_heat_capacity_from_json_value
    end interface

    interface fallible_heat_capacity_unit_t
            module procedure fallible_heat_capacity_unit_from_object_and_key
            module procedure fallible_heat_capacity_unit_from_fallible_json
            module procedure fallible_heat_capacity_unit_from_json_value
    end interface

    interface fallible_heat_transfer_coefficient_t
            module procedure fallible_heat_transfer_coefficient_from_object_and_key
            module procedure fallible_heat_transfer_coefficient_from_fallible_json
            module procedure fallible_heat_transfer_coefficient_from_json_value
    end interface

    interface fallible_heat_transfer_coefficient_unit_t
            module procedure fallible_heat_transfer_coefficient_unit_from_object_and_key
            module procedure fallible_heat_transfer_coefficient_unit_from_fallible_json
            module procedure fallible_heat_transfer_coefficient_unit_from_json_value
    end interface

    interface fallible_insulance_t
            module procedure fallible_insulance_from_object_and_key
            module procedure fallible_insulance_from_fallible_json
            module procedure fallible_insulance_from_json_value
    end interface

    interface fallible_insulance_unit_t
            module procedure fallible_insulance_unit_from_object_and_key
            module procedure fallible_insulance_unit_from_fallible_json
            module procedure fallible_insulance_unit_from_json_value
    end interface

    interface fallible_intensity_t
            module procedure fallible_intensity_from_object_and_key
            module procedure fallible_intensity_from_fallible_json
            module procedure fallible_intensity_from_json_value
    end interface

    interface fallible_intensity_unit_t
            module procedure fallible_intensity_unit_from_object_and_key
            module procedure fallible_intensity_unit_from_fallible_json
            module procedure fallible_intensity_unit_from_json_value
    end interface

    interface fallible_inverse_molar_mass_t
            module procedure fallible_inverse_molar_mass_from_object_and_key
            module procedure fallible_inverse_molar_mass_from_fallible_json
            module procedure fallible_inverse_molar_mass_from_json_value
    end interface

    interface fallible_inverse_molar_mass_unit_t
            module procedure fallible_inverse_molar_mass_unit_from_object_and_key
            module procedure fallible_inverse_molar_mass_unit_from_fallible_json
            module procedure fallible_inverse_molar_mass_unit_from_json_value
    end interface

    interface fallible_length_t
            module procedure fallible_length_from_object_and_key
            module procedure fallible_length_from_fallible_json
            module procedure fallible_length_from_json_value
    end interface

    interface fallible_length_unit_t
            module procedure fallible_length_unit_from_object_and_key
            module procedure fallible_length_unit_from_fallible_json
            module procedure fallible_length_unit_from_json_value
    end interface

    interface fallible_mass_t
            module procedure fallible_mass_from_object_and_key
            module procedure fallible_mass_from_fallible_json
            module procedure fallible_mass_from_json_value
    end interface

    interface fallible_mass_unit_t
            module procedure fallible_mass_unit_from_object_and_key
            module procedure fallible_mass_unit_from_fallible_json
            module procedure fallible_mass_unit_from_json_value
    end interface

    interface fallible_mass_flux_t
            module procedure fallible_mass_flux_from_object_and_key
            module procedure fallible_mass_flux_from_fallible_json
            module procedure fallible_mass_flux_from_json_value
    end interface

    interface fallible_mass_flux_unit_t
            module procedure fallible_mass_flux_unit_from_object_and_key
            module procedure fallible_mass_flux_unit_from_fallible_json
            module procedure fallible_mass_flux_unit_from_json_value
    end interface

    interface fallible_mass_rate_t
            module procedure fallible_mass_rate_from_object_and_key
            module procedure fallible_mass_rate_from_fallible_json
            module procedure fallible_mass_rate_from_json_value
    end interface

    interface fallible_mass_rate_unit_t
            module procedure fallible_mass_rate_unit_from_object_and_key
            module procedure fallible_mass_rate_unit_from_fallible_json
            module procedure fallible_mass_rate_unit_from_json_value
    end interface

    interface fallible_molar_density_t
            module procedure fallible_molar_density_from_object_and_key
            module procedure fallible_molar_density_from_fallible_json
            module procedure fallible_molar_density_from_json_value
    end interface

    interface fallible_molar_density_unit_t
            module procedure fallible_molar_density_unit_from_object_and_key
            module procedure fallible_molar_density_unit_from_fallible_json
            module procedure fallible_molar_density_unit_from_json_value
    end interface

    interface fallible_molar_enthalpy_t
            module procedure fallible_molar_enthalpy_from_object_and_key
            module procedure fallible_molar_enthalpy_from_fallible_json
            module procedure fallible_molar_enthalpy_from_json_value
    end interface

    interface fallible_molar_enthalpy_unit_t
            module procedure fallible_molar_enthalpy_unit_from_object_and_key
            module procedure fallible_molar_enthalpy_unit_from_fallible_json
            module procedure fallible_molar_enthalpy_unit_from_json_value
    end interface

    interface fallible_molar_mass_t
            module procedure fallible_molar_mass_from_object_and_key
            module procedure fallible_molar_mass_from_fallible_json
            module procedure fallible_molar_mass_from_json_value
    end interface

    interface fallible_molar_mass_unit_t
            module procedure fallible_molar_mass_unit_from_object_and_key
            module procedure fallible_molar_mass_unit_from_fallible_json
            module procedure fallible_molar_mass_unit_from_json_value
    end interface

    interface fallible_molar_specific_heat_t
            module procedure fallible_molar_specific_heat_from_object_and_key
            module procedure fallible_molar_specific_heat_from_fallible_json
            module procedure fallible_molar_specific_heat_from_json_value
    end interface

    interface fallible_molar_specific_heat_unit_t
            module procedure fallible_molar_specific_heat_unit_from_object_and_key
            module procedure fallible_molar_specific_heat_unit_from_fallible_json
            module procedure fallible_molar_specific_heat_unit_from_json_value
    end interface

    interface fallible_power_t
            module procedure fallible_power_from_object_and_key
            module procedure fallible_power_from_fallible_json
            module procedure fallible_power_from_json_value
    end interface

    interface fallible_power_unit_t
            module procedure fallible_power_unit_from_object_and_key
            module procedure fallible_power_unit_from_fallible_json
            module procedure fallible_power_unit_from_json_value
    end interface

    interface fallible_pressure_t
            module procedure fallible_pressure_from_object_and_key
            module procedure fallible_pressure_from_fallible_json
            module procedure fallible_pressure_from_json_value
    end interface

    interface fallible_pressure_unit_t
            module procedure fallible_pressure_unit_from_object_and_key
            module procedure fallible_pressure_unit_from_fallible_json
            module procedure fallible_pressure_unit_from_json_value
    end interface

    interface fallible_specific_heat_t
            module procedure fallible_specific_heat_from_object_and_key
            module procedure fallible_specific_heat_from_fallible_json
            module procedure fallible_specific_heat_from_json_value
    end interface

    interface fallible_specific_heat_unit_t
            module procedure fallible_specific_heat_unit_from_object_and_key
            module procedure fallible_specific_heat_unit_from_fallible_json
            module procedure fallible_specific_heat_unit_from_json_value
    end interface

    interface fallible_speed_t
            module procedure fallible_speed_from_object_and_key
            module procedure fallible_speed_from_fallible_json
            module procedure fallible_speed_from_json_value
    end interface

    interface fallible_speed_unit_t
            module procedure fallible_speed_unit_from_object_and_key
            module procedure fallible_speed_unit_from_fallible_json
            module procedure fallible_speed_unit_from_json_value
    end interface

    interface fallible_temperature_gradient_t
            module procedure fallible_temperature_gradient_from_object_and_key
            module procedure fallible_temperature_gradient_from_fallible_json
            module procedure fallible_temperature_gradient_from_json_value
    end interface

    interface fallible_temperature_gradient_unit_t
            module procedure fallible_temperature_gradient_unit_from_object_and_key
            module procedure fallible_temperature_gradient_unit_from_fallible_json
            module procedure fallible_temperature_gradient_unit_from_json_value
    end interface

    interface fallible_temperature_t
            module procedure fallible_temperature_from_object_and_key
            module procedure fallible_temperature_from_fallible_json
            module procedure fallible_temperature_from_json_value
    end interface

    interface fallible_temperature_unit_t
            module procedure fallible_temperature_unit_from_object_and_key
            module procedure fallible_temperature_unit_from_fallible_json
            module procedure fallible_temperature_unit_from_json_value
    end interface

    interface fallible_thermal_conductivity_t
            module procedure fallible_thermal_conductivity_from_object_and_key
            module procedure fallible_thermal_conductivity_from_fallible_json
            module procedure fallible_thermal_conductivity_from_json_value
    end interface

    interface fallible_thermal_conductivity_unit_t
            module procedure fallible_thermal_conductivity_unit_from_object_and_key
            module procedure fallible_thermal_conductivity_unit_from_fallible_json
            module procedure fallible_thermal_conductivity_unit_from_json_value
    end interface

    interface fallible_thermal_expansion_coefficient_t
            module procedure fallible_thermal_expansion_coefficient_from_object_and_key
            module procedure fallible_thermal_expansion_coefficient_from_fallible_json
            module procedure fallible_thermal_expansion_coefficient_from_json_value
    end interface

    interface fallible_thermal_expansion_coefficient_unit_t
            module procedure fallible_thermal_expansion_coefficient_unit_from_object_and_key
            module procedure fallible_thermal_expansion_coefficient_unit_from_fallible_json
            module procedure fallible_thermal_expansion_coefficient_unit_from_json_value
    end interface

    interface fallible_time_t
            module procedure fallible_time_from_object_and_key
            module procedure fallible_time_from_fallible_json
            module procedure fallible_time_from_json_value
    end interface

    interface fallible_time_unit_t
            module procedure fallible_time_unit_from_object_and_key
            module procedure fallible_time_unit_from_fallible_json
            module procedure fallible_time_unit_from_json_value
    end interface

    interface fallible_volume_t
            module procedure fallible_volume_from_object_and_key
            module procedure fallible_volume_from_fallible_json
            module procedure fallible_volume_from_json_value
    end interface

    interface fallible_volume_unit_t
            module procedure fallible_volume_unit_from_object_and_key
            module procedure fallible_volume_unit_from_fallible_json
            module procedure fallible_volume_unit_from_json_value
    end interface

    interface fallible_volume_rate_t
            module procedure fallible_volume_rate_from_object_and_key
            module procedure fallible_volume_rate_from_fallible_json
            module procedure fallible_volume_rate_from_json_value
    end interface

    interface fallible_volume_rate_unit_t
            module procedure fallible_volume_rate_unit_from_object_and_key
            module procedure fallible_volume_rate_unit_from_fallible_json
            module procedure fallible_volume_rate_unit_from_json_value
    end interface

    interface fallible_volumetric_heat_capacity_t
            module procedure fallible_volumetric_heat_capacity_from_object_and_key
            module procedure fallible_volumetric_heat_capacity_from_fallible_json
            module procedure fallible_volumetric_heat_capacity_from_json_value
    end interface

    interface fallible_volumetric_heat_capacity_unit_t
            module procedure fallible_volumetric_heat_capacity_unit_from_object_and_key
            module procedure fallible_volumetric_heat_capacity_unit_from_fallible_json
            module procedure fallible_volumetric_heat_capacity_unit_from_json_value
    end interface

    interface fallible_yank_t
            module procedure fallible_yank_from_object_and_key
            module procedure fallible_yank_from_fallible_json
            module procedure fallible_yank_from_json_value
    end interface

    interface fallible_yank_unit_t
            module procedure fallible_yank_unit_from_object_and_key
            module procedure fallible_yank_unit_from_fallible_json
            module procedure fallible_yank_unit_from_json_value
    end interface

    character(len=*), parameter :: MODULE_NAME = "rojff_to_quaff"
contains
    function fallible_acceleration_from_object_and_key( &
            object, key) result(fallible_acceleration)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_acceleration_t) :: fallible_acceleration

        character(len=*), parameter :: procedure_name = "fallible_acceleration_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_acceleration = fallible_acceleration_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_acceleration = fallible_acceleration_t( &
                    fallible_acceleration_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_acceleration%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_acceleration%errors()
                    fallible_acceleration = fallible_acceleration_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_acceleration_from_fallible_json(maybe_value) result(fallible_acceleration)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_acceleration_t) :: fallible_acceleration

        character(len=*), parameter :: procedure_name = "fallible_acceleration_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_acceleration = fallible_acceleration_t(maybe_value%errors)
        else
            fallible_acceleration = fallible_acceleration_t( &
                    fallible_acceleration_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_acceleration_from_json_value(value_) result(fallible_acceleration)
        class(json_value_t), intent(in) :: value_
        type(fallible_acceleration_t) :: fallible_acceleration

        character(len=*), parameter :: procedure_name = "fallible_acceleration_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_acceleration = fallible_acceleration_t( &
                    parse_acceleration(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_acceleration = fallible_acceleration_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a acceleration")))
        end select
    end function

    function fallible_acceleration_unit_from_object_and_key( &
            object, key) result(fallible_acceleration_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_acceleration_unit_t) :: fallible_acceleration_unit

        character(len=*), parameter :: procedure_name = "fallible_acceleration_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_acceleration_unit = fallible_acceleration_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_acceleration_unit = fallible_acceleration_unit_t( &
                    fallible_acceleration_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_acceleration_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_acceleration_unit%errors()
                    fallible_acceleration_unit = fallible_acceleration_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_acceleration_unit_from_fallible_json(maybe_value) result(fallible_acceleration_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_acceleration_unit_t) :: fallible_acceleration_unit

        character(len=*), parameter :: procedure_name = "fallible_acceleration_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_acceleration_unit = fallible_acceleration_unit_t(maybe_value%errors)
        else
            fallible_acceleration_unit = fallible_acceleration_unit_t( &
                    fallible_acceleration_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_acceleration_unit_from_json_value(value_) result(fallible_acceleration_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_acceleration_unit_t) :: fallible_acceleration_unit

        character(len=*), parameter :: procedure_name = "fallible_acceleration_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_acceleration_unit = fallible_acceleration_unit_t( &
                    parse_acceleration_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_acceleration_unit = fallible_acceleration_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a acceleration unit")))
        end select
    end function

    function fallible_amount_from_object_and_key( &
            object, key) result(fallible_amount)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_amount_t) :: fallible_amount

        character(len=*), parameter :: procedure_name = "fallible_amount_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_amount = fallible_amount_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_amount = fallible_amount_t( &
                    fallible_amount_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_amount%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_amount%errors()
                    fallible_amount = fallible_amount_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_amount_from_fallible_json(maybe_value) result(fallible_amount)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_amount_t) :: fallible_amount

        character(len=*), parameter :: procedure_name = "fallible_amount_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_amount = fallible_amount_t(maybe_value%errors)
        else
            fallible_amount = fallible_amount_t( &
                    fallible_amount_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_amount_from_json_value(value_) result(fallible_amount)
        class(json_value_t), intent(in) :: value_
        type(fallible_amount_t) :: fallible_amount

        character(len=*), parameter :: procedure_name = "fallible_amount_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_amount = fallible_amount_t( &
                    parse_amount(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_amount = fallible_amount_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a amount")))
        end select
    end function

    function fallible_amount_unit_from_object_and_key( &
            object, key) result(fallible_amount_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_amount_unit_t) :: fallible_amount_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_amount_unit = fallible_amount_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_amount_unit = fallible_amount_unit_t( &
                    fallible_amount_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_amount_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_amount_unit%errors()
                    fallible_amount_unit = fallible_amount_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_amount_unit_from_fallible_json(maybe_value) result(fallible_amount_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_amount_unit_t) :: fallible_amount_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_amount_unit = fallible_amount_unit_t(maybe_value%errors)
        else
            fallible_amount_unit = fallible_amount_unit_t( &
                    fallible_amount_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_amount_unit_from_json_value(value_) result(fallible_amount_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_amount_unit_t) :: fallible_amount_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_amount_unit = fallible_amount_unit_t( &
                    parse_amount_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_amount_unit = fallible_amount_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a amount unit")))
        end select
    end function

    function fallible_amount_rate_from_object_and_key( &
            object, key) result(fallible_amount_rate)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_amount_rate_t) :: fallible_amount_rate

        character(len=*), parameter :: procedure_name = "fallible_amount_rate_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_amount_rate = fallible_amount_rate_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_amount_rate = fallible_amount_rate_t( &
                    fallible_amount_rate_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_amount_rate%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_amount_rate%errors()
                    fallible_amount_rate = fallible_amount_rate_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_amount_rate_from_fallible_json(maybe_value) result(fallible_amount_rate)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_amount_rate_t) :: fallible_amount_rate

        character(len=*), parameter :: procedure_name = "fallible_amount_rate_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_amount_rate = fallible_amount_rate_t(maybe_value%errors)
        else
            fallible_amount_rate = fallible_amount_rate_t( &
                    fallible_amount_rate_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_amount_rate_from_json_value(value_) result(fallible_amount_rate)
        class(json_value_t), intent(in) :: value_
        type(fallible_amount_rate_t) :: fallible_amount_rate

        character(len=*), parameter :: procedure_name = "fallible_amount_rate_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_amount_rate = fallible_amount_rate_t( &
                    parse_amount_rate(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_amount_rate = fallible_amount_rate_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a amount_rate")))
        end select
    end function

    function fallible_amount_rate_unit_from_object_and_key( &
            object, key) result(fallible_amount_rate_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_amount_rate_unit_t) :: fallible_amount_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_rate_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_amount_rate_unit = fallible_amount_rate_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_amount_rate_unit = fallible_amount_rate_unit_t( &
                    fallible_amount_rate_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_amount_rate_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_amount_rate_unit%errors()
                    fallible_amount_rate_unit = fallible_amount_rate_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_amount_rate_unit_from_fallible_json(maybe_value) result(fallible_amount_rate_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_amount_rate_unit_t) :: fallible_amount_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_rate_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_amount_rate_unit = fallible_amount_rate_unit_t(maybe_value%errors)
        else
            fallible_amount_rate_unit = fallible_amount_rate_unit_t( &
                    fallible_amount_rate_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_amount_rate_unit_from_json_value(value_) result(fallible_amount_rate_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_amount_rate_unit_t) :: fallible_amount_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_rate_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_amount_rate_unit = fallible_amount_rate_unit_t( &
                    parse_amount_rate_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_amount_rate_unit = fallible_amount_rate_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a amount_rate unit")))
        end select
    end function

    function fallible_amount_temperature_from_object_and_key( &
            object, key) result(fallible_amount_temperature)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_amount_temperature_t) :: fallible_amount_temperature

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_amount_temperature = fallible_amount_temperature_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_amount_temperature = fallible_amount_temperature_t( &
                    fallible_amount_temperature_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_amount_temperature%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_amount_temperature%errors()
                    fallible_amount_temperature = fallible_amount_temperature_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_amount_temperature_from_fallible_json(maybe_value) result(fallible_amount_temperature)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_amount_temperature_t) :: fallible_amount_temperature

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_amount_temperature = fallible_amount_temperature_t(maybe_value%errors)
        else
            fallible_amount_temperature = fallible_amount_temperature_t( &
                    fallible_amount_temperature_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_amount_temperature_from_json_value(value_) result(fallible_amount_temperature)
        class(json_value_t), intent(in) :: value_
        type(fallible_amount_temperature_t) :: fallible_amount_temperature

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_amount_temperature = fallible_amount_temperature_t( &
                    parse_amount_temperature(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_amount_temperature = fallible_amount_temperature_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a amount_temperature")))
        end select
    end function

    function fallible_amount_temperature_unit_from_object_and_key( &
            object, key) result(fallible_amount_temperature_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_amount_temperature_unit_t) :: fallible_amount_temperature_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_amount_temperature_unit = fallible_amount_temperature_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_amount_temperature_unit = fallible_amount_temperature_unit_t( &
                    fallible_amount_temperature_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_amount_temperature_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_amount_temperature_unit%errors()
                    fallible_amount_temperature_unit = fallible_amount_temperature_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_amount_temperature_unit_from_fallible_json(maybe_value) result(fallible_amount_temperature_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_amount_temperature_unit_t) :: fallible_amount_temperature_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_amount_temperature_unit = fallible_amount_temperature_unit_t(maybe_value%errors)
        else
            fallible_amount_temperature_unit = fallible_amount_temperature_unit_t( &
                    fallible_amount_temperature_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_amount_temperature_unit_from_json_value(value_) result(fallible_amount_temperature_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_amount_temperature_unit_t) :: fallible_amount_temperature_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_amount_temperature_unit = fallible_amount_temperature_unit_t( &
                    parse_amount_temperature_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_amount_temperature_unit = fallible_amount_temperature_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a amount_temperature unit")))
        end select
    end function

    function fallible_amount_temperature_rate_from_object_and_key( &
            object, key) result(fallible_amount_temperature_rate)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_amount_temperature_rate_t) :: fallible_amount_temperature_rate

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_rate_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_amount_temperature_rate = fallible_amount_temperature_rate_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_amount_temperature_rate = fallible_amount_temperature_rate_t( &
                    fallible_amount_temperature_rate_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_amount_temperature_rate%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_amount_temperature_rate%errors()
                    fallible_amount_temperature_rate = fallible_amount_temperature_rate_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_amount_temperature_rate_from_fallible_json(maybe_value) result(fallible_amount_temperature_rate)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_amount_temperature_rate_t) :: fallible_amount_temperature_rate

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_rate_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_amount_temperature_rate = fallible_amount_temperature_rate_t(maybe_value%errors)
        else
            fallible_amount_temperature_rate = fallible_amount_temperature_rate_t( &
                    fallible_amount_temperature_rate_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_amount_temperature_rate_from_json_value(value_) result(fallible_amount_temperature_rate)
        class(json_value_t), intent(in) :: value_
        type(fallible_amount_temperature_rate_t) :: fallible_amount_temperature_rate

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_rate_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_amount_temperature_rate = fallible_amount_temperature_rate_t( &
                    parse_amount_temperature_rate(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_amount_temperature_rate = fallible_amount_temperature_rate_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a amount_temperature_rate")))
        end select
    end function

    function fallible_amount_temperature_rate_unit_from_object_and_key( &
            object, key) result(fallible_amount_temperature_rate_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_amount_temperature_rate_unit_t) :: fallible_amount_temperature_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_rate_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t( &
                    fallible_amount_temperature_rate_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_amount_temperature_rate_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_amount_temperature_rate_unit%errors()
                    fallible_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_amount_temperature_rate_unit_from_fallible_json(maybe_value) result(fallible_amount_temperature_rate_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_amount_temperature_rate_unit_t) :: fallible_amount_temperature_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_rate_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t(maybe_value%errors)
        else
            fallible_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t( &
                    fallible_amount_temperature_rate_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_amount_temperature_rate_unit_from_json_value(value_) result(fallible_amount_temperature_rate_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_amount_temperature_rate_unit_t) :: fallible_amount_temperature_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_amount_temperature_rate_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t( &
                    parse_amount_temperature_rate_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a amount_temperature_rate unit")))
        end select
    end function

    function fallible_angle_from_object_and_key( &
            object, key) result(fallible_angle)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_angle_t) :: fallible_angle

        character(len=*), parameter :: procedure_name = "fallible_angle_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_angle = fallible_angle_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_angle = fallible_angle_t( &
                    fallible_angle_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_angle%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_angle%errors()
                    fallible_angle = fallible_angle_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_angle_from_fallible_json(maybe_value) result(fallible_angle)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_angle_t) :: fallible_angle

        character(len=*), parameter :: procedure_name = "fallible_angle_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_angle = fallible_angle_t(maybe_value%errors)
        else
            fallible_angle = fallible_angle_t( &
                    fallible_angle_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_angle_from_json_value(value_) result(fallible_angle)
        class(json_value_t), intent(in) :: value_
        type(fallible_angle_t) :: fallible_angle

        character(len=*), parameter :: procedure_name = "fallible_angle_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_angle = fallible_angle_t( &
                    parse_angle(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_angle = fallible_angle_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a angle")))
        end select
    end function

    function fallible_angle_unit_from_object_and_key( &
            object, key) result(fallible_angle_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_angle_unit_t) :: fallible_angle_unit

        character(len=*), parameter :: procedure_name = "fallible_angle_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_angle_unit = fallible_angle_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_angle_unit = fallible_angle_unit_t( &
                    fallible_angle_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_angle_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_angle_unit%errors()
                    fallible_angle_unit = fallible_angle_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_angle_unit_from_fallible_json(maybe_value) result(fallible_angle_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_angle_unit_t) :: fallible_angle_unit

        character(len=*), parameter :: procedure_name = "fallible_angle_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_angle_unit = fallible_angle_unit_t(maybe_value%errors)
        else
            fallible_angle_unit = fallible_angle_unit_t( &
                    fallible_angle_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_angle_unit_from_json_value(value_) result(fallible_angle_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_angle_unit_t) :: fallible_angle_unit

        character(len=*), parameter :: procedure_name = "fallible_angle_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_angle_unit = fallible_angle_unit_t( &
                    parse_angle_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_angle_unit = fallible_angle_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a angle unit")))
        end select
    end function

    function fallible_area_from_object_and_key( &
            object, key) result(fallible_area)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_area_t) :: fallible_area

        character(len=*), parameter :: procedure_name = "fallible_area_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_area = fallible_area_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_area = fallible_area_t( &
                    fallible_area_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_area%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_area%errors()
                    fallible_area = fallible_area_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_area_from_fallible_json(maybe_value) result(fallible_area)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_area_t) :: fallible_area

        character(len=*), parameter :: procedure_name = "fallible_area_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_area = fallible_area_t(maybe_value%errors)
        else
            fallible_area = fallible_area_t( &
                    fallible_area_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_area_from_json_value(value_) result(fallible_area)
        class(json_value_t), intent(in) :: value_
        type(fallible_area_t) :: fallible_area

        character(len=*), parameter :: procedure_name = "fallible_area_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_area = fallible_area_t( &
                    parse_area(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_area = fallible_area_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a area")))
        end select
    end function

    function fallible_area_unit_from_object_and_key( &
            object, key) result(fallible_area_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_area_unit_t) :: fallible_area_unit

        character(len=*), parameter :: procedure_name = "fallible_area_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_area_unit = fallible_area_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_area_unit = fallible_area_unit_t( &
                    fallible_area_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_area_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_area_unit%errors()
                    fallible_area_unit = fallible_area_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_area_unit_from_fallible_json(maybe_value) result(fallible_area_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_area_unit_t) :: fallible_area_unit

        character(len=*), parameter :: procedure_name = "fallible_area_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_area_unit = fallible_area_unit_t(maybe_value%errors)
        else
            fallible_area_unit = fallible_area_unit_t( &
                    fallible_area_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_area_unit_from_json_value(value_) result(fallible_area_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_area_unit_t) :: fallible_area_unit

        character(len=*), parameter :: procedure_name = "fallible_area_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_area_unit = fallible_area_unit_t( &
                    parse_area_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_area_unit = fallible_area_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a area unit")))
        end select
    end function

    function fallible_conductance_from_object_and_key( &
            object, key) result(fallible_conductance)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_conductance_t) :: fallible_conductance

        character(len=*), parameter :: procedure_name = "fallible_conductance_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_conductance = fallible_conductance_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_conductance = fallible_conductance_t( &
                    fallible_conductance_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_conductance%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_conductance%errors()
                    fallible_conductance = fallible_conductance_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_conductance_from_fallible_json(maybe_value) result(fallible_conductance)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_conductance_t) :: fallible_conductance

        character(len=*), parameter :: procedure_name = "fallible_conductance_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_conductance = fallible_conductance_t(maybe_value%errors)
        else
            fallible_conductance = fallible_conductance_t( &
                    fallible_conductance_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_conductance_from_json_value(value_) result(fallible_conductance)
        class(json_value_t), intent(in) :: value_
        type(fallible_conductance_t) :: fallible_conductance

        character(len=*), parameter :: procedure_name = "fallible_conductance_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_conductance = fallible_conductance_t( &
                    parse_conductance(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_conductance = fallible_conductance_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a conductance")))
        end select
    end function

    function fallible_conductance_unit_from_object_and_key( &
            object, key) result(fallible_conductance_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_conductance_unit_t) :: fallible_conductance_unit

        character(len=*), parameter :: procedure_name = "fallible_conductance_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_conductance_unit = fallible_conductance_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_conductance_unit = fallible_conductance_unit_t( &
                    fallible_conductance_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_conductance_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_conductance_unit%errors()
                    fallible_conductance_unit = fallible_conductance_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_conductance_unit_from_fallible_json(maybe_value) result(fallible_conductance_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_conductance_unit_t) :: fallible_conductance_unit

        character(len=*), parameter :: procedure_name = "fallible_conductance_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_conductance_unit = fallible_conductance_unit_t(maybe_value%errors)
        else
            fallible_conductance_unit = fallible_conductance_unit_t( &
                    fallible_conductance_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_conductance_unit_from_json_value(value_) result(fallible_conductance_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_conductance_unit_t) :: fallible_conductance_unit

        character(len=*), parameter :: procedure_name = "fallible_conductance_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_conductance_unit = fallible_conductance_unit_t( &
                    parse_conductance_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_conductance_unit = fallible_conductance_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a conductance unit")))
        end select
    end function

    function fallible_delta_temperature_from_object_and_key( &
            object, key) result(fallible_delta_temperature)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_delta_temperature_t) :: fallible_delta_temperature

        character(len=*), parameter :: procedure_name = "fallible_delta_temperature_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_delta_temperature = fallible_delta_temperature_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_delta_temperature = fallible_delta_temperature_t( &
                    fallible_delta_temperature_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_delta_temperature%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_delta_temperature%errors()
                    fallible_delta_temperature = fallible_delta_temperature_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_delta_temperature_from_fallible_json(maybe_value) result(fallible_delta_temperature)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_delta_temperature_t) :: fallible_delta_temperature

        character(len=*), parameter :: procedure_name = "fallible_delta_temperature_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_delta_temperature = fallible_delta_temperature_t(maybe_value%errors)
        else
            fallible_delta_temperature = fallible_delta_temperature_t( &
                    fallible_delta_temperature_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_delta_temperature_from_json_value(value_) result(fallible_delta_temperature)
        class(json_value_t), intent(in) :: value_
        type(fallible_delta_temperature_t) :: fallible_delta_temperature

        character(len=*), parameter :: procedure_name = "fallible_delta_temperature_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_delta_temperature = fallible_delta_temperature_t( &
                    parse_delta_temperature(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_delta_temperature = fallible_delta_temperature_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a delta_temperature")))
        end select
    end function

    function fallible_delta_temperature_unit_from_object_and_key( &
            object, key) result(fallible_delta_temperature_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_delta_temperature_unit_t) :: fallible_delta_temperature_unit

        character(len=*), parameter :: procedure_name = "fallible_delta_temperature_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_delta_temperature_unit = fallible_delta_temperature_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_delta_temperature_unit = fallible_delta_temperature_unit_t( &
                    fallible_delta_temperature_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_delta_temperature_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_delta_temperature_unit%errors()
                    fallible_delta_temperature_unit = fallible_delta_temperature_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_delta_temperature_unit_from_fallible_json(maybe_value) result(fallible_delta_temperature_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_delta_temperature_unit_t) :: fallible_delta_temperature_unit

        character(len=*), parameter :: procedure_name = "fallible_delta_temperature_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_delta_temperature_unit = fallible_delta_temperature_unit_t(maybe_value%errors)
        else
            fallible_delta_temperature_unit = fallible_delta_temperature_unit_t( &
                    fallible_delta_temperature_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_delta_temperature_unit_from_json_value(value_) result(fallible_delta_temperature_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_delta_temperature_unit_t) :: fallible_delta_temperature_unit

        character(len=*), parameter :: procedure_name = "fallible_delta_temperature_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_delta_temperature_unit = fallible_delta_temperature_unit_t( &
                    parse_delta_temperature_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_delta_temperature_unit = fallible_delta_temperature_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a delta_temperature unit")))
        end select
    end function

    function fallible_density_from_object_and_key( &
            object, key) result(fallible_density)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_density_t) :: fallible_density

        character(len=*), parameter :: procedure_name = "fallible_density_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_density = fallible_density_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_density = fallible_density_t( &
                    fallible_density_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_density%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_density%errors()
                    fallible_density = fallible_density_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_density_from_fallible_json(maybe_value) result(fallible_density)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_density_t) :: fallible_density

        character(len=*), parameter :: procedure_name = "fallible_density_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_density = fallible_density_t(maybe_value%errors)
        else
            fallible_density = fallible_density_t( &
                    fallible_density_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_density_from_json_value(value_) result(fallible_density)
        class(json_value_t), intent(in) :: value_
        type(fallible_density_t) :: fallible_density

        character(len=*), parameter :: procedure_name = "fallible_density_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_density = fallible_density_t( &
                    parse_density(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_density = fallible_density_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a density")))
        end select
    end function

    function fallible_density_unit_from_object_and_key( &
            object, key) result(fallible_density_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_density_unit_t) :: fallible_density_unit

        character(len=*), parameter :: procedure_name = "fallible_density_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_density_unit = fallible_density_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_density_unit = fallible_density_unit_t( &
                    fallible_density_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_density_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_density_unit%errors()
                    fallible_density_unit = fallible_density_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_density_unit_from_fallible_json(maybe_value) result(fallible_density_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_density_unit_t) :: fallible_density_unit

        character(len=*), parameter :: procedure_name = "fallible_density_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_density_unit = fallible_density_unit_t(maybe_value%errors)
        else
            fallible_density_unit = fallible_density_unit_t( &
                    fallible_density_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_density_unit_from_json_value(value_) result(fallible_density_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_density_unit_t) :: fallible_density_unit

        character(len=*), parameter :: procedure_name = "fallible_density_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_density_unit = fallible_density_unit_t( &
                    parse_density_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_density_unit = fallible_density_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a density unit")))
        end select
    end function

    function fallible_diffusivity_from_object_and_key( &
            object, key) result(fallible_diffusivity)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_diffusivity_t) :: fallible_diffusivity

        character(len=*), parameter :: procedure_name = "fallible_diffusivity_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_diffusivity = fallible_diffusivity_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_diffusivity = fallible_diffusivity_t( &
                    fallible_diffusivity_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_diffusivity%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_diffusivity%errors()
                    fallible_diffusivity = fallible_diffusivity_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_diffusivity_from_fallible_json(maybe_value) result(fallible_diffusivity)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_diffusivity_t) :: fallible_diffusivity

        character(len=*), parameter :: procedure_name = "fallible_diffusivity_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_diffusivity = fallible_diffusivity_t(maybe_value%errors)
        else
            fallible_diffusivity = fallible_diffusivity_t( &
                    fallible_diffusivity_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_diffusivity_from_json_value(value_) result(fallible_diffusivity)
        class(json_value_t), intent(in) :: value_
        type(fallible_diffusivity_t) :: fallible_diffusivity

        character(len=*), parameter :: procedure_name = "fallible_diffusivity_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_diffusivity = fallible_diffusivity_t( &
                    parse_diffusivity(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_diffusivity = fallible_diffusivity_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a diffusivity")))
        end select
    end function

    function fallible_diffusivity_unit_from_object_and_key( &
            object, key) result(fallible_diffusivity_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_diffusivity_unit_t) :: fallible_diffusivity_unit

        character(len=*), parameter :: procedure_name = "fallible_diffusivity_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_diffusivity_unit = fallible_diffusivity_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_diffusivity_unit = fallible_diffusivity_unit_t( &
                    fallible_diffusivity_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_diffusivity_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_diffusivity_unit%errors()
                    fallible_diffusivity_unit = fallible_diffusivity_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_diffusivity_unit_from_fallible_json(maybe_value) result(fallible_diffusivity_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_diffusivity_unit_t) :: fallible_diffusivity_unit

        character(len=*), parameter :: procedure_name = "fallible_diffusivity_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_diffusivity_unit = fallible_diffusivity_unit_t(maybe_value%errors)
        else
            fallible_diffusivity_unit = fallible_diffusivity_unit_t( &
                    fallible_diffusivity_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_diffusivity_unit_from_json_value(value_) result(fallible_diffusivity_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_diffusivity_unit_t) :: fallible_diffusivity_unit

        character(len=*), parameter :: procedure_name = "fallible_diffusivity_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_diffusivity_unit = fallible_diffusivity_unit_t( &
                    parse_diffusivity_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_diffusivity_unit = fallible_diffusivity_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a diffusivity unit")))
        end select
    end function

    function fallible_dynamic_viscosity_from_object_and_key( &
            object, key) result(fallible_dynamic_viscosity)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_dynamic_viscosity_t) :: fallible_dynamic_viscosity

        character(len=*), parameter :: procedure_name = "fallible_dynamic_viscosity_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_dynamic_viscosity = fallible_dynamic_viscosity_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_dynamic_viscosity = fallible_dynamic_viscosity_t( &
                    fallible_dynamic_viscosity_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_dynamic_viscosity%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_dynamic_viscosity%errors()
                    fallible_dynamic_viscosity = fallible_dynamic_viscosity_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_dynamic_viscosity_from_fallible_json(maybe_value) result(fallible_dynamic_viscosity)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_dynamic_viscosity_t) :: fallible_dynamic_viscosity

        character(len=*), parameter :: procedure_name = "fallible_dynamic_viscosity_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_dynamic_viscosity = fallible_dynamic_viscosity_t(maybe_value%errors)
        else
            fallible_dynamic_viscosity = fallible_dynamic_viscosity_t( &
                    fallible_dynamic_viscosity_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_dynamic_viscosity_from_json_value(value_) result(fallible_dynamic_viscosity)
        class(json_value_t), intent(in) :: value_
        type(fallible_dynamic_viscosity_t) :: fallible_dynamic_viscosity

        character(len=*), parameter :: procedure_name = "fallible_dynamic_viscosity_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_dynamic_viscosity = fallible_dynamic_viscosity_t( &
                    parse_dynamic_viscosity(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_dynamic_viscosity = fallible_dynamic_viscosity_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a dynamic_viscosity")))
        end select
    end function

    function fallible_dynamic_viscosity_unit_from_object_and_key( &
            object, key) result(fallible_dynamic_viscosity_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_dynamic_viscosity_unit_t) :: fallible_dynamic_viscosity_unit

        character(len=*), parameter :: procedure_name = "fallible_dynamic_viscosity_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t( &
                    fallible_dynamic_viscosity_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_dynamic_viscosity_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_dynamic_viscosity_unit%errors()
                    fallible_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_dynamic_viscosity_unit_from_fallible_json(maybe_value) result(fallible_dynamic_viscosity_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_dynamic_viscosity_unit_t) :: fallible_dynamic_viscosity_unit

        character(len=*), parameter :: procedure_name = "fallible_dynamic_viscosity_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t(maybe_value%errors)
        else
            fallible_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t( &
                    fallible_dynamic_viscosity_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_dynamic_viscosity_unit_from_json_value(value_) result(fallible_dynamic_viscosity_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_dynamic_viscosity_unit_t) :: fallible_dynamic_viscosity_unit

        character(len=*), parameter :: procedure_name = "fallible_dynamic_viscosity_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t( &
                    parse_dynamic_viscosity_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a dynamic_viscosity unit")))
        end select
    end function

    function fallible_energy_from_object_and_key( &
            object, key) result(fallible_energy)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_energy_t) :: fallible_energy

        character(len=*), parameter :: procedure_name = "fallible_energy_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_energy = fallible_energy_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_energy = fallible_energy_t( &
                    fallible_energy_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_energy%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_energy%errors()
                    fallible_energy = fallible_energy_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_energy_from_fallible_json(maybe_value) result(fallible_energy)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_energy_t) :: fallible_energy

        character(len=*), parameter :: procedure_name = "fallible_energy_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_energy = fallible_energy_t(maybe_value%errors)
        else
            fallible_energy = fallible_energy_t( &
                    fallible_energy_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_energy_from_json_value(value_) result(fallible_energy)
        class(json_value_t), intent(in) :: value_
        type(fallible_energy_t) :: fallible_energy

        character(len=*), parameter :: procedure_name = "fallible_energy_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_energy = fallible_energy_t( &
                    parse_energy(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_energy = fallible_energy_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a energy")))
        end select
    end function

    function fallible_energy_unit_from_object_and_key( &
            object, key) result(fallible_energy_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_energy_unit_t) :: fallible_energy_unit

        character(len=*), parameter :: procedure_name = "fallible_energy_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_energy_unit = fallible_energy_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_energy_unit = fallible_energy_unit_t( &
                    fallible_energy_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_energy_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_energy_unit%errors()
                    fallible_energy_unit = fallible_energy_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_energy_unit_from_fallible_json(maybe_value) result(fallible_energy_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_energy_unit_t) :: fallible_energy_unit

        character(len=*), parameter :: procedure_name = "fallible_energy_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_energy_unit = fallible_energy_unit_t(maybe_value%errors)
        else
            fallible_energy_unit = fallible_energy_unit_t( &
                    fallible_energy_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_energy_unit_from_json_value(value_) result(fallible_energy_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_energy_unit_t) :: fallible_energy_unit

        character(len=*), parameter :: procedure_name = "fallible_energy_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_energy_unit = fallible_energy_unit_t( &
                    parse_energy_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_energy_unit = fallible_energy_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a energy unit")))
        end select
    end function

    function fallible_enthalpy_from_object_and_key( &
            object, key) result(fallible_enthalpy)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_enthalpy_t) :: fallible_enthalpy

        character(len=*), parameter :: procedure_name = "fallible_enthalpy_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_enthalpy = fallible_enthalpy_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_enthalpy = fallible_enthalpy_t( &
                    fallible_enthalpy_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_enthalpy%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_enthalpy%errors()
                    fallible_enthalpy = fallible_enthalpy_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_enthalpy_from_fallible_json(maybe_value) result(fallible_enthalpy)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_enthalpy_t) :: fallible_enthalpy

        character(len=*), parameter :: procedure_name = "fallible_enthalpy_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_enthalpy = fallible_enthalpy_t(maybe_value%errors)
        else
            fallible_enthalpy = fallible_enthalpy_t( &
                    fallible_enthalpy_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_enthalpy_from_json_value(value_) result(fallible_enthalpy)
        class(json_value_t), intent(in) :: value_
        type(fallible_enthalpy_t) :: fallible_enthalpy

        character(len=*), parameter :: procedure_name = "fallible_enthalpy_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_enthalpy = fallible_enthalpy_t( &
                    parse_enthalpy(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_enthalpy = fallible_enthalpy_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a enthalpy")))
        end select
    end function

    function fallible_enthalpy_unit_from_object_and_key( &
            object, key) result(fallible_enthalpy_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_enthalpy_unit_t) :: fallible_enthalpy_unit

        character(len=*), parameter :: procedure_name = "fallible_enthalpy_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_enthalpy_unit = fallible_enthalpy_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_enthalpy_unit = fallible_enthalpy_unit_t( &
                    fallible_enthalpy_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_enthalpy_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_enthalpy_unit%errors()
                    fallible_enthalpy_unit = fallible_enthalpy_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_enthalpy_unit_from_fallible_json(maybe_value) result(fallible_enthalpy_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_enthalpy_unit_t) :: fallible_enthalpy_unit

        character(len=*), parameter :: procedure_name = "fallible_enthalpy_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_enthalpy_unit = fallible_enthalpy_unit_t(maybe_value%errors)
        else
            fallible_enthalpy_unit = fallible_enthalpy_unit_t( &
                    fallible_enthalpy_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_enthalpy_unit_from_json_value(value_) result(fallible_enthalpy_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_enthalpy_unit_t) :: fallible_enthalpy_unit

        character(len=*), parameter :: procedure_name = "fallible_enthalpy_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_enthalpy_unit = fallible_enthalpy_unit_t( &
                    parse_enthalpy_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_enthalpy_unit = fallible_enthalpy_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a enthalpy unit")))
        end select
    end function

    function fallible_fluence_from_object_and_key( &
            object, key) result(fallible_fluence)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_fluence_t) :: fallible_fluence

        character(len=*), parameter :: procedure_name = "fallible_fluence_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_fluence = fallible_fluence_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_fluence = fallible_fluence_t( &
                    fallible_fluence_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_fluence%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_fluence%errors()
                    fallible_fluence = fallible_fluence_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_fluence_from_fallible_json(maybe_value) result(fallible_fluence)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_fluence_t) :: fallible_fluence

        character(len=*), parameter :: procedure_name = "fallible_fluence_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_fluence = fallible_fluence_t(maybe_value%errors)
        else
            fallible_fluence = fallible_fluence_t( &
                    fallible_fluence_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_fluence_from_json_value(value_) result(fallible_fluence)
        class(json_value_t), intent(in) :: value_
        type(fallible_fluence_t) :: fallible_fluence

        character(len=*), parameter :: procedure_name = "fallible_fluence_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_fluence = fallible_fluence_t( &
                    parse_fluence(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_fluence = fallible_fluence_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a fluence")))
        end select
    end function

    function fallible_fluence_unit_from_object_and_key( &
            object, key) result(fallible_fluence_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_fluence_unit_t) :: fallible_fluence_unit

        character(len=*), parameter :: procedure_name = "fallible_fluence_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_fluence_unit = fallible_fluence_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_fluence_unit = fallible_fluence_unit_t( &
                    fallible_fluence_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_fluence_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_fluence_unit%errors()
                    fallible_fluence_unit = fallible_fluence_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_fluence_unit_from_fallible_json(maybe_value) result(fallible_fluence_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_fluence_unit_t) :: fallible_fluence_unit

        character(len=*), parameter :: procedure_name = "fallible_fluence_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_fluence_unit = fallible_fluence_unit_t(maybe_value%errors)
        else
            fallible_fluence_unit = fallible_fluence_unit_t( &
                    fallible_fluence_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_fluence_unit_from_json_value(value_) result(fallible_fluence_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_fluence_unit_t) :: fallible_fluence_unit

        character(len=*), parameter :: procedure_name = "fallible_fluence_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_fluence_unit = fallible_fluence_unit_t( &
                    parse_fluence_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_fluence_unit = fallible_fluence_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a fluence unit")))
        end select
    end function

    function fallible_force_from_object_and_key( &
            object, key) result(fallible_force)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_force_t) :: fallible_force

        character(len=*), parameter :: procedure_name = "fallible_force_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_force = fallible_force_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_force = fallible_force_t( &
                    fallible_force_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_force%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_force%errors()
                    fallible_force = fallible_force_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_force_from_fallible_json(maybe_value) result(fallible_force)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_force_t) :: fallible_force

        character(len=*), parameter :: procedure_name = "fallible_force_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_force = fallible_force_t(maybe_value%errors)
        else
            fallible_force = fallible_force_t( &
                    fallible_force_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_force_from_json_value(value_) result(fallible_force)
        class(json_value_t), intent(in) :: value_
        type(fallible_force_t) :: fallible_force

        character(len=*), parameter :: procedure_name = "fallible_force_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_force = fallible_force_t( &
                    parse_force(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_force = fallible_force_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a force")))
        end select
    end function

    function fallible_force_unit_from_object_and_key( &
            object, key) result(fallible_force_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_force_unit_t) :: fallible_force_unit

        character(len=*), parameter :: procedure_name = "fallible_force_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_force_unit = fallible_force_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_force_unit = fallible_force_unit_t( &
                    fallible_force_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_force_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_force_unit%errors()
                    fallible_force_unit = fallible_force_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_force_unit_from_fallible_json(maybe_value) result(fallible_force_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_force_unit_t) :: fallible_force_unit

        character(len=*), parameter :: procedure_name = "fallible_force_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_force_unit = fallible_force_unit_t(maybe_value%errors)
        else
            fallible_force_unit = fallible_force_unit_t( &
                    fallible_force_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_force_unit_from_json_value(value_) result(fallible_force_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_force_unit_t) :: fallible_force_unit

        character(len=*), parameter :: procedure_name = "fallible_force_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_force_unit = fallible_force_unit_t( &
                    parse_force_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_force_unit = fallible_force_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a force unit")))
        end select
    end function

    function fallible_fracture_toughness_from_object_and_key( &
            object, key) result(fallible_fracture_toughness)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_fracture_toughness_t) :: fallible_fracture_toughness

        character(len=*), parameter :: procedure_name = "fallible_fracture_toughness_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_fracture_toughness = fallible_fracture_toughness_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_fracture_toughness = fallible_fracture_toughness_t( &
                    fallible_fracture_toughness_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_fracture_toughness%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_fracture_toughness%errors()
                    fallible_fracture_toughness = fallible_fracture_toughness_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_fracture_toughness_from_fallible_json(maybe_value) result(fallible_fracture_toughness)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_fracture_toughness_t) :: fallible_fracture_toughness

        character(len=*), parameter :: procedure_name = "fallible_fracture_toughness_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_fracture_toughness = fallible_fracture_toughness_t(maybe_value%errors)
        else
            fallible_fracture_toughness = fallible_fracture_toughness_t( &
                    fallible_fracture_toughness_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_fracture_toughness_from_json_value(value_) result(fallible_fracture_toughness)
        class(json_value_t), intent(in) :: value_
        type(fallible_fracture_toughness_t) :: fallible_fracture_toughness

        character(len=*), parameter :: procedure_name = "fallible_fracture_toughness_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_fracture_toughness = fallible_fracture_toughness_t( &
                    parse_fracture_toughness(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_fracture_toughness = fallible_fracture_toughness_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a fracture_toughness")))
        end select
    end function

    function fallible_fracture_toughness_unit_from_object_and_key( &
            object, key) result(fallible_fracture_toughness_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_fracture_toughness_unit_t) :: fallible_fracture_toughness_unit

        character(len=*), parameter :: procedure_name = "fallible_fracture_toughness_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_fracture_toughness_unit = fallible_fracture_toughness_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_fracture_toughness_unit = fallible_fracture_toughness_unit_t( &
                    fallible_fracture_toughness_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_fracture_toughness_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_fracture_toughness_unit%errors()
                    fallible_fracture_toughness_unit = fallible_fracture_toughness_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_fracture_toughness_unit_from_fallible_json(maybe_value) result(fallible_fracture_toughness_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_fracture_toughness_unit_t) :: fallible_fracture_toughness_unit

        character(len=*), parameter :: procedure_name = "fallible_fracture_toughness_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_fracture_toughness_unit = fallible_fracture_toughness_unit_t(maybe_value%errors)
        else
            fallible_fracture_toughness_unit = fallible_fracture_toughness_unit_t( &
                    fallible_fracture_toughness_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_fracture_toughness_unit_from_json_value(value_) result(fallible_fracture_toughness_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_fracture_toughness_unit_t) :: fallible_fracture_toughness_unit

        character(len=*), parameter :: procedure_name = "fallible_fracture_toughness_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_fracture_toughness_unit = fallible_fracture_toughness_unit_t( &
                    parse_fracture_toughness_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_fracture_toughness_unit = fallible_fracture_toughness_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a fracture_toughness unit")))
        end select
    end function

    function fallible_frequency_from_object_and_key( &
            object, key) result(fallible_frequency)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_frequency_t) :: fallible_frequency

        character(len=*), parameter :: procedure_name = "fallible_frequency_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_frequency = fallible_frequency_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_frequency = fallible_frequency_t( &
                    fallible_frequency_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_frequency%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_frequency%errors()
                    fallible_frequency = fallible_frequency_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_frequency_from_fallible_json(maybe_value) result(fallible_frequency)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_frequency_t) :: fallible_frequency

        character(len=*), parameter :: procedure_name = "fallible_frequency_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_frequency = fallible_frequency_t(maybe_value%errors)
        else
            fallible_frequency = fallible_frequency_t( &
                    fallible_frequency_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_frequency_from_json_value(value_) result(fallible_frequency)
        class(json_value_t), intent(in) :: value_
        type(fallible_frequency_t) :: fallible_frequency

        character(len=*), parameter :: procedure_name = "fallible_frequency_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_frequency = fallible_frequency_t( &
                    parse_frequency(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_frequency = fallible_frequency_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a frequency")))
        end select
    end function

    function fallible_frequency_unit_from_object_and_key( &
            object, key) result(fallible_frequency_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_frequency_unit_t) :: fallible_frequency_unit

        character(len=*), parameter :: procedure_name = "fallible_frequency_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_frequency_unit = fallible_frequency_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_frequency_unit = fallible_frequency_unit_t( &
                    fallible_frequency_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_frequency_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_frequency_unit%errors()
                    fallible_frequency_unit = fallible_frequency_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_frequency_unit_from_fallible_json(maybe_value) result(fallible_frequency_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_frequency_unit_t) :: fallible_frequency_unit

        character(len=*), parameter :: procedure_name = "fallible_frequency_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_frequency_unit = fallible_frequency_unit_t(maybe_value%errors)
        else
            fallible_frequency_unit = fallible_frequency_unit_t( &
                    fallible_frequency_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_frequency_unit_from_json_value(value_) result(fallible_frequency_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_frequency_unit_t) :: fallible_frequency_unit

        character(len=*), parameter :: procedure_name = "fallible_frequency_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_frequency_unit = fallible_frequency_unit_t( &
                    parse_frequency_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_frequency_unit = fallible_frequency_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a frequency unit")))
        end select
    end function

    function fallible_heat_capacity_from_object_and_key( &
            object, key) result(fallible_heat_capacity)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_heat_capacity_t) :: fallible_heat_capacity

        character(len=*), parameter :: procedure_name = "fallible_heat_capacity_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_heat_capacity = fallible_heat_capacity_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_heat_capacity = fallible_heat_capacity_t( &
                    fallible_heat_capacity_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_heat_capacity%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_heat_capacity%errors()
                    fallible_heat_capacity = fallible_heat_capacity_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_heat_capacity_from_fallible_json(maybe_value) result(fallible_heat_capacity)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_heat_capacity_t) :: fallible_heat_capacity

        character(len=*), parameter :: procedure_name = "fallible_heat_capacity_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_heat_capacity = fallible_heat_capacity_t(maybe_value%errors)
        else
            fallible_heat_capacity = fallible_heat_capacity_t( &
                    fallible_heat_capacity_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_heat_capacity_from_json_value(value_) result(fallible_heat_capacity)
        class(json_value_t), intent(in) :: value_
        type(fallible_heat_capacity_t) :: fallible_heat_capacity

        character(len=*), parameter :: procedure_name = "fallible_heat_capacity_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_heat_capacity = fallible_heat_capacity_t( &
                    parse_heat_capacity(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_heat_capacity = fallible_heat_capacity_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a heat_capacity")))
        end select
    end function

    function fallible_heat_capacity_unit_from_object_and_key( &
            object, key) result(fallible_heat_capacity_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_heat_capacity_unit_t) :: fallible_heat_capacity_unit

        character(len=*), parameter :: procedure_name = "fallible_heat_capacity_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_heat_capacity_unit = fallible_heat_capacity_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_heat_capacity_unit = fallible_heat_capacity_unit_t( &
                    fallible_heat_capacity_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_heat_capacity_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_heat_capacity_unit%errors()
                    fallible_heat_capacity_unit = fallible_heat_capacity_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_heat_capacity_unit_from_fallible_json(maybe_value) result(fallible_heat_capacity_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_heat_capacity_unit_t) :: fallible_heat_capacity_unit

        character(len=*), parameter :: procedure_name = "fallible_heat_capacity_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_heat_capacity_unit = fallible_heat_capacity_unit_t(maybe_value%errors)
        else
            fallible_heat_capacity_unit = fallible_heat_capacity_unit_t( &
                    fallible_heat_capacity_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_heat_capacity_unit_from_json_value(value_) result(fallible_heat_capacity_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_heat_capacity_unit_t) :: fallible_heat_capacity_unit

        character(len=*), parameter :: procedure_name = "fallible_heat_capacity_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_heat_capacity_unit = fallible_heat_capacity_unit_t( &
                    parse_heat_capacity_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_heat_capacity_unit = fallible_heat_capacity_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a heat_capacity unit")))
        end select
    end function

    function fallible_heat_transfer_coefficient_from_object_and_key( &
            object, key) result(fallible_heat_transfer_coefficient)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_heat_transfer_coefficient_t) :: fallible_heat_transfer_coefficient

        character(len=*), parameter :: procedure_name = "fallible_heat_transfer_coefficient_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t( &
                    fallible_heat_transfer_coefficient_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_heat_transfer_coefficient%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_heat_transfer_coefficient%errors()
                    fallible_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_heat_transfer_coefficient_from_fallible_json(maybe_value) result(fallible_heat_transfer_coefficient)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_heat_transfer_coefficient_t) :: fallible_heat_transfer_coefficient

        character(len=*), parameter :: procedure_name = "fallible_heat_transfer_coefficient_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t(maybe_value%errors)
        else
            fallible_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t( &
                    fallible_heat_transfer_coefficient_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_heat_transfer_coefficient_from_json_value(value_) result(fallible_heat_transfer_coefficient)
        class(json_value_t), intent(in) :: value_
        type(fallible_heat_transfer_coefficient_t) :: fallible_heat_transfer_coefficient

        character(len=*), parameter :: procedure_name = "fallible_heat_transfer_coefficient_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t( &
                    parse_heat_transfer_coefficient(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a heat_transfer_coefficient")))
        end select
    end function

    function fallible_heat_transfer_coefficient_unit_from_object_and_key( &
            object, key) result(fallible_heat_transfer_coefficient_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_heat_transfer_coefficient_unit_t) :: fallible_heat_transfer_coefficient_unit

        character(len=*), parameter :: procedure_name = "fallible_heat_transfer_coefficient_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t( &
                    fallible_heat_transfer_coefficient_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_heat_transfer_coefficient_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_heat_transfer_coefficient_unit%errors()
                    fallible_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_heat_transfer_coefficient_unit_from_fallible_json(maybe_value) result(fallible_heat_transfer_coefficient_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_heat_transfer_coefficient_unit_t) :: fallible_heat_transfer_coefficient_unit

        character(len=*), parameter :: procedure_name = "fallible_heat_transfer_coefficient_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t(maybe_value%errors)
        else
            fallible_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t( &
                    fallible_heat_transfer_coefficient_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_heat_transfer_coefficient_unit_from_json_value(value_) result(fallible_heat_transfer_coefficient_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_heat_transfer_coefficient_unit_t) :: fallible_heat_transfer_coefficient_unit

        character(len=*), parameter :: procedure_name = "fallible_heat_transfer_coefficient_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t( &
                    parse_heat_transfer_coefficient_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a heat_transfer_coefficient unit")))
        end select
    end function

    function fallible_insulance_from_object_and_key( &
            object, key) result(fallible_insulance)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_insulance_t) :: fallible_insulance

        character(len=*), parameter :: procedure_name = "fallible_insulance_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_insulance = fallible_insulance_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_insulance = fallible_insulance_t( &
                    fallible_insulance_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_insulance%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_insulance%errors()
                    fallible_insulance = fallible_insulance_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_insulance_from_fallible_json(maybe_value) result(fallible_insulance)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_insulance_t) :: fallible_insulance

        character(len=*), parameter :: procedure_name = "fallible_insulance_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_insulance = fallible_insulance_t(maybe_value%errors)
        else
            fallible_insulance = fallible_insulance_t( &
                    fallible_insulance_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_insulance_from_json_value(value_) result(fallible_insulance)
        class(json_value_t), intent(in) :: value_
        type(fallible_insulance_t) :: fallible_insulance

        character(len=*), parameter :: procedure_name = "fallible_insulance_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_insulance = fallible_insulance_t( &
                    parse_insulance(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_insulance = fallible_insulance_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a insulance")))
        end select
    end function

    function fallible_insulance_unit_from_object_and_key( &
            object, key) result(fallible_insulance_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_insulance_unit_t) :: fallible_insulance_unit

        character(len=*), parameter :: procedure_name = "fallible_insulance_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_insulance_unit = fallible_insulance_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_insulance_unit = fallible_insulance_unit_t( &
                    fallible_insulance_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_insulance_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_insulance_unit%errors()
                    fallible_insulance_unit = fallible_insulance_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_insulance_unit_from_fallible_json(maybe_value) result(fallible_insulance_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_insulance_unit_t) :: fallible_insulance_unit

        character(len=*), parameter :: procedure_name = "fallible_insulance_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_insulance_unit = fallible_insulance_unit_t(maybe_value%errors)
        else
            fallible_insulance_unit = fallible_insulance_unit_t( &
                    fallible_insulance_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_insulance_unit_from_json_value(value_) result(fallible_insulance_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_insulance_unit_t) :: fallible_insulance_unit

        character(len=*), parameter :: procedure_name = "fallible_insulance_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_insulance_unit = fallible_insulance_unit_t( &
                    parse_insulance_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_insulance_unit = fallible_insulance_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a insulance unit")))
        end select
    end function

    function fallible_intensity_from_object_and_key( &
            object, key) result(fallible_intensity)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_intensity_t) :: fallible_intensity

        character(len=*), parameter :: procedure_name = "fallible_intensity_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_intensity = fallible_intensity_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_intensity = fallible_intensity_t( &
                    fallible_intensity_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_intensity%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_intensity%errors()
                    fallible_intensity = fallible_intensity_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_intensity_from_fallible_json(maybe_value) result(fallible_intensity)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_intensity_t) :: fallible_intensity

        character(len=*), parameter :: procedure_name = "fallible_intensity_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_intensity = fallible_intensity_t(maybe_value%errors)
        else
            fallible_intensity = fallible_intensity_t( &
                    fallible_intensity_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_intensity_from_json_value(value_) result(fallible_intensity)
        class(json_value_t), intent(in) :: value_
        type(fallible_intensity_t) :: fallible_intensity

        character(len=*), parameter :: procedure_name = "fallible_intensity_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_intensity = fallible_intensity_t( &
                    parse_intensity(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_intensity = fallible_intensity_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a intensity")))
        end select
    end function

    function fallible_intensity_unit_from_object_and_key( &
            object, key) result(fallible_intensity_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_intensity_unit_t) :: fallible_intensity_unit

        character(len=*), parameter :: procedure_name = "fallible_intensity_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_intensity_unit = fallible_intensity_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_intensity_unit = fallible_intensity_unit_t( &
                    fallible_intensity_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_intensity_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_intensity_unit%errors()
                    fallible_intensity_unit = fallible_intensity_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_intensity_unit_from_fallible_json(maybe_value) result(fallible_intensity_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_intensity_unit_t) :: fallible_intensity_unit

        character(len=*), parameter :: procedure_name = "fallible_intensity_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_intensity_unit = fallible_intensity_unit_t(maybe_value%errors)
        else
            fallible_intensity_unit = fallible_intensity_unit_t( &
                    fallible_intensity_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_intensity_unit_from_json_value(value_) result(fallible_intensity_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_intensity_unit_t) :: fallible_intensity_unit

        character(len=*), parameter :: procedure_name = "fallible_intensity_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_intensity_unit = fallible_intensity_unit_t( &
                    parse_intensity_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_intensity_unit = fallible_intensity_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a intensity unit")))
        end select
    end function

    function fallible_inverse_molar_mass_from_object_and_key( &
            object, key) result(fallible_inverse_molar_mass)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_inverse_molar_mass_t) :: fallible_inverse_molar_mass

        character(len=*), parameter :: procedure_name = "fallible_inverse_molar_mass_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_inverse_molar_mass = fallible_inverse_molar_mass_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_inverse_molar_mass = fallible_inverse_molar_mass_t( &
                    fallible_inverse_molar_mass_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_inverse_molar_mass%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_inverse_molar_mass%errors()
                    fallible_inverse_molar_mass = fallible_inverse_molar_mass_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_inverse_molar_mass_from_fallible_json(maybe_value) result(fallible_inverse_molar_mass)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_inverse_molar_mass_t) :: fallible_inverse_molar_mass

        character(len=*), parameter :: procedure_name = "fallible_inverse_molar_mass_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_inverse_molar_mass = fallible_inverse_molar_mass_t(maybe_value%errors)
        else
            fallible_inverse_molar_mass = fallible_inverse_molar_mass_t( &
                    fallible_inverse_molar_mass_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_inverse_molar_mass_from_json_value(value_) result(fallible_inverse_molar_mass)
        class(json_value_t), intent(in) :: value_
        type(fallible_inverse_molar_mass_t) :: fallible_inverse_molar_mass

        character(len=*), parameter :: procedure_name = "fallible_inverse_molar_mass_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_inverse_molar_mass = fallible_inverse_molar_mass_t( &
                    parse_inverse_molar_mass(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_inverse_molar_mass = fallible_inverse_molar_mass_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a inverse_molar_mass")))
        end select
    end function

    function fallible_inverse_molar_mass_unit_from_object_and_key( &
            object, key) result(fallible_inverse_molar_mass_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_inverse_molar_mass_unit_t) :: fallible_inverse_molar_mass_unit

        character(len=*), parameter :: procedure_name = "fallible_inverse_molar_mass_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t( &
                    fallible_inverse_molar_mass_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_inverse_molar_mass_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_inverse_molar_mass_unit%errors()
                    fallible_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_inverse_molar_mass_unit_from_fallible_json(maybe_value) result(fallible_inverse_molar_mass_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_inverse_molar_mass_unit_t) :: fallible_inverse_molar_mass_unit

        character(len=*), parameter :: procedure_name = "fallible_inverse_molar_mass_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t(maybe_value%errors)
        else
            fallible_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t( &
                    fallible_inverse_molar_mass_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_inverse_molar_mass_unit_from_json_value(value_) result(fallible_inverse_molar_mass_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_inverse_molar_mass_unit_t) :: fallible_inverse_molar_mass_unit

        character(len=*), parameter :: procedure_name = "fallible_inverse_molar_mass_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t( &
                    parse_inverse_molar_mass_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a inverse_molar_mass unit")))
        end select
    end function

    function fallible_length_from_object_and_key( &
            object, key) result(fallible_length)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_length_t) :: fallible_length

        character(len=*), parameter :: procedure_name = "fallible_length_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_length = fallible_length_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_length = fallible_length_t( &
                    fallible_length_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_length%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_length%errors()
                    fallible_length = fallible_length_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_length_from_fallible_json(maybe_value) result(fallible_length)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_length_t) :: fallible_length

        character(len=*), parameter :: procedure_name = "fallible_length_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_length = fallible_length_t(maybe_value%errors)
        else
            fallible_length = fallible_length_t( &
                    fallible_length_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_length_from_json_value(value_) result(fallible_length)
        class(json_value_t), intent(in) :: value_
        type(fallible_length_t) :: fallible_length

        character(len=*), parameter :: procedure_name = "fallible_length_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_length = fallible_length_t( &
                    parse_length(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_length = fallible_length_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a length")))
        end select
    end function

    function fallible_length_unit_from_object_and_key( &
            object, key) result(fallible_length_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_length_unit_t) :: fallible_length_unit

        character(len=*), parameter :: procedure_name = "fallible_length_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_length_unit = fallible_length_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_length_unit = fallible_length_unit_t( &
                    fallible_length_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_length_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_length_unit%errors()
                    fallible_length_unit = fallible_length_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_length_unit_from_fallible_json(maybe_value) result(fallible_length_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_length_unit_t) :: fallible_length_unit

        character(len=*), parameter :: procedure_name = "fallible_length_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_length_unit = fallible_length_unit_t(maybe_value%errors)
        else
            fallible_length_unit = fallible_length_unit_t( &
                    fallible_length_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_length_unit_from_json_value(value_) result(fallible_length_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_length_unit_t) :: fallible_length_unit

        character(len=*), parameter :: procedure_name = "fallible_length_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_length_unit = fallible_length_unit_t( &
                    parse_length_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_length_unit = fallible_length_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a length unit")))
        end select
    end function

    function fallible_mass_from_object_and_key( &
            object, key) result(fallible_mass)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_mass_t) :: fallible_mass

        character(len=*), parameter :: procedure_name = "fallible_mass_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_mass = fallible_mass_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_mass = fallible_mass_t( &
                    fallible_mass_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_mass%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_mass%errors()
                    fallible_mass = fallible_mass_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_mass_from_fallible_json(maybe_value) result(fallible_mass)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_mass_t) :: fallible_mass

        character(len=*), parameter :: procedure_name = "fallible_mass_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_mass = fallible_mass_t(maybe_value%errors)
        else
            fallible_mass = fallible_mass_t( &
                    fallible_mass_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_mass_from_json_value(value_) result(fallible_mass)
        class(json_value_t), intent(in) :: value_
        type(fallible_mass_t) :: fallible_mass

        character(len=*), parameter :: procedure_name = "fallible_mass_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_mass = fallible_mass_t( &
                    parse_mass(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_mass = fallible_mass_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a mass")))
        end select
    end function

    function fallible_mass_unit_from_object_and_key( &
            object, key) result(fallible_mass_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_mass_unit_t) :: fallible_mass_unit

        character(len=*), parameter :: procedure_name = "fallible_mass_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_mass_unit = fallible_mass_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_mass_unit = fallible_mass_unit_t( &
                    fallible_mass_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_mass_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_mass_unit%errors()
                    fallible_mass_unit = fallible_mass_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_mass_unit_from_fallible_json(maybe_value) result(fallible_mass_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_mass_unit_t) :: fallible_mass_unit

        character(len=*), parameter :: procedure_name = "fallible_mass_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_mass_unit = fallible_mass_unit_t(maybe_value%errors)
        else
            fallible_mass_unit = fallible_mass_unit_t( &
                    fallible_mass_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_mass_unit_from_json_value(value_) result(fallible_mass_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_mass_unit_t) :: fallible_mass_unit

        character(len=*), parameter :: procedure_name = "fallible_mass_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_mass_unit = fallible_mass_unit_t( &
                    parse_mass_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_mass_unit = fallible_mass_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a mass unit")))
        end select
    end function

    function fallible_mass_flux_from_object_and_key( &
            object, key) result(fallible_mass_flux)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_mass_flux_t) :: fallible_mass_flux

        character(len=*), parameter :: procedure_name = "fallible_mass_flux_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_mass_flux = fallible_mass_flux_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_mass_flux = fallible_mass_flux_t( &
                    fallible_mass_flux_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_mass_flux%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_mass_flux%errors()
                    fallible_mass_flux = fallible_mass_flux_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_mass_flux_from_fallible_json(maybe_value) result(fallible_mass_flux)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_mass_flux_t) :: fallible_mass_flux

        character(len=*), parameter :: procedure_name = "fallible_mass_flux_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_mass_flux = fallible_mass_flux_t(maybe_value%errors)
        else
            fallible_mass_flux = fallible_mass_flux_t( &
                    fallible_mass_flux_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_mass_flux_from_json_value(value_) result(fallible_mass_flux)
        class(json_value_t), intent(in) :: value_
        type(fallible_mass_flux_t) :: fallible_mass_flux

        character(len=*), parameter :: procedure_name = "fallible_mass_flux_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_mass_flux = fallible_mass_flux_t( &
                    parse_mass_flux(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_mass_flux = fallible_mass_flux_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a mass_flux")))
        end select
    end function

    function fallible_mass_flux_unit_from_object_and_key( &
            object, key) result(fallible_mass_flux_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_mass_flux_unit_t) :: fallible_mass_flux_unit

        character(len=*), parameter :: procedure_name = "fallible_mass_flux_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_mass_flux_unit = fallible_mass_flux_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_mass_flux_unit = fallible_mass_flux_unit_t( &
                    fallible_mass_flux_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_mass_flux_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_mass_flux_unit%errors()
                    fallible_mass_flux_unit = fallible_mass_flux_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_mass_flux_unit_from_fallible_json(maybe_value) result(fallible_mass_flux_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_mass_flux_unit_t) :: fallible_mass_flux_unit

        character(len=*), parameter :: procedure_name = "fallible_mass_flux_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_mass_flux_unit = fallible_mass_flux_unit_t(maybe_value%errors)
        else
            fallible_mass_flux_unit = fallible_mass_flux_unit_t( &
                    fallible_mass_flux_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_mass_flux_unit_from_json_value(value_) result(fallible_mass_flux_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_mass_flux_unit_t) :: fallible_mass_flux_unit

        character(len=*), parameter :: procedure_name = "fallible_mass_flux_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_mass_flux_unit = fallible_mass_flux_unit_t( &
                    parse_mass_flux_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_mass_flux_unit = fallible_mass_flux_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a mass_flux unit")))
        end select
    end function

    function fallible_mass_rate_from_object_and_key( &
            object, key) result(fallible_mass_rate)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_mass_rate_t) :: fallible_mass_rate

        character(len=*), parameter :: procedure_name = "fallible_mass_rate_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_mass_rate = fallible_mass_rate_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_mass_rate = fallible_mass_rate_t( &
                    fallible_mass_rate_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_mass_rate%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_mass_rate%errors()
                    fallible_mass_rate = fallible_mass_rate_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_mass_rate_from_fallible_json(maybe_value) result(fallible_mass_rate)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_mass_rate_t) :: fallible_mass_rate

        character(len=*), parameter :: procedure_name = "fallible_mass_rate_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_mass_rate = fallible_mass_rate_t(maybe_value%errors)
        else
            fallible_mass_rate = fallible_mass_rate_t( &
                    fallible_mass_rate_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_mass_rate_from_json_value(value_) result(fallible_mass_rate)
        class(json_value_t), intent(in) :: value_
        type(fallible_mass_rate_t) :: fallible_mass_rate

        character(len=*), parameter :: procedure_name = "fallible_mass_rate_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_mass_rate = fallible_mass_rate_t( &
                    parse_mass_rate(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_mass_rate = fallible_mass_rate_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a mass_rate")))
        end select
    end function

    function fallible_mass_rate_unit_from_object_and_key( &
            object, key) result(fallible_mass_rate_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_mass_rate_unit_t) :: fallible_mass_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_mass_rate_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_mass_rate_unit = fallible_mass_rate_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_mass_rate_unit = fallible_mass_rate_unit_t( &
                    fallible_mass_rate_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_mass_rate_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_mass_rate_unit%errors()
                    fallible_mass_rate_unit = fallible_mass_rate_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_mass_rate_unit_from_fallible_json(maybe_value) result(fallible_mass_rate_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_mass_rate_unit_t) :: fallible_mass_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_mass_rate_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_mass_rate_unit = fallible_mass_rate_unit_t(maybe_value%errors)
        else
            fallible_mass_rate_unit = fallible_mass_rate_unit_t( &
                    fallible_mass_rate_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_mass_rate_unit_from_json_value(value_) result(fallible_mass_rate_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_mass_rate_unit_t) :: fallible_mass_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_mass_rate_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_mass_rate_unit = fallible_mass_rate_unit_t( &
                    parse_mass_rate_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_mass_rate_unit = fallible_mass_rate_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a mass_rate unit")))
        end select
    end function

    function fallible_molar_density_from_object_and_key( &
            object, key) result(fallible_molar_density)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_molar_density_t) :: fallible_molar_density

        character(len=*), parameter :: procedure_name = "fallible_molar_density_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_molar_density = fallible_molar_density_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_molar_density = fallible_molar_density_t( &
                    fallible_molar_density_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_molar_density%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_molar_density%errors()
                    fallible_molar_density = fallible_molar_density_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_molar_density_from_fallible_json(maybe_value) result(fallible_molar_density)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_molar_density_t) :: fallible_molar_density

        character(len=*), parameter :: procedure_name = "fallible_molar_density_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_molar_density = fallible_molar_density_t(maybe_value%errors)
        else
            fallible_molar_density = fallible_molar_density_t( &
                    fallible_molar_density_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_molar_density_from_json_value(value_) result(fallible_molar_density)
        class(json_value_t), intent(in) :: value_
        type(fallible_molar_density_t) :: fallible_molar_density

        character(len=*), parameter :: procedure_name = "fallible_molar_density_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_molar_density = fallible_molar_density_t( &
                    parse_molar_density(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_molar_density = fallible_molar_density_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a molar_density")))
        end select
    end function

    function fallible_molar_density_unit_from_object_and_key( &
            object, key) result(fallible_molar_density_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_molar_density_unit_t) :: fallible_molar_density_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_density_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_molar_density_unit = fallible_molar_density_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_molar_density_unit = fallible_molar_density_unit_t( &
                    fallible_molar_density_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_molar_density_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_molar_density_unit%errors()
                    fallible_molar_density_unit = fallible_molar_density_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_molar_density_unit_from_fallible_json(maybe_value) result(fallible_molar_density_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_molar_density_unit_t) :: fallible_molar_density_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_density_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_molar_density_unit = fallible_molar_density_unit_t(maybe_value%errors)
        else
            fallible_molar_density_unit = fallible_molar_density_unit_t( &
                    fallible_molar_density_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_molar_density_unit_from_json_value(value_) result(fallible_molar_density_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_molar_density_unit_t) :: fallible_molar_density_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_density_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_molar_density_unit = fallible_molar_density_unit_t( &
                    parse_molar_density_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_molar_density_unit = fallible_molar_density_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a molar_density unit")))
        end select
    end function

    function fallible_molar_enthalpy_from_object_and_key( &
            object, key) result(fallible_molar_enthalpy)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_molar_enthalpy_t) :: fallible_molar_enthalpy

        character(len=*), parameter :: procedure_name = "fallible_molar_enthalpy_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_molar_enthalpy = fallible_molar_enthalpy_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_molar_enthalpy = fallible_molar_enthalpy_t( &
                    fallible_molar_enthalpy_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_molar_enthalpy%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_molar_enthalpy%errors()
                    fallible_molar_enthalpy = fallible_molar_enthalpy_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_molar_enthalpy_from_fallible_json(maybe_value) result(fallible_molar_enthalpy)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_molar_enthalpy_t) :: fallible_molar_enthalpy

        character(len=*), parameter :: procedure_name = "fallible_molar_enthalpy_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_molar_enthalpy = fallible_molar_enthalpy_t(maybe_value%errors)
        else
            fallible_molar_enthalpy = fallible_molar_enthalpy_t( &
                    fallible_molar_enthalpy_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_molar_enthalpy_from_json_value(value_) result(fallible_molar_enthalpy)
        class(json_value_t), intent(in) :: value_
        type(fallible_molar_enthalpy_t) :: fallible_molar_enthalpy

        character(len=*), parameter :: procedure_name = "fallible_molar_enthalpy_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_molar_enthalpy = fallible_molar_enthalpy_t( &
                    parse_molar_enthalpy(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_molar_enthalpy = fallible_molar_enthalpy_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a molar_enthalpy")))
        end select
    end function

    function fallible_molar_enthalpy_unit_from_object_and_key( &
            object, key) result(fallible_molar_enthalpy_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_molar_enthalpy_unit_t) :: fallible_molar_enthalpy_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_enthalpy_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t( &
                    fallible_molar_enthalpy_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_molar_enthalpy_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_molar_enthalpy_unit%errors()
                    fallible_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_molar_enthalpy_unit_from_fallible_json(maybe_value) result(fallible_molar_enthalpy_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_molar_enthalpy_unit_t) :: fallible_molar_enthalpy_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_enthalpy_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t(maybe_value%errors)
        else
            fallible_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t( &
                    fallible_molar_enthalpy_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_molar_enthalpy_unit_from_json_value(value_) result(fallible_molar_enthalpy_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_molar_enthalpy_unit_t) :: fallible_molar_enthalpy_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_enthalpy_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t( &
                    parse_molar_enthalpy_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a molar_enthalpy unit")))
        end select
    end function

    function fallible_molar_mass_from_object_and_key( &
            object, key) result(fallible_molar_mass)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_molar_mass_t) :: fallible_molar_mass

        character(len=*), parameter :: procedure_name = "fallible_molar_mass_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_molar_mass = fallible_molar_mass_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_molar_mass = fallible_molar_mass_t( &
                    fallible_molar_mass_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_molar_mass%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_molar_mass%errors()
                    fallible_molar_mass = fallible_molar_mass_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_molar_mass_from_fallible_json(maybe_value) result(fallible_molar_mass)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_molar_mass_t) :: fallible_molar_mass

        character(len=*), parameter :: procedure_name = "fallible_molar_mass_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_molar_mass = fallible_molar_mass_t(maybe_value%errors)
        else
            fallible_molar_mass = fallible_molar_mass_t( &
                    fallible_molar_mass_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_molar_mass_from_json_value(value_) result(fallible_molar_mass)
        class(json_value_t), intent(in) :: value_
        type(fallible_molar_mass_t) :: fallible_molar_mass

        character(len=*), parameter :: procedure_name = "fallible_molar_mass_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_molar_mass = fallible_molar_mass_t( &
                    parse_molar_mass(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_molar_mass = fallible_molar_mass_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a molar_mass")))
        end select
    end function

    function fallible_molar_mass_unit_from_object_and_key( &
            object, key) result(fallible_molar_mass_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_molar_mass_unit_t) :: fallible_molar_mass_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_mass_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_molar_mass_unit = fallible_molar_mass_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_molar_mass_unit = fallible_molar_mass_unit_t( &
                    fallible_molar_mass_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_molar_mass_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_molar_mass_unit%errors()
                    fallible_molar_mass_unit = fallible_molar_mass_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_molar_mass_unit_from_fallible_json(maybe_value) result(fallible_molar_mass_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_molar_mass_unit_t) :: fallible_molar_mass_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_mass_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_molar_mass_unit = fallible_molar_mass_unit_t(maybe_value%errors)
        else
            fallible_molar_mass_unit = fallible_molar_mass_unit_t( &
                    fallible_molar_mass_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_molar_mass_unit_from_json_value(value_) result(fallible_molar_mass_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_molar_mass_unit_t) :: fallible_molar_mass_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_mass_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_molar_mass_unit = fallible_molar_mass_unit_t( &
                    parse_molar_mass_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_molar_mass_unit = fallible_molar_mass_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a molar_mass unit")))
        end select
    end function

    function fallible_molar_specific_heat_from_object_and_key( &
            object, key) result(fallible_molar_specific_heat)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_molar_specific_heat_t) :: fallible_molar_specific_heat

        character(len=*), parameter :: procedure_name = "fallible_molar_specific_heat_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_molar_specific_heat = fallible_molar_specific_heat_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_molar_specific_heat = fallible_molar_specific_heat_t( &
                    fallible_molar_specific_heat_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_molar_specific_heat%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_molar_specific_heat%errors()
                    fallible_molar_specific_heat = fallible_molar_specific_heat_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_molar_specific_heat_from_fallible_json(maybe_value) result(fallible_molar_specific_heat)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_molar_specific_heat_t) :: fallible_molar_specific_heat

        character(len=*), parameter :: procedure_name = "fallible_molar_specific_heat_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_molar_specific_heat = fallible_molar_specific_heat_t(maybe_value%errors)
        else
            fallible_molar_specific_heat = fallible_molar_specific_heat_t( &
                    fallible_molar_specific_heat_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_molar_specific_heat_from_json_value(value_) result(fallible_molar_specific_heat)
        class(json_value_t), intent(in) :: value_
        type(fallible_molar_specific_heat_t) :: fallible_molar_specific_heat

        character(len=*), parameter :: procedure_name = "fallible_molar_specific_heat_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_molar_specific_heat = fallible_molar_specific_heat_t( &
                    parse_molar_specific_heat(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_molar_specific_heat = fallible_molar_specific_heat_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a molar_specific_heat")))
        end select
    end function

    function fallible_molar_specific_heat_unit_from_object_and_key( &
            object, key) result(fallible_molar_specific_heat_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_molar_specific_heat_unit_t) :: fallible_molar_specific_heat_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_specific_heat_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t( &
                    fallible_molar_specific_heat_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_molar_specific_heat_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_molar_specific_heat_unit%errors()
                    fallible_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_molar_specific_heat_unit_from_fallible_json(maybe_value) result(fallible_molar_specific_heat_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_molar_specific_heat_unit_t) :: fallible_molar_specific_heat_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_specific_heat_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t(maybe_value%errors)
        else
            fallible_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t( &
                    fallible_molar_specific_heat_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_molar_specific_heat_unit_from_json_value(value_) result(fallible_molar_specific_heat_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_molar_specific_heat_unit_t) :: fallible_molar_specific_heat_unit

        character(len=*), parameter :: procedure_name = "fallible_molar_specific_heat_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t( &
                    parse_molar_specific_heat_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a molar_specific_heat unit")))
        end select
    end function

    function fallible_power_from_object_and_key( &
            object, key) result(fallible_power)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_power_t) :: fallible_power

        character(len=*), parameter :: procedure_name = "fallible_power_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_power = fallible_power_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_power = fallible_power_t( &
                    fallible_power_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_power%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_power%errors()
                    fallible_power = fallible_power_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_power_from_fallible_json(maybe_value) result(fallible_power)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_power_t) :: fallible_power

        character(len=*), parameter :: procedure_name = "fallible_power_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_power = fallible_power_t(maybe_value%errors)
        else
            fallible_power = fallible_power_t( &
                    fallible_power_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_power_from_json_value(value_) result(fallible_power)
        class(json_value_t), intent(in) :: value_
        type(fallible_power_t) :: fallible_power

        character(len=*), parameter :: procedure_name = "fallible_power_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_power = fallible_power_t( &
                    parse_power(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_power = fallible_power_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a power")))
        end select
    end function

    function fallible_power_unit_from_object_and_key( &
            object, key) result(fallible_power_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_power_unit_t) :: fallible_power_unit

        character(len=*), parameter :: procedure_name = "fallible_power_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_power_unit = fallible_power_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_power_unit = fallible_power_unit_t( &
                    fallible_power_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_power_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_power_unit%errors()
                    fallible_power_unit = fallible_power_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_power_unit_from_fallible_json(maybe_value) result(fallible_power_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_power_unit_t) :: fallible_power_unit

        character(len=*), parameter :: procedure_name = "fallible_power_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_power_unit = fallible_power_unit_t(maybe_value%errors)
        else
            fallible_power_unit = fallible_power_unit_t( &
                    fallible_power_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_power_unit_from_json_value(value_) result(fallible_power_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_power_unit_t) :: fallible_power_unit

        character(len=*), parameter :: procedure_name = "fallible_power_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_power_unit = fallible_power_unit_t( &
                    parse_power_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_power_unit = fallible_power_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a power unit")))
        end select
    end function

    function fallible_pressure_from_object_and_key( &
            object, key) result(fallible_pressure)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_pressure_t) :: fallible_pressure

        character(len=*), parameter :: procedure_name = "fallible_pressure_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_pressure = fallible_pressure_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_pressure = fallible_pressure_t( &
                    fallible_pressure_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_pressure%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_pressure%errors()
                    fallible_pressure = fallible_pressure_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_pressure_from_fallible_json(maybe_value) result(fallible_pressure)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_pressure_t) :: fallible_pressure

        character(len=*), parameter :: procedure_name = "fallible_pressure_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_pressure = fallible_pressure_t(maybe_value%errors)
        else
            fallible_pressure = fallible_pressure_t( &
                    fallible_pressure_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_pressure_from_json_value(value_) result(fallible_pressure)
        class(json_value_t), intent(in) :: value_
        type(fallible_pressure_t) :: fallible_pressure

        character(len=*), parameter :: procedure_name = "fallible_pressure_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_pressure = fallible_pressure_t( &
                    parse_pressure(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_pressure = fallible_pressure_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a pressure")))
        end select
    end function

    function fallible_pressure_unit_from_object_and_key( &
            object, key) result(fallible_pressure_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_pressure_unit_t) :: fallible_pressure_unit

        character(len=*), parameter :: procedure_name = "fallible_pressure_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_pressure_unit = fallible_pressure_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_pressure_unit = fallible_pressure_unit_t( &
                    fallible_pressure_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_pressure_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_pressure_unit%errors()
                    fallible_pressure_unit = fallible_pressure_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_pressure_unit_from_fallible_json(maybe_value) result(fallible_pressure_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_pressure_unit_t) :: fallible_pressure_unit

        character(len=*), parameter :: procedure_name = "fallible_pressure_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_pressure_unit = fallible_pressure_unit_t(maybe_value%errors)
        else
            fallible_pressure_unit = fallible_pressure_unit_t( &
                    fallible_pressure_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_pressure_unit_from_json_value(value_) result(fallible_pressure_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_pressure_unit_t) :: fallible_pressure_unit

        character(len=*), parameter :: procedure_name = "fallible_pressure_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_pressure_unit = fallible_pressure_unit_t( &
                    parse_pressure_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_pressure_unit = fallible_pressure_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a pressure unit")))
        end select
    end function

    function fallible_specific_heat_from_object_and_key( &
            object, key) result(fallible_specific_heat)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_specific_heat_t) :: fallible_specific_heat

        character(len=*), parameter :: procedure_name = "fallible_specific_heat_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_specific_heat = fallible_specific_heat_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_specific_heat = fallible_specific_heat_t( &
                    fallible_specific_heat_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_specific_heat%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_specific_heat%errors()
                    fallible_specific_heat = fallible_specific_heat_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_specific_heat_from_fallible_json(maybe_value) result(fallible_specific_heat)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_specific_heat_t) :: fallible_specific_heat

        character(len=*), parameter :: procedure_name = "fallible_specific_heat_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_specific_heat = fallible_specific_heat_t(maybe_value%errors)
        else
            fallible_specific_heat = fallible_specific_heat_t( &
                    fallible_specific_heat_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_specific_heat_from_json_value(value_) result(fallible_specific_heat)
        class(json_value_t), intent(in) :: value_
        type(fallible_specific_heat_t) :: fallible_specific_heat

        character(len=*), parameter :: procedure_name = "fallible_specific_heat_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_specific_heat = fallible_specific_heat_t( &
                    parse_specific_heat(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_specific_heat = fallible_specific_heat_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a specific_heat")))
        end select
    end function

    function fallible_specific_heat_unit_from_object_and_key( &
            object, key) result(fallible_specific_heat_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_specific_heat_unit_t) :: fallible_specific_heat_unit

        character(len=*), parameter :: procedure_name = "fallible_specific_heat_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_specific_heat_unit = fallible_specific_heat_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_specific_heat_unit = fallible_specific_heat_unit_t( &
                    fallible_specific_heat_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_specific_heat_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_specific_heat_unit%errors()
                    fallible_specific_heat_unit = fallible_specific_heat_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_specific_heat_unit_from_fallible_json(maybe_value) result(fallible_specific_heat_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_specific_heat_unit_t) :: fallible_specific_heat_unit

        character(len=*), parameter :: procedure_name = "fallible_specific_heat_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_specific_heat_unit = fallible_specific_heat_unit_t(maybe_value%errors)
        else
            fallible_specific_heat_unit = fallible_specific_heat_unit_t( &
                    fallible_specific_heat_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_specific_heat_unit_from_json_value(value_) result(fallible_specific_heat_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_specific_heat_unit_t) :: fallible_specific_heat_unit

        character(len=*), parameter :: procedure_name = "fallible_specific_heat_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_specific_heat_unit = fallible_specific_heat_unit_t( &
                    parse_specific_heat_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_specific_heat_unit = fallible_specific_heat_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a specific_heat unit")))
        end select
    end function

    function fallible_speed_from_object_and_key( &
            object, key) result(fallible_speed)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_speed_t) :: fallible_speed

        character(len=*), parameter :: procedure_name = "fallible_speed_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_speed = fallible_speed_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_speed = fallible_speed_t( &
                    fallible_speed_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_speed%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_speed%errors()
                    fallible_speed = fallible_speed_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_speed_from_fallible_json(maybe_value) result(fallible_speed)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_speed_t) :: fallible_speed

        character(len=*), parameter :: procedure_name = "fallible_speed_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_speed = fallible_speed_t(maybe_value%errors)
        else
            fallible_speed = fallible_speed_t( &
                    fallible_speed_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_speed_from_json_value(value_) result(fallible_speed)
        class(json_value_t), intent(in) :: value_
        type(fallible_speed_t) :: fallible_speed

        character(len=*), parameter :: procedure_name = "fallible_speed_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_speed = fallible_speed_t( &
                    parse_speed(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_speed = fallible_speed_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a speed")))
        end select
    end function

    function fallible_speed_unit_from_object_and_key( &
            object, key) result(fallible_speed_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_speed_unit_t) :: fallible_speed_unit

        character(len=*), parameter :: procedure_name = "fallible_speed_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_speed_unit = fallible_speed_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_speed_unit = fallible_speed_unit_t( &
                    fallible_speed_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_speed_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_speed_unit%errors()
                    fallible_speed_unit = fallible_speed_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_speed_unit_from_fallible_json(maybe_value) result(fallible_speed_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_speed_unit_t) :: fallible_speed_unit

        character(len=*), parameter :: procedure_name = "fallible_speed_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_speed_unit = fallible_speed_unit_t(maybe_value%errors)
        else
            fallible_speed_unit = fallible_speed_unit_t( &
                    fallible_speed_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_speed_unit_from_json_value(value_) result(fallible_speed_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_speed_unit_t) :: fallible_speed_unit

        character(len=*), parameter :: procedure_name = "fallible_speed_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_speed_unit = fallible_speed_unit_t( &
                    parse_speed_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_speed_unit = fallible_speed_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a speed unit")))
        end select
    end function

    function fallible_temperature_gradient_from_object_and_key( &
            object, key) result(fallible_temperature_gradient)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_temperature_gradient_t) :: fallible_temperature_gradient

        character(len=*), parameter :: procedure_name = "fallible_temperature_gradient_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_temperature_gradient = fallible_temperature_gradient_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_temperature_gradient = fallible_temperature_gradient_t( &
                    fallible_temperature_gradient_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_temperature_gradient%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_temperature_gradient%errors()
                    fallible_temperature_gradient = fallible_temperature_gradient_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_temperature_gradient_from_fallible_json(maybe_value) result(fallible_temperature_gradient)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_temperature_gradient_t) :: fallible_temperature_gradient

        character(len=*), parameter :: procedure_name = "fallible_temperature_gradient_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_temperature_gradient = fallible_temperature_gradient_t(maybe_value%errors)
        else
            fallible_temperature_gradient = fallible_temperature_gradient_t( &
                    fallible_temperature_gradient_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_temperature_gradient_from_json_value(value_) result(fallible_temperature_gradient)
        class(json_value_t), intent(in) :: value_
        type(fallible_temperature_gradient_t) :: fallible_temperature_gradient

        character(len=*), parameter :: procedure_name = "fallible_temperature_gradient_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_temperature_gradient = fallible_temperature_gradient_t( &
                    parse_temperature_gradient(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_temperature_gradient = fallible_temperature_gradient_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a temperature_gradient")))
        end select
    end function

    function fallible_temperature_gradient_unit_from_object_and_key( &
            object, key) result(fallible_temperature_gradient_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_temperature_gradient_unit_t) :: fallible_temperature_gradient_unit

        character(len=*), parameter :: procedure_name = "fallible_temperature_gradient_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_temperature_gradient_unit = fallible_temperature_gradient_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_temperature_gradient_unit = fallible_temperature_gradient_unit_t( &
                    fallible_temperature_gradient_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_temperature_gradient_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_temperature_gradient_unit%errors()
                    fallible_temperature_gradient_unit = fallible_temperature_gradient_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_temperature_gradient_unit_from_fallible_json(maybe_value) result(fallible_temperature_gradient_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_temperature_gradient_unit_t) :: fallible_temperature_gradient_unit

        character(len=*), parameter :: procedure_name = "fallible_temperature_gradient_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_temperature_gradient_unit = fallible_temperature_gradient_unit_t(maybe_value%errors)
        else
            fallible_temperature_gradient_unit = fallible_temperature_gradient_unit_t( &
                    fallible_temperature_gradient_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_temperature_gradient_unit_from_json_value(value_) result(fallible_temperature_gradient_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_temperature_gradient_unit_t) :: fallible_temperature_gradient_unit

        character(len=*), parameter :: procedure_name = "fallible_temperature_gradient_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_temperature_gradient_unit = fallible_temperature_gradient_unit_t( &
                    parse_temperature_gradient_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_temperature_gradient_unit = fallible_temperature_gradient_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a temperature_gradient unit")))
        end select
    end function

    function fallible_temperature_from_object_and_key( &
            object, key) result(fallible_temperature)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_temperature_t) :: fallible_temperature

        character(len=*), parameter :: procedure_name = "fallible_temperature_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_temperature = fallible_temperature_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_temperature = fallible_temperature_t( &
                    fallible_temperature_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_temperature%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_temperature%errors()
                    fallible_temperature = fallible_temperature_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_temperature_from_fallible_json(maybe_value) result(fallible_temperature)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_temperature_t) :: fallible_temperature

        character(len=*), parameter :: procedure_name = "fallible_temperature_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_temperature = fallible_temperature_t(maybe_value%errors)
        else
            fallible_temperature = fallible_temperature_t( &
                    fallible_temperature_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_temperature_from_json_value(value_) result(fallible_temperature)
        class(json_value_t), intent(in) :: value_
        type(fallible_temperature_t) :: fallible_temperature

        character(len=*), parameter :: procedure_name = "fallible_temperature_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_temperature = fallible_temperature_t( &
                    parse_temperature(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_temperature = fallible_temperature_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a temperature")))
        end select
    end function

    function fallible_temperature_unit_from_object_and_key( &
            object, key) result(fallible_temperature_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_temperature_unit_t) :: fallible_temperature_unit

        character(len=*), parameter :: procedure_name = "fallible_temperature_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_temperature_unit = fallible_temperature_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_temperature_unit = fallible_temperature_unit_t( &
                    fallible_temperature_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_temperature_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_temperature_unit%errors()
                    fallible_temperature_unit = fallible_temperature_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_temperature_unit_from_fallible_json(maybe_value) result(fallible_temperature_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_temperature_unit_t) :: fallible_temperature_unit

        character(len=*), parameter :: procedure_name = "fallible_temperature_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_temperature_unit = fallible_temperature_unit_t(maybe_value%errors)
        else
            fallible_temperature_unit = fallible_temperature_unit_t( &
                    fallible_temperature_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_temperature_unit_from_json_value(value_) result(fallible_temperature_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_temperature_unit_t) :: fallible_temperature_unit

        character(len=*), parameter :: procedure_name = "fallible_temperature_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_temperature_unit = fallible_temperature_unit_t( &
                    parse_temperature_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_temperature_unit = fallible_temperature_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a temperature unit")))
        end select
    end function

    function fallible_thermal_conductivity_from_object_and_key( &
            object, key) result(fallible_thermal_conductivity)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_thermal_conductivity_t) :: fallible_thermal_conductivity

        character(len=*), parameter :: procedure_name = "fallible_thermal_conductivity_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_thermal_conductivity = fallible_thermal_conductivity_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_thermal_conductivity = fallible_thermal_conductivity_t( &
                    fallible_thermal_conductivity_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_thermal_conductivity%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_thermal_conductivity%errors()
                    fallible_thermal_conductivity = fallible_thermal_conductivity_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_thermal_conductivity_from_fallible_json(maybe_value) result(fallible_thermal_conductivity)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_thermal_conductivity_t) :: fallible_thermal_conductivity

        character(len=*), parameter :: procedure_name = "fallible_thermal_conductivity_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_thermal_conductivity = fallible_thermal_conductivity_t(maybe_value%errors)
        else
            fallible_thermal_conductivity = fallible_thermal_conductivity_t( &
                    fallible_thermal_conductivity_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_thermal_conductivity_from_json_value(value_) result(fallible_thermal_conductivity)
        class(json_value_t), intent(in) :: value_
        type(fallible_thermal_conductivity_t) :: fallible_thermal_conductivity

        character(len=*), parameter :: procedure_name = "fallible_thermal_conductivity_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_thermal_conductivity = fallible_thermal_conductivity_t( &
                    parse_thermal_conductivity(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_thermal_conductivity = fallible_thermal_conductivity_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a thermal_conductivity")))
        end select
    end function

    function fallible_thermal_conductivity_unit_from_object_and_key( &
            object, key) result(fallible_thermal_conductivity_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_thermal_conductivity_unit_t) :: fallible_thermal_conductivity_unit

        character(len=*), parameter :: procedure_name = "fallible_thermal_conductivity_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t( &
                    fallible_thermal_conductivity_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_thermal_conductivity_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_thermal_conductivity_unit%errors()
                    fallible_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_thermal_conductivity_unit_from_fallible_json(maybe_value) result(fallible_thermal_conductivity_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_thermal_conductivity_unit_t) :: fallible_thermal_conductivity_unit

        character(len=*), parameter :: procedure_name = "fallible_thermal_conductivity_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t(maybe_value%errors)
        else
            fallible_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t( &
                    fallible_thermal_conductivity_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_thermal_conductivity_unit_from_json_value(value_) result(fallible_thermal_conductivity_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_thermal_conductivity_unit_t) :: fallible_thermal_conductivity_unit

        character(len=*), parameter :: procedure_name = "fallible_thermal_conductivity_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t( &
                    parse_thermal_conductivity_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a thermal_conductivity unit")))
        end select
    end function

    function fallible_thermal_expansion_coefficient_from_object_and_key( &
            object, key) result(fallible_thermal_expansion_coefficient)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_thermal_expansion_coefficient_t) :: fallible_thermal_expansion_coefficient

        character(len=*), parameter :: procedure_name = "fallible_thermal_expansion_coefficient_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t( &
                    fallible_thermal_expansion_coefficient_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_thermal_expansion_coefficient%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_thermal_expansion_coefficient%errors()
                    fallible_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_thermal_expansion_coefficient_from_fallible_json(maybe_value) result(fallible_thermal_expansion_coefficient)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_thermal_expansion_coefficient_t) :: fallible_thermal_expansion_coefficient

        character(len=*), parameter :: procedure_name = "fallible_thermal_expansion_coefficient_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t(maybe_value%errors)
        else
            fallible_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t( &
                    fallible_thermal_expansion_coefficient_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_thermal_expansion_coefficient_from_json_value(value_) result(fallible_thermal_expansion_coefficient)
        class(json_value_t), intent(in) :: value_
        type(fallible_thermal_expansion_coefficient_t) :: fallible_thermal_expansion_coefficient

        character(len=*), parameter :: procedure_name = "fallible_thermal_expansion_coefficient_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t( &
                    parse_thermal_expansion_coefficient(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a thermal_expansion_coefficient")))
        end select
    end function

    function fallible_thermal_expansion_coefficient_unit_from_object_and_key( &
            object, key) result(fallible_thermal_expansion_coefficient_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_thermal_expansion_coefficient_unit_t) :: fallible_thermal_expansion_coefficient_unit

        character(len=*), parameter :: procedure_name = "fallible_thermal_expansion_coefficient_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t( &
                    fallible_thermal_expansion_coefficient_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_thermal_expansion_coefficient_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_thermal_expansion_coefficient_unit%errors()
                    fallible_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_thermal_expansion_coefficient_unit_from_fallible_json(maybe_value) result(fallible_thermal_expansion_coefficient_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_thermal_expansion_coefficient_unit_t) :: fallible_thermal_expansion_coefficient_unit

        character(len=*), parameter :: procedure_name = "fallible_thermal_expansion_coefficient_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t(maybe_value%errors)
        else
            fallible_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t( &
                    fallible_thermal_expansion_coefficient_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_thermal_expansion_coefficient_unit_from_json_value(value_) result(fallible_thermal_expansion_coefficient_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_thermal_expansion_coefficient_unit_t) :: fallible_thermal_expansion_coefficient_unit

        character(len=*), parameter :: procedure_name = "fallible_thermal_expansion_coefficient_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t( &
                    parse_thermal_expansion_coefficient_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a thermal_expansion_coefficient unit")))
        end select
    end function

    function fallible_time_from_object_and_key( &
            object, key) result(fallible_time)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_time_t) :: fallible_time

        character(len=*), parameter :: procedure_name = "fallible_time_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_time = fallible_time_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_time = fallible_time_t( &
                    fallible_time_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_time%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_time%errors()
                    fallible_time = fallible_time_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_time_from_fallible_json(maybe_value) result(fallible_time)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_time_t) :: fallible_time

        character(len=*), parameter :: procedure_name = "fallible_time_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_time = fallible_time_t(maybe_value%errors)
        else
            fallible_time = fallible_time_t( &
                    fallible_time_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_time_from_json_value(value_) result(fallible_time)
        class(json_value_t), intent(in) :: value_
        type(fallible_time_t) :: fallible_time

        character(len=*), parameter :: procedure_name = "fallible_time_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_time = fallible_time_t( &
                    parse_time(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_time = fallible_time_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a time")))
        end select
    end function

    function fallible_time_unit_from_object_and_key( &
            object, key) result(fallible_time_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_time_unit_t) :: fallible_time_unit

        character(len=*), parameter :: procedure_name = "fallible_time_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_time_unit = fallible_time_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_time_unit = fallible_time_unit_t( &
                    fallible_time_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_time_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_time_unit%errors()
                    fallible_time_unit = fallible_time_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_time_unit_from_fallible_json(maybe_value) result(fallible_time_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_time_unit_t) :: fallible_time_unit

        character(len=*), parameter :: procedure_name = "fallible_time_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_time_unit = fallible_time_unit_t(maybe_value%errors)
        else
            fallible_time_unit = fallible_time_unit_t( &
                    fallible_time_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_time_unit_from_json_value(value_) result(fallible_time_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_time_unit_t) :: fallible_time_unit

        character(len=*), parameter :: procedure_name = "fallible_time_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_time_unit = fallible_time_unit_t( &
                    parse_time_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_time_unit = fallible_time_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a time unit")))
        end select
    end function

    function fallible_volume_from_object_and_key( &
            object, key) result(fallible_volume)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_volume_t) :: fallible_volume

        character(len=*), parameter :: procedure_name = "fallible_volume_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_volume = fallible_volume_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_volume = fallible_volume_t( &
                    fallible_volume_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_volume%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_volume%errors()
                    fallible_volume = fallible_volume_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_volume_from_fallible_json(maybe_value) result(fallible_volume)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_volume_t) :: fallible_volume

        character(len=*), parameter :: procedure_name = "fallible_volume_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_volume = fallible_volume_t(maybe_value%errors)
        else
            fallible_volume = fallible_volume_t( &
                    fallible_volume_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_volume_from_json_value(value_) result(fallible_volume)
        class(json_value_t), intent(in) :: value_
        type(fallible_volume_t) :: fallible_volume

        character(len=*), parameter :: procedure_name = "fallible_volume_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_volume = fallible_volume_t( &
                    parse_volume(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_volume = fallible_volume_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a volume")))
        end select
    end function

    function fallible_volume_unit_from_object_and_key( &
            object, key) result(fallible_volume_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_volume_unit_t) :: fallible_volume_unit

        character(len=*), parameter :: procedure_name = "fallible_volume_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_volume_unit = fallible_volume_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_volume_unit = fallible_volume_unit_t( &
                    fallible_volume_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_volume_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_volume_unit%errors()
                    fallible_volume_unit = fallible_volume_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_volume_unit_from_fallible_json(maybe_value) result(fallible_volume_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_volume_unit_t) :: fallible_volume_unit

        character(len=*), parameter :: procedure_name = "fallible_volume_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_volume_unit = fallible_volume_unit_t(maybe_value%errors)
        else
            fallible_volume_unit = fallible_volume_unit_t( &
                    fallible_volume_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_volume_unit_from_json_value(value_) result(fallible_volume_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_volume_unit_t) :: fallible_volume_unit

        character(len=*), parameter :: procedure_name = "fallible_volume_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_volume_unit = fallible_volume_unit_t( &
                    parse_volume_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_volume_unit = fallible_volume_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a volume unit")))
        end select
    end function

    function fallible_volume_rate_from_object_and_key( &
            object, key) result(fallible_volume_rate)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_volume_rate_t) :: fallible_volume_rate

        character(len=*), parameter :: procedure_name = "fallible_volume_rate_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_volume_rate = fallible_volume_rate_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_volume_rate = fallible_volume_rate_t( &
                    fallible_volume_rate_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_volume_rate%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_volume_rate%errors()
                    fallible_volume_rate = fallible_volume_rate_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_volume_rate_from_fallible_json(maybe_value) result(fallible_volume_rate)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_volume_rate_t) :: fallible_volume_rate

        character(len=*), parameter :: procedure_name = "fallible_volume_rate_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_volume_rate = fallible_volume_rate_t(maybe_value%errors)
        else
            fallible_volume_rate = fallible_volume_rate_t( &
                    fallible_volume_rate_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_volume_rate_from_json_value(value_) result(fallible_volume_rate)
        class(json_value_t), intent(in) :: value_
        type(fallible_volume_rate_t) :: fallible_volume_rate

        character(len=*), parameter :: procedure_name = "fallible_volume_rate_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_volume_rate = fallible_volume_rate_t( &
                    parse_volume_rate(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_volume_rate = fallible_volume_rate_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a volume_rate")))
        end select
    end function

    function fallible_volume_rate_unit_from_object_and_key( &
            object, key) result(fallible_volume_rate_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_volume_rate_unit_t) :: fallible_volume_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_volume_rate_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_volume_rate_unit = fallible_volume_rate_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_volume_rate_unit = fallible_volume_rate_unit_t( &
                    fallible_volume_rate_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_volume_rate_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_volume_rate_unit%errors()
                    fallible_volume_rate_unit = fallible_volume_rate_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_volume_rate_unit_from_fallible_json(maybe_value) result(fallible_volume_rate_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_volume_rate_unit_t) :: fallible_volume_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_volume_rate_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_volume_rate_unit = fallible_volume_rate_unit_t(maybe_value%errors)
        else
            fallible_volume_rate_unit = fallible_volume_rate_unit_t( &
                    fallible_volume_rate_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_volume_rate_unit_from_json_value(value_) result(fallible_volume_rate_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_volume_rate_unit_t) :: fallible_volume_rate_unit

        character(len=*), parameter :: procedure_name = "fallible_volume_rate_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_volume_rate_unit = fallible_volume_rate_unit_t( &
                    parse_volume_rate_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_volume_rate_unit = fallible_volume_rate_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a volume_rate unit")))
        end select
    end function

    function fallible_volumetric_heat_capacity_from_object_and_key( &
            object, key) result(fallible_volumetric_heat_capacity)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_volumetric_heat_capacity_t) :: fallible_volumetric_heat_capacity

        character(len=*), parameter :: procedure_name = "fallible_volumetric_heat_capacity_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t( &
                    fallible_volumetric_heat_capacity_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_volumetric_heat_capacity%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_volumetric_heat_capacity%errors()
                    fallible_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_volumetric_heat_capacity_from_fallible_json(maybe_value) result(fallible_volumetric_heat_capacity)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_volumetric_heat_capacity_t) :: fallible_volumetric_heat_capacity

        character(len=*), parameter :: procedure_name = "fallible_volumetric_heat_capacity_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t(maybe_value%errors)
        else
            fallible_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t( &
                    fallible_volumetric_heat_capacity_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_volumetric_heat_capacity_from_json_value(value_) result(fallible_volumetric_heat_capacity)
        class(json_value_t), intent(in) :: value_
        type(fallible_volumetric_heat_capacity_t) :: fallible_volumetric_heat_capacity

        character(len=*), parameter :: procedure_name = "fallible_volumetric_heat_capacity_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t( &
                    parse_volumetric_heat_capacity(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a volumetric_heat_capacity")))
        end select
    end function

    function fallible_volumetric_heat_capacity_unit_from_object_and_key( &
            object, key) result(fallible_volumetric_heat_capacity_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_volumetric_heat_capacity_unit_t) :: fallible_volumetric_heat_capacity_unit

        character(len=*), parameter :: procedure_name = "fallible_volumetric_heat_capacity_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t( &
                    fallible_volumetric_heat_capacity_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_volumetric_heat_capacity_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_volumetric_heat_capacity_unit%errors()
                    fallible_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_volumetric_heat_capacity_unit_from_fallible_json(maybe_value) result(fallible_volumetric_heat_capacity_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_volumetric_heat_capacity_unit_t) :: fallible_volumetric_heat_capacity_unit

        character(len=*), parameter :: procedure_name = "fallible_volumetric_heat_capacity_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t(maybe_value%errors)
        else
            fallible_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t( &
                    fallible_volumetric_heat_capacity_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_volumetric_heat_capacity_unit_from_json_value(value_) result(fallible_volumetric_heat_capacity_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_volumetric_heat_capacity_unit_t) :: fallible_volumetric_heat_capacity_unit

        character(len=*), parameter :: procedure_name = "fallible_volumetric_heat_capacity_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t( &
                    parse_volumetric_heat_capacity_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a volumetric_heat_capacity unit")))
        end select
    end function

    function fallible_yank_from_object_and_key( &
            object, key) result(fallible_yank)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_yank_t) :: fallible_yank

        character(len=*), parameter :: procedure_name = "fallible_yank_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_yank = fallible_yank_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_yank = fallible_yank_t( &
                    fallible_yank_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_yank%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_yank%errors()
                    fallible_yank = fallible_yank_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_yank_from_fallible_json(maybe_value) result(fallible_yank)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_yank_t) :: fallible_yank

        character(len=*), parameter :: procedure_name = "fallible_yank_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_yank = fallible_yank_t(maybe_value%errors)
        else
            fallible_yank = fallible_yank_t( &
                    fallible_yank_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_yank_from_json_value(value_) result(fallible_yank)
        class(json_value_t), intent(in) :: value_
        type(fallible_yank_t) :: fallible_yank

        character(len=*), parameter :: procedure_name = "fallible_yank_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_yank = fallible_yank_t( &
                    parse_yank(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_yank = fallible_yank_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a yank")))
        end select
    end function

    function fallible_yank_unit_from_object_and_key( &
            object, key) result(fallible_yank_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_yank_unit_t) :: fallible_yank_unit

        character(len=*), parameter :: procedure_name = "fallible_yank_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_yank_unit = fallible_yank_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_yank_unit = fallible_yank_unit_t( &
                    fallible_yank_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_yank_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_yank_unit%errors()
                    fallible_yank_unit = fallible_yank_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_yank_unit_from_fallible_json(maybe_value) result(fallible_yank_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_yank_unit_t) :: fallible_yank_unit

        character(len=*), parameter :: procedure_name = "fallible_yank_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_yank_unit = fallible_yank_unit_t(maybe_value%errors)
        else
            fallible_yank_unit = fallible_yank_unit_t( &
                    fallible_yank_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_yank_unit_from_json_value(value_) result(fallible_yank_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_yank_unit_t) :: fallible_yank_unit

        character(len=*), parameter :: procedure_name = "fallible_yank_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_yank_unit = fallible_yank_unit_t( &
                    parse_yank_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_yank_unit = fallible_yank_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a yank unit")))
        end select
    end function
end module
