module rojff_to_quaff_test
    use erloff, only: error_list_t, NOT_FOUND
    use quaff
    use quaff_asserts_m, only: assert_equals
    use rojff, only: &
            json_null_t, &
            json_object_t, &
            json_member_unsafe, &
            json_object_unsafe, &
            json_string_unsafe
    use rojff_to_quaff
    use veggies, only: &
            result_t, test_item_t, assert_equals, assert_that, describe, fail, it

    implicit none
    private
    public :: test_rojff_to_quaff
contains
    function test_rojff_to_quaff() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
            "rojff_to_quaff", &
            [ describe( &
                "obtains the original quantity from corectly constructed json for", &
                [ it("acceleration", check_valid_json_to_acceleration) &
                , it("amount", check_valid_json_to_amount) &
                , it("amount_rate", check_valid_json_to_amount_rate) &
                , it("amount_temperature", check_valid_json_to_amount_temperature) &
                , it("amount_temperature_rate", check_valid_json_to_amount_temperature_rate) &
                , it("angle", check_valid_json_to_angle) &
                , it("area", check_valid_json_to_area) &
                , it("conductance", check_valid_json_to_conductance) &
                , it("delta_temperature", check_valid_json_to_delta_temperature) &
                , it("density", check_valid_json_to_density) &
                , it("diffusivity", check_valid_json_to_diffusivity) &
                , it("dynamic_viscosity", check_valid_json_to_dynamic_viscosity) &
                , it("energy", check_valid_json_to_energy) &
                , it("enthalpy", check_valid_json_to_enthalpy) &
                , it("fluence", check_valid_json_to_fluence) &
                , it("force", check_valid_json_to_force) &
                , it("fracture_toughness", check_valid_json_to_fracture_toughness) &
                , it("frequency", check_valid_json_to_frequency) &
                , it("heat_capacity", check_valid_json_to_heat_capacity) &
                , it("heat_transfer_coefficient", check_valid_json_to_heat_transfer_coefficient) &
                , it("insulance", check_valid_json_to_insulance) &
                , it("intensity", check_valid_json_to_intensity) &
                , it("inverse_molar_mass", check_valid_json_to_inverse_molar_mass) &
                , it("length", check_valid_json_to_length) &
                , it("mass", check_valid_json_to_mass) &
                , it("mass_flux", check_valid_json_to_mass_flux) &
                , it("mass_rate", check_valid_json_to_mass_rate) &
                , it("molar_density", check_valid_json_to_molar_density) &
                , it("molar_enthalpy", check_valid_json_to_molar_enthalpy) &
                , it("molar_mass", check_valid_json_to_molar_mass) &
                , it("molar_specific_heat", check_valid_json_to_molar_specific_heat) &
                , it("power", check_valid_json_to_power) &
                , it("pressure", check_valid_json_to_pressure) &
                , it("specific_heat", check_valid_json_to_specific_heat) &
                , it("speed", check_valid_json_to_speed) &
                , it("temperature_gradient", check_valid_json_to_temperature_gradient) &
                , it("temperature", check_valid_json_to_temperature) &
                , it("thermal_conductivity", check_valid_json_to_thermal_conductivity) &
                , it("thermal_expansion_coefficient", check_valid_json_to_thermal_expansion_coefficient) &
                , it("time", check_valid_json_to_time) &
                , it("volume", check_valid_json_to_volume) &
                , it("volume_rate", check_valid_json_to_volume_rate) &
                , it("volumetric_heat_capacity", check_valid_json_to_volumetric_heat_capacity) &
                , it("yank", check_valid_json_to_yank) &
                ]) &
            , describe( &
                "forwards the error if the provided key is not in the given object for", &
                [ it("acceleration", check_missing_key_acceleration) &
                , it("amount", check_missing_key_amount) &
                , it("amount_rate", check_missing_key_amount_rate) &
                , it("amount_temperature", check_missing_key_amount_temperature) &
                , it("amount_temperature_rate", check_missing_key_amount_temperature_rate) &
                , it("angle", check_missing_key_angle) &
                , it("area", check_missing_key_area) &
                , it("conductance", check_missing_key_conductance) &
                , it("delta_temperature", check_missing_key_delta_temperature) &
                , it("density", check_missing_key_density) &
                , it("diffusivity", check_missing_key_diffusivity) &
                , it("dynamic_viscosity", check_missing_key_dynamic_viscosity) &
                , it("energy", check_missing_key_energy) &
                , it("enthalpy", check_missing_key_enthalpy) &
                , it("fluence", check_missing_key_fluence) &
                , it("force", check_missing_key_force) &
                , it("fracture_toughness", check_missing_key_fracture_toughness) &
                , it("frequency", check_missing_key_frequency) &
                , it("heat_capacity", check_missing_key_heat_capacity) &
                , it("heat_transfer_coefficient", check_missing_key_heat_transfer_coefficient) &
                , it("insulance", check_missing_key_insulance) &
                , it("intensity", check_missing_key_intensity) &
                , it("inverse_molar_mass", check_missing_key_inverse_molar_mass) &
                , it("length", check_missing_key_length) &
                , it("mass", check_missing_key_mass) &
                , it("mass_flux", check_missing_key_mass_flux) &
                , it("mass_rate", check_missing_key_mass_rate) &
                , it("molar_density", check_missing_key_molar_density) &
                , it("molar_enthalpy", check_missing_key_molar_enthalpy) &
                , it("molar_mass", check_missing_key_molar_mass) &
                , it("molar_specific_heat", check_missing_key_molar_specific_heat) &
                , it("power", check_missing_key_power) &
                , it("pressure", check_missing_key_pressure) &
                , it("specific_heat", check_missing_key_specific_heat) &
                , it("speed", check_missing_key_speed) &
                , it("temperature_gradient", check_missing_key_temperature_gradient) &
                , it("temperature", check_missing_key_temperature) &
                , it("thermal_conductivity", check_missing_key_thermal_conductivity) &
                , it("thermal_expansion_coefficient", check_missing_key_thermal_expansion_coefficient) &
                , it("time", check_missing_key_time) &
                , it("volume", check_missing_key_volume) &
                , it("volume_rate", check_missing_key_volume_rate) &
                , it("volumetric_heat_capacity", check_missing_key_volumetric_heat_capacity) &
                , it("yank", check_missing_key_yank) &
                ]) &
            , describe( &
                "catches error where the referenced json value is not a string for", &
                [ it("acceleration", check_not_string_acceleration) &
                , it("amount", check_not_string_amount) &
                , it("amount_rate", check_not_string_amount_rate) &
                , it("amount_temperature", check_not_string_amount_temperature) &
                , it("amount_temperature_rate", check_not_string_amount_temperature_rate) &
                , it("angle", check_not_string_angle) &
                , it("area", check_not_string_area) &
                , it("conductance", check_not_string_conductance) &
                , it("delta_temperature", check_not_string_delta_temperature) &
                , it("density", check_not_string_density) &
                , it("diffusivity", check_not_string_diffusivity) &
                , it("dynamic_viscosity", check_not_string_dynamic_viscosity) &
                , it("energy", check_not_string_energy) &
                , it("enthalpy", check_not_string_enthalpy) &
                , it("fluence", check_not_string_fluence) &
                , it("force", check_not_string_force) &
                , it("fracture_toughness", check_not_string_fracture_toughness) &
                , it("frequency", check_not_string_frequency) &
                , it("heat_capacity", check_not_string_heat_capacity) &
                , it("heat_transfer_coefficient", check_not_string_heat_transfer_coefficient) &
                , it("insulance", check_not_string_insulance) &
                , it("intensity", check_not_string_intensity) &
                , it("inverse_molar_mass", check_not_string_inverse_molar_mass) &
                , it("length", check_not_string_length) &
                , it("mass", check_not_string_mass) &
                , it("mass_flux", check_not_string_mass_flux) &
                , it("mass_rate", check_not_string_mass_rate) &
                , it("molar_density", check_not_string_molar_density) &
                , it("molar_enthalpy", check_not_string_molar_enthalpy) &
                , it("molar_mass", check_not_string_molar_mass) &
                , it("molar_specific_heat", check_not_string_molar_specific_heat) &
                , it("power", check_not_string_power) &
                , it("pressure", check_not_string_pressure) &
                , it("specific_heat", check_not_string_specific_heat) &
                , it("speed", check_not_string_speed) &
                , it("temperature_gradient", check_not_string_temperature_gradient) &
                , it("temperature", check_not_string_temperature) &
                , it("thermal_conductivity", check_not_string_thermal_conductivity) &
                , it("thermal_expansion_coefficient", check_not_string_thermal_expansion_coefficient) &
                , it("time", check_not_string_time) &
                , it("volume", check_not_string_volume) &
                , it("volume_rate", check_not_string_volume_rate) &
                , it("volumetric_heat_capacity", check_not_string_volumetric_heat_capacity) &
                , it("yank", check_not_string_yank) &
                ]) &
            , describe( &
                "forwards the error if the string could not be parsed for", &
                [ it("acceleration", check_parse_error_acceleration) &
                , it("amount", check_parse_error_amount) &
                , it("amount_rate", check_parse_error_amount_rate) &
                , it("amount_temperature", check_parse_error_amount_temperature) &
                , it("amount_temperature_rate", check_parse_error_amount_temperature_rate) &
                , it("angle", check_parse_error_angle) &
                , it("area", check_parse_error_area) &
                , it("conductance", check_parse_error_conductance) &
                , it("delta_temperature", check_parse_error_delta_temperature) &
                , it("density", check_parse_error_density) &
                , it("diffusivity", check_parse_error_diffusivity) &
                , it("dynamic_viscosity", check_parse_error_dynamic_viscosity) &
                , it("energy", check_parse_error_energy) &
                , it("enthalpy", check_parse_error_enthalpy) &
                , it("fluence", check_parse_error_fluence) &
                , it("force", check_parse_error_force) &
                , it("fracture_toughness", check_parse_error_fracture_toughness) &
                , it("frequency", check_parse_error_frequency) &
                , it("heat_capacity", check_parse_error_heat_capacity) &
                , it("heat_transfer_coefficient", check_parse_error_heat_transfer_coefficient) &
                , it("insulance", check_parse_error_insulance) &
                , it("intensity", check_parse_error_intensity) &
                , it("inverse_molar_mass", check_parse_error_inverse_molar_mass) &
                , it("length", check_parse_error_length) &
                , it("mass", check_parse_error_mass) &
                , it("mass_flux", check_parse_error_mass_flux) &
                , it("mass_rate", check_parse_error_mass_rate) &
                , it("molar_density", check_parse_error_molar_density) &
                , it("molar_enthalpy", check_parse_error_molar_enthalpy) &
                , it("molar_mass", check_parse_error_molar_mass) &
                , it("molar_specific_heat", check_parse_error_molar_specific_heat) &
                , it("power", check_parse_error_power) &
                , it("pressure", check_parse_error_pressure) &
                , it("specific_heat", check_parse_error_specific_heat) &
                , it("speed", check_parse_error_speed) &
                , it("temperature_gradient", check_parse_error_temperature_gradient) &
                , it("temperature", check_parse_error_temperature) &
                , it("thermal_conductivity", check_parse_error_thermal_conductivity) &
                , it("thermal_expansion_coefficient", check_parse_error_thermal_expansion_coefficient) &
                , it("time", check_parse_error_time) &
                , it("volume", check_parse_error_volume) &
                , it("volume_rate", check_parse_error_volume_rate) &
                , it("volumetric_heat_capacity", check_parse_error_volumetric_heat_capacity) &
                , it("yank", check_parse_error_yank) &
                ]) &
            , describe( &
                "obtains the original quantity unit from corectly constructed json for", &
                [ it("acceleration unit", check_valid_json_to_acceleration_unit) &
                , it("amount unit", check_valid_json_to_amount_unit) &
                , it("amount_rate unit", check_valid_json_to_amount_rate_unit) &
                , it("amount_temperature unit", check_valid_json_to_amount_temperature_unit) &
                , it("amount_temperature_rate unit", check_valid_json_to_amount_temperature_rate_unit) &
                , it("angle unit", check_valid_json_to_angle_unit) &
                , it("area unit", check_valid_json_to_area_unit) &
                , it("conductance unit", check_valid_json_to_conductance_unit) &
                , it("delta_temperature unit", check_valid_json_to_delta_temperature_unit) &
                , it("density unit", check_valid_json_to_density_unit) &
                , it("diffusivity unit", check_valid_json_to_diffusivity_unit) &
                , it("dynamic_viscosity unit", check_valid_json_to_dynamic_viscosity_unit) &
                , it("energy unit", check_valid_json_to_energy_unit) &
                , it("enthalpy unit", check_valid_json_to_enthalpy_unit) &
                , it("fluence unit", check_valid_json_to_fluence_unit) &
                , it("force unit", check_valid_json_to_force_unit) &
                , it("fracture_toughness unit", check_valid_json_to_fracture_toughness_unit) &
                , it("frequency unit", check_valid_json_to_frequency_unit) &
                , it("heat_capacity unit", check_valid_json_to_heat_capacity_unit) &
                , it("heat_transfer_coefficient unit", check_valid_json_to_heat_transfer_coefficient_unit) &
                , it("insulance unit", check_valid_json_to_insulance_unit) &
                , it("intensity unit", check_valid_json_to_intensity_unit) &
                , it("inverse_molar_mass unit", check_valid_json_to_inverse_molar_mass_unit) &
                , it("length unit", check_valid_json_to_length_unit) &
                , it("mass unit", check_valid_json_to_mass_unit) &
                , it("mass_flux unit", check_valid_json_to_mass_flux_unit) &
                , it("mass_rate unit", check_valid_json_to_mass_rate_unit) &
                , it("molar_density unit", check_valid_json_to_molar_density_unit) &
                , it("molar_enthalpy unit", check_valid_json_to_molar_enthalpy_unit) &
                , it("molar_mass unit", check_valid_json_to_molar_mass_unit) &
                , it("molar_specific_heat unit", check_valid_json_to_molar_specific_heat_unit) &
                , it("power unit", check_valid_json_to_power_unit) &
                , it("pressure unit", check_valid_json_to_pressure_unit) &
                , it("specific_heat unit", check_valid_json_to_specific_heat_unit) &
                , it("speed unit", check_valid_json_to_speed_unit) &
                , it("temperature_gradient unit", check_valid_json_to_temperature_gradient_unit) &
                , it("temperature unit", check_valid_json_to_temperature_unit) &
                , it("thermal_conductivity unit", check_valid_json_to_thermal_conductivity_unit) &
                , it("thermal_expansion_coefficient unit", check_valid_json_to_thermal_expansion_coefficient_unit) &
                , it("time unit", check_valid_json_to_time_unit) &
                , it("volume unit", check_valid_json_to_volume_unit) &
                , it("volume_rate unit", check_valid_json_to_volume_rate_unit) &
                , it("volumetric_heat_capacity unit", check_valid_json_to_volumetric_heat_capacity_unit) &
                , it("yank unit", check_valid_json_to_yank_unit) &
                ]) &
            , describe( &
                "forwards the error if the provided key is not in the given object for", &
                [ it("acceleration unit", check_missing_key_acceleration_unit) &
                , it("amount unit", check_missing_key_amount_unit) &
                , it("amount_rate unit", check_missing_key_amount_rate_unit) &
                , it("amount_temperature unit", check_missing_key_amount_temperature_unit) &
                , it("amount_temperature_rate unit", check_missing_key_amount_temperature_rate_unit) &
                , it("angle unit", check_missing_key_angle_unit) &
                , it("area unit", check_missing_key_area_unit) &
                , it("conductance unit", check_missing_key_conductance_unit) &
                , it("delta_temperature unit", check_missing_key_delta_temperature_unit) &
                , it("density unit", check_missing_key_density_unit) &
                , it("diffusivity unit", check_missing_key_diffusivity_unit) &
                , it("dynamic_viscosity unit", check_missing_key_dynamic_viscosity_unit) &
                , it("energy unit", check_missing_key_energy_unit) &
                , it("enthalpy unit", check_missing_key_enthalpy_unit) &
                , it("fluence unit", check_missing_key_fluence_unit) &
                , it("force unit", check_missing_key_force_unit) &
                , it("fracture_toughness unit", check_missing_key_fracture_toughness_unit) &
                , it("frequency unit", check_missing_key_frequency_unit) &
                , it("heat_capacity unit", check_missing_key_heat_capacity_unit) &
                , it("heat_transfer_coefficient unit", check_missing_key_heat_transfer_coefficient_unit) &
                , it("insulance unit", check_missing_key_insulance_unit) &
                , it("intensity unit", check_missing_key_intensity_unit) &
                , it("inverse_molar_mass unit", check_missing_key_inverse_molar_mass_unit) &
                , it("length unit", check_missing_key_length_unit) &
                , it("mass unit", check_missing_key_mass_unit) &
                , it("mass_flux unit", check_missing_key_mass_flux_unit) &
                , it("mass_rate unit", check_missing_key_mass_rate_unit) &
                , it("molar_density unit", check_missing_key_molar_density_unit) &
                , it("molar_enthalpy unit", check_missing_key_molar_enthalpy_unit) &
                , it("molar_mass unit", check_missing_key_molar_mass_unit) &
                , it("molar_specific_heat unit", check_missing_key_molar_specific_heat_unit) &
                , it("power unit", check_missing_key_power_unit) &
                , it("pressure unit", check_missing_key_pressure_unit) &
                , it("specific_heat unit", check_missing_key_specific_heat_unit) &
                , it("speed unit", check_missing_key_speed_unit) &
                , it("temperature_gradient unit", check_missing_key_temperature_gradient_unit) &
                , it("temperature unit", check_missing_key_temperature_unit) &
                , it("thermal_conductivity unit", check_missing_key_thermal_conductivity_unit) &
                , it("thermal_expansion_coefficient unit", check_missing_key_thermal_expansion_coefficient_unit) &
                , it("time unit", check_missing_key_time_unit) &
                , it("volume unit", check_missing_key_volume_unit) &
                , it("volume_rate unit", check_missing_key_volume_rate_unit) &
                , it("volumetric_heat_capacity unit", check_missing_key_volumetric_heat_capacity_unit) &
                , it("yank unit", check_missing_key_yank_unit) &
                ]) &
            , describe( &
                "catches error where the referenced json value is not a string for", &
                [ it("acceleration unit", check_not_string_acceleration_unit) &
                , it("amount unit", check_not_string_amount_unit) &
                , it("amount_rate unit", check_not_string_amount_rate_unit) &
                , it("amount_temperature unit", check_not_string_amount_temperature_unit) &
                , it("amount_temperature_rate unit", check_not_string_amount_temperature_rate_unit) &
                , it("angle unit", check_not_string_angle_unit) &
                , it("area unit", check_not_string_area_unit) &
                , it("conductance unit", check_not_string_conductance_unit) &
                , it("delta_temperature unit", check_not_string_delta_temperature_unit) &
                , it("density unit", check_not_string_density_unit) &
                , it("diffusivity unit", check_not_string_diffusivity_unit) &
                , it("dynamic_viscosity unit", check_not_string_dynamic_viscosity_unit) &
                , it("energy unit", check_not_string_energy_unit) &
                , it("enthalpy unit", check_not_string_enthalpy_unit) &
                , it("fluence unit", check_not_string_fluence_unit) &
                , it("force unit", check_not_string_force_unit) &
                , it("fracture_toughness unit", check_not_string_fracture_toughness_unit) &
                , it("frequency unit", check_not_string_frequency_unit) &
                , it("heat_capacity unit", check_not_string_heat_capacity_unit) &
                , it("heat_transfer_coefficient unit", check_not_string_heat_transfer_coefficient_unit) &
                , it("insulance unit", check_not_string_insulance_unit) &
                , it("intensity unit", check_not_string_intensity_unit) &
                , it("inverse_molar_mass unit", check_not_string_inverse_molar_mass_unit) &
                , it("length unit", check_not_string_length_unit) &
                , it("mass unit", check_not_string_mass_unit) &
                , it("mass_flux unit", check_not_string_mass_flux_unit) &
                , it("mass_rate unit", check_not_string_mass_rate_unit) &
                , it("molar_density unit", check_not_string_molar_density_unit) &
                , it("molar_enthalpy unit", check_not_string_molar_enthalpy_unit) &
                , it("molar_mass unit", check_not_string_molar_mass_unit) &
                , it("molar_specific_heat unit", check_not_string_molar_specific_heat_unit) &
                , it("power unit", check_not_string_power_unit) &
                , it("pressure unit", check_not_string_pressure_unit) &
                , it("specific_heat unit", check_not_string_specific_heat_unit) &
                , it("speed unit", check_not_string_speed_unit) &
                , it("temperature_gradient unit", check_not_string_temperature_gradient_unit) &
                , it("temperature unit", check_not_string_temperature_unit) &
                , it("thermal_conductivity unit", check_not_string_thermal_conductivity_unit) &
                , it("thermal_expansion_coefficient unit", check_not_string_thermal_expansion_coefficient_unit) &
                , it("time unit", check_not_string_time_unit) &
                , it("volume unit", check_not_string_volume_unit) &
                , it("volume_rate unit", check_not_string_volume_rate_unit) &
                , it("volumetric_heat_capacity unit", check_not_string_volumetric_heat_capacity_unit) &
                , it("yank unit", check_not_string_yank_unit) &
                ]) &
            , describe( &
                "forwards the error if the string could not be parsed for", &
                [ it("acceleration unit", check_parse_error_acceleration_unit) &
                , it("amount unit", check_parse_error_amount_unit) &
                , it("amount_rate unit", check_parse_error_amount_rate_unit) &
                , it("amount_temperature unit", check_parse_error_amount_temperature_unit) &
                , it("amount_temperature_rate unit", check_parse_error_amount_temperature_rate_unit) &
                , it("angle unit", check_parse_error_angle_unit) &
                , it("area unit", check_parse_error_area_unit) &
                , it("conductance unit", check_parse_error_conductance_unit) &
                , it("delta_temperature unit", check_parse_error_delta_temperature_unit) &
                , it("density unit", check_parse_error_density_unit) &
                , it("diffusivity unit", check_parse_error_diffusivity_unit) &
                , it("dynamic_viscosity unit", check_parse_error_dynamic_viscosity_unit) &
                , it("energy unit", check_parse_error_energy_unit) &
                , it("enthalpy unit", check_parse_error_enthalpy_unit) &
                , it("fluence unit", check_parse_error_fluence_unit) &
                , it("force unit", check_parse_error_force_unit) &
                , it("fracture_toughness unit", check_parse_error_fracture_toughness_unit) &
                , it("frequency unit", check_parse_error_frequency_unit) &
                , it("heat_capacity unit", check_parse_error_heat_capacity_unit) &
                , it("heat_transfer_coefficient unit", check_parse_error_heat_transfer_coefficient_unit) &
                , it("insulance unit", check_parse_error_insulance_unit) &
                , it("intensity unit", check_parse_error_intensity_unit) &
                , it("inverse_molar_mass unit", check_parse_error_inverse_molar_mass_unit) &
                , it("length unit", check_parse_error_length_unit) &
                , it("mass unit", check_parse_error_mass_unit) &
                , it("mass_flux unit", check_parse_error_mass_flux_unit) &
                , it("mass_rate unit", check_parse_error_mass_rate_unit) &
                , it("molar_density unit", check_parse_error_molar_density_unit) &
                , it("molar_enthalpy unit", check_parse_error_molar_enthalpy_unit) &
                , it("molar_mass unit", check_parse_error_molar_mass_unit) &
                , it("molar_specific_heat unit", check_parse_error_molar_specific_heat_unit) &
                , it("power unit", check_parse_error_power_unit) &
                , it("pressure unit", check_parse_error_pressure_unit) &
                , it("specific_heat unit", check_parse_error_specific_heat_unit) &
                , it("speed unit", check_parse_error_speed_unit) &
                , it("temperature_gradient unit", check_parse_error_temperature_gradient_unit) &
                , it("temperature unit", check_parse_error_temperature_unit) &
                , it("thermal_conductivity unit", check_parse_error_thermal_conductivity_unit) &
                , it("thermal_expansion_coefficient unit", check_parse_error_thermal_expansion_coefficient_unit) &
                , it("time unit", check_parse_error_time_unit) &
                , it("volume unit", check_parse_error_volume_unit) &
                , it("volume_rate unit", check_parse_error_volume_rate_unit) &
                , it("volumetric_heat_capacity unit", check_parse_error_volumetric_heat_capacity_unit) &
                , it("yank unit", check_parse_error_yank_unit) &
                ]) &
            ])
    end function

    function check_valid_json_to_acceleration() result(result_)
        type(result_t) :: result_

        type(acceleration_t) :: original
        type(json_object_t) :: json
        type(fallible_acceleration_t) :: maybe_acceleration
        type(error_list_t) :: errors

        original = 3.3.unit.meters_per_square_second
        json = json_object_unsafe([json_member_unsafe( &
                "acceleration", json_string_unsafe(original%to_string()))])
        maybe_acceleration = fallible_acceleration_t(json, "acceleration")
        if (maybe_acceleration%failed()) then
            errors = maybe_acceleration%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_acceleration%acceleration())
        end if
    end function

    function check_valid_json_to_amount() result(result_)
        type(result_t) :: result_

        type(amount_t) :: original
        type(json_object_t) :: json
        type(fallible_amount_t) :: maybe_amount
        type(error_list_t) :: errors

        original = 3.3.unit.mols
        json = json_object_unsafe([json_member_unsafe( &
                "amount", json_string_unsafe(original%to_string()))])
        maybe_amount = fallible_amount_t(json, "amount")
        if (maybe_amount%failed()) then
            errors = maybe_amount%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_amount%amount())
        end if
    end function

    function check_valid_json_to_amount_rate() result(result_)
        type(result_t) :: result_

        type(amount_rate_t) :: original
        type(json_object_t) :: json
        type(fallible_amount_rate_t) :: maybe_amount_rate
        type(error_list_t) :: errors

        original = 3.3.unit.mols_per_second
        json = json_object_unsafe([json_member_unsafe( &
                "amount_rate", json_string_unsafe(original%to_string()))])
        maybe_amount_rate = fallible_amount_rate_t(json, "amount_rate")
        if (maybe_amount_rate%failed()) then
            errors = maybe_amount_rate%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_amount_rate%amount_rate())
        end if
    end function

    function check_valid_json_to_amount_temperature() result(result_)
        type(result_t) :: result_

        type(amount_temperature_t) :: original
        type(json_object_t) :: json
        type(fallible_amount_temperature_t) :: maybe_amount_temperature
        type(error_list_t) :: errors

        original = 3.3.unit.mols_kelvin
        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature", json_string_unsafe(original%to_string()))])
        maybe_amount_temperature = fallible_amount_temperature_t(json, "amount_temperature")
        if (maybe_amount_temperature%failed()) then
            errors = maybe_amount_temperature%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_amount_temperature%amount_temperature())
        end if
    end function

    function check_valid_json_to_amount_temperature_rate() result(result_)
        type(result_t) :: result_

        type(amount_temperature_rate_t) :: original
        type(json_object_t) :: json
        type(fallible_amount_temperature_rate_t) :: maybe_amount_temperature_rate
        type(error_list_t) :: errors

        original = 3.3.unit.mols_kelvin_per_second
        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature_rate", json_string_unsafe(original%to_string()))])
        maybe_amount_temperature_rate = fallible_amount_temperature_rate_t(json, "amount_temperature_rate")
        if (maybe_amount_temperature_rate%failed()) then
            errors = maybe_amount_temperature_rate%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_amount_temperature_rate%amount_temperature_rate())
        end if
    end function

    function check_valid_json_to_angle() result(result_)
        type(result_t) :: result_

        type(angle_t) :: original
        type(json_object_t) :: json
        type(fallible_angle_t) :: maybe_angle
        type(error_list_t) :: errors

        original = 3.3.unit.radians
        json = json_object_unsafe([json_member_unsafe( &
                "angle", json_string_unsafe(original%to_string()))])
        maybe_angle = fallible_angle_t(json, "angle")
        if (maybe_angle%failed()) then
            errors = maybe_angle%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_angle%angle())
        end if
    end function

    function check_valid_json_to_area() result(result_)
        type(result_t) :: result_

        type(area_t) :: original
        type(json_object_t) :: json
        type(fallible_area_t) :: maybe_area
        type(error_list_t) :: errors

        original = 3.3.unit.square_meters
        json = json_object_unsafe([json_member_unsafe( &
                "area", json_string_unsafe(original%to_string()))])
        maybe_area = fallible_area_t(json, "area")
        if (maybe_area%failed()) then
            errors = maybe_area%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_area%area())
        end if
    end function

    function check_valid_json_to_conductance() result(result_)
        type(result_t) :: result_

        type(conductance_t) :: original
        type(json_object_t) :: json
        type(fallible_conductance_t) :: maybe_conductance
        type(error_list_t) :: errors

        original = 3.3.unit.watts_per_kelvin
        json = json_object_unsafe([json_member_unsafe( &
                "conductance", json_string_unsafe(original%to_string()))])
        maybe_conductance = fallible_conductance_t(json, "conductance")
        if (maybe_conductance%failed()) then
            errors = maybe_conductance%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_conductance%conductance())
        end if
    end function

    function check_valid_json_to_delta_temperature() result(result_)
        type(result_t) :: result_

        type(delta_temperature_t) :: original
        type(json_object_t) :: json
        type(fallible_delta_temperature_t) :: maybe_delta_temperature
        type(error_list_t) :: errors

        original = 3.3.unit.delta_kelvin
        json = json_object_unsafe([json_member_unsafe( &
                "delta_temperature", json_string_unsafe(original%to_string()))])
        maybe_delta_temperature = fallible_delta_temperature_t(json, "delta_temperature")
        if (maybe_delta_temperature%failed()) then
            errors = maybe_delta_temperature%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_delta_temperature%delta_temperature())
        end if
    end function

    function check_valid_json_to_density() result(result_)
        type(result_t) :: result_

        type(density_t) :: original
        type(json_object_t) :: json
        type(fallible_density_t) :: maybe_density
        type(error_list_t) :: errors

        original = 3.3.unit.kilograms_per_cubic_meter
        json = json_object_unsafe([json_member_unsafe( &
                "density", json_string_unsafe(original%to_string()))])
        maybe_density = fallible_density_t(json, "density")
        if (maybe_density%failed()) then
            errors = maybe_density%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_density%density())
        end if
    end function

    function check_valid_json_to_diffusivity() result(result_)
        type(result_t) :: result_

        type(diffusivity_t) :: original
        type(json_object_t) :: json
        type(fallible_diffusivity_t) :: maybe_diffusivity
        type(error_list_t) :: errors

        original = 3.3.unit.square_meters_per_second
        json = json_object_unsafe([json_member_unsafe( &
                "diffusivity", json_string_unsafe(original%to_string()))])
        maybe_diffusivity = fallible_diffusivity_t(json, "diffusivity")
        if (maybe_diffusivity%failed()) then
            errors = maybe_diffusivity%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_diffusivity%diffusivity())
        end if
    end function

    function check_valid_json_to_dynamic_viscosity() result(result_)
        type(result_t) :: result_

        type(dynamic_viscosity_t) :: original
        type(json_object_t) :: json
        type(fallible_dynamic_viscosity_t) :: maybe_dynamic_viscosity
        type(error_list_t) :: errors

        original = 3.3.unit.pascal_seconds
        json = json_object_unsafe([json_member_unsafe( &
                "dynamic_viscosity", json_string_unsafe(original%to_string()))])
        maybe_dynamic_viscosity = fallible_dynamic_viscosity_t(json, "dynamic_viscosity")
        if (maybe_dynamic_viscosity%failed()) then
            errors = maybe_dynamic_viscosity%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_dynamic_viscosity%dynamic_viscosity())
        end if
    end function

    function check_valid_json_to_energy() result(result_)
        type(result_t) :: result_

        type(energy_t) :: original
        type(json_object_t) :: json
        type(fallible_energy_t) :: maybe_energy
        type(error_list_t) :: errors

        original = 3.3.unit.joules
        json = json_object_unsafe([json_member_unsafe( &
                "energy", json_string_unsafe(original%to_string()))])
        maybe_energy = fallible_energy_t(json, "energy")
        if (maybe_energy%failed()) then
            errors = maybe_energy%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_energy%energy())
        end if
    end function

    function check_valid_json_to_enthalpy() result(result_)
        type(result_t) :: result_

        type(enthalpy_t) :: original
        type(json_object_t) :: json
        type(fallible_enthalpy_t) :: maybe_enthalpy
        type(error_list_t) :: errors

        original = 3.3.unit.joules_per_kilogram
        json = json_object_unsafe([json_member_unsafe( &
                "enthalpy", json_string_unsafe(original%to_string()))])
        maybe_enthalpy = fallible_enthalpy_t(json, "enthalpy")
        if (maybe_enthalpy%failed()) then
            errors = maybe_enthalpy%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_enthalpy%enthalpy())
        end if
    end function

    function check_valid_json_to_fluence() result(result_)
        type(result_t) :: result_

        type(fluence_t) :: original
        type(json_object_t) :: json
        type(fallible_fluence_t) :: maybe_fluence
        type(error_list_t) :: errors

        original = 3.3.unit.particles_per_square_meter
        json = json_object_unsafe([json_member_unsafe( &
                "fluence", json_string_unsafe(original%to_string()))])
        maybe_fluence = fallible_fluence_t(json, "fluence")
        if (maybe_fluence%failed()) then
            errors = maybe_fluence%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_fluence%fluence())
        end if
    end function

    function check_valid_json_to_force() result(result_)
        type(result_t) :: result_

        type(force_t) :: original
        type(json_object_t) :: json
        type(fallible_force_t) :: maybe_force
        type(error_list_t) :: errors

        original = 3.3.unit.newtons
        json = json_object_unsafe([json_member_unsafe( &
                "force", json_string_unsafe(original%to_string()))])
        maybe_force = fallible_force_t(json, "force")
        if (maybe_force%failed()) then
            errors = maybe_force%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_force%force())
        end if
    end function

    function check_valid_json_to_fracture_toughness() result(result_)
        type(result_t) :: result_

        type(fracture_toughness_t) :: original
        type(json_object_t) :: json
        type(fallible_fracture_toughness_t) :: maybe_fracture_toughness
        type(error_list_t) :: errors

        original = 3.3.unit.pascal_root_meter
        json = json_object_unsafe([json_member_unsafe( &
                "fracture_toughness", json_string_unsafe(original%to_string()))])
        maybe_fracture_toughness = fallible_fracture_toughness_t(json, "fracture_toughness")
        if (maybe_fracture_toughness%failed()) then
            errors = maybe_fracture_toughness%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_fracture_toughness%fracture_toughness())
        end if
    end function

    function check_valid_json_to_frequency() result(result_)
        type(result_t) :: result_

        type(frequency_t) :: original
        type(json_object_t) :: json
        type(fallible_frequency_t) :: maybe_frequency
        type(error_list_t) :: errors

        original = 3.3.unit.hertz
        json = json_object_unsafe([json_member_unsafe( &
                "frequency", json_string_unsafe(original%to_string()))])
        maybe_frequency = fallible_frequency_t(json, "frequency")
        if (maybe_frequency%failed()) then
            errors = maybe_frequency%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_frequency%frequency())
        end if
    end function

    function check_valid_json_to_heat_capacity() result(result_)
        type(result_t) :: result_

        type(heat_capacity_t) :: original
        type(json_object_t) :: json
        type(fallible_heat_capacity_t) :: maybe_heat_capacity
        type(error_list_t) :: errors

        original = 3.3.unit.joules_per_kelvin
        json = json_object_unsafe([json_member_unsafe( &
                "heat_capacity", json_string_unsafe(original%to_string()))])
        maybe_heat_capacity = fallible_heat_capacity_t(json, "heat_capacity")
        if (maybe_heat_capacity%failed()) then
            errors = maybe_heat_capacity%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_heat_capacity%heat_capacity())
        end if
    end function

    function check_valid_json_to_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_

        type(heat_transfer_coefficient_t) :: original
        type(json_object_t) :: json
        type(fallible_heat_transfer_coefficient_t) :: maybe_heat_transfer_coefficient
        type(error_list_t) :: errors

        original = 3.3.unit.WATTS_PER_SQUARE_METER_KELVIN
        json = json_object_unsafe([json_member_unsafe( &
                "heat_transfer_coefficient", json_string_unsafe(original%to_string()))])
        maybe_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t(json, "heat_transfer_coefficient")
        if (maybe_heat_transfer_coefficient%failed()) then
            errors = maybe_heat_transfer_coefficient%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_heat_transfer_coefficient%heat_transfer_coefficient())
        end if
    end function

    function check_valid_json_to_insulance() result(result_)
        type(result_t) :: result_

        type(insulance_t) :: original
        type(json_object_t) :: json
        type(fallible_insulance_t) :: maybe_insulance
        type(error_list_t) :: errors

        original = 3.3.unit.kelvin_square_meters_per_watt
        json = json_object_unsafe([json_member_unsafe( &
                "insulance", json_string_unsafe(original%to_string()))])
        maybe_insulance = fallible_insulance_t(json, "insulance")
        if (maybe_insulance%failed()) then
            errors = maybe_insulance%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_insulance%insulance())
        end if
    end function

    function check_valid_json_to_intensity() result(result_)
        type(result_t) :: result_

        type(intensity_t) :: original
        type(json_object_t) :: json
        type(fallible_intensity_t) :: maybe_intensity
        type(error_list_t) :: errors

        original = 3.3.unit.watts_per_square_meter
        json = json_object_unsafe([json_member_unsafe( &
                "intensity", json_string_unsafe(original%to_string()))])
        maybe_intensity = fallible_intensity_t(json, "intensity")
        if (maybe_intensity%failed()) then
            errors = maybe_intensity%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_intensity%intensity())
        end if
    end function

    function check_valid_json_to_inverse_molar_mass() result(result_)
        type(result_t) :: result_

        type(inverse_molar_mass_t) :: original
        type(json_object_t) :: json
        type(fallible_inverse_molar_mass_t) :: maybe_inverse_molar_mass
        type(error_list_t) :: errors

        original = 3.3.unit.mols_per_kilogram
        json = json_object_unsafe([json_member_unsafe( &
                "inverse_molar_mass", json_string_unsafe(original%to_string()))])
        maybe_inverse_molar_mass = fallible_inverse_molar_mass_t(json, "inverse_molar_mass")
        if (maybe_inverse_molar_mass%failed()) then
            errors = maybe_inverse_molar_mass%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_inverse_molar_mass%inverse_molar_mass())
        end if
    end function

    function check_valid_json_to_length() result(result_)
        type(result_t) :: result_

        type(length_t) :: original
        type(json_object_t) :: json
        type(fallible_length_t) :: maybe_length
        type(error_list_t) :: errors

        original = 3.3.unit.meters
        json = json_object_unsafe([json_member_unsafe( &
                "length", json_string_unsafe(original%to_string()))])
        maybe_length = fallible_length_t(json, "length")
        if (maybe_length%failed()) then
            errors = maybe_length%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_length%length())
        end if
    end function

    function check_valid_json_to_mass() result(result_)
        type(result_t) :: result_

        type(mass_t) :: original
        type(json_object_t) :: json
        type(fallible_mass_t) :: maybe_mass
        type(error_list_t) :: errors

        original = 3.3.unit.kilograms
        json = json_object_unsafe([json_member_unsafe( &
                "mass", json_string_unsafe(original%to_string()))])
        maybe_mass = fallible_mass_t(json, "mass")
        if (maybe_mass%failed()) then
            errors = maybe_mass%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_mass%mass())
        end if
    end function

    function check_valid_json_to_mass_flux() result(result_)
        type(result_t) :: result_

        type(mass_flux_t) :: original
        type(json_object_t) :: json
        type(fallible_mass_flux_t) :: maybe_mass_flux
        type(error_list_t) :: errors

        original = 3.3.unit.kilograms_per_square_meter_seconds
        json = json_object_unsafe([json_member_unsafe( &
                "mass_flux", json_string_unsafe(original%to_string()))])
        maybe_mass_flux = fallible_mass_flux_t(json, "mass_flux")
        if (maybe_mass_flux%failed()) then
            errors = maybe_mass_flux%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_mass_flux%mass_flux())
        end if
    end function

    function check_valid_json_to_mass_rate() result(result_)
        type(result_t) :: result_

        type(mass_rate_t) :: original
        type(json_object_t) :: json
        type(fallible_mass_rate_t) :: maybe_mass_rate
        type(error_list_t) :: errors

        original = 3.3.unit.kilograms_per_second
        json = json_object_unsafe([json_member_unsafe( &
                "mass_rate", json_string_unsafe(original%to_string()))])
        maybe_mass_rate = fallible_mass_rate_t(json, "mass_rate")
        if (maybe_mass_rate%failed()) then
            errors = maybe_mass_rate%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_mass_rate%mass_rate())
        end if
    end function

    function check_valid_json_to_molar_density() result(result_)
        type(result_t) :: result_

        type(molar_density_t) :: original
        type(json_object_t) :: json
        type(fallible_molar_density_t) :: maybe_molar_density
        type(error_list_t) :: errors

        original = 3.3.unit.mols_per_cubic_meter
        json = json_object_unsafe([json_member_unsafe( &
                "molar_density", json_string_unsafe(original%to_string()))])
        maybe_molar_density = fallible_molar_density_t(json, "molar_density")
        if (maybe_molar_density%failed()) then
            errors = maybe_molar_density%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_molar_density%molar_density())
        end if
    end function

    function check_valid_json_to_molar_enthalpy() result(result_)
        type(result_t) :: result_

        type(molar_enthalpy_t) :: original
        type(json_object_t) :: json
        type(fallible_molar_enthalpy_t) :: maybe_molar_enthalpy
        type(error_list_t) :: errors

        original = 3.3.unit.joules_per_mol
        json = json_object_unsafe([json_member_unsafe( &
                "molar_enthalpy", json_string_unsafe(original%to_string()))])
        maybe_molar_enthalpy = fallible_molar_enthalpy_t(json, "molar_enthalpy")
        if (maybe_molar_enthalpy%failed()) then
            errors = maybe_molar_enthalpy%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_molar_enthalpy%molar_enthalpy())
        end if
    end function

    function check_valid_json_to_molar_mass() result(result_)
        type(result_t) :: result_

        type(molar_mass_t) :: original
        type(json_object_t) :: json
        type(fallible_molar_mass_t) :: maybe_molar_mass
        type(error_list_t) :: errors

        original = 3.3.unit.kilograms_per_mol
        json = json_object_unsafe([json_member_unsafe( &
                "molar_mass", json_string_unsafe(original%to_string()))])
        maybe_molar_mass = fallible_molar_mass_t(json, "molar_mass")
        if (maybe_molar_mass%failed()) then
            errors = maybe_molar_mass%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_molar_mass%molar_mass())
        end if
    end function

    function check_valid_json_to_molar_specific_heat() result(result_)
        type(result_t) :: result_

        type(molar_specific_heat_t) :: original
        type(json_object_t) :: json
        type(fallible_molar_specific_heat_t) :: maybe_molar_specific_heat
        type(error_list_t) :: errors

        original = 3.3.unit.joules_per_kelvin_mol
        json = json_object_unsafe([json_member_unsafe( &
                "molar_specific_heat", json_string_unsafe(original%to_string()))])
        maybe_molar_specific_heat = fallible_molar_specific_heat_t(json, "molar_specific_heat")
        if (maybe_molar_specific_heat%failed()) then
            errors = maybe_molar_specific_heat%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_molar_specific_heat%molar_specific_heat())
        end if
    end function

    function check_valid_json_to_power() result(result_)
        type(result_t) :: result_

        type(power_t) :: original
        type(json_object_t) :: json
        type(fallible_power_t) :: maybe_power
        type(error_list_t) :: errors

        original = 3.3.unit.watts
        json = json_object_unsafe([json_member_unsafe( &
                "power", json_string_unsafe(original%to_string()))])
        maybe_power = fallible_power_t(json, "power")
        if (maybe_power%failed()) then
            errors = maybe_power%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_power%power())
        end if
    end function

    function check_valid_json_to_pressure() result(result_)
        type(result_t) :: result_

        type(pressure_t) :: original
        type(json_object_t) :: json
        type(fallible_pressure_t) :: maybe_pressure
        type(error_list_t) :: errors

        original = 3.3.unit.pascals
        json = json_object_unsafe([json_member_unsafe( &
                "pressure", json_string_unsafe(original%to_string()))])
        maybe_pressure = fallible_pressure_t(json, "pressure")
        if (maybe_pressure%failed()) then
            errors = maybe_pressure%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_pressure%pressure())
        end if
    end function

    function check_valid_json_to_specific_heat() result(result_)
        type(result_t) :: result_

        type(specific_heat_t) :: original
        type(json_object_t) :: json
        type(fallible_specific_heat_t) :: maybe_specific_heat
        type(error_list_t) :: errors

        original = 3.3.unit.joules_per_kilogram_kelvin
        json = json_object_unsafe([json_member_unsafe( &
                "specific_heat", json_string_unsafe(original%to_string()))])
        maybe_specific_heat = fallible_specific_heat_t(json, "specific_heat")
        if (maybe_specific_heat%failed()) then
            errors = maybe_specific_heat%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_specific_heat%specific_heat())
        end if
    end function

    function check_valid_json_to_speed() result(result_)
        type(result_t) :: result_

        type(speed_t) :: original
        type(json_object_t) :: json
        type(fallible_speed_t) :: maybe_speed
        type(error_list_t) :: errors

        original = 3.3.unit.meters_per_second
        json = json_object_unsafe([json_member_unsafe( &
                "speed", json_string_unsafe(original%to_string()))])
        maybe_speed = fallible_speed_t(json, "speed")
        if (maybe_speed%failed()) then
            errors = maybe_speed%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_speed%speed())
        end if
    end function

    function check_valid_json_to_temperature_gradient() result(result_)
        type(result_t) :: result_

        type(temperature_gradient_t) :: original
        type(json_object_t) :: json
        type(fallible_temperature_gradient_t) :: maybe_temperature_gradient
        type(error_list_t) :: errors

        original = 3.3.unit.kelvin_per_meter
        json = json_object_unsafe([json_member_unsafe( &
                "temperature_gradient", json_string_unsafe(original%to_string()))])
        maybe_temperature_gradient = fallible_temperature_gradient_t(json, "temperature_gradient")
        if (maybe_temperature_gradient%failed()) then
            errors = maybe_temperature_gradient%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_temperature_gradient%temperature_gradient())
        end if
    end function

    function check_valid_json_to_temperature() result(result_)
        type(result_t) :: result_

        type(temperature_t) :: original
        type(json_object_t) :: json
        type(fallible_temperature_t) :: maybe_temperature
        type(error_list_t) :: errors

        original = 3.3.unit.kelvin
        json = json_object_unsafe([json_member_unsafe( &
                "temperature", json_string_unsafe(original%to_string()))])
        maybe_temperature = fallible_temperature_t(json, "temperature")
        if (maybe_temperature%failed()) then
            errors = maybe_temperature%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_temperature%temperature())
        end if
    end function

    function check_valid_json_to_thermal_conductivity() result(result_)
        type(result_t) :: result_

        type(thermal_conductivity_t) :: original
        type(json_object_t) :: json
        type(fallible_thermal_conductivity_t) :: maybe_thermal_conductivity
        type(error_list_t) :: errors

        original = 3.3.unit.watts_per_meter_kelvin
        json = json_object_unsafe([json_member_unsafe( &
                "thermal_conductivity", json_string_unsafe(original%to_string()))])
        maybe_thermal_conductivity = fallible_thermal_conductivity_t(json, "thermal_conductivity")
        if (maybe_thermal_conductivity%failed()) then
            errors = maybe_thermal_conductivity%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_thermal_conductivity%thermal_conductivity())
        end if
    end function

    function check_valid_json_to_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_

        type(thermal_expansion_coefficient_t) :: original
        type(json_object_t) :: json
        type(fallible_thermal_expansion_coefficient_t) :: maybe_thermal_expansion_coefficient
        type(error_list_t) :: errors

        original = 3.3.unit.per_kelvin
        json = json_object_unsafe([json_member_unsafe( &
                "thermal_expansion_coefficient", json_string_unsafe(original%to_string()))])
        maybe_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t(json, "thermal_expansion_coefficient")
        if (maybe_thermal_expansion_coefficient%failed()) then
            errors = maybe_thermal_expansion_coefficient%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_thermal_expansion_coefficient%thermal_expansion_coefficient())
        end if
    end function

    function check_valid_json_to_time() result(result_)
        type(result_t) :: result_

        type(time_t) :: original
        type(json_object_t) :: json
        type(fallible_time_t) :: maybe_time
        type(error_list_t) :: errors

        original = 3.3.unit.seconds
        json = json_object_unsafe([json_member_unsafe( &
                "time", json_string_unsafe(original%to_string()))])
        maybe_time = fallible_time_t(json, "time")
        if (maybe_time%failed()) then
            errors = maybe_time%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_time%time())
        end if
    end function

    function check_valid_json_to_volume() result(result_)
        type(result_t) :: result_

        type(volume_t) :: original
        type(json_object_t) :: json
        type(fallible_volume_t) :: maybe_volume
        type(error_list_t) :: errors

        original = 3.3.unit.cubic_meters
        json = json_object_unsafe([json_member_unsafe( &
                "volume", json_string_unsafe(original%to_string()))])
        maybe_volume = fallible_volume_t(json, "volume")
        if (maybe_volume%failed()) then
            errors = maybe_volume%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_volume%volume())
        end if
    end function

    function check_valid_json_to_volume_rate() result(result_)
        type(result_t) :: result_

        type(volume_rate_t) :: original
        type(json_object_t) :: json
        type(fallible_volume_rate_t) :: maybe_volume_rate
        type(error_list_t) :: errors

        original = 3.3.unit.cubic_meters_per_second
        json = json_object_unsafe([json_member_unsafe( &
                "volume_rate", json_string_unsafe(original%to_string()))])
        maybe_volume_rate = fallible_volume_rate_t(json, "volume_rate")
        if (maybe_volume_rate%failed()) then
            errors = maybe_volume_rate%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_volume_rate%volume_rate())
        end if
    end function

    function check_valid_json_to_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: original
        type(json_object_t) :: json
        type(fallible_volumetric_heat_capacity_t) :: maybe_volumetric_heat_capacity
        type(error_list_t) :: errors

        original = 3.3.unit.pascals_per_kelvin
        json = json_object_unsafe([json_member_unsafe( &
                "volumetric_heat_capacity", json_string_unsafe(original%to_string()))])
        maybe_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t(json, "volumetric_heat_capacity")
        if (maybe_volumetric_heat_capacity%failed()) then
            errors = maybe_volumetric_heat_capacity%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_volumetric_heat_capacity%volumetric_heat_capacity())
        end if
    end function

    function check_valid_json_to_yank() result(result_)
        type(result_t) :: result_

        type(yank_t) :: original
        type(json_object_t) :: json
        type(fallible_yank_t) :: maybe_yank
        type(error_list_t) :: errors

        original = 3.3.unit.newtons_per_second
        json = json_object_unsafe([json_member_unsafe( &
                "yank", json_string_unsafe(original%to_string()))])
        maybe_yank = fallible_yank_t(json, "yank")
        if (maybe_yank%failed()) then
            errors = maybe_yank%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_yank%yank())
        end if
    end function

    function check_missing_key_acceleration() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_acceleration_t) :: maybe_acceleration
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_acceleration = fallible_acceleration_t(json, "acceleration")
        errors = maybe_acceleration%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."acceleration", &
                errors%to_string())
    end function

    function check_missing_key_amount() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_t) :: maybe_amount
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_amount = fallible_amount_t(json, "amount")
        errors = maybe_amount%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."amount", &
                errors%to_string())
    end function

    function check_missing_key_amount_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_rate_t) :: maybe_amount_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_amount_rate = fallible_amount_rate_t(json, "amount_rate")
        errors = maybe_amount_rate%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."amount_rate", &
                errors%to_string())
    end function

    function check_missing_key_amount_temperature() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_t) :: maybe_amount_temperature
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_amount_temperature = fallible_amount_temperature_t(json, "amount_temperature")
        errors = maybe_amount_temperature%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."amount_temperature", &
                errors%to_string())
    end function

    function check_missing_key_amount_temperature_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_rate_t) :: maybe_amount_temperature_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_amount_temperature_rate = fallible_amount_temperature_rate_t(json, "amount_temperature_rate")
        errors = maybe_amount_temperature_rate%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."amount_temperature_rate", &
                errors%to_string())
    end function

    function check_missing_key_angle() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_angle_t) :: maybe_angle
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_angle = fallible_angle_t(json, "angle")
        errors = maybe_angle%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."angle", &
                errors%to_string())
    end function

    function check_missing_key_area() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_area_t) :: maybe_area
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_area = fallible_area_t(json, "area")
        errors = maybe_area%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."area", &
                errors%to_string())
    end function

    function check_missing_key_conductance() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_conductance_t) :: maybe_conductance
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_conductance = fallible_conductance_t(json, "conductance")
        errors = maybe_conductance%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."conductance", &
                errors%to_string())
    end function

    function check_missing_key_delta_temperature() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_delta_temperature_t) :: maybe_delta_temperature
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_delta_temperature = fallible_delta_temperature_t(json, "delta_temperature")
        errors = maybe_delta_temperature%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."delta_temperature", &
                errors%to_string())
    end function

    function check_missing_key_density() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_density_t) :: maybe_density
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_density = fallible_density_t(json, "density")
        errors = maybe_density%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."density", &
                errors%to_string())
    end function

    function check_missing_key_diffusivity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_diffusivity_t) :: maybe_diffusivity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_diffusivity = fallible_diffusivity_t(json, "diffusivity")
        errors = maybe_diffusivity%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."diffusivity", &
                errors%to_string())
    end function

    function check_missing_key_dynamic_viscosity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_dynamic_viscosity_t) :: maybe_dynamic_viscosity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_dynamic_viscosity = fallible_dynamic_viscosity_t(json, "dynamic_viscosity")
        errors = maybe_dynamic_viscosity%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."dynamic_viscosity", &
                errors%to_string())
    end function

    function check_missing_key_energy() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_energy_t) :: maybe_energy
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_energy = fallible_energy_t(json, "energy")
        errors = maybe_energy%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."energy", &
                errors%to_string())
    end function

    function check_missing_key_enthalpy() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_enthalpy_t) :: maybe_enthalpy
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_enthalpy = fallible_enthalpy_t(json, "enthalpy")
        errors = maybe_enthalpy%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."enthalpy", &
                errors%to_string())
    end function

    function check_missing_key_fluence() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fluence_t) :: maybe_fluence
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_fluence = fallible_fluence_t(json, "fluence")
        errors = maybe_fluence%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."fluence", &
                errors%to_string())
    end function

    function check_missing_key_force() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_force_t) :: maybe_force
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_force = fallible_force_t(json, "force")
        errors = maybe_force%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."force", &
                errors%to_string())
    end function

    function check_missing_key_fracture_toughness() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fracture_toughness_t) :: maybe_fracture_toughness
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_fracture_toughness = fallible_fracture_toughness_t(json, "fracture_toughness")
        errors = maybe_fracture_toughness%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."fracture_toughness", &
                errors%to_string())
    end function

    function check_missing_key_frequency() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_frequency_t) :: maybe_frequency
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_frequency = fallible_frequency_t(json, "frequency")
        errors = maybe_frequency%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."frequency", &
                errors%to_string())
    end function

    function check_missing_key_heat_capacity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_capacity_t) :: maybe_heat_capacity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_heat_capacity = fallible_heat_capacity_t(json, "heat_capacity")
        errors = maybe_heat_capacity%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."heat_capacity", &
                errors%to_string())
    end function

    function check_missing_key_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_transfer_coefficient_t) :: maybe_heat_transfer_coefficient
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t(json, "heat_transfer_coefficient")
        errors = maybe_heat_transfer_coefficient%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."heat_transfer_coefficient", &
                errors%to_string())
    end function

    function check_missing_key_insulance() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_insulance_t) :: maybe_insulance
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_insulance = fallible_insulance_t(json, "insulance")
        errors = maybe_insulance%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."insulance", &
                errors%to_string())
    end function

    function check_missing_key_intensity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_intensity_t) :: maybe_intensity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_intensity = fallible_intensity_t(json, "intensity")
        errors = maybe_intensity%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."intensity", &
                errors%to_string())
    end function

    function check_missing_key_inverse_molar_mass() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_inverse_molar_mass_t) :: maybe_inverse_molar_mass
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_inverse_molar_mass = fallible_inverse_molar_mass_t(json, "inverse_molar_mass")
        errors = maybe_inverse_molar_mass%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."inverse_molar_mass", &
                errors%to_string())
    end function

    function check_missing_key_length() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_length_t) :: maybe_length
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_length = fallible_length_t(json, "length")
        errors = maybe_length%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."length", &
                errors%to_string())
    end function

    function check_missing_key_mass() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_t) :: maybe_mass
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_mass = fallible_mass_t(json, "mass")
        errors = maybe_mass%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."mass", &
                errors%to_string())
    end function

    function check_missing_key_mass_flux() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_flux_t) :: maybe_mass_flux
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_mass_flux = fallible_mass_flux_t(json, "mass_flux")
        errors = maybe_mass_flux%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."mass_flux", &
                errors%to_string())
    end function

    function check_missing_key_mass_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_rate_t) :: maybe_mass_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_mass_rate = fallible_mass_rate_t(json, "mass_rate")
        errors = maybe_mass_rate%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."mass_rate", &
                errors%to_string())
    end function

    function check_missing_key_molar_density() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_density_t) :: maybe_molar_density
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_molar_density = fallible_molar_density_t(json, "molar_density")
        errors = maybe_molar_density%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."molar_density", &
                errors%to_string())
    end function

    function check_missing_key_molar_enthalpy() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_enthalpy_t) :: maybe_molar_enthalpy
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_molar_enthalpy = fallible_molar_enthalpy_t(json, "molar_enthalpy")
        errors = maybe_molar_enthalpy%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."molar_enthalpy", &
                errors%to_string())
    end function

    function check_missing_key_molar_mass() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_mass_t) :: maybe_molar_mass
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_molar_mass = fallible_molar_mass_t(json, "molar_mass")
        errors = maybe_molar_mass%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."molar_mass", &
                errors%to_string())
    end function

    function check_missing_key_molar_specific_heat() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_specific_heat_t) :: maybe_molar_specific_heat
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_molar_specific_heat = fallible_molar_specific_heat_t(json, "molar_specific_heat")
        errors = maybe_molar_specific_heat%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."molar_specific_heat", &
                errors%to_string())
    end function

    function check_missing_key_power() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_power_t) :: maybe_power
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_power = fallible_power_t(json, "power")
        errors = maybe_power%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."power", &
                errors%to_string())
    end function

    function check_missing_key_pressure() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_pressure_t) :: maybe_pressure
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_pressure = fallible_pressure_t(json, "pressure")
        errors = maybe_pressure%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."pressure", &
                errors%to_string())
    end function

    function check_missing_key_specific_heat() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_specific_heat_t) :: maybe_specific_heat
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_specific_heat = fallible_specific_heat_t(json, "specific_heat")
        errors = maybe_specific_heat%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."specific_heat", &
                errors%to_string())
    end function

    function check_missing_key_speed() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_speed_t) :: maybe_speed
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_speed = fallible_speed_t(json, "speed")
        errors = maybe_speed%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."speed", &
                errors%to_string())
    end function

    function check_missing_key_temperature_gradient() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_gradient_t) :: maybe_temperature_gradient
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_temperature_gradient = fallible_temperature_gradient_t(json, "temperature_gradient")
        errors = maybe_temperature_gradient%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."temperature_gradient", &
                errors%to_string())
    end function

    function check_missing_key_temperature() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_t) :: maybe_temperature
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_temperature = fallible_temperature_t(json, "temperature")
        errors = maybe_temperature%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."temperature", &
                errors%to_string())
    end function

    function check_missing_key_thermal_conductivity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_conductivity_t) :: maybe_thermal_conductivity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_thermal_conductivity = fallible_thermal_conductivity_t(json, "thermal_conductivity")
        errors = maybe_thermal_conductivity%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."thermal_conductivity", &
                errors%to_string())
    end function

    function check_missing_key_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_expansion_coefficient_t) :: maybe_thermal_expansion_coefficient
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t(json, "thermal_expansion_coefficient")
        errors = maybe_thermal_expansion_coefficient%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."thermal_expansion_coefficient", &
                errors%to_string())
    end function

    function check_missing_key_time() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_time_t) :: maybe_time
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_time = fallible_time_t(json, "time")
        errors = maybe_time%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."time", &
                errors%to_string())
    end function

    function check_missing_key_volume() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_t) :: maybe_volume
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_volume = fallible_volume_t(json, "volume")
        errors = maybe_volume%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."volume", &
                errors%to_string())
    end function

    function check_missing_key_volume_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_rate_t) :: maybe_volume_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_volume_rate = fallible_volume_rate_t(json, "volume_rate")
        errors = maybe_volume_rate%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."volume_rate", &
                errors%to_string())
    end function

    function check_missing_key_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volumetric_heat_capacity_t) :: maybe_volumetric_heat_capacity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t(json, "volumetric_heat_capacity")
        errors = maybe_volumetric_heat_capacity%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."volumetric_heat_capacity", &
                errors%to_string())
    end function

    function check_missing_key_yank() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_yank_t) :: maybe_yank
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_yank = fallible_yank_t(json, "yank")
        errors = maybe_yank%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."yank", &
                errors%to_string())
    end function

    function check_not_string_acceleration() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_acceleration_t) :: maybe_acceleration
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "acceleration", json_null_t())])
        maybe_acceleration = fallible_acceleration_t(json, "acceleration")
        errors = maybe_acceleration%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."acceleration", &
                errors%to_string())
    end function

    function check_not_string_amount() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_t) :: maybe_amount
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount", json_null_t())])
        maybe_amount = fallible_amount_t(json, "amount")
        errors = maybe_amount%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."amount", &
                errors%to_string())
    end function

    function check_not_string_amount_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_rate_t) :: maybe_amount_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_rate", json_null_t())])
        maybe_amount_rate = fallible_amount_rate_t(json, "amount_rate")
        errors = maybe_amount_rate%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."amount_rate", &
                errors%to_string())
    end function

    function check_not_string_amount_temperature() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_t) :: maybe_amount_temperature
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature", json_null_t())])
        maybe_amount_temperature = fallible_amount_temperature_t(json, "amount_temperature")
        errors = maybe_amount_temperature%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."amount_temperature", &
                errors%to_string())
    end function

    function check_not_string_amount_temperature_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_rate_t) :: maybe_amount_temperature_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature_rate", json_null_t())])
        maybe_amount_temperature_rate = fallible_amount_temperature_rate_t(json, "amount_temperature_rate")
        errors = maybe_amount_temperature_rate%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."amount_temperature_rate", &
                errors%to_string())
    end function

    function check_not_string_angle() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_angle_t) :: maybe_angle
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "angle", json_null_t())])
        maybe_angle = fallible_angle_t(json, "angle")
        errors = maybe_angle%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."angle", &
                errors%to_string())
    end function

    function check_not_string_area() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_area_t) :: maybe_area
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "area", json_null_t())])
        maybe_area = fallible_area_t(json, "area")
        errors = maybe_area%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."area", &
                errors%to_string())
    end function

    function check_not_string_conductance() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_conductance_t) :: maybe_conductance
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "conductance", json_null_t())])
        maybe_conductance = fallible_conductance_t(json, "conductance")
        errors = maybe_conductance%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."conductance", &
                errors%to_string())
    end function

    function check_not_string_delta_temperature() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_delta_temperature_t) :: maybe_delta_temperature
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "delta_temperature", json_null_t())])
        maybe_delta_temperature = fallible_delta_temperature_t(json, "delta_temperature")
        errors = maybe_delta_temperature%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."delta_temperature", &
                errors%to_string())
    end function

    function check_not_string_density() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_density_t) :: maybe_density
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "density", json_null_t())])
        maybe_density = fallible_density_t(json, "density")
        errors = maybe_density%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."density", &
                errors%to_string())
    end function

    function check_not_string_diffusivity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_diffusivity_t) :: maybe_diffusivity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "diffusivity", json_null_t())])
        maybe_diffusivity = fallible_diffusivity_t(json, "diffusivity")
        errors = maybe_diffusivity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."diffusivity", &
                errors%to_string())
    end function

    function check_not_string_dynamic_viscosity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_dynamic_viscosity_t) :: maybe_dynamic_viscosity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "dynamic_viscosity", json_null_t())])
        maybe_dynamic_viscosity = fallible_dynamic_viscosity_t(json, "dynamic_viscosity")
        errors = maybe_dynamic_viscosity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."dynamic_viscosity", &
                errors%to_string())
    end function

    function check_not_string_energy() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_energy_t) :: maybe_energy
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "energy", json_null_t())])
        maybe_energy = fallible_energy_t(json, "energy")
        errors = maybe_energy%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."energy", &
                errors%to_string())
    end function

    function check_not_string_enthalpy() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_enthalpy_t) :: maybe_enthalpy
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "enthalpy", json_null_t())])
        maybe_enthalpy = fallible_enthalpy_t(json, "enthalpy")
        errors = maybe_enthalpy%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."enthalpy", &
                errors%to_string())
    end function

    function check_not_string_fluence() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fluence_t) :: maybe_fluence
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fluence", json_null_t())])
        maybe_fluence = fallible_fluence_t(json, "fluence")
        errors = maybe_fluence%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."fluence", &
                errors%to_string())
    end function

    function check_not_string_force() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_force_t) :: maybe_force
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "force", json_null_t())])
        maybe_force = fallible_force_t(json, "force")
        errors = maybe_force%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."force", &
                errors%to_string())
    end function

    function check_not_string_fracture_toughness() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fracture_toughness_t) :: maybe_fracture_toughness
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fracture_toughness", json_null_t())])
        maybe_fracture_toughness = fallible_fracture_toughness_t(json, "fracture_toughness")
        errors = maybe_fracture_toughness%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."fracture_toughness", &
                errors%to_string())
    end function

    function check_not_string_frequency() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_frequency_t) :: maybe_frequency
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "frequency", json_null_t())])
        maybe_frequency = fallible_frequency_t(json, "frequency")
        errors = maybe_frequency%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."frequency", &
                errors%to_string())
    end function

    function check_not_string_heat_capacity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_capacity_t) :: maybe_heat_capacity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_capacity", json_null_t())])
        maybe_heat_capacity = fallible_heat_capacity_t(json, "heat_capacity")
        errors = maybe_heat_capacity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."heat_capacity", &
                errors%to_string())
    end function

    function check_not_string_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_transfer_coefficient_t) :: maybe_heat_transfer_coefficient
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_transfer_coefficient", json_null_t())])
        maybe_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t(json, "heat_transfer_coefficient")
        errors = maybe_heat_transfer_coefficient%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."heat_transfer_coefficient", &
                errors%to_string())
    end function

    function check_not_string_insulance() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_insulance_t) :: maybe_insulance
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "insulance", json_null_t())])
        maybe_insulance = fallible_insulance_t(json, "insulance")
        errors = maybe_insulance%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."insulance", &
                errors%to_string())
    end function

    function check_not_string_intensity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_intensity_t) :: maybe_intensity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "intensity", json_null_t())])
        maybe_intensity = fallible_intensity_t(json, "intensity")
        errors = maybe_intensity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."intensity", &
                errors%to_string())
    end function

    function check_not_string_inverse_molar_mass() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_inverse_molar_mass_t) :: maybe_inverse_molar_mass
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "inverse_molar_mass", json_null_t())])
        maybe_inverse_molar_mass = fallible_inverse_molar_mass_t(json, "inverse_molar_mass")
        errors = maybe_inverse_molar_mass%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."inverse_molar_mass", &
                errors%to_string())
    end function

    function check_not_string_length() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_length_t) :: maybe_length
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "length", json_null_t())])
        maybe_length = fallible_length_t(json, "length")
        errors = maybe_length%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."length", &
                errors%to_string())
    end function

    function check_not_string_mass() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_t) :: maybe_mass
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass", json_null_t())])
        maybe_mass = fallible_mass_t(json, "mass")
        errors = maybe_mass%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."mass", &
                errors%to_string())
    end function

    function check_not_string_mass_flux() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_flux_t) :: maybe_mass_flux
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_flux", json_null_t())])
        maybe_mass_flux = fallible_mass_flux_t(json, "mass_flux")
        errors = maybe_mass_flux%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."mass_flux", &
                errors%to_string())
    end function

    function check_not_string_mass_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_rate_t) :: maybe_mass_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_rate", json_null_t())])
        maybe_mass_rate = fallible_mass_rate_t(json, "mass_rate")
        errors = maybe_mass_rate%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."mass_rate", &
                errors%to_string())
    end function

    function check_not_string_molar_density() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_density_t) :: maybe_molar_density
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_density", json_null_t())])
        maybe_molar_density = fallible_molar_density_t(json, "molar_density")
        errors = maybe_molar_density%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."molar_density", &
                errors%to_string())
    end function

    function check_not_string_molar_enthalpy() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_enthalpy_t) :: maybe_molar_enthalpy
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_enthalpy", json_null_t())])
        maybe_molar_enthalpy = fallible_molar_enthalpy_t(json, "molar_enthalpy")
        errors = maybe_molar_enthalpy%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."molar_enthalpy", &
                errors%to_string())
    end function

    function check_not_string_molar_mass() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_mass_t) :: maybe_molar_mass
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_mass", json_null_t())])
        maybe_molar_mass = fallible_molar_mass_t(json, "molar_mass")
        errors = maybe_molar_mass%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."molar_mass", &
                errors%to_string())
    end function

    function check_not_string_molar_specific_heat() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_specific_heat_t) :: maybe_molar_specific_heat
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_specific_heat", json_null_t())])
        maybe_molar_specific_heat = fallible_molar_specific_heat_t(json, "molar_specific_heat")
        errors = maybe_molar_specific_heat%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."molar_specific_heat", &
                errors%to_string())
    end function

    function check_not_string_power() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_power_t) :: maybe_power
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "power", json_null_t())])
        maybe_power = fallible_power_t(json, "power")
        errors = maybe_power%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."power", &
                errors%to_string())
    end function

    function check_not_string_pressure() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_pressure_t) :: maybe_pressure
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "pressure", json_null_t())])
        maybe_pressure = fallible_pressure_t(json, "pressure")
        errors = maybe_pressure%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."pressure", &
                errors%to_string())
    end function

    function check_not_string_specific_heat() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_specific_heat_t) :: maybe_specific_heat
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "specific_heat", json_null_t())])
        maybe_specific_heat = fallible_specific_heat_t(json, "specific_heat")
        errors = maybe_specific_heat%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."specific_heat", &
                errors%to_string())
    end function

    function check_not_string_speed() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_speed_t) :: maybe_speed
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "speed", json_null_t())])
        maybe_speed = fallible_speed_t(json, "speed")
        errors = maybe_speed%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."speed", &
                errors%to_string())
    end function

    function check_not_string_temperature_gradient() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_gradient_t) :: maybe_temperature_gradient
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature_gradient", json_null_t())])
        maybe_temperature_gradient = fallible_temperature_gradient_t(json, "temperature_gradient")
        errors = maybe_temperature_gradient%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."temperature_gradient", &
                errors%to_string())
    end function

    function check_not_string_temperature() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_t) :: maybe_temperature
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature", json_null_t())])
        maybe_temperature = fallible_temperature_t(json, "temperature")
        errors = maybe_temperature%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."temperature", &
                errors%to_string())
    end function

    function check_not_string_thermal_conductivity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_conductivity_t) :: maybe_thermal_conductivity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_conductivity", json_null_t())])
        maybe_thermal_conductivity = fallible_thermal_conductivity_t(json, "thermal_conductivity")
        errors = maybe_thermal_conductivity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."thermal_conductivity", &
                errors%to_string())
    end function

    function check_not_string_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_expansion_coefficient_t) :: maybe_thermal_expansion_coefficient
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_expansion_coefficient", json_null_t())])
        maybe_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t(json, "thermal_expansion_coefficient")
        errors = maybe_thermal_expansion_coefficient%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."thermal_expansion_coefficient", &
                errors%to_string())
    end function

    function check_not_string_time() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_time_t) :: maybe_time
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "time", json_null_t())])
        maybe_time = fallible_time_t(json, "time")
        errors = maybe_time%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."time", &
                errors%to_string())
    end function

    function check_not_string_volume() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_t) :: maybe_volume
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume", json_null_t())])
        maybe_volume = fallible_volume_t(json, "volume")
        errors = maybe_volume%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."volume", &
                errors%to_string())
    end function

    function check_not_string_volume_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_rate_t) :: maybe_volume_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume_rate", json_null_t())])
        maybe_volume_rate = fallible_volume_rate_t(json, "volume_rate")
        errors = maybe_volume_rate%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."volume_rate", &
                errors%to_string())
    end function

    function check_not_string_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volumetric_heat_capacity_t) :: maybe_volumetric_heat_capacity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volumetric_heat_capacity", json_null_t())])
        maybe_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t(json, "volumetric_heat_capacity")
        errors = maybe_volumetric_heat_capacity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."volumetric_heat_capacity", &
                errors%to_string())
    end function

    function check_not_string_yank() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_yank_t) :: maybe_yank
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "yank", json_null_t())])
        maybe_yank = fallible_yank_t(json, "yank")
        errors = maybe_yank%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."yank", &
                errors%to_string())
    end function

    function check_parse_error_acceleration() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_acceleration_t) :: maybe_acceleration
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "acceleration", json_string_unsafe("invalid"))])
        maybe_acceleration = fallible_acceleration_t(json, "acceleration")
        errors = maybe_acceleration%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_amount() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_t) :: maybe_amount
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount", json_string_unsafe("invalid"))])
        maybe_amount = fallible_amount_t(json, "amount")
        errors = maybe_amount%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_amount_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_rate_t) :: maybe_amount_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_rate", json_string_unsafe("invalid"))])
        maybe_amount_rate = fallible_amount_rate_t(json, "amount_rate")
        errors = maybe_amount_rate%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_amount_temperature() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_t) :: maybe_amount_temperature
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature", json_string_unsafe("invalid"))])
        maybe_amount_temperature = fallible_amount_temperature_t(json, "amount_temperature")
        errors = maybe_amount_temperature%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_amount_temperature_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_rate_t) :: maybe_amount_temperature_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature_rate", json_string_unsafe("invalid"))])
        maybe_amount_temperature_rate = fallible_amount_temperature_rate_t(json, "amount_temperature_rate")
        errors = maybe_amount_temperature_rate%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_angle() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_angle_t) :: maybe_angle
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "angle", json_string_unsafe("invalid"))])
        maybe_angle = fallible_angle_t(json, "angle")
        errors = maybe_angle%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_area() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_area_t) :: maybe_area
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "area", json_string_unsafe("invalid"))])
        maybe_area = fallible_area_t(json, "area")
        errors = maybe_area%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_conductance() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_conductance_t) :: maybe_conductance
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "conductance", json_string_unsafe("invalid"))])
        maybe_conductance = fallible_conductance_t(json, "conductance")
        errors = maybe_conductance%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_delta_temperature() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_delta_temperature_t) :: maybe_delta_temperature
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "delta_temperature", json_string_unsafe("invalid"))])
        maybe_delta_temperature = fallible_delta_temperature_t(json, "delta_temperature")
        errors = maybe_delta_temperature%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_density() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_density_t) :: maybe_density
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "density", json_string_unsafe("invalid"))])
        maybe_density = fallible_density_t(json, "density")
        errors = maybe_density%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_diffusivity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_diffusivity_t) :: maybe_diffusivity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "diffusivity", json_string_unsafe("invalid"))])
        maybe_diffusivity = fallible_diffusivity_t(json, "diffusivity")
        errors = maybe_diffusivity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_dynamic_viscosity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_dynamic_viscosity_t) :: maybe_dynamic_viscosity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "dynamic_viscosity", json_string_unsafe("invalid"))])
        maybe_dynamic_viscosity = fallible_dynamic_viscosity_t(json, "dynamic_viscosity")
        errors = maybe_dynamic_viscosity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_energy() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_energy_t) :: maybe_energy
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "energy", json_string_unsafe("invalid"))])
        maybe_energy = fallible_energy_t(json, "energy")
        errors = maybe_energy%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_enthalpy() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_enthalpy_t) :: maybe_enthalpy
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "enthalpy", json_string_unsafe("invalid"))])
        maybe_enthalpy = fallible_enthalpy_t(json, "enthalpy")
        errors = maybe_enthalpy%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_fluence() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fluence_t) :: maybe_fluence
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fluence", json_string_unsafe("invalid"))])
        maybe_fluence = fallible_fluence_t(json, "fluence")
        errors = maybe_fluence%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_force() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_force_t) :: maybe_force
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "force", json_string_unsafe("invalid"))])
        maybe_force = fallible_force_t(json, "force")
        errors = maybe_force%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_fracture_toughness() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fracture_toughness_t) :: maybe_fracture_toughness
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fracture_toughness", json_string_unsafe("invalid"))])
        maybe_fracture_toughness = fallible_fracture_toughness_t(json, "fracture_toughness")
        errors = maybe_fracture_toughness%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_frequency() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_frequency_t) :: maybe_frequency
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "frequency", json_string_unsafe("invalid"))])
        maybe_frequency = fallible_frequency_t(json, "frequency")
        errors = maybe_frequency%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_heat_capacity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_capacity_t) :: maybe_heat_capacity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_capacity", json_string_unsafe("invalid"))])
        maybe_heat_capacity = fallible_heat_capacity_t(json, "heat_capacity")
        errors = maybe_heat_capacity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_transfer_coefficient_t) :: maybe_heat_transfer_coefficient
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_transfer_coefficient", json_string_unsafe("invalid"))])
        maybe_heat_transfer_coefficient = fallible_heat_transfer_coefficient_t(json, "heat_transfer_coefficient")
        errors = maybe_heat_transfer_coefficient%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_insulance() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_insulance_t) :: maybe_insulance
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "insulance", json_string_unsafe("invalid"))])
        maybe_insulance = fallible_insulance_t(json, "insulance")
        errors = maybe_insulance%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_intensity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_intensity_t) :: maybe_intensity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "intensity", json_string_unsafe("invalid"))])
        maybe_intensity = fallible_intensity_t(json, "intensity")
        errors = maybe_intensity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_inverse_molar_mass() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_inverse_molar_mass_t) :: maybe_inverse_molar_mass
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "inverse_molar_mass", json_string_unsafe("invalid"))])
        maybe_inverse_molar_mass = fallible_inverse_molar_mass_t(json, "inverse_molar_mass")
        errors = maybe_inverse_molar_mass%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_length() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_length_t) :: maybe_length
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "length", json_string_unsafe("invalid"))])
        maybe_length = fallible_length_t(json, "length")
        errors = maybe_length%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_mass() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_t) :: maybe_mass
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass", json_string_unsafe("invalid"))])
        maybe_mass = fallible_mass_t(json, "mass")
        errors = maybe_mass%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_mass_flux() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_flux_t) :: maybe_mass_flux
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_flux", json_string_unsafe("invalid"))])
        maybe_mass_flux = fallible_mass_flux_t(json, "mass_flux")
        errors = maybe_mass_flux%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_mass_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_rate_t) :: maybe_mass_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_rate", json_string_unsafe("invalid"))])
        maybe_mass_rate = fallible_mass_rate_t(json, "mass_rate")
        errors = maybe_mass_rate%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_molar_density() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_density_t) :: maybe_molar_density
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_density", json_string_unsafe("invalid"))])
        maybe_molar_density = fallible_molar_density_t(json, "molar_density")
        errors = maybe_molar_density%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_molar_enthalpy() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_enthalpy_t) :: maybe_molar_enthalpy
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_enthalpy", json_string_unsafe("invalid"))])
        maybe_molar_enthalpy = fallible_molar_enthalpy_t(json, "molar_enthalpy")
        errors = maybe_molar_enthalpy%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_molar_mass() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_mass_t) :: maybe_molar_mass
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_mass", json_string_unsafe("invalid"))])
        maybe_molar_mass = fallible_molar_mass_t(json, "molar_mass")
        errors = maybe_molar_mass%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_molar_specific_heat() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_specific_heat_t) :: maybe_molar_specific_heat
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_specific_heat", json_string_unsafe("invalid"))])
        maybe_molar_specific_heat = fallible_molar_specific_heat_t(json, "molar_specific_heat")
        errors = maybe_molar_specific_heat%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_power() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_power_t) :: maybe_power
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "power", json_string_unsafe("invalid"))])
        maybe_power = fallible_power_t(json, "power")
        errors = maybe_power%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_pressure() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_pressure_t) :: maybe_pressure
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "pressure", json_string_unsafe("invalid"))])
        maybe_pressure = fallible_pressure_t(json, "pressure")
        errors = maybe_pressure%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_specific_heat() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_specific_heat_t) :: maybe_specific_heat
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "specific_heat", json_string_unsafe("invalid"))])
        maybe_specific_heat = fallible_specific_heat_t(json, "specific_heat")
        errors = maybe_specific_heat%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_speed() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_speed_t) :: maybe_speed
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "speed", json_string_unsafe("invalid"))])
        maybe_speed = fallible_speed_t(json, "speed")
        errors = maybe_speed%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_temperature_gradient() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_gradient_t) :: maybe_temperature_gradient
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature_gradient", json_string_unsafe("invalid"))])
        maybe_temperature_gradient = fallible_temperature_gradient_t(json, "temperature_gradient")
        errors = maybe_temperature_gradient%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_temperature() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_t) :: maybe_temperature
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature", json_string_unsafe("invalid"))])
        maybe_temperature = fallible_temperature_t(json, "temperature")
        errors = maybe_temperature%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_thermal_conductivity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_conductivity_t) :: maybe_thermal_conductivity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_conductivity", json_string_unsafe("invalid"))])
        maybe_thermal_conductivity = fallible_thermal_conductivity_t(json, "thermal_conductivity")
        errors = maybe_thermal_conductivity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_expansion_coefficient_t) :: maybe_thermal_expansion_coefficient
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_expansion_coefficient", json_string_unsafe("invalid"))])
        maybe_thermal_expansion_coefficient = fallible_thermal_expansion_coefficient_t(json, "thermal_expansion_coefficient")
        errors = maybe_thermal_expansion_coefficient%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_time() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_time_t) :: maybe_time
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "time", json_string_unsafe("invalid"))])
        maybe_time = fallible_time_t(json, "time")
        errors = maybe_time%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_volume() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_t) :: maybe_volume
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume", json_string_unsafe("invalid"))])
        maybe_volume = fallible_volume_t(json, "volume")
        errors = maybe_volume%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_volume_rate() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_rate_t) :: maybe_volume_rate
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume_rate", json_string_unsafe("invalid"))])
        maybe_volume_rate = fallible_volume_rate_t(json, "volume_rate")
        errors = maybe_volume_rate%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volumetric_heat_capacity_t) :: maybe_volumetric_heat_capacity
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volumetric_heat_capacity", json_string_unsafe("invalid"))])
        maybe_volumetric_heat_capacity = fallible_volumetric_heat_capacity_t(json, "volumetric_heat_capacity")
        errors = maybe_volumetric_heat_capacity%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_parse_error_yank() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_yank_t) :: maybe_yank
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "yank", json_string_unsafe("invalid"))])
        maybe_yank = fallible_yank_t(json, "yank")
        errors = maybe_yank%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function

    function check_valid_json_to_acceleration_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_acceleration_unit_t) :: maybe_acceleration_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "acceleration unit", json_string_unsafe(to_string(meters_per_square_second)))])
        maybe_acceleration_unit = fallible_acceleration_unit_t(json, "acceleration unit")
        if (maybe_acceleration_unit%failed()) then
            errors = maybe_acceleration_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(meters_per_square_second), to_string(maybe_acceleration_unit%unit()))
        end if
    end function

    function check_valid_json_to_amount_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_unit_t) :: maybe_amount_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount unit", json_string_unsafe(to_string(mols)))])
        maybe_amount_unit = fallible_amount_unit_t(json, "amount unit")
        if (maybe_amount_unit%failed()) then
            errors = maybe_amount_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(mols), to_string(maybe_amount_unit%unit()))
        end if
    end function

    function check_valid_json_to_amount_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_rate_unit_t) :: maybe_amount_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_rate unit", json_string_unsafe(to_string(mols_per_second)))])
        maybe_amount_rate_unit = fallible_amount_rate_unit_t(json, "amount_rate unit")
        if (maybe_amount_rate_unit%failed()) then
            errors = maybe_amount_rate_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(mols_per_second), to_string(maybe_amount_rate_unit%unit()))
        end if
    end function

    function check_valid_json_to_amount_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_unit_t) :: maybe_amount_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature unit", json_string_unsafe(to_string(mols_kelvin)))])
        maybe_amount_temperature_unit = fallible_amount_temperature_unit_t(json, "amount_temperature unit")
        if (maybe_amount_temperature_unit%failed()) then
            errors = maybe_amount_temperature_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(mols_kelvin), to_string(maybe_amount_temperature_unit%unit()))
        end if
    end function

    function check_valid_json_to_amount_temperature_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_rate_unit_t) :: maybe_amount_temperature_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature_rate unit", json_string_unsafe(to_string(mols_kelvin_per_second)))])
        maybe_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t(json, "amount_temperature_rate unit")
        if (maybe_amount_temperature_rate_unit%failed()) then
            errors = maybe_amount_temperature_rate_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(mols_kelvin_per_second), to_string(maybe_amount_temperature_rate_unit%unit()))
        end if
    end function

    function check_valid_json_to_angle_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_angle_unit_t) :: maybe_angle_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "angle unit", json_string_unsafe(to_string(radians)))])
        maybe_angle_unit = fallible_angle_unit_t(json, "angle unit")
        if (maybe_angle_unit%failed()) then
            errors = maybe_angle_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(radians), to_string(maybe_angle_unit%unit()))
        end if
    end function

    function check_valid_json_to_area_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_area_unit_t) :: maybe_area_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "area unit", json_string_unsafe(to_string(square_meters)))])
        maybe_area_unit = fallible_area_unit_t(json, "area unit")
        if (maybe_area_unit%failed()) then
            errors = maybe_area_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(square_meters), to_string(maybe_area_unit%unit()))
        end if
    end function

    function check_valid_json_to_conductance_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_conductance_unit_t) :: maybe_conductance_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "conductance unit", json_string_unsafe(to_string(watts_per_kelvin)))])
        maybe_conductance_unit = fallible_conductance_unit_t(json, "conductance unit")
        if (maybe_conductance_unit%failed()) then
            errors = maybe_conductance_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(watts_per_kelvin), to_string(maybe_conductance_unit%unit()))
        end if
    end function

    function check_valid_json_to_delta_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_delta_temperature_unit_t) :: maybe_delta_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "delta_temperature unit", json_string_unsafe(to_string(delta_kelvin)))])
        maybe_delta_temperature_unit = fallible_delta_temperature_unit_t(json, "delta_temperature unit")
        if (maybe_delta_temperature_unit%failed()) then
            errors = maybe_delta_temperature_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(delta_kelvin), to_string(maybe_delta_temperature_unit%unit()))
        end if
    end function

    function check_valid_json_to_density_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_density_unit_t) :: maybe_density_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "density unit", json_string_unsafe(to_string(kilograms_per_cubic_meter)))])
        maybe_density_unit = fallible_density_unit_t(json, "density unit")
        if (maybe_density_unit%failed()) then
            errors = maybe_density_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(kilograms_per_cubic_meter), to_string(maybe_density_unit%unit()))
        end if
    end function

    function check_valid_json_to_diffusivity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_diffusivity_unit_t) :: maybe_diffusivity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "diffusivity unit", json_string_unsafe(to_string(square_meters_per_second)))])
        maybe_diffusivity_unit = fallible_diffusivity_unit_t(json, "diffusivity unit")
        if (maybe_diffusivity_unit%failed()) then
            errors = maybe_diffusivity_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(square_meters_per_second), to_string(maybe_diffusivity_unit%unit()))
        end if
    end function

    function check_valid_json_to_dynamic_viscosity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_dynamic_viscosity_unit_t) :: maybe_dynamic_viscosity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "dynamic_viscosity unit", json_string_unsafe(to_string(pascal_seconds)))])
        maybe_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t(json, "dynamic_viscosity unit")
        if (maybe_dynamic_viscosity_unit%failed()) then
            errors = maybe_dynamic_viscosity_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(pascal_seconds), to_string(maybe_dynamic_viscosity_unit%unit()))
        end if
    end function

    function check_valid_json_to_energy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_energy_unit_t) :: maybe_energy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "energy unit", json_string_unsafe(to_string(joules)))])
        maybe_energy_unit = fallible_energy_unit_t(json, "energy unit")
        if (maybe_energy_unit%failed()) then
            errors = maybe_energy_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(joules), to_string(maybe_energy_unit%unit()))
        end if
    end function

    function check_valid_json_to_enthalpy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_enthalpy_unit_t) :: maybe_enthalpy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "enthalpy unit", json_string_unsafe(to_string(joules_per_kilogram)))])
        maybe_enthalpy_unit = fallible_enthalpy_unit_t(json, "enthalpy unit")
        if (maybe_enthalpy_unit%failed()) then
            errors = maybe_enthalpy_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(joules_per_kilogram), to_string(maybe_enthalpy_unit%unit()))
        end if
    end function

    function check_valid_json_to_fluence_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fluence_unit_t) :: maybe_fluence_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fluence unit", json_string_unsafe(to_string(particles_per_square_meter)))])
        maybe_fluence_unit = fallible_fluence_unit_t(json, "fluence unit")
        if (maybe_fluence_unit%failed()) then
            errors = maybe_fluence_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(particles_per_square_meter), to_string(maybe_fluence_unit%unit()))
        end if
    end function

    function check_valid_json_to_force_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_force_unit_t) :: maybe_force_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "force unit", json_string_unsafe(to_string(newtons)))])
        maybe_force_unit = fallible_force_unit_t(json, "force unit")
        if (maybe_force_unit%failed()) then
            errors = maybe_force_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(newtons), to_string(maybe_force_unit%unit()))
        end if
    end function

    function check_valid_json_to_fracture_toughness_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fracture_toughness_unit_t) :: maybe_fracture_toughness_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fracture_toughness unit", json_string_unsafe(to_string(pascal_root_meter)))])
        maybe_fracture_toughness_unit = fallible_fracture_toughness_unit_t(json, "fracture_toughness unit")
        if (maybe_fracture_toughness_unit%failed()) then
            errors = maybe_fracture_toughness_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(pascal_root_meter), to_string(maybe_fracture_toughness_unit%unit()))
        end if
    end function

    function check_valid_json_to_frequency_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_frequency_unit_t) :: maybe_frequency_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "frequency unit", json_string_unsafe(to_string(hertz)))])
        maybe_frequency_unit = fallible_frequency_unit_t(json, "frequency unit")
        if (maybe_frequency_unit%failed()) then
            errors = maybe_frequency_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(hertz), to_string(maybe_frequency_unit%unit()))
        end if
    end function

    function check_valid_json_to_heat_capacity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_capacity_unit_t) :: maybe_heat_capacity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_capacity unit", json_string_unsafe(to_string(joules_per_kelvin)))])
        maybe_heat_capacity_unit = fallible_heat_capacity_unit_t(json, "heat_capacity unit")
        if (maybe_heat_capacity_unit%failed()) then
            errors = maybe_heat_capacity_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(joules_per_kelvin), to_string(maybe_heat_capacity_unit%unit()))
        end if
    end function

    function check_valid_json_to_heat_transfer_coefficient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_transfer_coefficient_unit_t) :: maybe_heat_transfer_coefficient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_transfer_coefficient unit", json_string_unsafe(to_string(WATTS_PER_SQUARE_METER_KELVIN)))])
        maybe_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t(json, "heat_transfer_coefficient unit")
        if (maybe_heat_transfer_coefficient_unit%failed()) then
            errors = maybe_heat_transfer_coefficient_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(WATTS_PER_SQUARE_METER_KELVIN), to_string(maybe_heat_transfer_coefficient_unit%unit()))
        end if
    end function

    function check_valid_json_to_insulance_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_insulance_unit_t) :: maybe_insulance_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "insulance unit", json_string_unsafe(to_string(kelvin_square_meters_per_watt)))])
        maybe_insulance_unit = fallible_insulance_unit_t(json, "insulance unit")
        if (maybe_insulance_unit%failed()) then
            errors = maybe_insulance_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(kelvin_square_meters_per_watt), to_string(maybe_insulance_unit%unit()))
        end if
    end function

    function check_valid_json_to_intensity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_intensity_unit_t) :: maybe_intensity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "intensity unit", json_string_unsafe(to_string(watts_per_square_meter)))])
        maybe_intensity_unit = fallible_intensity_unit_t(json, "intensity unit")
        if (maybe_intensity_unit%failed()) then
            errors = maybe_intensity_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(watts_per_square_meter), to_string(maybe_intensity_unit%unit()))
        end if
    end function

    function check_valid_json_to_inverse_molar_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_inverse_molar_mass_unit_t) :: maybe_inverse_molar_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "inverse_molar_mass unit", json_string_unsafe(to_string(mols_per_kilogram)))])
        maybe_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t(json, "inverse_molar_mass unit")
        if (maybe_inverse_molar_mass_unit%failed()) then
            errors = maybe_inverse_molar_mass_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(mols_per_kilogram), to_string(maybe_inverse_molar_mass_unit%unit()))
        end if
    end function

    function check_valid_json_to_length_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_length_unit_t) :: maybe_length_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "length unit", json_string_unsafe(to_string(meters)))])
        maybe_length_unit = fallible_length_unit_t(json, "length unit")
        if (maybe_length_unit%failed()) then
            errors = maybe_length_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(meters), to_string(maybe_length_unit%unit()))
        end if
    end function

    function check_valid_json_to_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_unit_t) :: maybe_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass unit", json_string_unsafe(to_string(kilograms)))])
        maybe_mass_unit = fallible_mass_unit_t(json, "mass unit")
        if (maybe_mass_unit%failed()) then
            errors = maybe_mass_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(kilograms), to_string(maybe_mass_unit%unit()))
        end if
    end function

    function check_valid_json_to_mass_flux_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_flux_unit_t) :: maybe_mass_flux_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_flux unit", json_string_unsafe(to_string(kilograms_per_square_meter_seconds)))])
        maybe_mass_flux_unit = fallible_mass_flux_unit_t(json, "mass_flux unit")
        if (maybe_mass_flux_unit%failed()) then
            errors = maybe_mass_flux_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(kilograms_per_square_meter_seconds), to_string(maybe_mass_flux_unit%unit()))
        end if
    end function

    function check_valid_json_to_mass_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_rate_unit_t) :: maybe_mass_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_rate unit", json_string_unsafe(to_string(kilograms_per_second)))])
        maybe_mass_rate_unit = fallible_mass_rate_unit_t(json, "mass_rate unit")
        if (maybe_mass_rate_unit%failed()) then
            errors = maybe_mass_rate_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(kilograms_per_second), to_string(maybe_mass_rate_unit%unit()))
        end if
    end function

    function check_valid_json_to_molar_density_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_density_unit_t) :: maybe_molar_density_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_density unit", json_string_unsafe(to_string(mols_per_cubic_meter)))])
        maybe_molar_density_unit = fallible_molar_density_unit_t(json, "molar_density unit")
        if (maybe_molar_density_unit%failed()) then
            errors = maybe_molar_density_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(mols_per_cubic_meter), to_string(maybe_molar_density_unit%unit()))
        end if
    end function

    function check_valid_json_to_molar_enthalpy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_enthalpy_unit_t) :: maybe_molar_enthalpy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_enthalpy unit", json_string_unsafe(to_string(joules_per_mol)))])
        maybe_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t(json, "molar_enthalpy unit")
        if (maybe_molar_enthalpy_unit%failed()) then
            errors = maybe_molar_enthalpy_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(joules_per_mol), to_string(maybe_molar_enthalpy_unit%unit()))
        end if
    end function

    function check_valid_json_to_molar_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_mass_unit_t) :: maybe_molar_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_mass unit", json_string_unsafe(to_string(kilograms_per_mol)))])
        maybe_molar_mass_unit = fallible_molar_mass_unit_t(json, "molar_mass unit")
        if (maybe_molar_mass_unit%failed()) then
            errors = maybe_molar_mass_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(kilograms_per_mol), to_string(maybe_molar_mass_unit%unit()))
        end if
    end function

    function check_valid_json_to_molar_specific_heat_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_specific_heat_unit_t) :: maybe_molar_specific_heat_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_specific_heat unit", json_string_unsafe(to_string(joules_per_kelvin_mol)))])
        maybe_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t(json, "molar_specific_heat unit")
        if (maybe_molar_specific_heat_unit%failed()) then
            errors = maybe_molar_specific_heat_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(joules_per_kelvin_mol), to_string(maybe_molar_specific_heat_unit%unit()))
        end if
    end function

    function check_valid_json_to_power_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_power_unit_t) :: maybe_power_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "power unit", json_string_unsafe(to_string(watts)))])
        maybe_power_unit = fallible_power_unit_t(json, "power unit")
        if (maybe_power_unit%failed()) then
            errors = maybe_power_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(watts), to_string(maybe_power_unit%unit()))
        end if
    end function

    function check_valid_json_to_pressure_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_pressure_unit_t) :: maybe_pressure_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "pressure unit", json_string_unsafe(to_string(pascals)))])
        maybe_pressure_unit = fallible_pressure_unit_t(json, "pressure unit")
        if (maybe_pressure_unit%failed()) then
            errors = maybe_pressure_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(pascals), to_string(maybe_pressure_unit%unit()))
        end if
    end function

    function check_valid_json_to_specific_heat_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_specific_heat_unit_t) :: maybe_specific_heat_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "specific_heat unit", json_string_unsafe(to_string(joules_per_kilogram_kelvin)))])
        maybe_specific_heat_unit = fallible_specific_heat_unit_t(json, "specific_heat unit")
        if (maybe_specific_heat_unit%failed()) then
            errors = maybe_specific_heat_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(joules_per_kilogram_kelvin), to_string(maybe_specific_heat_unit%unit()))
        end if
    end function

    function check_valid_json_to_speed_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_speed_unit_t) :: maybe_speed_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "speed unit", json_string_unsafe(to_string(meters_per_second)))])
        maybe_speed_unit = fallible_speed_unit_t(json, "speed unit")
        if (maybe_speed_unit%failed()) then
            errors = maybe_speed_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(meters_per_second), to_string(maybe_speed_unit%unit()))
        end if
    end function

    function check_valid_json_to_temperature_gradient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_gradient_unit_t) :: maybe_temperature_gradient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature_gradient unit", json_string_unsafe(to_string(kelvin_per_meter)))])
        maybe_temperature_gradient_unit = fallible_temperature_gradient_unit_t(json, "temperature_gradient unit")
        if (maybe_temperature_gradient_unit%failed()) then
            errors = maybe_temperature_gradient_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(kelvin_per_meter), to_string(maybe_temperature_gradient_unit%unit()))
        end if
    end function

    function check_valid_json_to_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_unit_t) :: maybe_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature unit", json_string_unsafe(to_string(kelvin)))])
        maybe_temperature_unit = fallible_temperature_unit_t(json, "temperature unit")
        if (maybe_temperature_unit%failed()) then
            errors = maybe_temperature_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(kelvin), to_string(maybe_temperature_unit%unit()))
        end if
    end function

    function check_valid_json_to_thermal_conductivity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_conductivity_unit_t) :: maybe_thermal_conductivity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_conductivity unit", json_string_unsafe(to_string(watts_per_meter_kelvin)))])
        maybe_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t(json, "thermal_conductivity unit")
        if (maybe_thermal_conductivity_unit%failed()) then
            errors = maybe_thermal_conductivity_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(watts_per_meter_kelvin), to_string(maybe_thermal_conductivity_unit%unit()))
        end if
    end function

    function check_valid_json_to_thermal_expansion_coefficient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_expansion_coefficient_unit_t) :: maybe_thermal_expansion_coefficient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_expansion_coefficient unit", json_string_unsafe(to_string(per_kelvin)))])
        maybe_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t(json, "thermal_expansion_coefficient unit")
        if (maybe_thermal_expansion_coefficient_unit%failed()) then
            errors = maybe_thermal_expansion_coefficient_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(per_kelvin), to_string(maybe_thermal_expansion_coefficient_unit%unit()))
        end if
    end function

    function check_valid_json_to_time_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_time_unit_t) :: maybe_time_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "time unit", json_string_unsafe(to_string(seconds)))])
        maybe_time_unit = fallible_time_unit_t(json, "time unit")
        if (maybe_time_unit%failed()) then
            errors = maybe_time_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(seconds), to_string(maybe_time_unit%unit()))
        end if
    end function

    function check_valid_json_to_volume_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_unit_t) :: maybe_volume_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume unit", json_string_unsafe(to_string(cubic_meters)))])
        maybe_volume_unit = fallible_volume_unit_t(json, "volume unit")
        if (maybe_volume_unit%failed()) then
            errors = maybe_volume_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(cubic_meters), to_string(maybe_volume_unit%unit()))
        end if
    end function

    function check_valid_json_to_volume_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_rate_unit_t) :: maybe_volume_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume_rate unit", json_string_unsafe(to_string(cubic_meters_per_second)))])
        maybe_volume_rate_unit = fallible_volume_rate_unit_t(json, "volume_rate unit")
        if (maybe_volume_rate_unit%failed()) then
            errors = maybe_volume_rate_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(cubic_meters_per_second), to_string(maybe_volume_rate_unit%unit()))
        end if
    end function

    function check_valid_json_to_volumetric_heat_capacity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volumetric_heat_capacity_unit_t) :: maybe_volumetric_heat_capacity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volumetric_heat_capacity unit", json_string_unsafe(to_string(pascals_per_kelvin)))])
        maybe_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t(json, "volumetric_heat_capacity unit")
        if (maybe_volumetric_heat_capacity_unit%failed()) then
            errors = maybe_volumetric_heat_capacity_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(pascals_per_kelvin), to_string(maybe_volumetric_heat_capacity_unit%unit()))
        end if
    end function

    function check_valid_json_to_yank_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_yank_unit_t) :: maybe_yank_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "yank unit", json_string_unsafe(to_string(newtons_per_second)))])
        maybe_yank_unit = fallible_yank_unit_t(json, "yank unit")
        if (maybe_yank_unit%failed()) then
            errors = maybe_yank_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string(newtons_per_second), to_string(maybe_yank_unit%unit()))
        end if
    end function

    function check_missing_key_acceleration_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_acceleration_unit_t) :: maybe_acceleration_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_acceleration_unit = fallible_acceleration_unit_t(json, "acceleration unit")
        errors = maybe_acceleration_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."acceleration unit", &
                errors%to_string())
    end function

    function check_missing_key_amount_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_unit_t) :: maybe_amount_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_amount_unit = fallible_amount_unit_t(json, "amount unit")
        errors = maybe_amount_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."amount unit", &
                errors%to_string())
    end function

    function check_missing_key_amount_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_rate_unit_t) :: maybe_amount_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_amount_rate_unit = fallible_amount_rate_unit_t(json, "amount_rate unit")
        errors = maybe_amount_rate_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."amount_rate unit", &
                errors%to_string())
    end function

    function check_missing_key_amount_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_unit_t) :: maybe_amount_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_amount_temperature_unit = fallible_amount_temperature_unit_t(json, "amount_temperature unit")
        errors = maybe_amount_temperature_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."amount_temperature unit", &
                errors%to_string())
    end function

    function check_missing_key_amount_temperature_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_rate_unit_t) :: maybe_amount_temperature_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t(json, "amount_temperature_rate unit")
        errors = maybe_amount_temperature_rate_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."amount_temperature_rate unit", &
                errors%to_string())
    end function

    function check_missing_key_angle_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_angle_unit_t) :: maybe_angle_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_angle_unit = fallible_angle_unit_t(json, "angle unit")
        errors = maybe_angle_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."angle unit", &
                errors%to_string())
    end function

    function check_missing_key_area_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_area_unit_t) :: maybe_area_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_area_unit = fallible_area_unit_t(json, "area unit")
        errors = maybe_area_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."area unit", &
                errors%to_string())
    end function

    function check_missing_key_conductance_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_conductance_unit_t) :: maybe_conductance_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_conductance_unit = fallible_conductance_unit_t(json, "conductance unit")
        errors = maybe_conductance_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."conductance unit", &
                errors%to_string())
    end function

    function check_missing_key_delta_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_delta_temperature_unit_t) :: maybe_delta_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_delta_temperature_unit = fallible_delta_temperature_unit_t(json, "delta_temperature unit")
        errors = maybe_delta_temperature_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."delta_temperature unit", &
                errors%to_string())
    end function

    function check_missing_key_density_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_density_unit_t) :: maybe_density_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_density_unit = fallible_density_unit_t(json, "density unit")
        errors = maybe_density_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."density unit", &
                errors%to_string())
    end function

    function check_missing_key_diffusivity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_diffusivity_unit_t) :: maybe_diffusivity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_diffusivity_unit = fallible_diffusivity_unit_t(json, "diffusivity unit")
        errors = maybe_diffusivity_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."diffusivity unit", &
                errors%to_string())
    end function

    function check_missing_key_dynamic_viscosity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_dynamic_viscosity_unit_t) :: maybe_dynamic_viscosity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t(json, "dynamic_viscosity unit")
        errors = maybe_dynamic_viscosity_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."dynamic_viscosity unit", &
                errors%to_string())
    end function

    function check_missing_key_energy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_energy_unit_t) :: maybe_energy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_energy_unit = fallible_energy_unit_t(json, "energy unit")
        errors = maybe_energy_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."energy unit", &
                errors%to_string())
    end function

    function check_missing_key_enthalpy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_enthalpy_unit_t) :: maybe_enthalpy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_enthalpy_unit = fallible_enthalpy_unit_t(json, "enthalpy unit")
        errors = maybe_enthalpy_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."enthalpy unit", &
                errors%to_string())
    end function

    function check_missing_key_fluence_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fluence_unit_t) :: maybe_fluence_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_fluence_unit = fallible_fluence_unit_t(json, "fluence unit")
        errors = maybe_fluence_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."fluence unit", &
                errors%to_string())
    end function

    function check_missing_key_force_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_force_unit_t) :: maybe_force_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_force_unit = fallible_force_unit_t(json, "force unit")
        errors = maybe_force_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."force unit", &
                errors%to_string())
    end function

    function check_missing_key_fracture_toughness_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fracture_toughness_unit_t) :: maybe_fracture_toughness_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_fracture_toughness_unit = fallible_fracture_toughness_unit_t(json, "fracture_toughness unit")
        errors = maybe_fracture_toughness_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."fracture_toughness unit", &
                errors%to_string())
    end function

    function check_missing_key_frequency_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_frequency_unit_t) :: maybe_frequency_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_frequency_unit = fallible_frequency_unit_t(json, "frequency unit")
        errors = maybe_frequency_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."frequency unit", &
                errors%to_string())
    end function

    function check_missing_key_heat_capacity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_capacity_unit_t) :: maybe_heat_capacity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_heat_capacity_unit = fallible_heat_capacity_unit_t(json, "heat_capacity unit")
        errors = maybe_heat_capacity_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."heat_capacity unit", &
                errors%to_string())
    end function

    function check_missing_key_heat_transfer_coefficient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_transfer_coefficient_unit_t) :: maybe_heat_transfer_coefficient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t(json, "heat_transfer_coefficient unit")
        errors = maybe_heat_transfer_coefficient_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."heat_transfer_coefficient unit", &
                errors%to_string())
    end function

    function check_missing_key_insulance_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_insulance_unit_t) :: maybe_insulance_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_insulance_unit = fallible_insulance_unit_t(json, "insulance unit")
        errors = maybe_insulance_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."insulance unit", &
                errors%to_string())
    end function

    function check_missing_key_intensity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_intensity_unit_t) :: maybe_intensity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_intensity_unit = fallible_intensity_unit_t(json, "intensity unit")
        errors = maybe_intensity_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."intensity unit", &
                errors%to_string())
    end function

    function check_missing_key_inverse_molar_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_inverse_molar_mass_unit_t) :: maybe_inverse_molar_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t(json, "inverse_molar_mass unit")
        errors = maybe_inverse_molar_mass_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."inverse_molar_mass unit", &
                errors%to_string())
    end function

    function check_missing_key_length_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_length_unit_t) :: maybe_length_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_length_unit = fallible_length_unit_t(json, "length unit")
        errors = maybe_length_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."length unit", &
                errors%to_string())
    end function

    function check_missing_key_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_unit_t) :: maybe_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_mass_unit = fallible_mass_unit_t(json, "mass unit")
        errors = maybe_mass_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."mass unit", &
                errors%to_string())
    end function

    function check_missing_key_mass_flux_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_flux_unit_t) :: maybe_mass_flux_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_mass_flux_unit = fallible_mass_flux_unit_t(json, "mass_flux unit")
        errors = maybe_mass_flux_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."mass_flux unit", &
                errors%to_string())
    end function

    function check_missing_key_mass_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_rate_unit_t) :: maybe_mass_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_mass_rate_unit = fallible_mass_rate_unit_t(json, "mass_rate unit")
        errors = maybe_mass_rate_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."mass_rate unit", &
                errors%to_string())
    end function

    function check_missing_key_molar_density_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_density_unit_t) :: maybe_molar_density_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_molar_density_unit = fallible_molar_density_unit_t(json, "molar_density unit")
        errors = maybe_molar_density_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."molar_density unit", &
                errors%to_string())
    end function

    function check_missing_key_molar_enthalpy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_enthalpy_unit_t) :: maybe_molar_enthalpy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t(json, "molar_enthalpy unit")
        errors = maybe_molar_enthalpy_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."molar_enthalpy unit", &
                errors%to_string())
    end function

    function check_missing_key_molar_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_mass_unit_t) :: maybe_molar_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_molar_mass_unit = fallible_molar_mass_unit_t(json, "molar_mass unit")
        errors = maybe_molar_mass_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."molar_mass unit", &
                errors%to_string())
    end function

    function check_missing_key_molar_specific_heat_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_specific_heat_unit_t) :: maybe_molar_specific_heat_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t(json, "molar_specific_heat unit")
        errors = maybe_molar_specific_heat_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."molar_specific_heat unit", &
                errors%to_string())
    end function

    function check_missing_key_power_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_power_unit_t) :: maybe_power_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_power_unit = fallible_power_unit_t(json, "power unit")
        errors = maybe_power_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."power unit", &
                errors%to_string())
    end function

    function check_missing_key_pressure_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_pressure_unit_t) :: maybe_pressure_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_pressure_unit = fallible_pressure_unit_t(json, "pressure unit")
        errors = maybe_pressure_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."pressure unit", &
                errors%to_string())
    end function

    function check_missing_key_specific_heat_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_specific_heat_unit_t) :: maybe_specific_heat_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_specific_heat_unit = fallible_specific_heat_unit_t(json, "specific_heat unit")
        errors = maybe_specific_heat_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."specific_heat unit", &
                errors%to_string())
    end function

    function check_missing_key_speed_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_speed_unit_t) :: maybe_speed_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_speed_unit = fallible_speed_unit_t(json, "speed unit")
        errors = maybe_speed_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."speed unit", &
                errors%to_string())
    end function

    function check_missing_key_temperature_gradient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_gradient_unit_t) :: maybe_temperature_gradient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_temperature_gradient_unit = fallible_temperature_gradient_unit_t(json, "temperature_gradient unit")
        errors = maybe_temperature_gradient_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."temperature_gradient unit", &
                errors%to_string())
    end function

    function check_missing_key_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_unit_t) :: maybe_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_temperature_unit = fallible_temperature_unit_t(json, "temperature unit")
        errors = maybe_temperature_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."temperature unit", &
                errors%to_string())
    end function

    function check_missing_key_thermal_conductivity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_conductivity_unit_t) :: maybe_thermal_conductivity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t(json, "thermal_conductivity unit")
        errors = maybe_thermal_conductivity_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."thermal_conductivity unit", &
                errors%to_string())
    end function

    function check_missing_key_thermal_expansion_coefficient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_expansion_coefficient_unit_t) :: maybe_thermal_expansion_coefficient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t(json, "thermal_expansion_coefficient unit")
        errors = maybe_thermal_expansion_coefficient_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."thermal_expansion_coefficient unit", &
                errors%to_string())
    end function

    function check_missing_key_time_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_time_unit_t) :: maybe_time_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_time_unit = fallible_time_unit_t(json, "time unit")
        errors = maybe_time_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."time unit", &
                errors%to_string())
    end function

    function check_missing_key_volume_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_unit_t) :: maybe_volume_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_volume_unit = fallible_volume_unit_t(json, "volume unit")
        errors = maybe_volume_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."volume unit", &
                errors%to_string())
    end function

    function check_missing_key_volume_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_rate_unit_t) :: maybe_volume_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_volume_rate_unit = fallible_volume_rate_unit_t(json, "volume_rate unit")
        errors = maybe_volume_rate_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."volume_rate unit", &
                errors%to_string())
    end function

    function check_missing_key_volumetric_heat_capacity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volumetric_heat_capacity_unit_t) :: maybe_volumetric_heat_capacity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t(json, "volumetric_heat_capacity unit")
        errors = maybe_volumetric_heat_capacity_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."volumetric_heat_capacity unit", &
                errors%to_string())
    end function

    function check_missing_key_yank_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_yank_unit_t) :: maybe_yank_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_yank_unit = fallible_yank_unit_t(json, "yank unit")
        errors = maybe_yank_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."yank unit", &
                errors%to_string())
    end function

    function check_not_string_acceleration_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_acceleration_unit_t) :: maybe_acceleration_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "acceleration unit", json_null_t())])
        maybe_acceleration_unit = fallible_acceleration_unit_t(json, "acceleration unit")
        errors = maybe_acceleration_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."acceleration unit", &
                errors%to_string())
    end function

    function check_not_string_amount_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_unit_t) :: maybe_amount_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount unit", json_null_t())])
        maybe_amount_unit = fallible_amount_unit_t(json, "amount unit")
        errors = maybe_amount_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."amount unit", &
                errors%to_string())
    end function

    function check_not_string_amount_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_rate_unit_t) :: maybe_amount_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_rate unit", json_null_t())])
        maybe_amount_rate_unit = fallible_amount_rate_unit_t(json, "amount_rate unit")
        errors = maybe_amount_rate_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."amount_rate unit", &
                errors%to_string())
    end function

    function check_not_string_amount_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_unit_t) :: maybe_amount_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature unit", json_null_t())])
        maybe_amount_temperature_unit = fallible_amount_temperature_unit_t(json, "amount_temperature unit")
        errors = maybe_amount_temperature_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."amount_temperature unit", &
                errors%to_string())
    end function

    function check_not_string_amount_temperature_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_rate_unit_t) :: maybe_amount_temperature_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature_rate unit", json_null_t())])
        maybe_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t(json, "amount_temperature_rate unit")
        errors = maybe_amount_temperature_rate_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."amount_temperature_rate unit", &
                errors%to_string())
    end function

    function check_not_string_angle_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_angle_unit_t) :: maybe_angle_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "angle unit", json_null_t())])
        maybe_angle_unit = fallible_angle_unit_t(json, "angle unit")
        errors = maybe_angle_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."angle unit", &
                errors%to_string())
    end function

    function check_not_string_area_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_area_unit_t) :: maybe_area_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "area unit", json_null_t())])
        maybe_area_unit = fallible_area_unit_t(json, "area unit")
        errors = maybe_area_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."area unit", &
                errors%to_string())
    end function

    function check_not_string_conductance_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_conductance_unit_t) :: maybe_conductance_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "conductance unit", json_null_t())])
        maybe_conductance_unit = fallible_conductance_unit_t(json, "conductance unit")
        errors = maybe_conductance_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."conductance unit", &
                errors%to_string())
    end function

    function check_not_string_delta_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_delta_temperature_unit_t) :: maybe_delta_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "delta_temperature unit", json_null_t())])
        maybe_delta_temperature_unit = fallible_delta_temperature_unit_t(json, "delta_temperature unit")
        errors = maybe_delta_temperature_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."delta_temperature unit", &
                errors%to_string())
    end function

    function check_not_string_density_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_density_unit_t) :: maybe_density_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "density unit", json_null_t())])
        maybe_density_unit = fallible_density_unit_t(json, "density unit")
        errors = maybe_density_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."density unit", &
                errors%to_string())
    end function

    function check_not_string_diffusivity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_diffusivity_unit_t) :: maybe_diffusivity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "diffusivity unit", json_null_t())])
        maybe_diffusivity_unit = fallible_diffusivity_unit_t(json, "diffusivity unit")
        errors = maybe_diffusivity_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."diffusivity unit", &
                errors%to_string())
    end function

    function check_not_string_dynamic_viscosity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_dynamic_viscosity_unit_t) :: maybe_dynamic_viscosity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "dynamic_viscosity unit", json_null_t())])
        maybe_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t(json, "dynamic_viscosity unit")
        errors = maybe_dynamic_viscosity_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."dynamic_viscosity unit", &
                errors%to_string())
    end function

    function check_not_string_energy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_energy_unit_t) :: maybe_energy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "energy unit", json_null_t())])
        maybe_energy_unit = fallible_energy_unit_t(json, "energy unit")
        errors = maybe_energy_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."energy unit", &
                errors%to_string())
    end function

    function check_not_string_enthalpy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_enthalpy_unit_t) :: maybe_enthalpy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "enthalpy unit", json_null_t())])
        maybe_enthalpy_unit = fallible_enthalpy_unit_t(json, "enthalpy unit")
        errors = maybe_enthalpy_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."enthalpy unit", &
                errors%to_string())
    end function

    function check_not_string_fluence_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fluence_unit_t) :: maybe_fluence_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fluence unit", json_null_t())])
        maybe_fluence_unit = fallible_fluence_unit_t(json, "fluence unit")
        errors = maybe_fluence_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."fluence unit", &
                errors%to_string())
    end function

    function check_not_string_force_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_force_unit_t) :: maybe_force_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "force unit", json_null_t())])
        maybe_force_unit = fallible_force_unit_t(json, "force unit")
        errors = maybe_force_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."force unit", &
                errors%to_string())
    end function

    function check_not_string_fracture_toughness_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fracture_toughness_unit_t) :: maybe_fracture_toughness_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fracture_toughness unit", json_null_t())])
        maybe_fracture_toughness_unit = fallible_fracture_toughness_unit_t(json, "fracture_toughness unit")
        errors = maybe_fracture_toughness_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."fracture_toughness unit", &
                errors%to_string())
    end function

    function check_not_string_frequency_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_frequency_unit_t) :: maybe_frequency_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "frequency unit", json_null_t())])
        maybe_frequency_unit = fallible_frequency_unit_t(json, "frequency unit")
        errors = maybe_frequency_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."frequency unit", &
                errors%to_string())
    end function

    function check_not_string_heat_capacity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_capacity_unit_t) :: maybe_heat_capacity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_capacity unit", json_null_t())])
        maybe_heat_capacity_unit = fallible_heat_capacity_unit_t(json, "heat_capacity unit")
        errors = maybe_heat_capacity_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."heat_capacity unit", &
                errors%to_string())
    end function

    function check_not_string_heat_transfer_coefficient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_transfer_coefficient_unit_t) :: maybe_heat_transfer_coefficient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_transfer_coefficient unit", json_null_t())])
        maybe_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t(json, "heat_transfer_coefficient unit")
        errors = maybe_heat_transfer_coefficient_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."heat_transfer_coefficient unit", &
                errors%to_string())
    end function

    function check_not_string_insulance_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_insulance_unit_t) :: maybe_insulance_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "insulance unit", json_null_t())])
        maybe_insulance_unit = fallible_insulance_unit_t(json, "insulance unit")
        errors = maybe_insulance_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."insulance unit", &
                errors%to_string())
    end function

    function check_not_string_intensity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_intensity_unit_t) :: maybe_intensity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "intensity unit", json_null_t())])
        maybe_intensity_unit = fallible_intensity_unit_t(json, "intensity unit")
        errors = maybe_intensity_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."intensity unit", &
                errors%to_string())
    end function

    function check_not_string_inverse_molar_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_inverse_molar_mass_unit_t) :: maybe_inverse_molar_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "inverse_molar_mass unit", json_null_t())])
        maybe_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t(json, "inverse_molar_mass unit")
        errors = maybe_inverse_molar_mass_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."inverse_molar_mass unit", &
                errors%to_string())
    end function

    function check_not_string_length_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_length_unit_t) :: maybe_length_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "length unit", json_null_t())])
        maybe_length_unit = fallible_length_unit_t(json, "length unit")
        errors = maybe_length_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."length unit", &
                errors%to_string())
    end function

    function check_not_string_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_unit_t) :: maybe_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass unit", json_null_t())])
        maybe_mass_unit = fallible_mass_unit_t(json, "mass unit")
        errors = maybe_mass_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."mass unit", &
                errors%to_string())
    end function

    function check_not_string_mass_flux_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_flux_unit_t) :: maybe_mass_flux_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_flux unit", json_null_t())])
        maybe_mass_flux_unit = fallible_mass_flux_unit_t(json, "mass_flux unit")
        errors = maybe_mass_flux_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."mass_flux unit", &
                errors%to_string())
    end function

    function check_not_string_mass_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_rate_unit_t) :: maybe_mass_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_rate unit", json_null_t())])
        maybe_mass_rate_unit = fallible_mass_rate_unit_t(json, "mass_rate unit")
        errors = maybe_mass_rate_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."mass_rate unit", &
                errors%to_string())
    end function

    function check_not_string_molar_density_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_density_unit_t) :: maybe_molar_density_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_density unit", json_null_t())])
        maybe_molar_density_unit = fallible_molar_density_unit_t(json, "molar_density unit")
        errors = maybe_molar_density_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."molar_density unit", &
                errors%to_string())
    end function

    function check_not_string_molar_enthalpy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_enthalpy_unit_t) :: maybe_molar_enthalpy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_enthalpy unit", json_null_t())])
        maybe_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t(json, "molar_enthalpy unit")
        errors = maybe_molar_enthalpy_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."molar_enthalpy unit", &
                errors%to_string())
    end function

    function check_not_string_molar_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_mass_unit_t) :: maybe_molar_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_mass unit", json_null_t())])
        maybe_molar_mass_unit = fallible_molar_mass_unit_t(json, "molar_mass unit")
        errors = maybe_molar_mass_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."molar_mass unit", &
                errors%to_string())
    end function

    function check_not_string_molar_specific_heat_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_specific_heat_unit_t) :: maybe_molar_specific_heat_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_specific_heat unit", json_null_t())])
        maybe_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t(json, "molar_specific_heat unit")
        errors = maybe_molar_specific_heat_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."molar_specific_heat unit", &
                errors%to_string())
    end function

    function check_not_string_power_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_power_unit_t) :: maybe_power_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "power unit", json_null_t())])
        maybe_power_unit = fallible_power_unit_t(json, "power unit")
        errors = maybe_power_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."power unit", &
                errors%to_string())
    end function

    function check_not_string_pressure_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_pressure_unit_t) :: maybe_pressure_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "pressure unit", json_null_t())])
        maybe_pressure_unit = fallible_pressure_unit_t(json, "pressure unit")
        errors = maybe_pressure_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."pressure unit", &
                errors%to_string())
    end function

    function check_not_string_specific_heat_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_specific_heat_unit_t) :: maybe_specific_heat_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "specific_heat unit", json_null_t())])
        maybe_specific_heat_unit = fallible_specific_heat_unit_t(json, "specific_heat unit")
        errors = maybe_specific_heat_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."specific_heat unit", &
                errors%to_string())
    end function

    function check_not_string_speed_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_speed_unit_t) :: maybe_speed_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "speed unit", json_null_t())])
        maybe_speed_unit = fallible_speed_unit_t(json, "speed unit")
        errors = maybe_speed_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."speed unit", &
                errors%to_string())
    end function

    function check_not_string_temperature_gradient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_gradient_unit_t) :: maybe_temperature_gradient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature_gradient unit", json_null_t())])
        maybe_temperature_gradient_unit = fallible_temperature_gradient_unit_t(json, "temperature_gradient unit")
        errors = maybe_temperature_gradient_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."temperature_gradient unit", &
                errors%to_string())
    end function

    function check_not_string_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_unit_t) :: maybe_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature unit", json_null_t())])
        maybe_temperature_unit = fallible_temperature_unit_t(json, "temperature unit")
        errors = maybe_temperature_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."temperature unit", &
                errors%to_string())
    end function

    function check_not_string_thermal_conductivity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_conductivity_unit_t) :: maybe_thermal_conductivity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_conductivity unit", json_null_t())])
        maybe_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t(json, "thermal_conductivity unit")
        errors = maybe_thermal_conductivity_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."thermal_conductivity unit", &
                errors%to_string())
    end function

    function check_not_string_thermal_expansion_coefficient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_expansion_coefficient_unit_t) :: maybe_thermal_expansion_coefficient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_expansion_coefficient unit", json_null_t())])
        maybe_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t(json, "thermal_expansion_coefficient unit")
        errors = maybe_thermal_expansion_coefficient_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."thermal_expansion_coefficient unit", &
                errors%to_string())
    end function

    function check_not_string_time_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_time_unit_t) :: maybe_time_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "time unit", json_null_t())])
        maybe_time_unit = fallible_time_unit_t(json, "time unit")
        errors = maybe_time_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."time unit", &
                errors%to_string())
    end function

    function check_not_string_volume_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_unit_t) :: maybe_volume_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume unit", json_null_t())])
        maybe_volume_unit = fallible_volume_unit_t(json, "volume unit")
        errors = maybe_volume_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."volume unit", &
                errors%to_string())
    end function

    function check_not_string_volume_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_rate_unit_t) :: maybe_volume_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume_rate unit", json_null_t())])
        maybe_volume_rate_unit = fallible_volume_rate_unit_t(json, "volume_rate unit")
        errors = maybe_volume_rate_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."volume_rate unit", &
                errors%to_string())
    end function

    function check_not_string_volumetric_heat_capacity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volumetric_heat_capacity_unit_t) :: maybe_volumetric_heat_capacity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volumetric_heat_capacity unit", json_null_t())])
        maybe_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t(json, "volumetric_heat_capacity unit")
        errors = maybe_volumetric_heat_capacity_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."volumetric_heat_capacity unit", &
                errors%to_string())
    end function

    function check_not_string_yank_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_yank_unit_t) :: maybe_yank_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "yank unit", json_null_t())])
        maybe_yank_unit = fallible_yank_unit_t(json, "yank unit")
        errors = maybe_yank_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."yank unit", &
                errors%to_string())
    end function

    function check_parse_error_acceleration_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_acceleration_unit_t) :: maybe_acceleration_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "acceleration unit", json_string_unsafe("invalid"))])
        maybe_acceleration_unit = fallible_acceleration_unit_t(json, "acceleration unit")
        errors = maybe_acceleration_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_amount_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_unit_t) :: maybe_amount_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount unit", json_string_unsafe("invalid"))])
        maybe_amount_unit = fallible_amount_unit_t(json, "amount unit")
        errors = maybe_amount_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_amount_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_rate_unit_t) :: maybe_amount_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_rate unit", json_string_unsafe("invalid"))])
        maybe_amount_rate_unit = fallible_amount_rate_unit_t(json, "amount_rate unit")
        errors = maybe_amount_rate_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_amount_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_unit_t) :: maybe_amount_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature unit", json_string_unsafe("invalid"))])
        maybe_amount_temperature_unit = fallible_amount_temperature_unit_t(json, "amount_temperature unit")
        errors = maybe_amount_temperature_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_amount_temperature_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_amount_temperature_rate_unit_t) :: maybe_amount_temperature_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "amount_temperature_rate unit", json_string_unsafe("invalid"))])
        maybe_amount_temperature_rate_unit = fallible_amount_temperature_rate_unit_t(json, "amount_temperature_rate unit")
        errors = maybe_amount_temperature_rate_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_angle_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_angle_unit_t) :: maybe_angle_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "angle unit", json_string_unsafe("invalid"))])
        maybe_angle_unit = fallible_angle_unit_t(json, "angle unit")
        errors = maybe_angle_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_area_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_area_unit_t) :: maybe_area_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "area unit", json_string_unsafe("invalid"))])
        maybe_area_unit = fallible_area_unit_t(json, "area unit")
        errors = maybe_area_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_conductance_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_conductance_unit_t) :: maybe_conductance_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "conductance unit", json_string_unsafe("invalid"))])
        maybe_conductance_unit = fallible_conductance_unit_t(json, "conductance unit")
        errors = maybe_conductance_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_delta_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_delta_temperature_unit_t) :: maybe_delta_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "delta_temperature unit", json_string_unsafe("invalid"))])
        maybe_delta_temperature_unit = fallible_delta_temperature_unit_t(json, "delta_temperature unit")
        errors = maybe_delta_temperature_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_density_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_density_unit_t) :: maybe_density_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "density unit", json_string_unsafe("invalid"))])
        maybe_density_unit = fallible_density_unit_t(json, "density unit")
        errors = maybe_density_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_diffusivity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_diffusivity_unit_t) :: maybe_diffusivity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "diffusivity unit", json_string_unsafe("invalid"))])
        maybe_diffusivity_unit = fallible_diffusivity_unit_t(json, "diffusivity unit")
        errors = maybe_diffusivity_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_dynamic_viscosity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_dynamic_viscosity_unit_t) :: maybe_dynamic_viscosity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "dynamic_viscosity unit", json_string_unsafe("invalid"))])
        maybe_dynamic_viscosity_unit = fallible_dynamic_viscosity_unit_t(json, "dynamic_viscosity unit")
        errors = maybe_dynamic_viscosity_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_energy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_energy_unit_t) :: maybe_energy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "energy unit", json_string_unsafe("invalid"))])
        maybe_energy_unit = fallible_energy_unit_t(json, "energy unit")
        errors = maybe_energy_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_enthalpy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_enthalpy_unit_t) :: maybe_enthalpy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "enthalpy unit", json_string_unsafe("invalid"))])
        maybe_enthalpy_unit = fallible_enthalpy_unit_t(json, "enthalpy unit")
        errors = maybe_enthalpy_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_fluence_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fluence_unit_t) :: maybe_fluence_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fluence unit", json_string_unsafe("invalid"))])
        maybe_fluence_unit = fallible_fluence_unit_t(json, "fluence unit")
        errors = maybe_fluence_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_force_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_force_unit_t) :: maybe_force_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "force unit", json_string_unsafe("invalid"))])
        maybe_force_unit = fallible_force_unit_t(json, "force unit")
        errors = maybe_force_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_fracture_toughness_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_fracture_toughness_unit_t) :: maybe_fracture_toughness_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "fracture_toughness unit", json_string_unsafe("invalid"))])
        maybe_fracture_toughness_unit = fallible_fracture_toughness_unit_t(json, "fracture_toughness unit")
        errors = maybe_fracture_toughness_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_frequency_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_frequency_unit_t) :: maybe_frequency_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "frequency unit", json_string_unsafe("invalid"))])
        maybe_frequency_unit = fallible_frequency_unit_t(json, "frequency unit")
        errors = maybe_frequency_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_heat_capacity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_capacity_unit_t) :: maybe_heat_capacity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_capacity unit", json_string_unsafe("invalid"))])
        maybe_heat_capacity_unit = fallible_heat_capacity_unit_t(json, "heat_capacity unit")
        errors = maybe_heat_capacity_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_heat_transfer_coefficient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_heat_transfer_coefficient_unit_t) :: maybe_heat_transfer_coefficient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "heat_transfer_coefficient unit", json_string_unsafe("invalid"))])
        maybe_heat_transfer_coefficient_unit = fallible_heat_transfer_coefficient_unit_t(json, "heat_transfer_coefficient unit")
        errors = maybe_heat_transfer_coefficient_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_insulance_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_insulance_unit_t) :: maybe_insulance_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "insulance unit", json_string_unsafe("invalid"))])
        maybe_insulance_unit = fallible_insulance_unit_t(json, "insulance unit")
        errors = maybe_insulance_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_intensity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_intensity_unit_t) :: maybe_intensity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "intensity unit", json_string_unsafe("invalid"))])
        maybe_intensity_unit = fallible_intensity_unit_t(json, "intensity unit")
        errors = maybe_intensity_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_inverse_molar_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_inverse_molar_mass_unit_t) :: maybe_inverse_molar_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "inverse_molar_mass unit", json_string_unsafe("invalid"))])
        maybe_inverse_molar_mass_unit = fallible_inverse_molar_mass_unit_t(json, "inverse_molar_mass unit")
        errors = maybe_inverse_molar_mass_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_length_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_length_unit_t) :: maybe_length_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "length unit", json_string_unsafe("invalid"))])
        maybe_length_unit = fallible_length_unit_t(json, "length unit")
        errors = maybe_length_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_unit_t) :: maybe_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass unit", json_string_unsafe("invalid"))])
        maybe_mass_unit = fallible_mass_unit_t(json, "mass unit")
        errors = maybe_mass_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_mass_flux_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_flux_unit_t) :: maybe_mass_flux_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_flux unit", json_string_unsafe("invalid"))])
        maybe_mass_flux_unit = fallible_mass_flux_unit_t(json, "mass_flux unit")
        errors = maybe_mass_flux_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_mass_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_mass_rate_unit_t) :: maybe_mass_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "mass_rate unit", json_string_unsafe("invalid"))])
        maybe_mass_rate_unit = fallible_mass_rate_unit_t(json, "mass_rate unit")
        errors = maybe_mass_rate_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_molar_density_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_density_unit_t) :: maybe_molar_density_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_density unit", json_string_unsafe("invalid"))])
        maybe_molar_density_unit = fallible_molar_density_unit_t(json, "molar_density unit")
        errors = maybe_molar_density_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_molar_enthalpy_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_enthalpy_unit_t) :: maybe_molar_enthalpy_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_enthalpy unit", json_string_unsafe("invalid"))])
        maybe_molar_enthalpy_unit = fallible_molar_enthalpy_unit_t(json, "molar_enthalpy unit")
        errors = maybe_molar_enthalpy_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_molar_mass_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_mass_unit_t) :: maybe_molar_mass_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_mass unit", json_string_unsafe("invalid"))])
        maybe_molar_mass_unit = fallible_molar_mass_unit_t(json, "molar_mass unit")
        errors = maybe_molar_mass_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_molar_specific_heat_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_molar_specific_heat_unit_t) :: maybe_molar_specific_heat_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "molar_specific_heat unit", json_string_unsafe("invalid"))])
        maybe_molar_specific_heat_unit = fallible_molar_specific_heat_unit_t(json, "molar_specific_heat unit")
        errors = maybe_molar_specific_heat_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_power_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_power_unit_t) :: maybe_power_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "power unit", json_string_unsafe("invalid"))])
        maybe_power_unit = fallible_power_unit_t(json, "power unit")
        errors = maybe_power_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_pressure_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_pressure_unit_t) :: maybe_pressure_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "pressure unit", json_string_unsafe("invalid"))])
        maybe_pressure_unit = fallible_pressure_unit_t(json, "pressure unit")
        errors = maybe_pressure_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_specific_heat_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_specific_heat_unit_t) :: maybe_specific_heat_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "specific_heat unit", json_string_unsafe("invalid"))])
        maybe_specific_heat_unit = fallible_specific_heat_unit_t(json, "specific_heat unit")
        errors = maybe_specific_heat_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_speed_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_speed_unit_t) :: maybe_speed_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "speed unit", json_string_unsafe("invalid"))])
        maybe_speed_unit = fallible_speed_unit_t(json, "speed unit")
        errors = maybe_speed_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_temperature_gradient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_gradient_unit_t) :: maybe_temperature_gradient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature_gradient unit", json_string_unsafe("invalid"))])
        maybe_temperature_gradient_unit = fallible_temperature_gradient_unit_t(json, "temperature_gradient unit")
        errors = maybe_temperature_gradient_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_temperature_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_temperature_unit_t) :: maybe_temperature_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "temperature unit", json_string_unsafe("invalid"))])
        maybe_temperature_unit = fallible_temperature_unit_t(json, "temperature unit")
        errors = maybe_temperature_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_thermal_conductivity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_conductivity_unit_t) :: maybe_thermal_conductivity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_conductivity unit", json_string_unsafe("invalid"))])
        maybe_thermal_conductivity_unit = fallible_thermal_conductivity_unit_t(json, "thermal_conductivity unit")
        errors = maybe_thermal_conductivity_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_thermal_expansion_coefficient_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_thermal_expansion_coefficient_unit_t) :: maybe_thermal_expansion_coefficient_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "thermal_expansion_coefficient unit", json_string_unsafe("invalid"))])
        maybe_thermal_expansion_coefficient_unit = fallible_thermal_expansion_coefficient_unit_t(json, "thermal_expansion_coefficient unit")
        errors = maybe_thermal_expansion_coefficient_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_time_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_time_unit_t) :: maybe_time_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "time unit", json_string_unsafe("invalid"))])
        maybe_time_unit = fallible_time_unit_t(json, "time unit")
        errors = maybe_time_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_volume_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_unit_t) :: maybe_volume_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume unit", json_string_unsafe("invalid"))])
        maybe_volume_unit = fallible_volume_unit_t(json, "volume unit")
        errors = maybe_volume_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_volume_rate_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volume_rate_unit_t) :: maybe_volume_rate_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volume_rate unit", json_string_unsafe("invalid"))])
        maybe_volume_rate_unit = fallible_volume_rate_unit_t(json, "volume_rate unit")
        errors = maybe_volume_rate_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_volumetric_heat_capacity_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_volumetric_heat_capacity_unit_t) :: maybe_volumetric_heat_capacity_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "volumetric_heat_capacity unit", json_string_unsafe("invalid"))])
        maybe_volumetric_heat_capacity_unit = fallible_volumetric_heat_capacity_unit_t(json, "volumetric_heat_capacity unit")
        errors = maybe_volumetric_heat_capacity_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function

    function check_parse_error_yank_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_yank_unit_t) :: maybe_yank_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "yank unit", json_string_unsafe("invalid"))])
        maybe_yank_unit = fallible_yank_unit_t(json, "yank unit")
        errors = maybe_yank_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function
end module