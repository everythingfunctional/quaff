#!/usr/bin/env python

from dataclasses import dataclass, field
from pathlib import Path
import subprocess
import os

@dataclass
class Unit:
    name: str
    symbol: str
    conversion_factor: str
    conversion_difference: str = ""

@dataclass
class Standard:
    pass

@dataclass
class Temperature:
    pass

@dataclass
class Angle:
    pass

Specific = Standard | Temperature | Angle

@dataclass
class Frac:
    numer: int
    denom: int

    def __add__(self, o):
        if self.denom == o.denom:
            return Frac(self.numer + o.numer, self.denom)
        else:
            return Frac(self.numer*o.denom + o.numer*self.denom, self.denom*o.denom)

    def __sub__(self, o):
        if self.denom == o.denom:
            return Frac(self.numer - o.numer, self.denom)
        else:
            return Frac(self.numer*o.denom - o.numer*self.denom, self.denom*o.denom)

    def __eq__(self, o):
        return self.numer == o.numer and self.denom == o.denom

zero = Frac(0, 1)
one = Frac(1, 1)
two = Frac(2, 1)
three = Frac(3, 1)
half = Frac(1, 2)
minus_one = Frac(-1, 1)
minus_two = Frac(-2, 1)
minus_three = Frac(-3, 1)
minus_half = Frac(-1, 2)

@dataclass
class FundamentalPowers:
    length: Frac = field(default_factory=lambda: zero)
    mass: Frac = field(default_factory=lambda: zero)
    time: Frac = field(default_factory=lambda: zero)
    temperature: Frac = field(default_factory=lambda: zero)
    temp_diff: Frac = field(default_factory=lambda: zero)
    amount: Frac = field(default_factory=lambda: zero)
    angle: Frac = field(default_factory=lambda: zero)

    def __add__(self, o):
        return FundamentalPowers(
            length = self.length + o.length,
            mass = self.mass + o.mass,
            time = self.time + o.time,
            temperature = self.temperature + o.temperature,
            temp_diff = self.temp_diff + o.temp_diff,
            amount = self.amount + o.amount,
            angle = self.angle + o.angle
        )

    def __sub__(self, o):
        return FundamentalPowers(
            length = self.length - o.length,
            mass = self.mass - o.mass,
            time = self.time - o.time,
            temperature = self.temperature - o.temperature,
            temp_diff = self.temp_diff - o.temp_diff,
            amount = self.amount - o.amount,
            angle = self.angle - o.angle
        )

    def __eq__(self, o):
        return all([
            self.length == o.length,
            self.mass == o.mass,
            self.time == o.time,
            self.temperature == o.temperature,
            self.temp_diff == o.temp_diff,
            self.amount == o.amount,
            self.angle == o.angle
        ])

unitless = FundamentalPowers()
delta_temp_fundamental = FundamentalPowers(temp_diff=one)

@dataclass
class Quantity:
    name: str
    fundamental: FundamentalPowers
    units: [Unit] # Note that first unit is default unit
    specific: Specific = field(default_factory=lambda: Standard())

@dataclass
class MultOp:
    lhs_name: str
    lhs_units: str
    lhs_sym: str
    rhs_name: str
    rhs_units: str
    rhs_sym: str
    result_name: str
    result_units: str
    result_sym: str

@dataclass
class DivOp:
    numer_name: str
    numer_units: str
    numer_sym: str
    denom_name: str
    denom_units: str
    denom_sym: str
    quot_name: str
    quot_units: str
    quot_sym: str

InterOp = MultOp | DivOp

quantities = [
    Quantity(
        name = "acceleration",
        fundamental = FundamentalPowers(
            length = one,
            time = minus_two,
        ),
        units = [
            Unit(
                name = "meters_per_square_second",
                symbol = "m/s^2",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "centimeters_per_square_second",
                symbol = "cm/s^2",
                conversion_factor = "CENTIMETERS_PER_SQUARE_SECOND_PER_METERS_PER_SQUARE_SECOND",
            ),
            Unit(
                name = "feet_per_square_second",
                symbol = "ft/s^2",
                conversion_factor = "FEET_PER_SQUARE_SECOND_PER_METERS_PER_SQUARE_SECOND",
            )
        ]
    ),
    Quantity(
        name = "amount",
        fundamental = FundamentalPowers(
            amount = one,
        ),
        units = [
            Unit(
                name = "mols",
                symbol = "mol",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "particles",
                symbol = "particles",
                conversion_factor = "AVOGADROS_NUMBER",
            ),
            Unit(
                name = "kilomols",
                symbol = "kmol",
                conversion_factor = "KILO_PER_BASE",
            )
        ]
    ),
    Quantity(
        name = "amount_rate",
        fundamental = FundamentalPowers(
            time = minus_one,
            amount = one,
        ),
        units = [
            Unit(
                name = "mols_per_second",
                symbol = "mol/s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilomols_per_second",
                symbol = "kmol/s",
                conversion_factor = "KILO_PER_BASE",
            )
        ]
    ),
    Quantity(
        name = "amount_temperature",
        fundamental = FundamentalPowers(
            temp_diff = one,
            amount = one
        ),
        units = [
            Unit(
                name = "mols_kelvin",
                symbol = "mol K",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilomols_kelvin",
                symbol = "kmol K",
                conversion_factor = "KILO_PER_BASE",
            )
        ]
    ),
    Quantity(
        name = "amount_temperature_rate",
        fundamental = FundamentalPowers(
            time = minus_one,
            temp_diff = one,
            amount = one
        ),
        units = [
            Unit(
                name = "mols_kelvin_per_second",
                symbol = "(mol K)/s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilomols_kelvin_per_second",
                symbol = "(kmol K)/s",
                conversion_factor = "KILO_PER_BASE",
            )
        ]
    ),
    Quantity(
        name = "angle",
        specific = Angle(),
        fundamental = FundamentalPowers(
            angle = one
        ),
        units = [
            Unit(
                name = "radians",
                symbol = "rad",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "degrees",
                symbol = "deg",
                conversion_factor = "DEGREES_PER_RADIAN",
            )
        ]
    ),
    Quantity(
        name = "area",
        fundamental = FundamentalPowers(
            length = two
        ),
        units = [
            Unit(
                name = "square_meters",
                symbol = "m^2",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "square_centimeters",
                symbol = "cm^2",
                conversion_factor = "SQUARE_CENTIMETERS_PER_SQUARE_METER",
            ),
            Unit(
                name = "square_millimeters",
                symbol = "mm^2",
                conversion_factor = "square_millimeters_per_square_meter"
            ),
            Unit(
                name = "square_feet",
                symbol = "ft^2",
                conversion_factor = "SQUARE_FEET_PER_SQUARE_METER",
            ),
            Unit(
                name = "square_inches",
                symbol = "in^2",
                conversion_factor = "SQUARE_INCHES_PER_SQUARE_METER",
            )
        ]
    ),
    Quantity(
        name = "conductance",
        fundamental = FundamentalPowers(
            length = two,
            mass = one,
            time = minus_three,
            temp_diff = minus_one
        ),
        units = [
            Unit(
                name = "watts_per_kelvin",
                symbol = "W/K",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilowatts_per_kelvin",
                symbol = "kW/K",
                conversion_factor = "KILO_PER_BASE",
            )
        ]
    ),
    Quantity(
        name = "delta_temperature",
        fundamental = FundamentalPowers(
            temp_diff = one
        ),
        units = [
            Unit(
                name = "delta_kelvin",
                symbol = "K",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "delta_celsius",
                symbol = "C",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "delta_rankine",
                symbol = "R",
                conversion_factor = "RANKINE_PER_KELVIN",
            ),
            Unit(
                name = "delta_fahrenheit",
                symbol = "F",
                conversion_factor = "RANKINE_PER_KELVIN",
            )
        ]
    ),
    Quantity(
        name = "density",
        fundamental = FundamentalPowers(
            length = minus_three,
            mass = one
        ),
        units = [
            Unit(
                name = "kilograms_per_cubic_meter",
                symbol = "kg/m^3",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "grams_per_cubic_meter",
                symbol = "g/m^3",
                conversion_factor = "GRAMS_PER_CUBIC_METER_PER_KILOGRAMS_PER_CUBIC_METER",
            ),
            Unit(
                name = "grams_per_cubic_centimeter",
                symbol = "g/cm^3",
                conversion_factor = "GRAMS_PER_CUBIC_CENTIMETER_PER_KILOGRAMS_PER_CUBIC_METER",
            ),
            Unit(
                name = "pounds_per_cubic_foot",
                symbol = "lbm/ft^3",
                conversion_factor = "POUNDS_PER_CUBIC_FOOT_PER_KILOGRAMS_PER_CUBIC_METER",
            )
        ]
    ),
    Quantity(
        name = "diffusivity",
        fundamental = FundamentalPowers(
            length = two,
            time = minus_one
        ),
        units = [
            Unit(
                name = "square_meters_per_second",
                symbol = "m^2/s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "square_centimeters_per_second",
                symbol = "cm^2/s",
                conversion_factor = "SQUARE_CENTIMETERS_PER_SQUARE_METER",
            )
        ]
    ),
    Quantity(
        name = "dynamic_viscosity",
        fundamental = FundamentalPowers(
            length = minus_one,
            mass = one,
            time = minus_one
        ),
        units = [
            Unit(
                name = "pascal_seconds",
                symbol = "Pa s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilograms_per_meter_second",
                symbol = "kg/(m s)",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "megapascal_seconds",
                symbol = "MPa s",
                conversion_factor = "MEGAPASCAL_SECONDS_PER_PASCAL_SECOND",
            )
        ]
    ),
    Quantity(
        name = "energy",
        fundamental = FundamentalPowers(
            length = two,
            mass = one,
            time = minus_two
        ),
        units = [
            Unit(
                name = "joules",
                symbol = "J",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilojoules",
                symbol = "kJ",
                conversion_factor = "KILOJOULES_PER_JOULE",
            ),
            Unit(
                name = "calories",
                symbol = "cal",
                conversion_factor = "CALORIES_PER_JOULE",
            ),
            Unit(
                name = "btu",
                symbol = "BTU",
                conversion_factor = "BTU_PER_JOULE",
            ),
            Unit(
                name = "megabtu",
                symbol = "MBTU",
                conversion_factor = "MEGABTU_PER_JOULE",
            ),
            Unit(
                name = "megawatt_days",
                symbol = "MW d",
                conversion_factor = "MEGAWATT_DAYS_PER_JOULE",
            ),
            Unit(
                name = "pounds_force_feet",
                symbol = "lbf ft",
                conversion_factor = "POUNDS_FORCE_FOOT_PER_NEWTON_METER",
            )
        ]
    ),
    Quantity(
        name = "enthalpy",
        fundamental = FundamentalPowers(
            length = two,
            time = minus_two
        ),
        units = [
            Unit(
                name = "joules_per_kilogram",
                symbol = "J/kg",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilojoules_per_kilogram",
                symbol = "kJ/kg",
                conversion_factor = "KILOJOULES_PER_KILOGRAM_PER_JOULES_PER_KILOGRAM",
            )
        ]
    ),
    Quantity(
        name = "fluence",
        fundamental = FundamentalPowers(
            length = minus_two
        ),
        units = [
            Unit(
                name = "particles_per_square_meter",
                symbol = "particles/m^2",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "particles_per_square_centimeter",
                symbol = "particles/cm^2",
                conversion_factor = "PARTICLES_PER_SQUARE_CENTIMETER_PER_PARTICLES_PER_SQUARE_METER",
            )
        ]
    ),
    Quantity(
        name = "force",
        fundamental = FundamentalPowers(
            length = one,
            mass = one,
            time = minus_two
        ),
        units = [
            Unit(
                name = "newtons",
                symbol = "N",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "millinewtons",
                symbol = "mN",
                conversion_factor = "MILLINEWTONS_PER_NEWTON",
            ),
            Unit(
                name = "pounds_force",
                symbol = "lbf",
                conversion_factor = "POUNDS_PER_NEWTON",
            ),
            Unit(
                name = "kiloponds",
                symbol = "kp",
                conversion_factor = "KILOPONDS_PER_NEWTON",
            ),
            Unit(
                name = "dynes",
                symbol = "dyn",
                conversion_factor = "DYNES_PER_NEWTON",
            )
        ]
    ),
    Quantity(
        name = "fracture_toughness",
        fundamental = FundamentalPowers(
            length = minus_half,
            mass = one,
            time = minus_two
        ),
        units = [
            Unit(
                name = "pascal_root_meter",
                symbol = "Pa m^0.5",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "megapascal_root_meter",
                symbol = "MPa m^0.5",
                conversion_factor = "MEGAPASCAL_ROOT_METER_PER_PASCAL_ROOT_METER",
            ),
            Unit(
                name = "ksi_root_inch",
                symbol = "ksi in^0.5",
                conversion_factor = "KSI_ROOT_INCH_PER_PASCAL_ROOT_METER",
            )
        ]
    ),
    Quantity(
        name = "frequency",
        fundamental = FundamentalPowers(
            time = minus_one
        ),
        units = [
            Unit(
                name = "hertz",
                symbol = "Hz",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "per_second",
                symbol = "1/s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "per_minute",
                symbol = "1/m",
                conversion_factor = "SECONDS_PER_MINUTE",
            ),
            Unit(
                name = "per_year",
                symbol = "1/yr",
                conversion_factor = "SECONDS_PER_YEAR",
            )
        ]
    ),
    Quantity(
        name = "heat_capacity",
        fundamental = FundamentalPowers(
            length = two,
            mass = one,
            time = minus_two,
            temp_diff = minus_one
        ),
        units = [
            Unit(
                name = "joules_per_kelvin",
                symbol = "J/K",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilojoules_per_kelvin",
                symbol = "kJ/K",
                conversion_factor = "KILOJOULES_PER_JOULE",
            )
        ]
    ),
    Quantity(
        name = "heat_transfer_coefficient",
        fundamental = FundamentalPowers(
            mass = one,
            time = minus_three,
            temp_diff = minus_one
        ),
        units = [
            Unit(
                name = "WATTS_PER_SQUARE_METER_KELVIN",
                symbol = "W/(m^2 K)",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "btu_per_hour_square_feet_rankine",
                symbol = "BTU/(hr ft^2 R)",
                conversion_factor = "BTU_PER_HR_SQ_FT_RANKINE_PER_WATTS_PER_SQUARE_METER_KELVIN",
            ),
            Unit(
                name = "btu_per_hour_square_feet_fahrenheit",
                symbol = "BTU/(hr ft^2 F)",
                conversion_factor = "BTU_PER_HR_SQ_FT_RANKINE_PER_WATTS_PER_SQUARE_METER_KELVIN",
            )
        ]
    ),
    Quantity(
        name = "insulance",
        fundamental = FundamentalPowers(
            mass = minus_one,
            time = three,
            temp_diff = one
        ),
        units = [
            Unit(
                name = "kelvin_square_meters_per_watt",
                symbol = "(K m^2)/W",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kelvin_square_meters_per_megawatt",
                symbol = "(K m^2)/MW",
                conversion_factor = "1/MEGAWATTS_PER_WATT",
            )
        ]
    ),
    Quantity(
        name = "intensity",
        fundamental = FundamentalPowers(
            mass = one,
            time = minus_three
        ),
        units = [
            Unit(
                name = "watts_per_square_meter",
                symbol = "W/m^2",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "watts_per_square_centimeter",
                symbol = "W/cm^2",
                conversion_factor = "1/SQUARE_CENTIMETERS_PER_SQUARE_METER",
            )
        ]
    ),
    Quantity(
        name = "inverse_molar_mass",
        fundamental = FundamentalPowers(
            mass = minus_one,
            amount = one
        ),
        units = [
            Unit(
                name = "mols_per_kilogram",
                symbol = "mol/kg",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "mols_per_gram",
                symbol = "mol/g",
                conversion_factor = "MOLS_PER_GRAM_PER_MOLS_PER_KILOGRAM",
            )
        ]
    ),
    Quantity(
        name = "length",
        fundamental = FundamentalPowers(
            length = one
        ),
        units = [
            Unit(
                name = "meters",
                symbol = "m",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "centimeters",
                symbol = "cm",
                conversion_factor = "CENTIMETERS_PER_METER",
            ),
            Unit(
                name = "millimeters",
                symbol = "mm",
                conversion_factor = "MILLIMETERS_PER_METER",
            ),
            Unit(
                name = "micrometers",
                symbol = "um",
                conversion_factor = "MICROMETERS_PER_METER",
            ),
            Unit(
                name = "feet",
                symbol = "ft",
                conversion_factor = "FEET_PER_METER",
            ),
            Unit(
                name = "inches",
                symbol = "in",
                conversion_factor = "INCHES_PER_METER",
            ),
            Unit(
                name = "microinches",
                symbol = "uin",
                conversion_factor = "MICROINCHES_PER_METER",
            )
        ]
    ),
    Quantity(
        name = "mass",
        fundamental = FundamentalPowers(
            mass = one
        ),
        units = [
            Unit(
                name = "kilograms",
                symbol = "kg",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "grams",
                symbol = "g",
                conversion_factor = "GRAMS_PER_KILOGRAM",
            ),
            Unit(
                name = "pounds_mass",
                symbol = "lbm",
                conversion_factor = "pounds_per_kilogram",
            ),
            Unit(
                name = "tons",
                symbol = "t",
                conversion_factor = "tons_per_kilogram",
            )
        ]
    ),
    Quantity(
        name = "mass_flux",
        fundamental = FundamentalPowers(
            length = minus_two,
            mass = one,
            time = minus_one
        ),
        units = [
            Unit(
                name = "kilograms_per_square_meter_seconds",
                symbol = "kg/(m^2 s)",
                conversion_factor = "1.d0"
            ),
            Unit(
                name = "grams_per_square_meter_seconds",
                symbol = "g/(m^2 s)",
                conversion_factor = "grams_per_kilogram"
            )
        ]
    ),
    Quantity(
        name = "mass_rate",
        fundamental = FundamentalPowers(
            mass = one,
            time = minus_one
        ),
        units = [
            Unit(
                name = "kilograms_per_second",
                symbol = "kg/s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "grams_per_second",
                symbol = "g/s",
                conversion_factor = "GRAMS_PER_KILOGRAM",
            )
        ]
    ),
    Quantity(
        name = "molar_density",
        fundamental = FundamentalPowers(
            length = minus_three,
            amount = one
        ),
        units = [
            Unit(
                name = "mols_per_cubic_meter",
                symbol = "mol/m^3",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "mols_per_cubic_centimeter",
                symbol = "mol/cm^3",
                conversion_factor = "1.d0/CUBIC_CENTIMETERS_PER_CUBIC_METER",
            )
        ]
    ),
    Quantity(
        name = "molar_enthalpy",
        fundamental = FundamentalPowers(
            length = two,
            mass = one,
            time = minus_two,
            amount = minus_one
        ),
        units = [
            Unit(
                name = "joules_per_mol",
                symbol = "J/mol",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilojoules_per_mol",
                symbol = "kJ/mol",
                conversion_factor = "KILOJOULES_PER_MOL_PER_JOULES_PER_MOL",
            )
        ]
    ),
    Quantity(
        name = "molar_mass",
        fundamental = FundamentalPowers(
            mass = one,
            amount = minus_one
        ),
        units = [
            Unit(
                name = "kilograms_per_mol",
                symbol = "kg/mol",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "grams_per_mol",
                symbol = "g/mol",
                conversion_factor = "GRAMS_PER_MOL_PER_KILOGRAMS_PER_MOL",
            )
        ]
    ),
    Quantity(
        name = "molar_specific_heat",
        fundamental = FundamentalPowers(
            length = two,
            mass = one,
            time = minus_two,
            temp_diff = minus_one,
            amount = minus_one
        ),
        units = [
            Unit(
                name = "joules_per_kelvin_mol",
                symbol = "J/(K mol)",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilojoules_per_kelvin_mol",
                symbol = "kJ/(K mol)",
                conversion_factor = "KILOJOULES_PER_KELVIN_MOL_PER_JOULES_PER_KELVIN_MOL",
            )
        ]
    ),
    Quantity(
        name = "power",
        fundamental = FundamentalPowers(
            length = two,
            mass = one,
            time = minus_three
        ),
        units = [
            Unit(
                name = "watts",
                symbol = "W",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "megawatts",
                symbol = "MW",
                conversion_factor = "MEGAWATTS_PER_WATT",
            ),
            Unit(
                name = "btu_per_hour",
                symbol = "BTU/h",
                conversion_factor = "BTU_PER_HOUR_PER_WATT",
            ),
            Unit(
                name = "megabtu_per_hour",
                symbol = "MBTU/h",
                conversion_factor = "MEGABTU_PER_HOUR_PER_WATT",
            ),
            Unit(
                name = "calories_per_second",
                symbol = "cal/s",
                conversion_factor = "CALORIES_PER_SECOND_PER_WATT",
            )
        ]
    ),
    Quantity(
        name = "pressure",
        fundamental = FundamentalPowers(
            length = minus_one,
            mass = one,
            time = minus_two
        ),
        units = [
            Unit(
                name = "pascals",
                symbol = "Pa",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kilopascals",
                symbol = "kPa",
                conversion_factor = "KILOPASCALS_PER_PASCAL",
            ),
            Unit(
                name = "megapascals",
                symbol = "MPa",
                conversion_factor = "MEGAPASCALS_PER_PASCAL",
            ),
            Unit(
                name = "pounds_per_square_inch",
                symbol = "psi",
                conversion_factor = "POUNDS_PER_SQUARE_INCH_PER_PASCAL",
            ),
            Unit(
                name = "kilopounds_per_square_inch",
                symbol = "ksi",
                conversion_factor = "KILOPOUNDS_PER_SQUARE_INCH_PER_PASCAL",
            ),
            Unit(
                name = "atmospheres",
                symbol = "atm",
                conversion_factor = "ATMOSPHERES_PER_PASCAL",
            ),
            Unit(
                name = "bar",
                symbol = "bar",
                conversion_factor = "BAR_PER_PASCAL",
            ),
            Unit(
                name = "kiloponds_per_square_centimeter",
                symbol = "kp/cm^2",
                conversion_factor = "KILOPONDS_PER_SQUARE_CENTIMETER_PER_PASCAL",
            ),
            Unit(
                name = "dynes_per_square_centimeter",
                symbol = "dyn/cm^2",
                conversion_factor = "DYNES_PER_SQUARE_CENTIMETER_PER_PASCAL",
            )
        ]
    ),
    Quantity(
        name = "specific_heat",
        fundamental = FundamentalPowers(
            length = two,
            time = minus_two,
            temp_diff = minus_one
        ),
        units = [
            Unit(
                name = "joules_per_kilogram_kelvin",
                symbol = "J/(kg K)",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "btu_per_pounds_fahrenheit",
                symbol = "BTU/(lbm F)",
                conversion_factor = "BTU_PER_POUNDS_RANKINE_PER_JOULES_PER_KILOGRAM_KELVIN",
            ),
            Unit(
                name = "btu_per_pounds_rankine",
                symbol = "BTU/(lbm R)",
                conversion_factor = "BTU_PER_POUNDS_RANKINE_PER_JOULES_PER_KILOGRAM_KELVIN",
            )
        ]
    ),
    Quantity(
        name = "speed",
        fundamental = FundamentalPowers(
            length = one,
            time = minus_one
        ),
        units = [
            Unit(
                name = "meters_per_second",
                symbol = "m/s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "centimeters_per_second",
                symbol = "cm/s",
                conversion_factor = "CENTIMETERS_PER_SECOND_PER_METERS_PER_SECOND",
            ),
            Unit(
                name = "millimeters_per_second",
                symbol = "mm/s",
                conversion_factor = "MILLIMETERS_PER_SECOND_PER_METERS_PER_SECOND",
            ),
            Unit(
                name = "feet_per_second",
                symbol = "ft/s",
                conversion_factor = "FEET_PER_SECOND_PER_METERS_PER_SECOND",
            )
        ]
    ),
    Quantity(
        name = "temperature_gradient",
        fundamental = FundamentalPowers(
            length = minus_one,
            temp_diff = one
        ),
        units = [
            Unit(
                name = "kelvin_per_meter",
                symbol = "K/m",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "kelvin_per_centimeter",
                symbol = "K/cm",
                conversion_factor = "1/CENTIMETERS_PER_METER",
            )
        ]
    ),
    Quantity(
        name = "temperature",
        specific = Temperature(),
        fundamental = FundamentalPowers(
            temperature = one
        ),
        units = [
            Unit(
                name = "kelvin",
                symbol = "K",
                conversion_factor = "1.d0",
                conversion_difference = "0.d0"
            ),
            Unit(
                name = "celsius",
                symbol = "C",
                conversion_factor = "1.d0",
                conversion_difference = "CELSIUS_KELVIN_DIFFERENCE"
            ),
            Unit(
                name = "rankine",
                symbol = "R",
                conversion_factor = "RANKINE_PER_KELVIN",
                conversion_difference = "0.d0"
            ),
            Unit(
                name = "fahrenheit",
                symbol = "F",
                conversion_factor = "RANKINE_PER_KELVIN",
                conversion_difference = "FAHRENHEIT_RANKINE_DIFFERENCE"
            )
        ]
    ),
    Quantity(
        name = "thermal_conductivity",
        fundamental = FundamentalPowers(
            length = one,
            mass = one,
            time = minus_three,
            temp_diff = minus_one
        ),
        units = [
            Unit(
                name = "watts_per_meter_kelvin",
                symbol = "W/(m K)",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "watts_per_centimeter_kelvin",
                symbol = "W/(cm K)",
                conversion_factor = "WATTS_PER_CENTIMETER_KELVIN_PER_WATTS_PER_METER_KELVIN",
            ),
            Unit(
                name = "btu_per_hour_feet_fahrenheit",
                symbol = "BTU/(h ft F)",
                conversion_factor = "BTU_PER_HOUR_FEET_RANKINE_PER_WATTS_PER_METER_KELVIN",
            ),
            Unit(
                name = "btu_per_hour_feet_rankine",
                symbol = "BTU/(h ft R)",
                conversion_factor = "BTU_PER_HOUR_FEET_RANKINE_PER_WATTS_PER_METER_KELVIN",
            ),
            Unit(
                name = "calories_per_second_centimeter_kelvin",
                symbol = "cal/(s cm K)",
                conversion_factor = "CAL_PER_SEC_CM_K_PER_WATTS_PER_METER_KELVIN",
            )
        ]
    ),
    Quantity(
        name = "thermal_expansion_coefficient",
        fundamental = FundamentalPowers(
            temp_diff = minus_one
        ),
        units = [
            Unit(
                name = "per_kelvin",
                symbol = "1/K",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "per_rankine",
                symbol = "1/R",
                conversion_factor = "PER_RANKINE_PER_KELVIN",
            ),
            Unit(
                name = "per_fahrenheit",
                symbol = "1/F",
                conversion_factor = "PER_RANKINE_PER_KELVIN",
            )
        ]
    ),
    Quantity(
        name = "time",
        fundamental = FundamentalPowers(
            time = one
        ),
        units = [
            Unit(
                name = "seconds",
                symbol = "s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "minutes",
                symbol = "m",
                conversion_factor = "MINUTES_PER_SECOND",
            ),
            Unit(
                name = "hours",
                symbol = "h",
                conversion_factor = "HOURS_PER_SECOND",
            ),
            Unit(
                name = "days",
                symbol = "d",
                conversion_factor = "DAYS_PER_SECOND",
            ),
            Unit(
                name = "years",
                symbol = "yr",
                conversion_factor = "YEARS_PER_SECOND",
            ),
            Unit(
                name = "milliseconds",
                symbol = "ms",
                conversion_factor = "MILLISECONDS_PER_SECOND",
            ),
            Unit(
                name = "microseconds",
                symbol = "us",
                conversion_factor = "MICROSECONDS_PER_SECOND",
            )
        ]
    ),
    Quantity(
        name = "volume",
        fundamental = FundamentalPowers(
            length = three
        ),
        units = [
            Unit(
                name = "cubic_meters",
                symbol = "m^3",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "cubic_centimeters",
                symbol = "cm^3",
                conversion_factor = "CUBIC_CENTIMETERS_PER_CUBIC_METER",
            ),
            Unit(
                name = "cubic_millimeters",
                symbol = "mm^3",
                conversion_factor = "CUBIC_MILLIMETERS_PER_CUBIC_METER",
            ),
            Unit(
                name = "liters",
                symbol = "L",
                conversion_factor = "LITERS_PER_CUBIC_METER",
            ),
            Unit(
                name = "cubic_inches",
                symbol = "in^3",
                conversion_factor = "CUBIC_INCHES_PER_CUBIC_METER",
            ),
            Unit(
                name = "cubic_feet",
                symbol = "ft^3",
                conversion_factor = "CUBIC_FEET_PER_CUBIC_METER",
            )
        ]
    ),
    Quantity(
        name = "volume_rate",
        fundamental = FundamentalPowers(
            length = three,
            time = minus_one
        ),
        units = [
            Unit(
                name = "cubic_meters_per_second",
                symbol = "m^3/s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "cubic_centimeters_per_second",
                symbol = "cm^3/s",
                conversion_factor = "CUBIC_CENTIMETERS_PER_CUBIC_METER",
            )
        ]
    ),
    Quantity(
        name = "volumetric_heat_capacity",
        fundamental = FundamentalPowers(
            length = minus_one,
            mass = one,
            time = minus_two,
            temp_diff = minus_one
        ),
        units = [
            Unit(
                name = "pascals_per_kelvin",
                symbol = "Pa/K",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "joules_per_cubic_meter_kelvin",
                symbol = "J/(m^3 K)",
                conversion_factor = "1.d0",
            )
        ]
    ),
    Quantity(
        name = "yank",
        fundamental = FundamentalPowers(
            length = one,
            mass = one,
            time = minus_three
        ),
        units = [
            Unit(
                name = "newtons_per_second",
                symbol = "N/s",
                conversion_factor = "1.d0",
            ),
            Unit(
                name = "watts_per_meter",
                symbol = "W/m",
                conversion_factor = "1.d0",
            )
        ]
    )
]

def valid_interops(quantities : [Quantity]) -> [InterOp]:
    ops = []
    for q1 in quantities:
        for q2 in quantities:
            if (unitless - q1.fundamental) == q2.fundamental:
                ops.append(DivOp(
                    "number",
                    "",
                    "",
                    q1.name,
                    q1.units[0].name,
                    q1.units[0].symbol,
                    q2.name,
                    q2.units[0].name,
                    q2.units[0].symbol
                ))
            mul_res = q1.fundamental + q2.fundamental
            if mul_res == unitless:
                ops.append(MultOp(
                        q1.name,
                        q1.units[0].name,
                        q1.units[0].symbol,
                        q2.name,
                        q2.units[0].name,
                        q2.units[0].symbol,
                        "number",
                        "",
                        ""
                    ))
            else:
                for q3 in quantities:
                    if mul_res == q3.fundamental:
                        mulop = MultOp(
                            q1.name,
                            q1.units[0].name,
                            q1.units[0].symbol,
                            q2.name,
                            q2.units[0].name,
                            q2.units[0].symbol,
                            q3.name,
                            q3.units[0].name,
                            q3.units[0].symbol
                        )
                        if mulop not in ops:
                            ops.append(mulop)
                            if q1.fundamental == delta_temp_fundamental:
                                ops.append(MultOp(
                                    "temperature",
                                    "kelvin",
                                    "K",
                                    q2.name,
                                    q2.units[0].name,
                                    q2.units[0].symbol,
                                    q3.name,
                                    q3.units[0].name,
                                    q3.units[0].symbol
                                ))
                            elif q2.fundamental == delta_temp_fundamental:
                                ops.append(MultOp(
                                    q1.name,
                                    q1.units[0].name,
                                    q1.units[0].symbol,
                                    "temperature",
                                    "kelvin",
                                    "K",
                                    q3.name,
                                    q3.units[0].name,
                                    q3.units[0].symbol
                                ))
            div_res = q1.fundamental - q2.fundamental
            for q3 in quantities:
                if div_res == q3.fundamental:
                    divop = DivOp(
                        q1.name,
                        q1.units[0].name,
                        q1.units[0].symbol,
                        q2.name,
                        q2.units[0].name,
                        q2.units[0].symbol,
                        q3.name,
                        q3.units[0].name,
                        q3.units[0].symbol
                    )
                    if divop not in ops:
                        ops.append(divop)
                        if q2.fundamental == delta_temp_fundamental:
                            ops.append(DivOp(
                                q1.name,
                                q1.units[0].name,
                                q1.units[0].symbol,
                                "temperature",
                                "kelvin",
                                "K",
                                q3.name,
                                q3.units[0].name,
                                q3.units[0].symbol
                            ))
    return ops

def quaff_module(quantities : [Quantity], inter_ops : [InterOp]) -> str:
    return """
module quaff
    use erloff, only: &
            error_list_t, fatal_t, message_type_t, module_t, procedure_t
    use iso_varying_string, only: &
            varying_string, &
            assignment(=), &
            operator(==), &
            operator(//), &
            var_str
    use parff, only: &
            parse_result_t, &
            parsed_rational_t, &
            parser_output_t, &
            state_t, &
            either, &
            parse_char, &
            parse_end_of_input, &
            parse_rational, &
            parse_string, &
            parse_whitespace, &
            parse_with, &
            then_drop
    use strff, only: join, to_string

    implicit none
    private
    public :: &
""" + ", &\n".join([public_entities(quantity) for quantity in quantities]) + """, &
            operator(.unit.), &
            operator(.in.), &
            operator(+), &
            operator(-), &
            operator(*), &
            operator(/), &
            abs, &
            sum, &
            operator(>), &
            operator(<), &
            operator(>=), &
            operator(<=), &
            operator(==), &
            operator(/=), &
            equal, &
            min, &
            max, &
            to_string, &
            sin, &
            cos, &
            tan, &
            asin_, &
            acos_, &
            atan_, &
            atan2_, &
            sqrt, &
            cbrt, &
            atmospheric_pressure, &
            gravity, &
            universal_gas_constant, &
            parse_error, &
            unknown_unit

    ! SI Scaling
    double precision, parameter :: CENTI_PER_BASE = 100.0d0
    double precision, parameter :: MILLI_PER_BASE = 1.0d3
    double precision, parameter :: MICRO_PER_BASE = 1.0d6
    double precision, parameter :: BASE_PER_KILO = 1.0d3
    double precision, parameter :: KILO_PER_BASE = 1.0d0 / BASE_PER_KILO
    double precision, parameter :: BASE_PER_MEGA = 1.0d6
    double precision, parameter :: MEGA_PER_BASE = 1.0d0 / BASE_PER_MEGA

    ! Length
    double precision, parameter :: METERS_PER_INCH = 0.0254d0
    double precision, parameter :: INCHES_PER_METER = 1.0d0 / METERS_PER_INCH
    double precision, parameter :: INCHES_PER_FOOT = 12.0d0
    double precision, parameter :: FEET_PER_INCH = 1.0d0 / INCHES_PER_FOOT
    double precision, parameter :: CENTIMETERS_PER_METER = CENTI_PER_BASE
    double precision, parameter :: METERS_PER_CENTIMETER = 1.0d0 / CENTIMETERS_PER_METER
    double precision, parameter :: FEET_PER_METER = INCHES_PER_METER * FEET_PER_INCH
    double precision, parameter :: MICROINCHES_PER_METER = INCHES_PER_METER * MICRO_PER_BASE
    double precision, parameter :: MICROMETERS_PER_METER = MICRO_PER_BASE
    double precision, parameter :: MILLIMETERS_PER_METER = MILLI_PER_BASE

    ! Mass
    double precision, parameter :: KILOGRAMS_PER_POUND = 0.45359237d0 ! Taken from NIST (https://www.nist.gov/physical-measurement-laboratory/nist-guide-si-footnotes#f22)
    double precision, parameter :: POUNDS_PER_KILOGRAM = 1.0d0 / KILOGRAMS_PER_POUND
    double precision, parameter :: GRAMS_PER_KILOGRAM = BASE_PER_KILO
    double precision, parameter :: OUNCES_PER_POUND = 16.0d0
    double precision, parameter :: POUNDS_PER_TON = 2000.0d0
    double precision, parameter :: TONS_PER_POUND = 1.0d0 / POUNDS_PER_TON
    double precision, parameter :: OUNCES_PER_KILOGRAM = POUNDS_PER_KILOGRAM * OUNCES_PER_POUND
    double precision, parameter :: TONS_PER_KILOGRAM = TONS_PER_POUND * POUNDS_PER_KILOGRAM

    ! Temperature
    double precision, parameter :: CELSIUS_KELVIN_DIFFERENCE = 273.15d0
    double precision, parameter :: FAHRENHEIT_RANKINE_DIFFERENCE = 459.67d0
    double precision, parameter :: RANKINE_PER_KELVIN = 9.0d0 / 5.0d0

    ! Amount
    double precision, parameter :: AVOGADROS_NUMBER = 6.022140857d23

    ! Angle
    double precision, parameter :: PI = 3.14159265359d0
    double precision, parameter :: DEGREES_PER_RADIAN = 180.0d0 / PI

    ! Time
    double precision, parameter :: SECONDS_PER_MINUTE = 60.0d0
    double precision, parameter :: MINUTES_PER_HOUR = 60.0d0
    double precision, parameter :: HOURS_PER_DAY = 24.0d0
    double precision, parameter :: DAYS_PER_HOUR = 1.0d0 / HOURS_PER_DAY
    double precision, parameter :: SECONDS_PER_HOUR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR
    double precision, parameter :: MICROSECONDS_PER_SECOND = MICRO_PER_BASE
    double precision, parameter :: MILLISECONDS_PER_SECOND = MILLI_PER_BASE
    double precision, parameter :: MINUTES_PER_SECOND = 1.0d0 / SECONDS_PER_MINUTE
    double precision, parameter :: HOURS_PER_SECOND = 1.0d0 / SECONDS_PER_HOUR
    double precision, parameter :: DAYS_PER_SECOND = DAYS_PER_HOUR * HOURS_PER_SECOND
    double precision, parameter :: DAYS_PER_YEAR = 365.25
    double precision, parameter :: SECONDS_PER_YEAR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR * HOURS_PER_DAY * DAYS_PER_YEAR
    double precision, parameter :: YEARS_PER_SECOND = 1.0d0 / SECONDS_PER_YEAR

    ! Area
    double precision, parameter :: SQUARE_CENTIMETERS_PER_SQUARE_METER = CENTIMETERS_PER_METER**2
    double precision, parameter :: SQUARE_MILLIMETERS_PER_SQUARE_METER = MILLIMETERS_PER_METER**2
    double precision, parameter :: SQUARE_INCHES_PER_SQUARE_METER = INCHES_PER_METER**2
    double precision, parameter :: SQUARE_FEET_PER_SQUARE_METER = FEET_PER_METER**2

    ! Volume
    double precision, parameter :: CUBIC_CENTIMETERS_PER_CUBIC_METER = CENTIMETERS_PER_METER**3
    double precision, parameter :: CUBIC_MILLIMETERS_PER_CUBIC_METER = MILLIMETERS_PER_METER**3
    double precision, parameter :: LITERS_PER_CUBIC_METER = 1.0d3
    double precision, parameter :: CUBIC_INCHES_PER_CUBIC_METER = INCHES_PER_METER**3
    double precision, parameter :: CUBIC_FEET_PER_CUBIC_METER = FEET_PER_METER**3

    ! Density
    double precision, parameter :: GRAMS_PER_CUBIC_METER_PER_KILOGRAMS_PER_CUBIC_METER = GRAMS_PER_KILOGRAM
    double precision, parameter :: GRAMS_PER_CUBIC_CENTIMETER_PER_KILOGRAMS_PER_CUBIC_METER = &
          GRAMS_PER_KILOGRAM/CUBIC_CENTIMETERS_PER_CUBIC_METER
    double precision, parameter :: POUNDS_PER_CUBIC_FOOT_PER_KILOGRAMS_PER_CUBIC_METER = &
                POUNDS_PER_KILOGRAM / FEET_PER_METER**3

    ! Molar Mass
    double precision, parameter :: GRAMS_PER_MOL_PER_KILOGRAMS_PER_MOL = BASE_PER_KILO
    double precision, parameter :: MOLS_PER_GRAM_PER_MOLS_PER_KILOGRAM = 1.d0 / GRAMS_PER_MOL_PER_KILOGRAMS_PER_MOL

    ! Speed
    double precision, parameter :: CENTIMETERS_PER_SECOND_PER_METERS_PER_SECOND = CENTIMETERS_PER_METER
    double precision, parameter :: FEET_PER_SECOND_PER_METERS_PER_SECOND = FEET_PER_METER
    double precision, parameter :: MILLIMETERS_PER_SECOND_PER_METERS_PER_SECOND = MILLIMETERS_PER_METER

    ! Acceleration
    double precision, parameter :: GRAVITY_ = 9.80665d0 ! m/s^2 according to Wikipedia
    double precision, parameter :: CENTIMETERS_PER_SQUARE_SECOND_PER_METERS_PER_SQUARE_SECOND = CENTIMETERS_PER_METER
    double precision, parameter :: FEET_PER_SQUARE_SECOND_PER_METERS_PER_SQUARE_SECOND = FEET_PER_METER

    ! Force
    ! Note: 1 N = 1 (kg m)/s^2
    double precision, parameter :: DYNES_PER_NEWTON = &
            GRAMS_PER_KILOGRAM * CENTIMETERS_PER_SQUARE_SECOND_PER_METERS_PER_SQUARE_SECOND
    double precision, parameter :: KILOPONDS_PER_NEWTON = 1.0d0 / GRAVITY_ ! 1 kp = 1 kg * gravity
    double precision, parameter :: MILLINEWTONS_PER_NEWTON = MILLI_PER_BASE
    double precision, parameter :: POUNDS_PER_NEWTON = POUNDS_PER_KILOGRAM / GRAVITY_ ! 1 lbf = 1 lbm * gravity

    ! Energy
    ! Note: 1 J = 1 (N m)
    double precision, parameter :: JOULES_PER_CALORIE = 4.1868d0 ! Taken from NIST for "IT" (https://www.nist.gov/physical-measurement-laboratory/nist-guide-si-footnotes#f09)
    double precision, parameter :: JOULE_PER_BTU = 1.05505585262d3 ! Taken from NIST for "IT" (https://www.nist.gov/physical-measurement-laboratory/nist-guide-si-footnotes#f09)
    double precision, parameter :: CALORIES_PER_JOULE = 1.0d0 / JOULES_PER_CALORIE
    double precision, parameter :: BTU_PER_JOULE = 1.0d0 / JOULE_PER_BTU
    double precision, parameter :: KILOJOULES_PER_JOULE = KILO_PER_BASE
    double precision, parameter :: MEGABTU_PER_JOULE = MEGA_PER_BASE * BTU_PER_JOULE
    double precision, parameter :: MEGAWATT_DAYS_PER_JOULE = MEGA_PER_BASE * DAYS_PER_SECOND
    double precision, parameter :: POUNDS_FORCE_FOOT_PER_NEWTON_METER = POUNDS_PER_NEWTON * FEET_PER_METER

    ! Power
    ! Note: 1 W = 1 J/s
    double precision, parameter :: BTU_PER_HOUR_PER_WATT = BTU_PER_JOULE * SECONDS_PER_HOUR
    double precision, parameter :: CALORIES_PER_SECOND_PER_WATT = CALORIES_PER_JOULE
    double precision, parameter :: MEGABTU_PER_HOUR_PER_WATT = MEGA_PER_BASE * BTU_PER_HOUR_PER_WATT
    double precision, parameter :: MEGAWATTS_PER_WATT = MEGA_PER_BASE

    ! Pressure
    ! Note: 1 Pa = 1 N/m^2
    double precision, parameter :: DYNES_PER_SQUARE_CENTIMETER_PER_PASCAL = DYNES_PER_NEWTON / SQUARE_CENTIMETERS_PER_SQUARE_METER
    double precision, parameter :: KILOPASCALS_PER_PASCAL = KILO_PER_BASE
    double precision, parameter :: KILOPONDS_PER_SQUARE_CENTIMETER_PER_PASCAL = &
            KILOPONDS_PER_NEWTON / SQUARE_CENTIMETERS_PER_SQUARE_METER
    double precision, parameter :: MEGAPASCALS_PER_PASCAL = MEGA_PER_BASE
    double precision, parameter :: POUNDS_PER_SQUARE_INCH_PER_PASCAL = POUNDS_PER_NEWTON / SQUARE_INCHES_PER_SQUARE_METER
    double precision, parameter :: PASCALS_PER_BAR = 100000.0d0
    double precision, parameter :: BAR_PER_PASCAL = 1.0d0 / PASCALS_PER_BAR
    double precision, parameter :: PASCALS_PER_ATMOSPHERE = 101325.0d0
    double precision, parameter :: ATMOSPHERES_PER_PASCAL = 1.0d0 / PASCALS_PER_ATMOSPHERE
    double precision, parameter :: KILOPOUNDS_PER_SQUARE_INCH_PER_PASCAL = &
            POUNDS_PER_SQUARE_INCH_PER_PASCAL / 1000.0d0

    ! Dynamic Viscosity
    double precision, parameter :: MEGAPASCAL_SECONDS_PER_PASCAL_SECOND = MEGA_PER_BASE

    ! Enthalpy
    double precision, parameter :: KILOJOULES_PER_KILOGRAM_PER_JOULES_PER_KILOGRAM = KILO_PER_BASE

    ! Burnup
    double precision, parameter :: MEGAWATT_DAYS_PER_TON_PER_WATT_SECONDS_PER_KILOGRAM = &
            MEGA_PER_BASE * DAYS_PER_SECOND / TONS_PER_KILOGRAM

    ! Thermal Conductivity
    double precision, parameter :: CAL_PER_SEC_CM_K_PER_WATTS_PER_METER_KELVIN = &
            CALORIES_PER_SECOND_PER_WATT / CENTIMETERS_PER_METER
    double precision, parameter :: WATTS_PER_CENTIMETER_KELVIN_PER_WATTS_PER_METER_KELVIN = &
            METERS_PER_CENTIMETER
    double precision, parameter :: BTU_PER_HOUR_FEET_RANKINE_PER_WATTS_PER_METER_KELVIN = &
            BTU_PER_HOUR_PER_WATT  / (FEET_PER_METER * RANKINE_PER_KELVIN)

    ! Energy Per Amount
    double precision, parameter :: KILOJOULES_PER_MOL_PER_JOULES_PER_MOL = &
            KILO_PER_BASE

    ! Energy Per Temperature Amount
    double precision, parameter :: KILOJOULES_PER_KELVIN_MOL_PER_JOULES_PER_KELVIN_MOL = &
            KILO_PER_BASE

    ! Specific Heat
    double precision, parameter :: BTU_PER_POUNDS_RANKINE_PER_JOULES_PER_KILOGRAM_KELVIN = &
            BTU_PER_JOULE / POUNDS_PER_KILOGRAM / RANKINE_PER_KELVIN

    ! convective heat transfer
    double precision, parameter :: BTU_PER_HR_SQ_FT_RANKINE_PER_WATTS_PER_SQUARE_METER_KELVIN = &
            BTU_PER_HOUR_PER_WATT / SQUARE_FEET_PER_SQUARE_METER / RANKINE_PER_KELVIN

    ! thermal expansion coefficient
    double precision, parameter ::  PER_RANKINE_PER_KELVIN = &
            1 / RANKINE_PER_KELVIN

    ! fluence
    double precision, parameter ::  PARTICLES_PER_SQUARE_CENTIMETER_PER_PARTICLES_PER_SQUARE_METER = &
            1 / SQUARE_CENTIMETERS_PER_SQUARE_METER
    ! fluence
    double precision, parameter ::  MEGAPASCAL_ROOT_METER_PER_PASCAL_ROOT_METER = &
            MEGA_PER_BASE
    double precision, parameter :: KSI_ROOT_INCH_PER_PASCAL_ROOT_METER = &
            KILO_PER_BASE * POUNDS_PER_SQUARE_INCH_PER_PASCAL / sqrt(INCHES_PER_METER)
""" + "\n".join([quantity_specs(quantity) for quantity in quantities]) + """
""" + interquantity_interfaces(inter_ops) + """

    type(pressure_t), parameter :: ATMOSPHERIC_PRESSURE = &
            pressure_t(pascals = PASCALS_PER_ATMOSPHERE)
    type(acceleration_t), parameter :: GRAVITY = &
            acceleration_t(meters_per_square_second = GRAVITY_)
    type(molar_specific_heat_t), parameter :: UNIVERSAL_GAS_CONSTANT = &
            molar_specific_heat_t(joules_per_kelvin_mol = 8.31446261815324d0)


    interface operator(.safeEq.)
        module procedure safe_eq
    end interface

    double precision, parameter :: MACHINE_EPSILON = epsilon(1.0d0)

    type(message_type_t), parameter :: parse_error = &
            message_type_t("Parse Error")
    type(message_type_t), parameter :: unknown_unit = &
            message_type_t("Unknown Unit")

    character(len=*), parameter :: MODULE_NAME = "quaff"
contains""" + "\n".join([quantity_implementation(quantity) for quantity in quantities]) + """

""" + interquantity_implementation(inter_ops) + """
    elemental function effectively_zero(a)
        double precision, intent(in) :: a
        logical :: effectively_zero

        effectively_zero = abs(a) < MACHINE_EPSILON
    end function

    pure function equal_within_absolute(a, b, tolerance)
        double precision, intent(in) :: a
        double precision, intent(in) :: b
        double precision, intent(in) :: tolerance
        logical :: equal_within_absolute

        equal_within_absolute = abs(a - b) < tolerance
    end function

    pure function equal_within_relative(a, b, tolerance)
        double precision, intent(in) :: a
        double precision, intent(in) :: b
        double precision, intent(in) :: tolerance
        logical :: equal_within_relative

        if (effectively_zero(a) .and. effectively_zero(b)) then
            equal_within_relative = .true.
        else
            equal_within_relative = &
                    (abs(a - b) / max(abs(a), abs(b))) < tolerance
        end if
    end function

    function parse_space(state_) result(result_)
        type(state_t), intent(in) :: state_
        type(parser_output_t) :: result_

        result_ = parse_char(" ", state_)
    end function

    elemental function safe_eq(a, b)
        double precision, intent(in) :: a
        double precision, intent(in) :: b
        logical :: safe_eq

        safe_eq = equal_within_relative(a, b, MACHINE_EPSILON)
    end function
end module
"""

def public_entities(quantity: Quantity) -> str:
    return (
        """            {quantity}_t, &
            fallible_{quantity}_t, &
            {quantity}_unit_t, &
            fallible_{quantity}_unit_t, &
            {quantity}_simple_unit_t, &
            parse_{quantity}, &
            parse_{quantity}_unit, &
            default_{quantity}_output_units, &
            provided_{quantity}_units, &
""".format(quantity = quantity.name)
    + public_units(quantity.units)
)

def public_units(units : [Unit]) -> str:
    return ", &\n".join(["            {unit_name}".format(unit_name=unit.name) for unit in units])

def quantity_specs(quantity: Quantity) -> str:
    if isinstance(quantity.specific, Standard):
        return standard_quantity_specs(quantity)
    elif isinstance(quantity.specific, Temperature):
        return temperature_quantity_specs(quantity)
    elif isinstance(quantity.specific, Angle):
        return angle_quantity_specs(quantity)

def standard_quantity_specs(quantity: Quantity) -> str:
    return (
        addable_type_defs(quantity)
        + addable_interface_blocks(quantity)
        + addable_unit_decls(quantity)
        + default_unit_decls(quantity)
    )

def temperature_quantity_specs(quantity: Quantity) -> str:
    return (
        non_addable_type_defs(quantity)
        + non_addable_interface_blocks(quantity)
        + non_addable_unit_decls(quantity)
        + default_unit_decls(quantity)
    )

def angle_quantity_specs(quantity: Quantity) -> str:
    return (
        addable_type_defs(quantity)
        + addable_interface_blocks(quantity)
        + angle_interfaces()
        + addable_unit_decls(quantity)
        + default_unit_decls(quantity)
    )

def addable_type_defs(quantity: Quantity) -> str:
    return (
        type_defs_before_addable(quantity)
        + type_defs_after_addable(quantity)
    )

def non_addable_type_defs(quantity: Quantity) -> str:
    return (
        type_defs_before_addable(quantity)
        + type_def_nonaddable_line()
        + type_defs_after_addable(quantity)
    )

def type_defs_before_addable(quantity: Quantity) -> str:
    return """
    type :: {quantity}_t
        double precision :: {unit_name}
    contains
        private
        procedure :: {quantity}_tb_to_string_full_precision
        procedure :: {quantity}_tb_to_string_with_precision
        procedure :: {quantity}_tb_to_string_in_full_precision
        procedure :: {quantity}_tb_to_string_in_with_precision
        generic, public :: to_string => &
                {quantity}_tb_to_string_full_precision, &
                {quantity}_tb_to_string_with_precision, &
                {quantity}_tb_to_string_in_full_precision, &
                {quantity}_tb_to_string_in_with_precision
        generic, public :: to_string_in => &
                {quantity}_tb_to_string_in_full_precision, &
                {quantity}_tb_to_string_in_with_precision
    end type

    type :: fallible_{quantity}_t
        private
        type({quantity}_t) :: {quantity}_ = {quantity}_t(0.0d0)
        type(error_list_t) :: errors_
    contains
        private
        procedure, public :: failed => fallible_{quantity}_failed
        procedure, public :: {quantity} => fallible_to_{quantity}
        procedure, public :: errors => fallible_{quantity}_errors
    end type

    type, abstract :: {quantity}_unit_t
        double precision :: conversion_factor""".format(
            quantity = quantity.name, unit_name = quantity.units[0].name)

def type_def_nonaddable_line() -> str:
    return "\n        double precision :: difference"

def type_defs_after_addable(quantity: Quantity) -> str:
    return """
    contains
        procedure({quantity}_unit_to_string_i), deferred :: unit_to_string
        procedure({quantity}_value_to_string_i), deferred :: value_to_string
        generic :: to_string => unit_to_string, value_to_string
        procedure({quantity}_parse_as_i), deferred :: parse_as
    end type

    type :: fallible_{quantity}_unit_t
        private
        class({quantity}_unit_t), allocatable :: unit_
        type(error_list_t) :: errors_
    contains
        private
        procedure, public :: failed => fallible_{quantity}_unit_failed
        procedure, public :: unit => fallible_{quantity}_unit_unit
        procedure, public :: errors => fallible_{quantity}_unit_errors
    end type

    type, extends({quantity}_unit_t) :: {quantity}_simple_unit_t
        character(len=20) :: symbol
    contains
        procedure :: unit_to_string => {quantity}_simple_unit_to_string
        procedure :: value_to_string => {quantity}_simple_value_to_string
        procedure :: parse_as => {quantity}_simple_parse_as
    end type

    abstract interface
        elemental function {quantity}_unit_to_string_i(self) result(string)
            import :: {quantity}_unit_t, varying_string

            implicit none

            class({quantity}_unit_t), intent(in) :: self
            type(varying_string) :: string
        end function

        pure function {quantity}_value_to_string_i(self, value_) result(string)
            import :: {quantity}_unit_t, varying_string

            implicit none

            class({quantity}_unit_t), intent(in) :: self
            type(varying_string), intent(in) :: value_
            type(varying_string) :: string
        end function

        function {quantity}_parse_as_i(self, string) result(fallible_{quantity})
            import :: {quantity}_unit_t, fallible_{quantity}_t, varying_string

            implicit none

            class({quantity}_unit_t), intent(in) :: self
            type(varying_string), intent(in) :: string
            type(fallible_{quantity}_t) :: fallible_{quantity}
        end function
    end interface
""".format(quantity=quantity.name)

def addable_interface_blocks(quantity: Quantity) -> str:
    return (
        interface_blocks_before_addable(quantity)
        + addable_interfaces(quantity)
        + interface_blocks_after_addable(quantity)
    )

def non_addable_interface_blocks(quantity: Quantity) -> str:
    return (
        interface_blocks_before_addable(quantity)
        + interface_blocks_after_addable(quantity)
    )

def interface_blocks_before_addable(quantity: Quantity) -> str:
    return """
    interface fallible_{quantity}_t
        module procedure fal_{quantity}_from_quantity
        module procedure fal_{quantity}_from_errors
        module procedure fal_{quantity}_from_fal_quantity
    end interface

    interface fallible_{quantity}_unit_t
        module procedure fal_{quantity}_unit_from_unit
        module procedure fal_{quantity}_unit_from_errors
        module procedure fal_{quantity}_unit_from_fal_quantity_unit
    end interface

    interface parse_{quantity}
        module procedure parse_{quantity}_c
        module procedure parse_{quantity}_s
        module procedure parse_{quantity}_with_units_c
        module procedure parse_{quantity}_with_units_s
    end interface

    interface parse_{quantity}_unit
        module procedure parse_{quantity}_unit_c
        module procedure parse_{quantity}_unit_s
        module procedure parse_{quantity}_unit_with_units_c
        module procedure parse_{quantity}_unit_with_units_s
    end interface
    
    interface operator(.unit.)
        module procedure {quantity}_from_real_units
        module procedure {quantity}_from_double_units
        module procedure {quantity}_from_integer_units
    end interface
    
    interface operator(.in.)
        module procedure {quantity}_to_units
    end interface
""".format(quantity=quantity.name)

def addable_interfaces(quantity: Quantity) -> str:
    return """
    interface operator(+)
        module procedure {quantity}_pls_{quantity}
    end interface

    interface operator(-)
        module procedure negate_{quantity}
        module procedure {quantity}_mns_{quantity}
    end interface
    
    interface abs
        module procedure abs_{quantity}
    end interface

    interface sum
        module procedure sum_{quantity}
    end interface
""".format(quantity=quantity.name)

def interface_blocks_after_addable(quantity: Quantity) -> str:
    return """
    interface operator(*)
        module procedure real_times_{quantity}
        module procedure double_times_{quantity}
        module procedure integer_times_{quantity}
        module procedure {quantity}_times_real
        module procedure {quantity}_times_double
        module procedure {quantity}_times_integer
    end interface

    interface operator(/)
        module procedure {quantity}_divided_by_real
        module procedure {quantity}_divided_by_double
        module procedure {quantity}_divided_by_integer
        module procedure {quantity}_div_{quantity}
    end interface

    interface operator(>)
        module procedure {quantity}_greater_than
    end interface

    interface operator(<)
        module procedure {quantity}_less_than
    end interface

    interface operator(>=)
        module procedure {quantity}_greater_than_or_equal
    end interface

    interface operator(<=)
        module procedure {quantity}_less_than_or_equal
    end interface

    interface operator(==)
        module procedure {quantity}_equal_
    end interface

    interface operator(/=)
        module procedure {quantity}_not_equal
    end interface

    interface equal
        module procedure {quantity}_equal_
        module procedure {quantity}_equal_within_absolute_
        module procedure {quantity}_equal_within_relative_
    end interface

    interface max
        module procedure {quantity}_max
    end interface

    interface min
        module procedure {quantity}_min
    end interface

    interface to_string
        module procedure {quantity}_to_string_full_precision
        module procedure {quantity}_to_string_with_precision
        module procedure {quantity}_to_string_in_full_precision
        module procedure {quantity}_to_string_in_with_precision
        module procedure generic_{quantity}_unit_to_string
    end interface""".format(quantity=quantity.name)

def angle_interfaces() -> str:
    return """

    interface sin
        module procedure sin_
    end interface

    interface cos
        module procedure cos_
    end interface

    interface tan
        module procedure tan_
    end interface"""

def addable_unit_decls(quantity: Quantity) -> str:
    return "\n\n" + "\n".join([
"""    type({quantity}_simple_unit_t), parameter :: {unit_name} = &
            {quantity}_simple_unit_t( &
                    conversion_factor = {conv_fac}, &
                    symbol = "{sym}")""".format(
                        quantity=quantity.name, 
                        unit_name=unit.name, 
                        conv_fac=unit.conversion_factor, 
                        sym=unit.symbol) for unit in quantity.units
        ])

def non_addable_unit_decls(quantity: Quantity) -> str:
    return "\n\n" + "\n".join([
"""    type({quantity}_simple_unit_t), parameter :: {unit_name} = &
            {quantity}_simple_unit_t( &
                    conversion_factor = {conv_fac}, &
                    difference = {conv_diff}, &
                    symbol = "{sym}")""".format(
                        quantity=quantity.name,
                        unit_name=unit.name,
                        conv_fac=unit.conversion_factor,
                        conv_diff=unit.conversion_difference,
                        sym=unit.symbol) for unit in quantity.units
        ])

def default_unit_decls(quantity: Quantity) -> str:
    return (
        """

    type({quantity}_simple_unit_t) :: default_{quantity}_output_units = {unit_name}

    type({quantity}_simple_unit_t), parameter :: provided_{quantity}_units(*) = &
""".format(quantity=quantity.name, unit_name=quantity.units[0].name)
        + "            [ " 
        + " &\n            , ".join(unit.name for unit in quantity.units) 
        + " &\n            ]"
    )

def quantity_implementation(quantity: Quantity) -> str:
    if isinstance(quantity.specific, Standard):
        return standard_quantity_implementation(quantity)
    elif isinstance(quantity.specific, Temperature):
        return temperature_implementation(quantity)
    elif isinstance(quantity.specific, Angle):
        return angle_implementation(quantity)

def standard_quantity_implementation(quantity: Quantity) -> str:
    return (
        implementation_before_addable(quantity)
        + addable_implementation(quantity)
        + implementation_after_addable(quantity)
    )

def temperature_implementation(quantity: Quantity) -> str:
    return (
        implementation_before_addable(quantity)
        + non_addable_implementation(quantity)
        + implementation_after_addable(quantity)
    )

def angle_implementation(quantity: Quantity) -> str:
    return (
        implementation_before_addable(quantity)
        + addable_implementation(quantity)
        + implementation_after_addable(quantity)
        + trig_funcs(quantity)
    )

def implementation_before_addable(quantity: Quantity) -> str:
    return """
    function parse_{quantity}_c(string) result(fallible_{quantity})
        character(len=*), intent(in) :: string
        type(fallible_{quantity}_t) :: fallible_{quantity}

        fallible_{quantity} = fallible_{quantity}_t( &
                parse_{quantity}(var_str(string), provided_{quantity}_units), &
                module_t(MODULE_NAME), &
                procedure_t("parse_{quantity}_c"))
    end function

    function parse_{quantity}_s(string) result(fallible_{quantity})
        type(varying_string), intent(in) :: string
        type(fallible_{quantity}_t) :: fallible_{quantity}

        fallible_{quantity} = fallible_{quantity}_t( &
                parse_{quantity}(string, provided_{quantity}_units), &
                module_t(MODULE_NAME), &
                procedure_t("parse_{quantity}_s"))
    end function

    function parse_{quantity}_with_units_c( &
            string, units) result(fallible_{quantity})
        character(len=*), intent(in) :: string
        class({quantity}_unit_t), intent(in) :: units(:)
        type(fallible_{quantity}_t) :: fallible_{quantity}

        fallible_{quantity} = fallible_{quantity}_t( &
                parse_{quantity}(var_str(string), units), &
                module_t(MODULE_NAME), &
                procedure_t("parse_{quantity}_with_units_c"))
    end function

    function parse_{quantity}_with_units_s( &
            string, units) result(fallible_{quantity})
        type(varying_string), intent(in) :: string
        class({quantity}_unit_t), intent(in) :: units(:)
        type(fallible_{quantity}_t) :: fallible_{quantity}

        integer :: i

        do i = 1, size(units)
            fallible_{quantity} = units(i)%parse_as(string)
            if (.not. fallible_{quantity}%failed()) return
        end do
        fallible_{quantity} = fallible_{quantity}_t(error_list_t(fatal_t( &
                PARSE_ERROR, &
                module_t(MODULE_NAME), &
                procedure_t("parse_{quantity}_with_units_s"), &
                "Unable to parse '" // string // "' as a {quantity}_t. Tried with units: " &
                // join(units%to_string(), ", "))))
    end function""".format(quantity=quantity.name)

def addable_implementation(quantity: Quantity) -> str:
    return """

    elemental function {quantity}_from_real_units(value_, units) result({quantity})
        real, intent(in) :: value_
        class({quantity}_unit_t), intent(in) :: units
        type({quantity}_t) :: {quantity}

        {quantity}%{unit_name} = value_ / units%conversion_factor
    end function

    elemental function {quantity}_from_double_units(value_, units) result({quantity})
        double precision, intent(in) :: value_
        class({quantity}_unit_t), intent(in) :: units
        type({quantity}_t) :: {quantity}

        {quantity}%{unit_name} = value_ / units%conversion_factor
    end function

    elemental function {quantity}_from_integer_units(value_, units) result({quantity})
        integer, intent(in) :: value_
        class({quantity}_unit_t), intent(in) :: units
        type({quantity}_t) :: {quantity}

        {quantity}%{unit_name} = value_ / units%conversion_factor
    end function

    elemental function {quantity}_to_units(self, units) result({quantity})
        type({quantity}_t), intent(in) :: self
        class({quantity}_unit_t), intent(in) :: units
        double precision :: {quantity}

        {quantity} = self%{unit_name} * units%conversion_factor
    end function

    elemental function {quantity}_pls_{quantity}( &
            lhs, rhs) result(sum_)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        type({quantity}_t) :: sum_

        sum_%{unit_name} = &
                lhs%{unit_name} + rhs%{unit_name}
    end function

    elemental function negate_{quantity}(self) result(negated)
        type({quantity}_t), intent(in) :: self
        type({quantity}_t) :: negated

        negated%{unit_name} = -self%{unit_name}
    end function

    elemental function {quantity}_mns_{quantity}( &
            lhs, rhs) result(difference)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        type({quantity}_t) :: difference

        difference%{unit_name} = &
                lhs%{unit_name} - rhs%{unit_name}
    end function

    elemental function abs_{quantity}({quantity}) result(abs_)
        type({quantity}_t), intent(in) :: {quantity}
        type({quantity}_t) :: abs_

        abs_%{unit_name} = abs({quantity}%{unit_name})
    end function

    pure function sum_{quantity}({quantity}s) result(sum_)
        type({quantity}_t), intent(in) :: {quantity}s(:)
        type({quantity}_t) :: sum_

        sum_%{unit_name} = sum({quantity}s%{unit_name})
    end function
""".format(quantity=quantity.name, unit_name=quantity.units[0].name)

def non_addable_implementation(quantity: Quantity) -> str:
    return """

    elemental function {quantity}_from_real_units(value_, units) result({quantity})
        real, intent(in) :: value_
        class({quantity}_unit_t), intent(in) :: units
        type({quantity}_t) :: {quantity}

        {quantity}%{unit_name} = (value_ + units%difference) / units%conversion_factor
    end function

    elemental function {quantity}_from_double_units(value_, units) result({quantity})
        double precision, intent(in) :: value_
        class({quantity}_unit_t), intent(in) :: units
        type({quantity}_t) :: {quantity}

        {quantity}%{unit_name} = (value_ + units%difference) / units%conversion_factor
    end function

    elemental function {quantity}_from_integer_units(value_, units) result({quantity})
        integer, intent(in) :: value_
        class({quantity}_unit_t), intent(in) :: units
        type({quantity}_t) :: {quantity}

        {quantity}%{unit_name} = (value_ + units%difference) / units%conversion_factor
    end function

    elemental function {quantity}_to_units(self, units) result({quantity})
        type({quantity}_t), intent(in) :: self
        class({quantity}_unit_t), intent(in) :: units
        double precision :: {quantity}

        {quantity} = self%{unit_name} * units%conversion_factor - units%difference
    end function
""".format(quantity=quantity.name, unit_name=quantity.units[0].name)

def implementation_after_addable(quantity: Quantity) -> str:
    return """
    elemental function real_times_{quantity}( &
            multiplier, {quantity}) result(new_{quantity})
        real, intent(in) :: multiplier
        type({quantity}_t), intent(in) :: {quantity}
        type({quantity}_t) :: new_{quantity}

        new_{quantity}%{unit_name} = &
                multiplier * {quantity}%{unit_name}
    end function

    elemental function double_times_{quantity}( &
            multiplier, {quantity}) result(new_{quantity})
        double precision, intent(in) :: multiplier
        type({quantity}_t), intent(in) :: {quantity}
        type({quantity}_t) :: new_{quantity}

        new_{quantity}%{unit_name} = &
                multiplier * {quantity}%{unit_name}
    end function

    elemental function integer_times_{quantity}( &
            multiplier, {quantity}) result(new_{quantity})
        integer, intent(in) :: multiplier
        type({quantity}_t), intent(in) :: {quantity}
        type({quantity}_t) :: new_{quantity}

        new_{quantity}%{unit_name} = &
                dble(multiplier) * {quantity}%{unit_name}
    end function

    elemental function {quantity}_times_real( &
            {quantity}, multiplier) result(new_{quantity})
        type({quantity}_t), intent(in) :: {quantity}
        real, intent(in) :: multiplier
        type({quantity}_t) :: new_{quantity}

        new_{quantity}%{unit_name} = &
                {quantity}%{unit_name} * multiplier
    end function

    elemental function {quantity}_times_double( &
            {quantity}, multiplier) result(new_{quantity})
        type({quantity}_t), intent(in) :: {quantity}
        double precision, intent(in) :: multiplier
        type({quantity}_t) :: new_{quantity}

        new_{quantity}%{unit_name} = &
                {quantity}%{unit_name} * multiplier
    end function

    elemental function {quantity}_times_integer( &
            {quantity}, multiplier) result(new_{quantity})
        type({quantity}_t), intent(in) :: {quantity}
        integer, intent(in) :: multiplier
        type({quantity}_t) :: new_{quantity}

        new_{quantity}%{unit_name} = &
                {quantity}%{unit_name} * dble(multiplier)
    end function

    elemental function {quantity}_divided_by_real( &
            {quantity}, divisor) result(new_{quantity})
        type({quantity}_t), intent(in) :: {quantity}
        real, intent(in) :: divisor
        type({quantity}_t) :: new_{quantity}

        new_{quantity}%{unit_name} = &
                {quantity}%{unit_name} / divisor
    end function

    elemental function {quantity}_divided_by_double( &
            {quantity}, divisor) result(new_{quantity})
        type({quantity}_t), intent(in) :: {quantity}
        double precision, intent(in) :: divisor
        type({quantity}_t) :: new_{quantity}

        new_{quantity}%{unit_name} = &
                {quantity}%{unit_name} / divisor
    end function

    elemental function {quantity}_divided_by_integer( &
            {quantity}, divisor) result(new_{quantity})
        type({quantity}_t), intent(in) :: {quantity}
        integer, intent(in) :: divisor
        type({quantity}_t) :: new_{quantity}

        new_{quantity}%{unit_name} = &
                {quantity}%{unit_name} / dble(divisor)
    end function

    elemental function {quantity}_div_{quantity}( &
            numerator, denomenator) result(ratio)
        type({quantity}_t), intent(in) :: numerator
        type({quantity}_t), intent(in) :: denomenator
        double precision :: ratio

        ratio = numerator%{unit_name} / denomenator%{unit_name}
    end function
    
    elemental function {quantity}_greater_than(lhs, rhs)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        logical :: {quantity}_greater_than

        {quantity}_greater_than = lhs%{unit_name} > rhs%{unit_name}
    end function

    elemental function {quantity}_less_than(lhs, rhs)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        logical :: {quantity}_less_than

        {quantity}_less_than = lhs%{unit_name} < rhs%{unit_name}
    end function

    elemental function {quantity}_greater_than_or_equal(lhs, rhs)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        logical :: {quantity}_greater_than_or_equal

        {quantity}_greater_than_or_equal = lhs%{unit_name} >= rhs%{unit_name}
    end function

    elemental function {quantity}_less_than_or_equal(lhs, rhs)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        logical :: {quantity}_less_than_or_equal

        {quantity}_less_than_or_equal = lhs%{unit_name} <= rhs%{unit_name}
    end function

    elemental function {quantity}_equal_(lhs, rhs)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        logical :: {quantity}_equal_

        {quantity}_equal_ = lhs%{unit_name} .safeEq. rhs%{unit_name}
    end function

    elemental function {quantity}_equal_within_absolute_(lhs, rhs, within)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        type({quantity}_t), intent(in) :: within
        logical :: {quantity}_equal_within_absolute_

        {quantity}_equal_within_absolute_ = equal_within_absolute( &
                lhs%{unit_name}, &
                rhs%{unit_name}, &
                within%{unit_name})
    end function

    elemental function {quantity}_equal_within_relative_(lhs, rhs, within)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        double precision, intent(in) :: within
        logical :: {quantity}_equal_within_relative_

        {quantity}_equal_within_relative_ = equal_within_relative( &
                lhs%{unit_name}, &
                rhs%{unit_name}, &
                within)
    end function

    elemental function {quantity}_not_equal(lhs, rhs)
        type({quantity}_t), intent(in) :: lhs
        type({quantity}_t), intent(in) :: rhs
        logical :: {quantity}_not_equal

        {quantity}_not_equal = .not. lhs == rhs
    end function
    
    elemental function {quantity}_min(first, second)
        type({quantity}_t), intent(in) :: first
        type({quantity}_t), intent(in) :: second
        type({quantity}_t) :: {quantity}_min
        
        {quantity}_min%{unit_name} = min( &
                first%{unit_name}, &
                second%{unit_name})
    end function

    elemental function {quantity}_max(first, second)
        type({quantity}_t), intent(in) :: first
        type({quantity}_t), intent(in) :: second
        type({quantity}_t) :: {quantity}_max
        
        {quantity}_max%{unit_name} = max( &
                first%{unit_name}, &
                second%{unit_name})
    end function

    elemental function {quantity}_to_string_full_precision(self) result(string)
        type({quantity}_t), intent(in) :: self
        type(varying_string) :: string

        string = to_string(self, default_{quantity}_output_units)
    end function

    elemental function {quantity}_to_string_with_precision(self, significant_digits) result(string)
        type({quantity}_t), intent(in) :: self
        integer, intent(in) :: significant_digits
        type(varying_string) :: string

        string = to_string(self, default_{quantity}_output_units, significant_digits)
    end function

    elemental function {quantity}_to_string_in_full_precision(self, units) result(string)
        type({quantity}_t), intent(in) :: self
        class({quantity}_unit_t), intent(in) :: units
        type(varying_string) :: string

        string = units%to_string(to_string(self.in.units))
    end function

    elemental function {quantity}_to_string_in_with_precision( &
            self, units, significant_digits) result(string)
        type({quantity}_t), intent(in) :: self
        class({quantity}_unit_t), intent(in) :: units
        integer, intent(in) :: significant_digits
        type(varying_string) :: string

        string = units%to_string(to_string(self.in.units, significant_digits))
    end function

    elemental function {quantity}_tb_to_string_full_precision(self) result(string)
        class({quantity}_t), intent(in) :: self
        type(varying_string) :: string

        string = to_string(self, default_{quantity}_output_units)
    end function

    elemental function {quantity}_tb_to_string_with_precision(self, significant_digits) result(string)
        class({quantity}_t), intent(in) :: self
        integer, intent(in) :: significant_digits
        type(varying_string) :: string

        string = to_string(self, default_{quantity}_output_units, significant_digits)
    end function

    elemental function {quantity}_tb_to_string_in_full_precision(self, units) result(string)
        class({quantity}_t), intent(in) :: self
        class({quantity}_unit_t), intent(in) :: units
        type(varying_string) :: string

        string = units%to_string(to_string(self.in.units))
    end function

    elemental function {quantity}_tb_to_string_in_with_precision( &
            self, units, significant_digits) result(string)
        class({quantity}_t), intent(in) :: self
        class({quantity}_unit_t), intent(in) :: units
        integer, intent(in) :: significant_digits
        type(varying_string) :: string

        string = units%to_string(to_string(self.in.units, significant_digits))
    end function

    elemental function generic_{quantity}_unit_to_string(unit) result(string)
        class({quantity}_unit_t), intent(in) :: unit
        type(varying_string) :: string

        string = unit%to_string()
    end function

    function fal_{quantity}_from_quantity( &
            {quantity}) result(fallible_{quantity})
        type({quantity}_t), intent(in) :: {quantity}
        type(fallible_{quantity}_t) :: fallible_{quantity}

        fallible_{quantity}%{quantity}_ = {quantity}
    end function

    function fal_{quantity}_from_errors( &
            errors) result(fallible_{quantity})
        type(error_list_t), intent(in) :: errors
        type(fallible_{quantity}_t) :: fallible_{quantity}

        fallible_{quantity}%errors_ = errors
    end function

    function fal_{quantity}_from_fal_quantity( &
            fallible_{quantity}, &
            module_, &
            procedure_) &
            result(new_fallible_{quantity})
        type(fallible_{quantity}_t), intent(in) :: fallible_{quantity}
        type(module_t), intent(in) :: module_
        type(procedure_t), intent(in) :: procedure_
        type(fallible_{quantity}_t) :: new_fallible_{quantity}

        if (fallible_{quantity}%failed()) then
            new_fallible_{quantity}%errors_ = error_list_t( &
                    fallible_{quantity}%errors_, module_, procedure_)
        else
            new_fallible_{quantity}%{quantity}_ = &
                    fallible_{quantity}%{quantity}_
        end if
    end function

    elemental function fallible_{quantity}_failed( &
            self) result(failed)
        class(fallible_{quantity}_t), intent(in) :: self
        logical :: failed

        failed = self%errors_%has_any()
    end function

    elemental function fallible_to_{quantity}( &
            self) result({quantity})
        class(fallible_{quantity}_t), intent(in) :: self
        type({quantity}_t) :: {quantity}

        {quantity} = self%{quantity}_
    end function

    impure elemental function fallible_{quantity}_errors( &
            self) result(errors)
        class(fallible_{quantity}_t), intent(in) :: self
        type(error_list_t) :: errors

        errors = self%errors_
    end function

    function fal_{quantity}_unit_from_unit( &
            unit) result(fallible_{quantity}_unit)
        class({quantity}_unit_t), intent(in) :: unit
        type(fallible_{quantity}_unit_t) :: fallible_{quantity}_unit

        allocate(fallible_{quantity}_unit%unit_, source = unit)
    end function

    function fal_{quantity}_unit_from_errors( &
            errors) result(fallible_{quantity}_unit)
        type(error_list_t), intent(in) :: errors
        type(fallible_{quantity}_unit_t) :: fallible_{quantity}_unit

        fallible_{quantity}_unit%errors_ = errors
    end function

    function fal_{quantity}_unit_from_fal_quantity_unit( &
            fallible_{quantity}_unit, &
            module_, &
            procedure_) &
            result(new_fallible_{quantity}_unit)
        type(fallible_{quantity}_unit_t), intent(in) :: fallible_{quantity}_unit
        type(module_t), intent(in) :: module_
        type(procedure_t), intent(in) :: procedure_
        type(fallible_{quantity}_unit_t) :: new_fallible_{quantity}_unit

        if (fallible_{quantity}_unit%failed()) then
            new_fallible_{quantity}_unit%errors_ = error_list_t( &
                    fallible_{quantity}_unit%errors_, module_, procedure_)
        else
            allocate(new_fallible_{quantity}_unit%unit_, source = &
                    fallible_{quantity}_unit%unit_)
        end if
    end function

    elemental function fallible_{quantity}_unit_failed( &
            self) result(failed)
        class(fallible_{quantity}_unit_t), intent(in) :: self
        logical :: failed

        failed = self%errors_%has_any()
    end function

    function fallible_{quantity}_unit_unit( &
            self) result(unit)
        class(fallible_{quantity}_unit_t), intent(in) :: self
        class({quantity}_unit_t), allocatable :: unit

        allocate(unit, source = self%unit_)
    end function

    impure elemental function fallible_{quantity}_unit_errors( &
            self) result(errors)
        class(fallible_{quantity}_unit_t), intent(in) :: self
        type(error_list_t) :: errors

        errors = self%errors_
    end function

    elemental function {quantity}_simple_unit_to_string(self) result(string)
        class({quantity}_simple_unit_t), intent(in) :: self
        type(varying_string) :: string

        string = trim(self%symbol)
    end function

    pure function {quantity}_simple_value_to_string(self, value_) result(string)
        class({quantity}_simple_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: value_
        type(varying_string) :: string

        string = value_ // " " // self%to_string()
    end function

    function {quantity}_simple_parse_as(self, string) result(fallible_{quantity})
        class({quantity}_simple_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: string
        type(fallible_{quantity}_t) :: fallible_{quantity}

        type(parse_result_t) :: parse_result

        parse_result = parse_with(the_parser, string)
        if (parse_result%ok) then
            select type (the_number => parse_result%parsed)
            type is (parsed_rational_t)
                fallible_{quantity} = fallible_{quantity}_t(the_number%value_.unit.self)
            end select
        else
            fallible_{quantity} = fallible_{quantity}_t(error_list_t(fatal_t( &
                    PARSE_ERROR, &
                    module_t(MODULE_NAME), &
                    procedure_t("simple_parse_as"), &
                    parse_result%message)))
        end if
    contains
        function the_parser(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = then_drop( &
                    then_drop(parse_rational, parse_space, state_), &
                    parse_unit)
        end function

        function parse_unit(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = then_drop( &
                    parse_string(trim(self%symbol), state_), &
                    parse_end)
        end function

        function parse_end(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = either(parse_end_of_input, parse_whitespace, state_)
        end function
    end function

    function parse_{quantity}_unit_c(string) result(fallible_{quantity}_unit)
        character(len=*), intent(in) :: string
        type(fallible_{quantity}_unit_t) :: fallible_{quantity}_unit

        fallible_{quantity}_unit = fallible_{quantity}_unit_t( &
                parse_{quantity}_unit(var_str(string), provided_{quantity}_units), &
                module_t(MODULE_NAME), &
                procedure_t("parse_{quantity}_unit_c"))
    end function

    function parse_{quantity}_unit_s(string) result(fallible_{quantity}_unit)
        type(varying_string), intent(in) :: string
        type(fallible_{quantity}_unit_t) :: fallible_{quantity}_unit

        fallible_{quantity}_unit = fallible_{quantity}_unit_t( &
                parse_{quantity}_unit(string, provided_{quantity}_units), &
                module_t(MODULE_NAME), &
                procedure_t("parse_{quantity}_unit_s"))
    end function

    function parse_{quantity}_unit_with_units_c( &
            string, units) result(fallible_{quantity}_unit)
        character(len=*), intent(in) :: string
        class({quantity}_unit_t), intent(in) :: units(:)
        type(fallible_{quantity}_unit_t) :: fallible_{quantity}_unit

        fallible_{quantity}_unit = fallible_{quantity}_unit_t( &
                parse_{quantity}_unit(var_str(string), units), &
                module_t(MODULE_NAME), &
                procedure_t("parse_{quantity}_unit_with_units_c"))
    end function

    function parse_{quantity}_unit_with_units_s( &
            string, units) result(fallible_{quantity}_unit)
        type(varying_string), intent(in) :: string
        class({quantity}_unit_t), intent(in) :: units(:)
        type(fallible_{quantity}_unit_t) :: fallible_{quantity}_unit

        integer :: i
        type(varying_string), allocatable :: unit_strings(:)

        do i = 1, size(units)
            if (string == units(i)%to_string()) then
                fallible_{quantity}_unit = fallible_{quantity}_unit_t(units(i))
                return
            end if
        end do
        allocate(unit_strings(size(units)))
        do i = 1, size(units)
            unit_strings(i) = units(i)%to_string()
        end do
        fallible_{quantity}_unit = fallible_{quantity}_unit_t(error_list_t(fatal_t( &
                UNKNOWN_UNIT, &
                module_t(MODULE_NAME), &
                procedure_t("parse_{quantity}_unit_with_units_s"), &
                '"' // string // '", known units: [' // join(unit_strings, ', ') // ']')))
    end function""".format(quantity=quantity.name, unit_name = quantity.units[0].name)

def trig_funcs(quantity: Quantity) -> str:
    return """

    elemental function sin_({quantity})
        type({quantity}_t), intent(in) :: {quantity}
        double precision :: sin_

        sin_ = sin({quantity}%{units})
    end function

    elemental function cos_({quantity})
        type({quantity}_t), intent(in) :: {quantity}
        double precision :: cos_

        cos_ = cos({quantity}%{units})
    end function

    elemental function tan_({quantity})
        type({quantity}_t), intent(in) :: {quantity}
        double precision :: tan_

        tan_ = tan({quantity}%{units})
    end function

    elemental function asin_(number) result({quantity})
        double precision, intent(in) :: number
        type({quantity}_t) :: {quantity}

        {quantity}%{units} = asin(number)
    end function

    elemental function acos_(number) result({quantity})
        double precision, intent(in) :: number
        type({quantity}_t) :: {quantity}

        {quantity}%{units} = acos(number)
    end function

    elemental function atan_(number) result({quantity})
        double precision, intent(in) :: number
        type({quantity}_t) :: {quantity}

        {quantity}%{units} = atan(number)
    end function

    elemental function atan2_(y, x) result({quantity})
        double precision, intent(in) :: y
        double precision, intent(in) :: x
        type({quantity}_t) :: {quantity}

        {quantity}%{units} = atan2(y, x)
    end function""".format(quantity=quantity.name, units=quantity.units[0].name)

def interquantity_interfaces(inter_ops : [InterOp]) -> str:
    return ("""
    interface operator(*)
"""
    + "\n".join(["        module procedure {q1}_times_{q2}".format(q1=op.lhs_name, q2=op.rhs_name) for op in inter_ops if isinstance(op, MultOp)])
    + """
    end interface

    interface operator(/)
"""
    + "\n".join(["        module procedure {q1}_div_by_{q2}".format(q1=op.numer_name, q2=op.denom_name) for op in inter_ops if isinstance(op, DivOp)])
    + """
    end interface

    interface operator(-)
        module procedure temperature_minus_delta_temperature
        module procedure temperature_minus_temperature
    end interface

    interface operator(+)
        module procedure delta_temperature_plus_temperature
        module procedure temperature_plus_delta_temperature
    end interface

    interface sqrt
        module procedure square_root_of_area
        module procedure square_root_of_enthalpy
    end interface

    interface cbrt
        module procedure cube_root_of_volume
    end interface"""
    )

def interquantity_implementation(inter_ops : [InterOp]) -> str:
    return (
        "\n\n".join([
        """    elemental function {q1}_times_{q2}( &
            {q1_var}, {q2_var}) result({q3})
        type({q1}_t), intent(in) :: {q1_var}
        type({q2}_t), intent(in) :: {q2_var}
        {q3_type_decl} :: {q3}

        {q3}{u3} = &
                {q1_var}%{u1} &
                * {q2_var}%{u2}
    end function""".format(
        q1=op.lhs_name,
        q1_var="{q}1".format(q=op.lhs_name) if op.lhs_name == op.rhs_name else op.lhs_name,
        q2=op.rhs_name,
        q2_var="{q}2".format(q=op.rhs_name) if op.lhs_name == op.rhs_name else op.rhs_name,
        q3=op.result_name, 
        u1=op.lhs_units, 
        u2=op.rhs_units,
        u3="" if op.result_units == "" else "%{u}".format(u=op.result_units),
        q3_type_decl="double precision" if op.result_units == "" else "type({q}_t)".format(q=op.result_name)
    ) for op in inter_ops if isinstance(op, MultOp)
    ])
    + "\n\n"
    + "\n\n".join([
        """    elemental function {q1}_div_by_{q2}( &
            {q1}, {q2_var}) result({q3_var})
        {q1_type_decl}, intent(in) :: {q1}
        type({q2}_t), intent(in) :: {q2_var}
        {q3_type_decl} :: {q3_var}

        {q3_var}{u3} = &
                {q1_op} &
                / {q2_var}%{u2}
    end function""".format(
        q1=op.numer_name,
        q1_type_decl="double precision" if op.numer_units == "" else "type({q}_t)".format(q=op.numer_name),
        q1_op="number" if op.numer_units == "" else "{q}%{u}".format(q=op.numer_name, u=op.numer_units),
        q2=op.denom_name, 
        q2_var="{q}2".format(q=op.denom_name) if op.denom_name == op.quot_name else op.denom_name,
        q3=op.quot_name, 
        q3_var="{q}3".format(q=op.quot_name) if op.denom_name == op.quot_name else op.quot_name,
        u2=op.denom_units,
        u3="" if op.quot_units == "" else "%{u}".format(u=op.quot_units),
        q3_type_decl="double precision" if op.quot_units == "" else "type({q}_t)".format(q=op.quot_name)
    ) for op in inter_ops if isinstance(op, DivOp)
    ])
    + """

    elemental function temperature_minus_delta_temperature( &
            temperature, delta_temperature) result(new_temperature)
        type(temperature_t), intent(in) :: temperature
        type(delta_temperature_t), intent(in) :: delta_temperature
        type(temperature_t) :: new_temperature

        new_temperature%kelvin = temperature%kelvin - delta_temperature%delta_kelvin
    end function

    elemental function temperature_minus_temperature(lhs, rhs) result(delta_temperature)
        type(temperature_t), intent(in) :: lhs, rhs
        type(delta_temperature_t) :: delta_temperature

        delta_temperature%delta_kelvin = lhs%kelvin - rhs%kelvin
    end function

    elemental function delta_temperature_plus_temperature( &
            delta_temperature, temperature) result(new_temperature)
        type(delta_temperature_t), intent(in) :: delta_temperature
        type(temperature_t), intent(in) :: temperature
        type(temperature_t) :: new_temperature

        new_temperature%kelvin = delta_temperature%delta_kelvin + temperature%kelvin
    end function

    elemental function temperature_plus_delta_temperature( &
            temperature, delta_temperature) result(new_temperature)
        type(temperature_t), intent(in) :: temperature
        type(delta_temperature_t), intent(in) :: delta_temperature
        type(temperature_t) :: new_temperature

        new_temperature%kelvin = temperature%kelvin + delta_temperature%delta_kelvin
    end function

    elemental function square_root_of_area(area) result(length)
        type(area_t), intent(in) :: area
        type(length_t) :: length

        length%meters = sqrt(area%square_meters)
    end function

    elemental function square_root_of_enthalpy(enthalpy) result(speed)
        type(enthalpy_t), intent(in) :: enthalpy
        type(speed_t) :: speed

        speed%meters_per_second = sqrt(enthalpy%joules_per_kilogram)
    end function

    elemental function cube_root_of_volume(volume) result(length)
        type(volume_t), intent(in) :: volume
        type(length_t) :: length

        length%meters = volume%cubic_meters**(1.d0/3.d0)
    end function
"""
    )

def quantity_test(quantity : Quantity) -> str:
    if isinstance(quantity.specific, Standard):
        return standard_quantity_test(quantity)
    elif isinstance(quantity.specific, Temperature):
        return temperature_quantity_test(quantity)
    elif isinstance(quantity.specific, Angle):
        return standard_quantity_test(quantity) # TODO: add tests for trig functions

def standard_quantity_test(quantity : Quantity) -> str:
    return (
        test_desc_before_addable(quantity)
        + test_desc_addable(quantity)
        + test_after_addable_def(quantity)
        + test_def_addable(quantity)
        + test_def_after_addable(quantity)
    )

def test_desc_before_addable(quantity : Quantity) -> str:
    return """module {quantity}_test
    use double_precision_generator_m, only: DOUBLE_PRECISION_GENERATOR
    use double_precision_pair_generator_m, only: DOUBLE_PRECISION_PAIR_GENERATOR
    use double_precision_pair_input_m, only: double_precision_pair_input_t
    use erloff, only: error_list_t
    use iso_varying_string, only: operator(//)
    use non_zero_double_precision_generator_m, only: &
            NON_ZERO_DOUBLE_PRECISION_GENERATOR
    use non_zero_double_precision_pair_generator_m, only: &
            NON_ZERO_DOUBLE_PRECISION_PAIR_GENERATOR
    use quaff
    use quaff_asserts_m, only: assert_equals, assert_equals_within_relative
    use {quantity}_utilities_m, only: &
            units_input_t, units_pair_input_t, make_units_examples
    use units_examples_m, only: units_examples_t
    use veggies, only: &
            double_precision_input_t, &
            input_t, &
            result_t, &
            test_item_t, &
            test_result_item_t, &
            assert_equals, &
            assert_equals_within_relative, &
            assert_not, &
            assert_that, &
            describe, &
            fail, &
            it

    implicit none
    private
    public :: test_{quantity}
contains
    function test_{quantity}() result(tests)
        type(test_item_t) :: tests

        type(units_examples_t) :: examples

        examples = make_units_examples(provided_{quantity}_units)
        tests = describe( &
                "{quantity}_t", &
                [ it( &
                        "returns the same value given the same units", &
                        examples%units(), &
                        check_round_trip) &
                , it( &
                        "preserves its value converting to and from a string", &
                        examples%units(), &
                        check_to_and_from_string) &
                , it( &
                        "returns an error trying to parse a bad string", &
                        check_bad_string) &
                , it( &
                        "returns an error trying to parse an unknown unit", &
                        check_bad_unit) &
                , it( &
                        "returns an error trying to parse a bad number", &
                        check_bad_number) &""".format(quantity=quantity.name, units=quantity.units[0].name)

def test_desc_addable(quantity : Quantity) -> str:
    return """
                , it( &
                        "the conversion factors between two units are inverses", &
                        examples%pairs(), &
                        check_conversion_factors_inverse) &
                , it("arrays can be summed", check_sum) &
                , it("can take the absolute value", check_abs) &
                , it("can be negated", DOUBLE_PRECISION_GENERATOR, check_negation) &
                , it( &
                        "adding zero returns the original {quantity}", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_add_zero) &
                , it( &
                        "subtracting zero returns the original {quantity}", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_subtract_zero) &
                , it( &
                        "adding and subtracting the same {quantity} &
                        &returns the original {quantity}", &
                        DOUBLE_PRECISION_PAIR_GENERATOR, &
                        check_add_subtract) &""".format(quantity=quantity.name, units=quantity.units[0].name)

def test_after_addable_def(quantity : Quantity) -> str:
    return """
                , it( &
                        "multiplying by one returns the original {quantity}", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_multiply_by_one) &
                , it( &
                        "multiplying by zero returns zero {quantity}", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_multiply_by_zero) &
                , it( &
                        "dividing by one returns the original {quantity}", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_divide_by_one) &
                , it( &
                        "dividing by itself returns one", &
                        NON_ZERO_DOUBLE_PRECISION_GENERATOR, &
                        check_divide_by_self) &
                , it( &
                        "multiplying and dividing by the same number returns the original {quantity}", &
                        NON_ZERO_DOUBLE_PRECISION_PAIR_GENERATOR, &
                        check_multiply_divide) &
                , it( &
                        "min and max return the smaller or larger of its argument respectively", &
                        check_min_max) &
                , describe( &
                        "operator(==)", &
                        [ it( &
                                "is true for the same {quantity}", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_with_same_number) &
                        , it( &
                                "is false for different {quantity}s", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_with_different_numbers) &
                        ]) &
                , describe( &
                        "operator(/=)", &
                        [ it( &
                                "is false for the same {quantity}", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_not_equal_with_same_number) &
                        , it( &
                                "is true for different {quantity}s", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_not_equal_with_different_numbers) &
                        ]) &
                , describe( &
                        "%equal({quantity}, within)", &
                        [ it( &
                                "is true for the same {quantity} even for tiny tolerance", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_within_with_same_number) &
                        , it( &
                                "is true for sufficiently close values", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_within_with_close_numbers) &
                        , it( &
                                "is false for sufficiently different numbers", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_within_with_different_numbers) &
                        ]) &
                , describe( &
                        "operator(>=)", &
                        [ it( &
                                "is true if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_or_equal_with_greater_number) &
                        , it( &
                                "is true if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_or_equal_with_same_numbers) &
                        , it( &
                                "is false if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_or_equal_with_lesser_number) &
                        ]) &
                , describe( &
                        "operator(<=)", &
                        [ it( &
                                "is true if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_or_equal_with_less_number) &
                        , it( &
                                "is true if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_or_equal_with_same_numbers) &
                        , it( &
                                "is false if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_or_equal_with_greater_number) &
                        ]) &
                , describe( &
                        "operator(>)", &
                        [ it( &
                                "is true if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_with_greater_number) &
                        , it( &
                                "is false if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_with_same_numbers) &
                        , it( &
                                "is false if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_with_lesser_number) &
                        ]) &
                , describe( &
                        "operator(<)", &
                        [ it( &
                                "is true if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_with_less_number) &
                        , it( &
                                "is false if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_with_same_numbers) &
                        , it( &
                                "is false if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_with_greater_number) &
                        ]) &
                ])
    end function

    function check_round_trip(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_input_t)
            result_ = check_round_trip_in(input%unit())
        class default
            result_ = fail("Expected to get a units_input_t")
        end select
    end function

    function check_round_trip_in(units) result(result_)
        class({quantity}_unit_t), intent(in) :: units
        type(result_t) :: result_

        type(test_item_t) :: the_test
        type(test_result_item_t) :: the_result

        the_test = it(to_string(units), DOUBLE_PRECISION_GENERATOR, check_round_trip_)
        the_result = the_test%run()
        result_ = assert_that(the_result%passed(), the_result%verbose_description(.false.))
    contains
        pure function check_round_trip_(input) result(result__)
            class(input_t), intent(in) :: input
            type(result_t) :: result__

            type({quantity}_t) :: intermediate
            double precision :: original
            real :: lower_precision
            integer :: int_part

            select type (input)
            type is (double_precision_input_t)
                original = input%input()
                intermediate = original.unit.units
                result__ = assert_equals_within_relative( &
                        original, &
                        intermediate.in.units, &
                        1.0d-12)
                lower_precision = real(original)
                intermediate = lower_precision.unit.units
                result__ = result__.and.assert_equals_within_relative( &
                        real(lower_precision, kind=kind(original)), &
                        intermediate.in.units, &
                        1.0d-12)
                int_part = int(original)
                intermediate = int_part.unit.units
                result__ = result__.and.assert_equals_within_relative( &
                        real(int_part, kind=kind(original)), &
                        intermediate.in.units, &
                        1.0d-12)
            class default
                result__ = fail("Expected to get a double_precision_input_t")
            end select
        end function
    end function

    function check_to_and_from_string(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_input_t)
            result_ = check_string_trip(input%unit())
        class default
            result_ = fail("Expected to get an units_input_t")
        end select
    end function

    function check_string_trip(units) result(result_)
        class({quantity}_unit_t), intent(in) :: units
        type(result_t) :: result_

        type(test_item_t) :: the_test
        type(test_result_item_t) :: the_result

        the_test = it(to_string(units), DOUBLE_PRECISION_GENERATOR, do_check)
        the_result = the_test%run()
        result_ = assert_that(the_result%passed(), the_result%verbose_description(.false.))
    contains
        function do_check(input) result(result__)
            class(input_t), intent(in) :: input
            type(result_t) :: result__

            type(error_list_t) :: errors
            type({quantity}_t) :: original_{quantity}
            type(fallible_{quantity}_t) :: maybe_{quantity}
            type({quantity}_t) :: new_{quantity}

            select type (input)
            type is (double_precision_input_t)
                original_{quantity} = input%input().unit.units
                maybe_{quantity} = parse_{quantity}( &
                        to_string(original_{quantity}, units))
                new_{quantity} = maybe_{quantity}%{quantity}()
                errors = maybe_{quantity}%errors()
                result__ = &
                        assert_equals( &
                                original_{quantity}, &
                                new_{quantity}) &
                        .and.assert_not(errors%has_any(), errors%to_string())
            class default
                result__ = fail("Expected to get a double_precision_input_t")
            end select
        end function
    end function

    function check_bad_string() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_{quantity}_t) :: maybe_{quantity}

        maybe_{quantity} = parse_{quantity}("bad")
        errors = maybe_{quantity}%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_bad_unit() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_{quantity}_t) :: maybe_{quantity}

        maybe_{quantity} = parse_{quantity}("1.0 {sym}bad", [{units}])
        errors = maybe_{quantity}%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_bad_number() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_{quantity}_t) :: maybe_{quantity}

        maybe_{quantity} = parse_{quantity}("bad {sym}")
        errors = maybe_{quantity}%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function
""".format(quantity=quantity.name, units=quantity.units[0].name, sym=quantity.units[0].symbol)

def test_def_addable(quantity : Quantity) -> str:
    return """
    function check_conversion_factors_inverse(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_pair_input_t)
            result_ = check_conversion_factors_are_inverse(input%first(), input%second_())
        class default
            result_ = fail("Expected to get a units_pair_input_t")
        end select
    end function

    pure function check_conversion_factors_are_inverse( &
            from, to) result(result_)
        class({quantity}_unit_t), intent(in) :: to
        class({quantity}_unit_t), intent(in) :: from
        type(result_t) :: result_

        double precision :: factor1
        double precision :: factor2

        factor1 = (1.0d0.unit.from).in.to
        factor2 = (1.0d0.unit.to).in.from
        result_ = assert_equals_within_relative( &
                factor1, &
                1.0d0 / factor2, &
                1.0d-12, &
                to_string(from) // " to " // to_string(to))
    end function

    pure function check_sum() result(result_)
        type(result_t) :: result_

        double precision, parameter :: numbers(*) = [1.0d0, 2.0d0, 3.0d0]

        result_ = assert_equals( &
                sum(numbers).unit.{units}, &
                sum(numbers.unit.{units}))
    end function

    pure function check_abs() result(result_)
        type(result_t) :: result_

        result_ = &
                assert_equals( &
                        abs(1.0d0).unit.{units}, &
                        abs(1.0d0.unit.{units})) &
                .and.assert_equals( &
                        abs(-1.0d0).unit.{units}, &
                        abs((-1.0d0).unit.{units}))
    end function

    pure function check_negation(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type(input)
        type is (double_precision_input_t)
            result_ = assert_equals( &
                    (-input%input()).unit.{units}, &
                    -(input%input().unit.{units}))
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_add_zero(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}
        type({quantity}_t) :: zero

        select type(input)
        type is (double_precision_input_t)
            {quantity} = input%input().unit.{units}
            zero = 0.0d0.unit.{units}
            result_ = assert_equals({quantity}, {quantity} + zero)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_subtract_zero(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}
        type({quantity}_t) :: zero

        select type(input)
        type is (double_precision_input_t)
            {quantity} = input%input().unit.{units}
            zero = 0.0d0.unit.{units}
            result_ = assert_equals({quantity}, {quantity} - zero)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_add_subtract(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type(input)
        type is (double_precision_pair_input_t)
            {quantity}1 = input%first().unit.{units}
            {quantity}2 = input%second_().unit.{units}
            result_ = assert_equals_within_relative( &
                    {quantity}1, &
                    ({quantity}1 + {quantity}2) - {quantity}2, &
                    1.0d-8, &
                    "{quantity}1 = " // to_string({quantity}1) &
                    // ", {quantity}2 = " // to_string({quantity}2))
        class default
            result_ = fail("Expected a double_precision_pair_input_t")
        end select
    end function
""".format(quantity=quantity.name, units=quantity.units[0].name)

def test_def_after_addable(quantity : Quantity) -> str:
    return """
    pure function check_multiply_by_one(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}

        select type(input)
        type is (double_precision_input_t)
            {quantity} = input%input().unit.{units}
            result_ = &
                    assert_equals({quantity}, {quantity} * 1.0d0) &
                    .and. assert_equals({quantity}, {quantity} * 1.0)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_multiply_by_zero(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}
        type({quantity}_t) :: zero

        select type(input)
        type is (double_precision_input_t)
            {quantity} = input%input().unit.{units}
            zero = 0.0d0.unit.{units}
            result_ = &
                    assert_equals(zero, {quantity} * 0.0d0) &
                    .and. assert_equals(zero, {quantity} * 0.0)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_divide_by_one(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}

        select type(input)
        type is (double_precision_input_t)
            {quantity} = input%input().unit.{units}
            result_ = &
                    assert_equals({quantity}, {quantity} / 1.0d0) &
                    .and. assert_equals({quantity}, {quantity} / 1.0)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_divide_by_self(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}

        select type(input)
        type is (double_precision_input_t)
            {quantity} = input%input().unit.{units}
            result_ = assert_equals(1.0d0, {quantity} / {quantity})
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_multiply_divide(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}

        select type (input)
        type is (double_precision_pair_input_t)
            {quantity} = input%first().unit.{units}
            result_ = assert_equals( &
                    {quantity}, &
                    ({quantity} * input%second_()) / input%second_())
        class default
            result_ = fail("Expected a double_precision_pair_input_t")
        end select
    end function
    
    pure function check_min_max() result(result_)
        type(result_t) :: result_
        
        type({quantity}_t) :: smaller, larger

        smaller = 1.d0.unit.{units}
        larger = 2.d0.unit.{units}

        result_ = &
            assert_equals(smaller, min(smaller, larger)) &
            .and.assert_equals(smaller, min(larger, smaller)) &
            .and.assert_equals(larger, max(smaller, larger)) &
            .and.assert_equals(larger, max(larger, smaller))
    end function

    pure function check_equal_with_same_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: the_{quantity}

        select type (input)
        type is (double_precision_input_t)
            the_{quantity} = input%input().unit.{units}
            result_ = assert_that( &
                    the_{quantity} == the_{quantity}, &
                    to_string(the_{quantity}) // " == " // to_string(the_{quantity}))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_with_different_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = (input%input() + 1.0d0).unit.{units}
            result_ = assert_not( &
                    {quantity}1 == {quantity}2, &
                    to_string({quantity}1) // " == " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_not_equal_with_same_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: the_{quantity}

        select type (input)
        type is (double_precision_input_t)
            the_{quantity} = input%input().unit.{units}
            result_ = assert_not( &
                    the_{quantity} /= the_{quantity}, &
                    to_string(the_{quantity}) // " /= " // to_string(the_{quantity}))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_not_equal_with_different_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = (input%input() + 1.0d0).unit.{units}
            result_ = assert_that( &
                    {quantity}1 /= {quantity}2, &
                    to_string({quantity}1) // " /= " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_within_with_same_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: the_{quantity}
        type({quantity}_t) :: tolerance

        select type (input)
        type is (double_precision_input_t)
            the_{quantity} = input%input().unit.{units}
            tolerance = tiny(1.0d0).unit.{units}
            result_ = assert_that( &
                    equal(the_{quantity}, the_{quantity}, within = tolerance), &
                    "equal(" // to_string(the_{quantity}) // ", " &
                        // to_string(the_{quantity}) // ", within = " &
                        // to_string(tolerance) // ")")
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_within_with_close_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2
        type({quantity}_t) :: tolerance

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = (input%input() + 0.05d0).unit.{units}
            tolerance = 0.1d0.unit.{units}
            result_ = assert_that( &
                    equal({quantity}1, {quantity}2, within = tolerance), &
                    "equal(" // to_string({quantity}1) // ", " &
                        // to_string({quantity}2) // ", within = " &
                        // to_string(tolerance) // ")")
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_within_with_different_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2
        type({quantity}_t) :: tolerance

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = (input%input() + 0.2d0).unit.{units}
            tolerance = 0.1d0.unit.{units}
            result_ = assert_not( &
                    equal({quantity}1, {quantity}2, within = tolerance), &
                    "equal(" // to_string({quantity}1) // ", " &
                    // to_string({quantity}2) // ", within = " &
                    // to_string(tolerance) // ")")
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_or_equal_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit. {units}
            {quantity}2 = (input%input() - 1.0d0).unit.{units}
            result_ = assert_that( &
                    {quantity}1 >= {quantity}2, &
                    to_string({quantity}1) // " >= " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_or_equal_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = input%input().unit.{units}
            result_ = assert_that( &
                    {quantity}1 >= {quantity}2, &
                    to_string({quantity}1) // " >= " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_or_equal_with_lesser_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = (input%input() + 1.0d0).unit.{units}
            result_ = assert_not( &
                    {quantity}1 >= {quantity}2, &
                    to_string({quantity}1) // " >= " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_or_equal_with_less_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit. {units}
            {quantity}2 = (input%input() + 1.0d0).unit.{units}
            result_ = assert_that( &
                    {quantity}1 <= {quantity}2, &
                    to_string({quantity}1) // " <= " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_or_equal_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = input%input().unit.{units}
            result_ = assert_that( &
                    {quantity}1 <= {quantity}2, &
                    to_string({quantity}1) // " <= " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_or_equal_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = (input%input() - 1.0d0).unit.{units}
            result_ = assert_not( &
                    {quantity}1 <= {quantity}2, &
                    to_string({quantity}1) // " <= " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit. {units}
            {quantity}2 = (input%input() - 1.0d0).unit.{units}
            result_ = assert_that( &
                    {quantity}1 > {quantity}2, &
                    to_string({quantity}1) // " > " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = input%input().unit.{units}
            result_ = assert_not( &
                    {quantity}1 > {quantity}2, &
                    to_string({quantity}1) // " > " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_with_lesser_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = (input%input() + 1.0d0).unit.{units}
            result_ = assert_not( &
                    {quantity}1 > {quantity}2, &
                    to_string({quantity}1) // " > " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_with_less_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit. {units}
            {quantity}2 = (input%input() + 1.0d0).unit.{units}
            result_ = assert_that( &
                    {quantity}1 < {quantity}2, &
                    to_string({quantity}1) // " < " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = input%input().unit.{units}
            result_ = assert_not( &
                    {quantity}1 < {quantity}2, &
                    to_string({quantity}1) // " <=" // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type({quantity}_t) :: {quantity}1
        type({quantity}_t) :: {quantity}2

        select type (input)
        type is (double_precision_input_t)
            {quantity}1 = input%input().unit.{units}
            {quantity}2 = (input%input() - 1.0d0).unit.{units}
            result_ = assert_not( &
                    {quantity}1 < {quantity}2, &
                    to_string({quantity}1) // " < " // to_string({quantity}2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function
end module
""".format(quantity=quantity.name, units=quantity.units[0].name)

def temperature_quantity_test(quantity : Quantity) -> str:
    return (
        test_desc_before_addable(quantity)
        + test_after_addable_def(quantity)
        + test_def_after_addable(quantity)
    )

def quantity_test_utility(quantity_name : str) -> str:
    return """module {quantity}_utilities_m
    use quaff, only: {quantity}_unit_t
    use test_utilities_m, only: combinations
    use units_examples_m, only: units_examples_t
    use veggies, only: example_t, input_t

    implicit none
    private
    public :: units_input_t, units_pair_input_t, make_units_examples

    type, extends(input_t) :: units_input_t
        private
        class({quantity}_unit_t), allocatable :: unit_
    contains
        private
        procedure, public :: unit
    end type

    type, extends(input_t) :: units_pair_input_t
        private
        class({quantity}_unit_t), allocatable :: first_
        class({quantity}_unit_t), allocatable :: second__
    contains
        private
        procedure, public :: first
        procedure, public :: second_
    end type

    interface units_input_t
        module procedure units_input_constructor
    end interface

    interface units_pair_input_t
        module procedure units_pair_input_constructor
    end interface
contains
    function units_input_constructor(unit) result(units_input)
        class({quantity}_unit_t), intent(in) :: unit
        type(units_input_t) :: units_input

        allocate(units_input%unit_, source = unit)
    end function

    function unit(self)
        class(units_input_t), intent(in) :: self
        class({quantity}_unit_t), allocatable :: unit

        allocate(unit, source = self%unit_)
    end function

    function units_pair_input_constructor(first, second_) result(units_pair_input)
        class({quantity}_unit_t), intent(in) :: first
        class({quantity}_unit_t), intent(in) :: second_
        type(units_pair_input_t) :: units_pair_input

        allocate(units_pair_input%first_, source = first)
        allocate(units_pair_input%second__, source = second_)
    end function

    function first(self)
        class(units_pair_input_t), intent(in) :: self
        class({quantity}_unit_t), allocatable :: first

        allocate(first, source = self%first_)
    end function

    function second_(self)
        class(units_pair_input_t), intent(in) :: self
        class({quantity}_unit_t), allocatable :: second_

        allocate(second_, source = self%second__)
    end function

    function make_units_examples(units) result(units_examples)
        class({quantity}_unit_t), intent(in) :: units(:)
        type(units_examples_t) :: units_examples

        integer :: i
        integer :: j
        integer :: num_pairs
        type(example_t), allocatable :: pair_examples(:)
        integer :: pair_index
        type(example_t) :: single_examples(size(units))

        single_examples = [(example_t(units_input_t(units(i))), i = 1, size(units))]
        num_pairs = combinations(size(units))
        allocate(pair_examples(num_pairs))
        pair_index = 1
        do i = 1, size(units) - 1
            do j = i + 1, size(units)
                pair_examples(pair_index) = example_t(units_pair_input_t( &
                        units(i), units(j)))
                pair_index = pair_index + 1
            end do
        end do
        units_examples = units_examples_t(single_examples, pair_examples)
    end function
end module
""".format(quantity=quantity_name)

def quantity_asserts(quantity : str, framework : str) -> str:
    return """module {quantity}_asserts_m
    use iso_varying_string, only: varying_string, operator(//), var_str
    use quaff
    use strff, only: to_string
    use {framework}, only: &
            result_t, &
            fail, &
            make_equals_failure_message, &
            make_equals_success_message, &
            make_within_failure_message, &
            make_within_success_message, &
            succeed, &
            with_user_message

    implicit none
    private
    public :: &
            assert_equals, &
            assert_equals_within_absolute, &
            assert_equals_within_relative

    interface assert_equals
        module procedure assert_equals_basic
        module procedure assert_equals_with_message_c
        module procedure assert_equals_with_message_s
        module procedure assert_equals_with_messages_cc
        module procedure assert_equals_with_messages_cs
        module procedure assert_equals_with_messages_sc
        module procedure assert_equals_with_messages_ss
    end interface

    interface assert_equals_within_absolute
        module procedure assert_equals_within_absolute_basic
        module procedure assert_equals_within_absolute_with_message_c
        module procedure assert_equals_within_absolute_with_message_s
        module procedure assert_equals_within_absolute_with_messages_cc
        module procedure assert_equals_within_absolute_with_messages_cs
        module procedure assert_equals_within_absolute_with_messages_sc
        module procedure assert_equals_within_absolute_with_messages_ss
    end interface

    interface assert_equals_within_relative
        module procedure assert_equals_within_relative_basic
        module procedure assert_equals_within_relative_with_message_c
        module procedure assert_equals_within_relative_with_message_s
        module procedure assert_equals_within_relative_with_messages_cc
        module procedure assert_equals_within_relative_with_messages_cs
        module procedure assert_equals_within_relative_with_messages_sc
        module procedure assert_equals_within_relative_with_messages_ss
    end interface
contains
    pure function assert_equals_basic(expected, actual) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type(result_t) :: result_

        result_ = assert_equals(expected, actual, var_str(""), var_str(""))
    end function

    pure function assert_equals_with_message_c( &
            expected, actual, message) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        character(len=*), intent(in) :: message
        type(result_t) :: result_

        result_ = assert_equals( &
                expected, actual, var_str(message), var_str(message))
    end function

    pure function assert_equals_with_message_s( &
            expected, actual, message) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type(varying_string), intent(in) :: message
        type(result_t) :: result_

        result_ = assert_equals( &
                expected, actual, message, message)
    end function

    pure function assert_equals_with_messages_cc( &
            expected, actual, success_message, failure_message) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        character(len=*), intent(in) :: success_message
        character(len=*), intent(in) :: failure_message
        type(result_t) :: result_

        result_ = assert_equals( &
                expected, &
                actual, &
                var_str(success_message), &
                var_str(failure_message))
    end function

    pure function assert_equals_with_messages_cs( &
            expected, actual, success_message, failure_message) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        character(len=*), intent(in) :: success_message
        type(varying_string), intent(in) :: failure_message
        type(result_t) :: result_

        result_ = assert_equals( &
                expected, &
                actual, &
                var_str(success_message), &
                failure_message)
    end function

    pure function assert_equals_with_messages_sc( &
            expected, actual, success_message, failure_message) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type(varying_string), intent(in) :: success_message
        character(len=*), intent(in) :: failure_message
        type(result_t) :: result_

        result_ = assert_equals( &
                expected, &
                actual, &
                success_message, &
                var_str(failure_message))
    end function

    pure function assert_equals_with_messages_ss( &
            expected, &
            actual, &
            success_message, &
            failure_message) &
            result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type(varying_string), intent(in) :: success_message
        type(varying_string), intent(in) :: failure_message
        type(result_t) :: result_

        if (expected == actual) then
            result_ = succeed(with_user_message( &
                    make_equals_success_message( &
                            to_string(expected)), &
                    success_message))
        else
            result_ = fail(with_user_message( &
                    make_equals_failure_message( &
                            to_string(expected), &
                            to_string(actual)), &
                    failure_message))
        end if
    end function

    pure function assert_equals_within_absolute_basic( &
            expected, actual, tolerance) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type({quantity}_t), intent(in) :: tolerance
        type(result_t) :: result_

        result_ = assert_equals_within_absolute( &
                expected, actual, tolerance, var_str(""), var_str(""))
    end function

    pure function assert_equals_within_absolute_with_message_c( &
            expected, actual, tolerance, message) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type({quantity}_t), intent(in) :: tolerance
        character(len=*), intent(in) :: message
        type(result_t) :: result_

        result_ = assert_equals_within_absolute( &
                expected, actual, tolerance, var_str(message), var_str(message))
    end function

    pure function assert_equals_within_absolute_with_message_s( &
            expected, actual, tolerance, message) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type({quantity}_t), intent(in) :: tolerance
        type(varying_string), intent(in) :: message
        type(result_t) :: result_

        result_ = assert_equals_within_absolute( &
                expected, actual, tolerance, message, message)
    end function

    pure function assert_equals_within_absolute_with_messages_cc( &
            expected, &
            actual, &
            tolerance, &
            success_message, &
            failure_message) &
            result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type({quantity}_t), intent(in) :: tolerance
        character(len=*), intent(in) :: success_message
        character(len=*), intent(in) :: failure_message
        type(result_t) :: result_

        result_ = assert_equals_within_absolute( &
                expected, &
                actual, &
                tolerance, &
                var_str(success_message), &
                var_str(failure_message))
    end function

    pure function assert_equals_within_absolute_with_messages_cs( &
            expected, &
            actual, &
            tolerance, &
            success_message, &
            failure_message) &
            result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type({quantity}_t), intent(in) :: tolerance
        character(len=*), intent(in) :: success_message
        type(varying_string), intent(in) :: failure_message
        type(result_t) :: result_

        result_ = assert_equals_within_absolute( &
                expected, &
                actual, &
                tolerance, &
                var_str(success_message), &
                failure_message)
    end function

    pure function assert_equals_within_absolute_with_messages_sc( &
            expected, &
            actual, &
            tolerance, &
            success_message, &
            failure_message) &
            result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type({quantity}_t), intent(in) :: tolerance
        type(varying_string), intent(in) :: success_message
        character(len=*), intent(in) :: failure_message
        type(result_t) :: result_

        result_ = assert_equals_within_absolute( &
                expected, &
                actual, &
                tolerance, &
                success_message, &
                var_str(failure_message))
    end function

    pure function assert_equals_within_absolute_with_messages_ss( &
            expected, &
            actual, &
            tolerance, &
            success_message, &
            failure_message) &
            result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        type({quantity}_t), intent(in) :: tolerance
        type(varying_string), intent(in) :: success_message
        type(varying_string), intent(in) :: failure_message
        type(result_t) :: result_

        if (equal(expected, actual, within = tolerance)) then
            result_ =  succeed(with_user_message( &
                    make_within_success_message( &
                            to_string(expected), &
                            to_string(actual), &
                            to_string(tolerance)), &
                    success_message))
        else
            result_ = fail(with_user_message( &
                    make_within_failure_message( &
                            to_string(expected), &
                            to_string(actual), &
                            to_string(tolerance)), &
                    failure_message))
        end if
    end function

    pure function assert_equals_within_relative_basic( &
            expected, actual, tolerance) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        double precision, intent(in) :: tolerance
        type(result_t) :: result_

        result_ = assert_equals_within_relative( &
                expected, actual, tolerance, var_str(""), var_str(""))
    end function

    pure function assert_equals_within_relative_with_message_c( &
            expected, actual, tolerance, message) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        double precision, intent(in) :: tolerance
        character(len=*), intent(in) :: message
        type(result_t) :: result_

        result_ = assert_equals_within_relative( &
                expected, actual, tolerance, var_str(message), var_str(message))
    end function

    pure function assert_equals_within_relative_with_message_s( &
            expected, actual, tolerance, message) result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        double precision, intent(in) :: tolerance
        type(varying_string), intent(in) :: message
        type(result_t) :: result_

        result_ = assert_equals_within_relative( &
                expected, actual, tolerance, message, message)
    end function

    pure function assert_equals_within_relative_with_messages_cc( &
            expected, &
            actual, &
            tolerance, &
            success_message, &
            failure_message) &
            result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        double precision, intent(in) :: tolerance
        character(len=*), intent(in) :: success_message
        character(len=*), intent(in) :: failure_message
        type(result_t) :: result_

        result_ = assert_equals_within_relative( &
                expected, &
                actual, &
                tolerance, &
                var_str(success_message), &
                var_str(failure_message))
    end function

    pure function assert_equals_within_relative_with_messages_cs( &
            expected, &
            actual, &
            tolerance, &
            success_message, &
            failure_message) &
            result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        double precision, intent(in) :: tolerance
        character(len=*), intent(in) :: success_message
        type(varying_string), intent(in) :: failure_message
        type(result_t) :: result_

        result_ = assert_equals_within_relative( &
                expected, &
                actual, &
                tolerance, &
                var_str(success_message), &
                failure_message)
    end function

    pure function assert_equals_within_relative_with_messages_sc( &
            expected, &
            actual, &
            tolerance, &
            success_message, &
            failure_message) &
            result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        double precision, intent(in) :: tolerance
        type(varying_string), intent(in) :: success_message
        character(len=*), intent(in) :: failure_message
        type(result_t) :: result_

        result_ = assert_equals_within_relative( &
                expected, &
                actual, &
                tolerance, &
                success_message, &
                var_str(failure_message))
    end function

    pure function assert_equals_within_relative_with_messages_ss( &
            expected, &
            actual, &
            tolerance, &
            success_message, &
            failure_message) &
            result(result_)
        type({quantity}_t), intent(in) :: expected
        type({quantity}_t), intent(in) :: actual
        double precision, intent(in) :: tolerance
        type(varying_string), intent(in) :: success_message
        type(varying_string), intent(in) :: failure_message
        type(result_t) :: result_

        if (equal(expected, actual, within = tolerance)) then
            result_ =  succeed(with_user_message( &
                    make_within_success_message( &
                            to_string(expected), &
                            to_string(actual), &
                            to_string(tolerance * 100.0d0) // "%"), &
                    success_message))
        else
            result_ = fail(with_user_message( &
                    make_within_failure_message( &
                            to_string(expected), &
                            to_string(actual), &
                            to_string(tolerance * 100.0d0) // "%"), &
                    failure_message))
        end if
    end function
end module
""".format(quantity=quantity, framework=framework)

def quaff_asserts(quantity_names : [str]) -> str:
    return (
        "module quaff_asserts_m\n"
        + "\n".join([quantity_assert_use(quantity) for quantity in quantity_names])
        + "\nend module\n"
    )

def quantity_assert_use(name : str) -> str:
    return """    use {quantity}_asserts_m, only: &
            assert_equals, &
            assert_equals_within_absolute, &
            assert_equals_within_relative""".format(quantity = name)

def interquantity_tests(interops : [InterOp]) -> str:
    return ("""module interquantity_test
    use quaff
    use quaff_asserts_m, only: assert_equals
    use veggies, only: result_t, test_item_t, describe, it, assert_equals
    
    implicit none
    private
    public :: test_interquantity_operators
contains
    function test_interquantity_operators() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
            "operations between quantities give expected results", &
            [ """
    + "\n            , ".join([
        ("it('2 {u1} * 3 {u2} = 6 {u3}', chk_{q1}_mul_{q2}) &".format(u1=op.lhs_sym, u2=op.rhs_sym, u3=op.result_sym, q1=op.lhs_name, q2=op.rhs_name)
        if isinstance(op, MultOp) else
        "it('6 {u1} / 3 {u2} = 2 {u3}', chk_{q1}_div_{q2}) &".format(u1=op.numer_sym, u2=op.denom_sym, u3=op.quot_sym, q1=op.numer_name, q2=op.denom_name))
        for op in interops
    ])
    + """
            , it('6 K - 4 K = 2 K', chk_temp_minus_temp) &
            , it('6 K - 4 K = 2 K', chk_temp_minus_delta_temp) &
            , it('6 K + 3 K = 9 K', chk_temp_plus_delta_temp) &
            , it('6 K + 3 K = 9 K', chk_delta_temp_plus_temp) &
            , it('sqrt(9 m^2) = 3 m', chk_sqrt_area) &
            , it('sqrt(9 J/kg) = 3 m/s', chk_sqrt_enthalpy) &
            , it('cbrt(27 m^3) = 3 m', chk_cbrt_volume) &
            ])
    end function

"""
    + "\n\n".join([
        ("""    pure function chk_{q1}_mul_{q2}() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0{u3}, &
                (2.d0.unit.{u1}) * (3.d0.unit.{u2}))
    end function""".format(q1=op.lhs_name, q2=op.rhs_name, u1=op.lhs_units, u2=op.rhs_units, u3="" if op.result_units == "" else ".unit.{0}".format(op.result_units))
    if isinstance(op, MultOp) else
    """    pure function chk_{q1}_div_{q2}() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.{u3}, &
                (6.d0{u1}) / (3.d0.unit.{u2}))
    end function""".format(q1=op.numer_name, q2=op.denom_name, u1="" if op.numer_units == "" else ".unit.{0}".format(op.numer_units), u2=op.denom_units, u3=op.quot_units))
    for op in interops
    ])
    + """

    pure function chk_temp_minus_temp() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.kelvin) - (4.d0.unit.kelvin))
    end function
    
    pure function chk_temp_minus_delta_temp() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                2.d0.unit.kelvin, &
                (6.d0.unit.kelvin) - (4.d0.unit.delta_kelvin))
    end function
    
    pure function chk_temp_plus_delta_temp() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                9.d0.unit.kelvin, &
                (6.d0.unit.kelvin) + (3.d0.unit.delta_kelvin))
    end function
    
    pure function chk_delta_temp_plus_temp() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                9.d0.unit.kelvin, &
                (6.d0.unit.delta_kelvin) + (3.d0.unit.kelvin))
    end function
    
    pure function chk_sqrt_area() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                3.d0.unit.meters, &
                sqrt(9.d0.unit.square_meters))
    end function
    
    pure function chk_sqrt_enthalpy() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                3.d0.unit.meters_per_second, &
                sqrt(9.d0.unit.joules_per_kilogram))
    end function

    pure function chk_cbrt_volume() result(result_)
        type(result_t) :: result_

        result_ = assert_equals( &
                3.d0.unit.meters, &
                cbrt(27.d0.unit.cubic_meters))
    end function
end module
"""
    )

def rojff_to_quaff_module(quantities : [Quantity]) -> str:
    return """module rojff_to_quaff
    use erloff, only: error_list_t, fatal_t, module_t, procedure_t
    use quaff
    use rojff, only: &
            fallible_json_value_t, json_object_t, json_string_t, json_value_t

    implicit none
    private
    public :: &
""" + ", &\n".join([fallible_types(quantity) for quantity in quantities]) + """
""" + "\n".join([fallible_interface(quantity) for quantity in quantities]) + """

    character(len=*), parameter :: MODULE_NAME = "rojff_to_quaff"
contains""" + "\n".join([fallible_implementation(quantity) for quantity in quantities]) + """
end module
"""

def fallible_types(quantity : Quantity) -> str:
    return "            fallible_{quantity}_t, &\n            fallible_{quantity}_unit_t".format(quantity=quantity.name)

def fallible_interface(quantity : Quantity) -> str:
    return """
    interface fallible_{quantity}_t
            module procedure fallible_{quantity}_from_object_and_key
            module procedure fallible_{quantity}_from_fallible_json
            module procedure fallible_{quantity}_from_json_value
    end interface

    interface fallible_{quantity}_unit_t
            module procedure fallible_{quantity}_unit_from_object_and_key
            module procedure fallible_{quantity}_unit_from_fallible_json
            module procedure fallible_{quantity}_unit_from_json_value
    end interface""".format(quantity = quantity.name)

def fallible_implementation(quantity : Quantity) -> str:
    return """
    function fallible_{quantity}_from_object_and_key( &
            object, key) result(fallible_{quantity})
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_{quantity}_t) :: fallible_{quantity}

        character(len=*), parameter :: procedure_name = "fallible_{quantity}_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_{quantity} = fallible_{quantity}_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_{quantity} = fallible_{quantity}_t( &
                    fallible_{quantity}_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_{quantity}%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_{quantity}%errors()
                    fallible_{quantity} = fallible_{quantity}_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_{quantity}_from_fallible_json(maybe_value) result(fallible_{quantity})
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_{quantity}_t) :: fallible_{quantity}

        character(len=*), parameter :: procedure_name = "fallible_{quantity}_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_{quantity} = fallible_{quantity}_t(maybe_value%errors)
        else
            fallible_{quantity} = fallible_{quantity}_t( &
                    fallible_{quantity}_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_{quantity}_from_json_value(value_) result(fallible_{quantity})
        class(json_value_t), intent(in) :: value_
        type(fallible_{quantity}_t) :: fallible_{quantity}

        character(len=*), parameter :: procedure_name = "fallible_{quantity}_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_{quantity} = fallible_{quantity}_t( &
                    parse_{quantity}(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_{quantity} = fallible_{quantity}_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a {quantity}")))
        end select
    end function

    function fallible_{quantity}_unit_from_object_and_key( &
            object, key) result(fallible_{quantity}_unit)
        type(json_object_t), intent(in) :: object
        character(len=*), intent(in) :: key
        type(fallible_{quantity}_unit_t) :: fallible_{quantity}_unit

        character(len=*), parameter :: procedure_name = "fallible_{quantity}_unit_from_object_and_key"
        type(fallible_json_value_t) :: maybe_value

        maybe_value = object%get(key)
        if (maybe_value%failed()) then
            fallible_{quantity}_unit = fallible_{quantity}_unit_t(error_list_t( &
                    maybe_value%errors, &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name)))
        else
            fallible_{quantity}_unit = fallible_{quantity}_unit_t( &
                    fallible_{quantity}_unit_t(maybe_value), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
            if (fallible_{quantity}_unit%failed()) then
                block
                    type(error_list_t) :: errors
                    errors = fallible_{quantity}_unit%errors()
                    fallible_{quantity}_unit = fallible_{quantity}_unit_t( &
                            errors%with_content_prepended('in "' // key // '", '))
                end block
            end if
        end if
    end function

    function fallible_{quantity}_unit_from_fallible_json(maybe_value) result(fallible_{quantity}_unit)
        type(fallible_json_value_t), intent(in) :: maybe_value
        type(fallible_{quantity}_unit_t) :: fallible_{quantity}_unit

        character(len=*), parameter :: procedure_name = "fallible_{quantity}_unit_from_fallible_json"

        if (maybe_value%failed()) then
            fallible_{quantity}_unit = fallible_{quantity}_unit_t(maybe_value%errors)
        else
            fallible_{quantity}_unit = fallible_{quantity}_unit_t( &
                    fallible_{quantity}_unit_t(maybe_value%value_), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        end if
    end function

    function fallible_{quantity}_unit_from_json_value(value_) result(fallible_{quantity}_unit)
        class(json_value_t), intent(in) :: value_
        type(fallible_{quantity}_unit_t) :: fallible_{quantity}_unit

        character(len=*), parameter :: procedure_name = "fallible_{quantity}_unit_from_json_value"

        select type (value_)
        type is (json_string_t)
            fallible_{quantity}_unit = fallible_{quantity}_unit_t( &
                    parse_{quantity}_unit(value_%string), &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name))
        class default
            fallible_{quantity}_unit = fallible_{quantity}_unit_t(error_list_t(fatal_t( &
                    module_t(MODULE_NAME), &
                    procedure_t(procedure_name), &
                    value_%to_compact_string() &
                    // " was expected to be a string containing a {quantity} unit")))
        end select
    end function""".format(quantity = quantity.name)

def rojff_to_quaff_tests(quantities : [Quantity]) -> str:
    return """module rojff_to_quaff_test
    use erloff, only: error_list_t, NOT_FOUND
    use quaff
    use quaff_asserts_m, only: assert_equals
    use rojff, only: &
            json_null_t, &
            json_object_t, &
            json_member_unsafe, &
            json_object_unsafe, &
            json_string_unsafe
    use rojff_to_quaff
    use veggies, only: &
            result_t, test_item_t, assert_equals, assert_that, describe, fail, it

    implicit none
    private
    public :: test_rojff_to_quaff
contains
    function test_rojff_to_quaff() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
            "rojff_to_quaff", &
            [ describe( &
                "obtains the original quantity from corectly constructed json for", &
                [ """ + " &\n                , ".join([valid_json_to_quaff_spec(quantity) for quantity in quantities]) + """ &
                ]) &
            , describe( &
                "forwards the error if the provided key is not in the given object for", &
                [ """ + " &\n                , ".join([missing_key_spec(quantity) for quantity in quantities]) + """ &
                ]) &
            , describe( &
                "catches error where the referenced json value is not a string for", &
                [ """ + " &\n                , ".join([not_string_spec(quantity) for quantity in quantities]) + """ &
                ]) &
            , describe( &
                "forwards the error if the string could not be parsed for", &
                [ """ + " &\n                , ".join([parse_error_spec(quantity) for quantity in quantities]) + """ &
                ]) &
            , describe( &
                "obtains the original quantity unit from corectly constructed json for", &
                [ """ + " &\n                , ".join([valid_json_to_quaff_unit_spec(quantity) for quantity in quantities]) + """ &
                ]) &
            , describe( &
                "forwards the error if the provided key is not in the given object for", &
                [ """ + " &\n                , ".join([missing_key_unit_spec(quantity) for quantity in quantities]) + """ &
                ]) &
            , describe( &
                "catches error where the referenced json value is not a string for", &
                [ """ + " &\n                , ".join([not_string_unit_spec(quantity) for quantity in quantities]) + """ &
                ]) &
            , describe( &
                "forwards the error if the string could not be parsed for", &
                [ """ + " &\n                , ".join([parse_error_unit_spec(quantity) for quantity in quantities]) + """ &
                ]) &
            ])
    end function
""" + "\n".join([valid_json_to_quaff_check(quantity) for quantity in quantities]) + """
""" + "\n".join([missing_key_check(quantity) for quantity in quantities]) + """
""" + "\n".join([not_string_check(quantity) for quantity in quantities]) + """
""" + "\n".join([parse_error_check(quantity) for quantity in quantities]) + """
""" + "\n".join([valid_json_to_quaff_unit_check(quantity) for quantity in quantities]) + """
""" + "\n".join([missing_key_unit_check(quantity) for quantity in quantities]) + """
""" + "\n".join([not_string_unit_check(quantity) for quantity in quantities]) + """
""" + "\n".join([parse_error_unit_check(quantity) for quantity in quantities]) + """
end module"""

def valid_json_to_quaff_spec(quantity : Quantity) -> str:
    return "it(\"{quantity}\", check_valid_json_to_{quantity})".format(quantity = quantity.name)

def missing_key_spec(quantity : Quantity) -> str:
    return "it(\"{quantity}\", check_missing_key_{quantity})".format(quantity=quantity.name)

def not_string_spec(quantity : Quantity) -> str:
    return "it(\"{quantity}\", check_not_string_{quantity})".format(quantity=quantity.name)

def parse_error_spec(quantity : Quantity) -> str:
    return "it(\"{quantity}\", check_parse_error_{quantity})".format(quantity=quantity.name)

def valid_json_to_quaff_unit_spec(quantity : Quantity) -> str:
    return "it(\"{quantity} unit\", check_valid_json_to_{quantity}_unit)".format(quantity = quantity.name)

def missing_key_unit_spec(quantity : Quantity) -> str:
    return "it(\"{quantity} unit\", check_missing_key_{quantity}_unit)".format(quantity=quantity.name)

def not_string_unit_spec(quantity : Quantity) -> str:
    return "it(\"{quantity} unit\", check_not_string_{quantity}_unit)".format(quantity=quantity.name)

def parse_error_unit_spec(quantity : Quantity) -> str:
    return "it(\"{quantity} unit\", check_parse_error_{quantity}_unit)".format(quantity=quantity.name)

def valid_json_to_quaff_check(quantity : Quantity) -> str:
    return """
    function check_valid_json_to_{quantity}() result(result_)
        type(result_t) :: result_

        type({quantity}_t) :: original
        type(json_object_t) :: json
        type(fallible_{quantity}_t) :: maybe_{quantity}
        type(error_list_t) :: errors

        original = 3.3.unit.{unit}
        json = json_object_unsafe([json_member_unsafe( &
                "{quantity}", json_string_unsafe(original%to_string()))])
        maybe_{quantity} = fallible_{quantity}_t(json, "{quantity}")
        if (maybe_{quantity}%failed()) then
            errors = maybe_{quantity}%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(original, maybe_{quantity}%{quantity}())
        end if
    end function""".format(quantity=quantity.name, unit=quantity.units[0].name)

def missing_key_check(quantity : Quantity) -> str:
    return """
    function check_missing_key_{quantity}() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_{quantity}_t) :: maybe_{quantity}
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_{quantity} = fallible_{quantity}_t(json, "{quantity}")
        errors = maybe_{quantity}%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."{quantity}", &
                errors%to_string())
    end function""".format(quantity=quantity.name, unit=quantity.units[0].name)

def not_string_check(quantity : Quantity) -> str:
    return """
    function check_not_string_{quantity}() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_{quantity}_t) :: maybe_{quantity}
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "{quantity}", json_null_t())])
        maybe_{quantity} = fallible_{quantity}_t(json, "{quantity}")
        errors = maybe_{quantity}%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."{quantity}", &
                errors%to_string())
    end function""".format(quantity=quantity.name, unit=quantity.units[0].name)

def parse_error_check(quantity : Quantity) -> str:
    return """
    function check_parse_error_{quantity}() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_{quantity}_t) :: maybe_{quantity}
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "{quantity}", json_string_unsafe("invalid"))])
        maybe_{quantity} = fallible_{quantity}_t(json, "{quantity}")
        errors = maybe_{quantity}%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."nable to parse", &
                errors%to_string())
    end function""".format(quantity=quantity.name, unit=quantity.units[0].name)

def valid_json_to_quaff_unit_check(quantity : Quantity) -> str:
    return """
    function check_valid_json_to_{quantity}_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_{quantity}_unit_t) :: maybe_{quantity}_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "{quantity} unit", json_string_unsafe(to_string({unit})))])
        maybe_{quantity}_unit = fallible_{quantity}_unit_t(json, "{quantity} unit")
        if (maybe_{quantity}_unit%failed()) then
            errors = maybe_{quantity}_unit%errors()
            result_ = fail(errors%to_string())
        else
            result_ = assert_equals(to_string({unit}), to_string(maybe_{quantity}_unit%unit()))
        end if
    end function""".format(quantity=quantity.name, unit=quantity.units[0].name)

def missing_key_unit_check(quantity : Quantity) -> str:
    return """
    function check_missing_key_{quantity}_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_{quantity}_unit_t) :: maybe_{quantity}_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "incorrect", json_string_unsafe("irrelevant"))])
        maybe_{quantity}_unit = fallible_{quantity}_unit_t(json, "{quantity} unit")
        errors = maybe_{quantity}_unit%errors()

        result_ = assert_that( &
                errors.ofType.NOT_FOUND.hasAnyIncluding."{quantity} unit", &
                errors%to_string())
    end function""".format(quantity=quantity.name, unit=quantity.units[0].name)

def not_string_unit_check(quantity : Quantity) -> str:
    return """
    function check_not_string_{quantity}_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_{quantity}_unit_t) :: maybe_{quantity}_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "{quantity} unit", json_null_t())])
        maybe_{quantity}_unit = fallible_{quantity}_unit_t(json, "{quantity} unit")
        errors = maybe_{quantity}_unit%errors()

        result_ = assert_that( &
                errors.hasAnyIncluding."{quantity} unit", &
                errors%to_string())
    end function""".format(quantity=quantity.name, unit=quantity.units[0].name)

def parse_error_unit_check(quantity : Quantity) -> str:
    return """
    function check_parse_error_{quantity}_unit() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: json
        type(fallible_{quantity}_unit_t) :: maybe_{quantity}_unit
        type(error_list_t) :: errors

        json = json_object_unsafe([json_member_unsafe( &
                "{quantity} unit", json_string_unsafe("invalid"))])
        maybe_{quantity}_unit = fallible_{quantity}_unit_t(json, "{quantity} unit")
        errors = maybe_{quantity}_unit%errors()

        result_ = assert_that( &
                errors.hasType.unknown_unit, &
                errors%to_string())
    end function""".format(quantity=quantity.name, unit=quantity.units[0].name)

if __name__ == "__main__":
    for quantity in quantities:
        with open(Path("test").joinpath("{0}_test.f90".format(quantity.name)), "w") as test_file:
            test_file.write(quantity_test(quantity))
        with open(Path("test").joinpath("utilities", "{0}_utilities_m.f90".format(quantity.name)), "w") as utility_file:
            utility_file.write(quantity_test_utility(quantity.name))
        with open(Path("quaff_asserts").joinpath("src", "{0}_asserts_m.f90".format(quantity.name)), "w") as assert_file:
            assert_file.write(quantity_asserts(quantity.name, "veggies"))
        with open(Path("quaff_garden_asserts").joinpath("src", "{0}_asserts_m.f90".format(quantity.name)), "w") as assert_file:
            assert_file.write(quantity_asserts(quantity.name, "garden"))
    interops = valid_interops(quantities)
    with open(Path("src").joinpath("quaff.f90"), "w") as quaff_file:
        quaff_file.write(quaff_module(quantities, interops))
    with open(Path("quaff_asserts").joinpath("src", "quaff_asserts_m.f90"), "w") as assert_file:
        assert_file.write(quaff_asserts([quantity.name for quantity in quantities]))
    with open(Path("quaff_garden_asserts").joinpath("src", "quaff_asserts_m.f90"), "w") as assert_file:
        assert_file.write(quaff_asserts([quantity.name for quantity in quantities]))
    with open(Path("test").joinpath("interquantity_test.f90"), "w") as interop_test_file:
        interop_test_file.write(interquantity_tests(interops))
    subprocess.run(["cart", "test/main.f90"] + ["test/{0}".format(file) for file in os.listdir("test") if "_test.f90" in file])
    with open(Path("rojff_to_quaff").joinpath("src", "rojff_to_quaff.f90"), "w") as rojff_to_quaff_file:
        rojff_to_quaff_file.write(rojff_to_quaff_module(quantities))
    with open(Path("rojff_to_quaff").joinpath("test", "rojff_to_quaff_test.f90"), "w") as rojff_to_quaff_test_file:
        rojff_to_quaff_test_file.write(rojff_to_quaff_tests(quantities))
