module volumetric_heat_capacity_test
    use double_precision_generator_m, only: DOUBLE_PRECISION_GENERATOR
    use double_precision_pair_generator_m, only: DOUBLE_PRECISION_PAIR_GENERATOR
    use double_precision_pair_input_m, only: double_precision_pair_input_t
    use erloff, only: error_list_t
    use iso_varying_string, only: operator(//)
    use non_zero_double_precision_generator_m, only: &
            NON_ZERO_DOUBLE_PRECISION_GENERATOR
    use non_zero_double_precision_pair_generator_m, only: &
            NON_ZERO_DOUBLE_PRECISION_PAIR_GENERATOR
    use quaff
    use quaff_asserts_m, only: assert_equals, assert_equals_within_relative
    use volumetric_heat_capacity_utilities_m, only: &
            units_input_t, units_pair_input_t, make_units_examples
    use units_examples_m, only: units_examples_t
    use veggies, only: &
            double_precision_input_t, &
            input_t, &
            result_t, &
            test_item_t, &
            test_result_item_t, &
            assert_equals, &
            assert_equals_within_relative, &
            assert_not, &
            assert_that, &
            describe, &
            fail, &
            it

    implicit none
    private
    public :: test_volumetric_heat_capacity
contains
    function test_volumetric_heat_capacity() result(tests)
        type(test_item_t) :: tests

        type(units_examples_t) :: examples

        examples = make_units_examples(provided_volumetric_heat_capacity_units)
        tests = describe( &
                "volumetric_heat_capacity_t", &
                [ it( &
                        "returns the same value given the same units", &
                        examples%units(), &
                        check_round_trip) &
                , it( &
                        "preserves its value converting to and from a string", &
                        examples%units(), &
                        check_to_and_from_string) &
                , it( &
                        "returns an error trying to parse a bad string", &
                        check_bad_string) &
                , it( &
                        "returns an error trying to parse an unknown unit", &
                        check_bad_unit) &
                , it( &
                        "returns an error trying to parse a bad number", &
                        check_bad_number) &
                , it( &
                        "the conversion factors between two units are inverses", &
                        examples%pairs(), &
                        check_conversion_factors_inverse) &
                , it("arrays can be summed", check_sum) &
                , it("can take the absolute value", check_abs) &
                , it("can be negated", DOUBLE_PRECISION_GENERATOR, check_negation) &
                , it( &
                        "adding zero returns the original volumetric_heat_capacity", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_add_zero) &
                , it( &
                        "subtracting zero returns the original volumetric_heat_capacity", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_subtract_zero) &
                , it( &
                        "adding and subtracting the same volumetric_heat_capacity &
                        &returns the original volumetric_heat_capacity", &
                        DOUBLE_PRECISION_PAIR_GENERATOR, &
                        check_add_subtract) &
                , it( &
                        "multiplying by one returns the original volumetric_heat_capacity", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_multiply_by_one) &
                , it( &
                        "multiplying by zero returns zero volumetric_heat_capacity", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_multiply_by_zero) &
                , it( &
                        "dividing by one returns the original volumetric_heat_capacity", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_divide_by_one) &
                , it( &
                        "dividing by itself returns one", &
                        NON_ZERO_DOUBLE_PRECISION_GENERATOR, &
                        check_divide_by_self) &
                , it( &
                        "multiplying and dividing by the same number returns the original volumetric_heat_capacity", &
                        NON_ZERO_DOUBLE_PRECISION_PAIR_GENERATOR, &
                        check_multiply_divide) &
                , it( &
                        "min and max return the smaller or larger of its argument respectively", &
                        check_min_max) &
                , describe( &
                        "operator(==)", &
                        [ it( &
                                "is true for the same volumetric_heat_capacity", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_with_same_number) &
                        , it( &
                                "is false for different volumetric_heat_capacitys", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_with_different_numbers) &
                        ]) &
                , describe( &
                        "operator(/=)", &
                        [ it( &
                                "is false for the same volumetric_heat_capacity", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_not_equal_with_same_number) &
                        , it( &
                                "is true for different volumetric_heat_capacitys", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_not_equal_with_different_numbers) &
                        ]) &
                , describe( &
                        "%equal(volumetric_heat_capacity, within)", &
                        [ it( &
                                "is true for the same volumetric_heat_capacity even for tiny tolerance", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_within_with_same_number) &
                        , it( &
                                "is true for sufficiently close values", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_within_with_close_numbers) &
                        , it( &
                                "is false for sufficiently different numbers", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_within_with_different_numbers) &
                        ]) &
                , describe( &
                        "operator(>=)", &
                        [ it( &
                                "is true if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_or_equal_with_greater_number) &
                        , it( &
                                "is true if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_or_equal_with_same_numbers) &
                        , it( &
                                "is false if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_or_equal_with_lesser_number) &
                        ]) &
                , describe( &
                        "operator(<=)", &
                        [ it( &
                                "is true if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_or_equal_with_less_number) &
                        , it( &
                                "is true if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_or_equal_with_same_numbers) &
                        , it( &
                                "is false if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_or_equal_with_greater_number) &
                        ]) &
                , describe( &
                        "operator(>)", &
                        [ it( &
                                "is true if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_with_greater_number) &
                        , it( &
                                "is false if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_with_same_numbers) &
                        , it( &
                                "is false if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_with_lesser_number) &
                        ]) &
                , describe( &
                        "operator(<)", &
                        [ it( &
                                "is true if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_with_less_number) &
                        , it( &
                                "is false if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_with_same_numbers) &
                        , it( &
                                "is false if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_with_greater_number) &
                        ]) &
                ])
    end function

    function check_round_trip(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_input_t)
            result_ = check_round_trip_in(input%unit())
        class default
            result_ = fail("Expected to get a units_input_t")
        end select
    end function

    function check_round_trip_in(units) result(result_)
        class(volumetric_heat_capacity_unit_t), intent(in) :: units
        type(result_t) :: result_

        type(test_item_t) :: the_test
        type(test_result_item_t) :: the_result

        the_test = it(to_string(units), DOUBLE_PRECISION_GENERATOR, check_round_trip_)
        the_result = the_test%run()
        result_ = assert_that(the_result%passed(), the_result%verbose_description(.false.))
    contains
        pure function check_round_trip_(input) result(result__)
            class(input_t), intent(in) :: input
            type(result_t) :: result__

            type(volumetric_heat_capacity_t) :: intermediate
            double precision :: original
            real :: lower_precision
            integer :: int_part

            select type (input)
            type is (double_precision_input_t)
                original = input%input()
                intermediate = original.unit.units
                result__ = assert_equals_within_relative( &
                        original, &
                        intermediate.in.units, &
                        1.0d-12)
                lower_precision = real(original)
                intermediate = lower_precision.unit.units
                result__ = result__.and.assert_equals_within_relative( &
                        real(lower_precision, kind=kind(original)), &
                        intermediate.in.units, &
                        1.0d-12)
                int_part = int(original)
                intermediate = int_part.unit.units
                result__ = result__.and.assert_equals_within_relative( &
                        real(int_part, kind=kind(original)), &
                        intermediate.in.units, &
                        1.0d-12)
            class default
                result__ = fail("Expected to get a double_precision_input_t")
            end select
        end function
    end function

    function check_to_and_from_string(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_input_t)
            result_ = check_string_trip(input%unit())
        class default
            result_ = fail("Expected to get an units_input_t")
        end select
    end function

    function check_string_trip(units) result(result_)
        class(volumetric_heat_capacity_unit_t), intent(in) :: units
        type(result_t) :: result_

        type(test_item_t) :: the_test
        type(test_result_item_t) :: the_result

        the_test = it(to_string(units), DOUBLE_PRECISION_GENERATOR, do_check)
        the_result = the_test%run()
        result_ = assert_that(the_result%passed(), the_result%verbose_description(.false.))
    contains
        function do_check(input) result(result__)
            class(input_t), intent(in) :: input
            type(result_t) :: result__

            type(error_list_t) :: errors
            type(volumetric_heat_capacity_t) :: original_volumetric_heat_capacity
            type(fallible_volumetric_heat_capacity_t) :: maybe_volumetric_heat_capacity
            type(volumetric_heat_capacity_t) :: new_volumetric_heat_capacity

            select type (input)
            type is (double_precision_input_t)
                original_volumetric_heat_capacity = input%input().unit.units
                maybe_volumetric_heat_capacity = parse_volumetric_heat_capacity( &
                        to_string(original_volumetric_heat_capacity, units))
                new_volumetric_heat_capacity = maybe_volumetric_heat_capacity%volumetric_heat_capacity()
                errors = maybe_volumetric_heat_capacity%errors()
                result__ = &
                        assert_equals( &
                                original_volumetric_heat_capacity, &
                                new_volumetric_heat_capacity) &
                        .and.assert_not(errors%has_any(), errors%to_string())
            class default
                result__ = fail("Expected to get a double_precision_input_t")
            end select
        end function
    end function

    function check_bad_string() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_volumetric_heat_capacity_t) :: maybe_volumetric_heat_capacity

        maybe_volumetric_heat_capacity = parse_volumetric_heat_capacity("bad")
        errors = maybe_volumetric_heat_capacity%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_bad_unit() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_volumetric_heat_capacity_t) :: maybe_volumetric_heat_capacity

        maybe_volumetric_heat_capacity = parse_volumetric_heat_capacity("1.0 Pa/Kbad", [pascals_per_kelvin])
        errors = maybe_volumetric_heat_capacity%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_bad_number() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_volumetric_heat_capacity_t) :: maybe_volumetric_heat_capacity

        maybe_volumetric_heat_capacity = parse_volumetric_heat_capacity("bad Pa/K")
        errors = maybe_volumetric_heat_capacity%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_conversion_factors_inverse(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_pair_input_t)
            result_ = check_conversion_factors_are_inverse(input%first(), input%second_())
        class default
            result_ = fail("Expected to get a units_pair_input_t")
        end select
    end function

    pure function check_conversion_factors_are_inverse( &
            from, to) result(result_)
        class(volumetric_heat_capacity_unit_t), intent(in) :: to
        class(volumetric_heat_capacity_unit_t), intent(in) :: from
        type(result_t) :: result_

        double precision :: factor1
        double precision :: factor2

        factor1 = (1.0d0.unit.from).in.to
        factor2 = (1.0d0.unit.to).in.from
        result_ = assert_equals_within_relative( &
                factor1, &
                1.0d0 / factor2, &
                1.0d-12, &
                to_string(from) // " to " // to_string(to))
    end function

    pure function check_sum() result(result_)
        type(result_t) :: result_

        double precision, parameter :: numbers(*) = [1.0d0, 2.0d0, 3.0d0]

        result_ = assert_equals( &
                sum(numbers).unit.pascals_per_kelvin, &
                sum(numbers.unit.pascals_per_kelvin))
    end function

    pure function check_abs() result(result_)
        type(result_t) :: result_

        result_ = &
                assert_equals( &
                        abs(1.0d0).unit.pascals_per_kelvin, &
                        abs(1.0d0.unit.pascals_per_kelvin)) &
                .and.assert_equals( &
                        abs(-1.0d0).unit.pascals_per_kelvin, &
                        abs((-1.0d0).unit.pascals_per_kelvin))
    end function

    pure function check_negation(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type(input)
        type is (double_precision_input_t)
            result_ = assert_equals( &
                    (-input%input()).unit.pascals_per_kelvin, &
                    -(input%input().unit.pascals_per_kelvin))
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_add_zero(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity
        type(volumetric_heat_capacity_t) :: zero

        select type(input)
        type is (double_precision_input_t)
            volumetric_heat_capacity = input%input().unit.pascals_per_kelvin
            zero = 0.0d0.unit.pascals_per_kelvin
            result_ = assert_equals(volumetric_heat_capacity, volumetric_heat_capacity + zero)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_subtract_zero(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity
        type(volumetric_heat_capacity_t) :: zero

        select type(input)
        type is (double_precision_input_t)
            volumetric_heat_capacity = input%input().unit.pascals_per_kelvin
            zero = 0.0d0.unit.pascals_per_kelvin
            result_ = assert_equals(volumetric_heat_capacity, volumetric_heat_capacity - zero)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_add_subtract(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type(input)
        type is (double_precision_pair_input_t)
            volumetric_heat_capacity1 = input%first().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = input%second_().unit.pascals_per_kelvin
            result_ = assert_equals_within_relative( &
                    volumetric_heat_capacity1, &
                    (volumetric_heat_capacity1 + volumetric_heat_capacity2) - volumetric_heat_capacity2, &
                    1.0d-8, &
                    "volumetric_heat_capacity1 = " // to_string(volumetric_heat_capacity1) &
                    // ", volumetric_heat_capacity2 = " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected a double_precision_pair_input_t")
        end select
    end function

    pure function check_multiply_by_one(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity

        select type(input)
        type is (double_precision_input_t)
            volumetric_heat_capacity = input%input().unit.pascals_per_kelvin
            result_ = &
                    assert_equals(volumetric_heat_capacity, volumetric_heat_capacity * 1.0d0) &
                    .and. assert_equals(volumetric_heat_capacity, volumetric_heat_capacity * 1.0)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_multiply_by_zero(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity
        type(volumetric_heat_capacity_t) :: zero

        select type(input)
        type is (double_precision_input_t)
            volumetric_heat_capacity = input%input().unit.pascals_per_kelvin
            zero = 0.0d0.unit.pascals_per_kelvin
            result_ = &
                    assert_equals(zero, volumetric_heat_capacity * 0.0d0) &
                    .and. assert_equals(zero, volumetric_heat_capacity * 0.0)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_divide_by_one(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity

        select type(input)
        type is (double_precision_input_t)
            volumetric_heat_capacity = input%input().unit.pascals_per_kelvin
            result_ = &
                    assert_equals(volumetric_heat_capacity, volumetric_heat_capacity / 1.0d0) &
                    .and. assert_equals(volumetric_heat_capacity, volumetric_heat_capacity / 1.0)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_divide_by_self(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity

        select type(input)
        type is (double_precision_input_t)
            volumetric_heat_capacity = input%input().unit.pascals_per_kelvin
            result_ = assert_equals(1.0d0, volumetric_heat_capacity / volumetric_heat_capacity)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_multiply_divide(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity

        select type (input)
        type is (double_precision_pair_input_t)
            volumetric_heat_capacity = input%first().unit.pascals_per_kelvin
            result_ = assert_equals( &
                    volumetric_heat_capacity, &
                    (volumetric_heat_capacity * input%second_()) / input%second_())
        class default
            result_ = fail("Expected a double_precision_pair_input_t")
        end select
    end function
    
    pure function check_min_max() result(result_)
        type(result_t) :: result_
        
        type(volumetric_heat_capacity_t) :: smaller, larger

        smaller = 1.d0.unit.pascals_per_kelvin
        larger = 2.d0.unit.pascals_per_kelvin

        result_ = &
            assert_equals(smaller, min(smaller, larger)) &
            .and.assert_equals(smaller, min(larger, smaller)) &
            .and.assert_equals(larger, max(smaller, larger)) &
            .and.assert_equals(larger, max(larger, smaller))
    end function

    pure function check_equal_with_same_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: the_volumetric_heat_capacity

        select type (input)
        type is (double_precision_input_t)
            the_volumetric_heat_capacity = input%input().unit.pascals_per_kelvin
            result_ = assert_that( &
                    the_volumetric_heat_capacity == the_volumetric_heat_capacity, &
                    to_string(the_volumetric_heat_capacity) // " == " // to_string(the_volumetric_heat_capacity))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_with_different_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() + 1.0d0).unit.pascals_per_kelvin
            result_ = assert_not( &
                    volumetric_heat_capacity1 == volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " == " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_not_equal_with_same_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: the_volumetric_heat_capacity

        select type (input)
        type is (double_precision_input_t)
            the_volumetric_heat_capacity = input%input().unit.pascals_per_kelvin
            result_ = assert_not( &
                    the_volumetric_heat_capacity /= the_volumetric_heat_capacity, &
                    to_string(the_volumetric_heat_capacity) // " /= " // to_string(the_volumetric_heat_capacity))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_not_equal_with_different_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() + 1.0d0).unit.pascals_per_kelvin
            result_ = assert_that( &
                    volumetric_heat_capacity1 /= volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " /= " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_within_with_same_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: the_volumetric_heat_capacity
        type(volumetric_heat_capacity_t) :: tolerance

        select type (input)
        type is (double_precision_input_t)
            the_volumetric_heat_capacity = input%input().unit.pascals_per_kelvin
            tolerance = tiny(1.0d0).unit.pascals_per_kelvin
            result_ = assert_that( &
                    equal(the_volumetric_heat_capacity, the_volumetric_heat_capacity, within = tolerance), &
                    "equal(" // to_string(the_volumetric_heat_capacity) // ", " &
                        // to_string(the_volumetric_heat_capacity) // ", within = " &
                        // to_string(tolerance) // ")")
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_within_with_close_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2
        type(volumetric_heat_capacity_t) :: tolerance

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() + 0.05d0).unit.pascals_per_kelvin
            tolerance = 0.1d0.unit.pascals_per_kelvin
            result_ = assert_that( &
                    equal(volumetric_heat_capacity1, volumetric_heat_capacity2, within = tolerance), &
                    "equal(" // to_string(volumetric_heat_capacity1) // ", " &
                        // to_string(volumetric_heat_capacity2) // ", within = " &
                        // to_string(tolerance) // ")")
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_within_with_different_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2
        type(volumetric_heat_capacity_t) :: tolerance

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() + 0.2d0).unit.pascals_per_kelvin
            tolerance = 0.1d0.unit.pascals_per_kelvin
            result_ = assert_not( &
                    equal(volumetric_heat_capacity1, volumetric_heat_capacity2, within = tolerance), &
                    "equal(" // to_string(volumetric_heat_capacity1) // ", " &
                    // to_string(volumetric_heat_capacity2) // ", within = " &
                    // to_string(tolerance) // ")")
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_or_equal_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit. pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() - 1.0d0).unit.pascals_per_kelvin
            result_ = assert_that( &
                    volumetric_heat_capacity1 >= volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " >= " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_or_equal_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = input%input().unit.pascals_per_kelvin
            result_ = assert_that( &
                    volumetric_heat_capacity1 >= volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " >= " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_or_equal_with_lesser_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() + 1.0d0).unit.pascals_per_kelvin
            result_ = assert_not( &
                    volumetric_heat_capacity1 >= volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " >= " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_or_equal_with_less_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit. pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() + 1.0d0).unit.pascals_per_kelvin
            result_ = assert_that( &
                    volumetric_heat_capacity1 <= volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " <= " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_or_equal_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = input%input().unit.pascals_per_kelvin
            result_ = assert_that( &
                    volumetric_heat_capacity1 <= volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " <= " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_or_equal_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() - 1.0d0).unit.pascals_per_kelvin
            result_ = assert_not( &
                    volumetric_heat_capacity1 <= volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " <= " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit. pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() - 1.0d0).unit.pascals_per_kelvin
            result_ = assert_that( &
                    volumetric_heat_capacity1 > volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " > " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = input%input().unit.pascals_per_kelvin
            result_ = assert_not( &
                    volumetric_heat_capacity1 > volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " > " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_with_lesser_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() + 1.0d0).unit.pascals_per_kelvin
            result_ = assert_not( &
                    volumetric_heat_capacity1 > volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " > " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_with_less_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit. pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() + 1.0d0).unit.pascals_per_kelvin
            result_ = assert_that( &
                    volumetric_heat_capacity1 < volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " < " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = input%input().unit.pascals_per_kelvin
            result_ = assert_not( &
                    volumetric_heat_capacity1 < volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " <=" // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity1
        type(volumetric_heat_capacity_t) :: volumetric_heat_capacity2

        select type (input)
        type is (double_precision_input_t)
            volumetric_heat_capacity1 = input%input().unit.pascals_per_kelvin
            volumetric_heat_capacity2 = (input%input() - 1.0d0).unit.pascals_per_kelvin
            result_ = assert_not( &
                    volumetric_heat_capacity1 < volumetric_heat_capacity2, &
                    to_string(volumetric_heat_capacity1) // " < " // to_string(volumetric_heat_capacity2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function
end module
