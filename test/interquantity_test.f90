module interquantity_test
    use quaff
    use quaff_asserts_m, only: assert_equals
    use veggies, only: result_t, test_item_t, describe, it, assert_equals
    
    implicit none
    private
    public :: test_interquantity_operators
contains
    function test_interquantity_operators() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
            "operations between quantities give expected results", &
            [ it('2 m/s^2 * 3 Pa s = 6 W/m^2', chk_acceleration_mul_dynamic_viscosity) &
            , it('6 m/s^2 / 3 Hz = 2 m/s', chk_acceleration_div_frequency) &
            , it('2 m/s^2 * 3 m = 6 J/kg', chk_acceleration_mul_length) &
            , it('2 m/s^2 * 3 kg = 6 N', chk_acceleration_mul_mass) &
            , it('2 m/s^2 * 3 kg/s = 6 N/s', chk_acceleration_mul_mass_rate) &
            , it('6 m/s^2 / 3 J/(kg K) = 2 K/m', chk_acceleration_div_specific_heat) &
            , it('6 m/s^2 / 3 m/s = 2 Hz', chk_acceleration_div_speed) &
            , it('6 m/s^2 / 3 K/m = 2 J/(kg K)', chk_acceleration_div_temperature_gradient) &
            , it('2 m/s^2 * 3 s = 6 m/s', chk_acceleration_mul_time) &
            , it('6 mol / 3 mol/s = 2 s', chk_amount_div_amount_rate) &
            , it('6 mol / 3 mol K = 2 1/K', chk_amount_div_amount_temperature) &
            , it('2 mol * 3 K = 6 mol K', chk_amount_mul_delta_temperature) &
            , it('2 mol * 3 K = 6 mol K', chk_amount_mul_temperature) &
            , it('2 mol * 3 Hz = 6 mol/s', chk_amount_mul_frequency) &
            , it('6 mol / 3 mol/kg = 2 kg', chk_amount_div_inverse_molar_mass) &
            , it('6 mol / 3 kg = 2 mol/kg', chk_amount_div_mass) &
            , it('6 mol / 3 mol/m^3 = 2 m^3', chk_amount_div_molar_density) &
            , it('2 mol * 3 J/mol = 6 J', chk_amount_mul_molar_enthalpy) &
            , it('2 mol * 3 kg/mol = 6 kg', chk_amount_mul_molar_mass) &
            , it('2 mol * 3 J/(K mol) = 6 J/K', chk_amount_mul_molar_specific_heat) &
            , it('6 mol / 3 1/K = 2 mol K', chk_amount_div_thermal_expansion_coefficient) &
            , it('6 mol / 3 s = 2 mol/s', chk_amount_div_time) &
            , it('6 mol / 3 m^3 = 2 mol/m^3', chk_amount_div_volume) &
            , it('6 mol/s / 3 mol = 2 Hz', chk_amount_rate_div_amount) &
            , it('6 mol/s / 3 (mol K)/s = 2 1/K', chk_amount_rate_div_amount_temperature_rate) &
            , it('2 mol/s * 3 K = 6 (mol K)/s', chk_amount_rate_mul_delta_temperature) &
            , it('2 mol/s * 3 K = 6 (mol K)/s', chk_amount_rate_mul_temperature) &
            , it('6 mol/s / 3 Hz = 2 mol', chk_amount_rate_div_frequency) &
            , it('6 mol/s / 3 mol/kg = 2 kg/s', chk_amount_rate_div_inverse_molar_mass) &
            , it('6 mol/s / 3 kg/s = 2 mol/kg', chk_amount_rate_div_mass_rate) &
            , it('6 mol/s / 3 mol/m^3 = 2 m^3/s', chk_amount_rate_div_molar_density) &
            , it('2 mol/s * 3 J/mol = 6 W', chk_amount_rate_mul_molar_enthalpy) &
            , it('2 mol/s * 3 kg/mol = 6 kg/s', chk_amount_rate_mul_molar_mass) &
            , it('2 mol/s * 3 J/(K mol) = 6 W/K', chk_amount_rate_mul_molar_specific_heat) &
            , it('6 mol/s / 3 1/K = 2 (mol K)/s', chk_amount_rate_div_thermal_expansion_coefficient) &
            , it('2 mol/s * 3 s = 6 mol', chk_amount_rate_mul_time) &
            , it('6 mol/s / 3 m^3/s = 2 mol/m^3', chk_amount_rate_div_volume_rate) &
            , it('6 mol K / 3 mol = 2 K', chk_amount_temperature_div_amount) &
            , it('6 mol K / 3 (mol K)/s = 2 s', chk_amount_temperature_div_amount_temperature_rate) &
            , it('6 mol K / 3 K = 2 mol', chk_amount_temperature_div_delta_temperature) &
            , it('6 mol K / 3 K = 2 mol', chk_amount_temperature_div_temperature) &
            , it('2 mol K * 3 Hz = 6 (mol K)/s', chk_amount_temperature_mul_frequency) &
            , it('2 mol K * 3 J/(K mol) = 6 J', chk_amount_temperature_mul_molar_specific_heat) &
            , it('2 mol K * 3 1/K = 6 mol', chk_amount_temperature_mul_thermal_expansion_coefficient) &
            , it('6 mol K / 3 s = 2 (mol K)/s', chk_amount_temperature_div_time) &
            , it('6 (mol K)/s / 3 mol/s = 2 K', chk_amount_temperature_rate_div_amount_rate) &
            , it('6 (mol K)/s / 3 mol K = 2 Hz', chk_amount_temperature_rate_div_amount_temperature) &
            , it('6 (mol K)/s / 3 K = 2 mol/s', chk_amount_temperature_rate_div_delta_temperature) &
            , it('6 (mol K)/s / 3 K = 2 mol/s', chk_amount_temperature_rate_div_temperature) &
            , it('6 (mol K)/s / 3 Hz = 2 mol K', chk_amount_temperature_rate_div_frequency) &
            , it('2 (mol K)/s * 3 J/(K mol) = 6 W', chk_amount_temperature_rate_mul_molar_specific_heat) &
            , it('2 (mol K)/s * 3 1/K = 6 mol/s', chk_amount_temperature_rate_mul_thermal_expansion_coefficient) &
            , it('2 (mol K)/s * 3 s = 6 mol K', chk_amount_temperature_rate_mul_time) &
            , it('6 m^2 / 3 W/K = 2 (K m^2)/W', chk_area_div_conductance) &
            , it('6 m^2 / 3 m^2/s = 2 s', chk_area_div_diffusivity) &
            , it('6  / 3 m^2 = 2 particles/m^2', chk_number_div_area) &
            , it('2 m^2 * 3 particles/m^2 = 6 ', chk_area_mul_fluence) &
            , it('2 m^2 * 3 Hz = 6 m^2/s', chk_area_mul_frequency) &
            , it('2 m^2 * 3 W/(m^2 K) = 6 W/K', chk_area_mul_heat_transfer_coefficient) &
            , it('6 m^2 / 3 (K m^2)/W = 2 W/K', chk_area_div_insulance) &
            , it('2 m^2 * 3 W/m^2 = 6 W', chk_area_mul_intensity) &
            , it('2 m^2 * 3 m = 6 m^3', chk_area_mul_length) &
            , it('6 m^2 / 3 m = 2 m', chk_area_div_length) &
            , it('2 m^2 * 3 kg/(m^2 s) = 6 kg/s', chk_area_mul_mass_flux) &
            , it('2 m^2 * 3 Pa = 6 N', chk_area_mul_pressure) &
            , it('2 m^2 * 3 m/s = 6 m^3/s', chk_area_mul_speed) &
            , it('6 m^2 / 3 s = 2 m^2/s', chk_area_div_time) &
            , it('6 W/K / 3 mol/s = 2 J/(K mol)', chk_conductance_div_amount_rate) &
            , it('6 W/K / 3 m^2 = 2 W/(m^2 K)', chk_conductance_div_area) &
            , it('2 W/K * 3 K = 6 W', chk_conductance_mul_delta_temperature) &
            , it('2 W/K * 3 K = 6 W', chk_conductance_mul_temperature) &
            , it('2 W/K * 3 particles/m^2 = 6 W/(m^2 K)', chk_conductance_mul_fluence) &
            , it('6 W/K / 3 Hz = 2 J/K', chk_conductance_div_frequency) &
            , it('6 W/K / 3 J/K = 2 Hz', chk_conductance_div_heat_capacity) &
            , it('6 W/K / 3 W/(m^2 K) = 2 m^2', chk_conductance_div_heat_transfer_coefficient) &
            , it('2 W/K * 3 (K m^2)/W = 6 m^2', chk_conductance_mul_insulance) &
            , it('6 W/K / 3 m = 2 W/(m K)', chk_conductance_div_length) &
            , it('6 W/K / 3 kg/s = 2 J/(kg K)', chk_conductance_div_mass_rate) &
            , it('6 W/K / 3 J/(K mol) = 2 mol/s', chk_conductance_div_molar_specific_heat) &
            , it('6 W/K / 3 W = 2 1/K', chk_conductance_div_power) &
            , it('6 W/K / 3 J/(kg K) = 2 kg/s', chk_conductance_div_specific_heat) &
            , it('2 W/K * 3 K/m = 6 N/s', chk_conductance_mul_temperature_gradient) &
            , it('6 W/K / 3 W/(m K) = 2 m', chk_conductance_div_thermal_conductivity) &
            , it('6 W/K / 3 1/K = 2 W', chk_conductance_div_thermal_expansion_coefficient) &
            , it('2 W/K * 3 s = 6 J/K', chk_conductance_mul_time) &
            , it('6 W/K / 3 m^3/s = 2 Pa/K', chk_conductance_div_volume_rate) &
            , it('6 W/K / 3 Pa/K = 2 m^3/s', chk_conductance_div_volumetric_heat_capacity) &
            , it('2 K * 3 mol = 6 mol K', chk_delta_temperature_mul_amount) &
            , it('2 K * 3 mol = 6 mol K', chk_temperature_mul_amount) &
            , it('2 K * 3 mol/s = 6 (mol K)/s', chk_delta_temperature_mul_amount_rate) &
            , it('2 K * 3 mol/s = 6 (mol K)/s', chk_temperature_mul_amount_rate) &
            , it('2 K * 3 W/K = 6 W', chk_delta_temperature_mul_conductance) &
            , it('2 K * 3 W/K = 6 W', chk_temperature_mul_conductance) &
            , it('2 K * 3 J/K = 6 J', chk_delta_temperature_mul_heat_capacity) &
            , it('2 K * 3 J/K = 6 J', chk_temperature_mul_heat_capacity) &
            , it('2 K * 3 W/(m^2 K) = 6 W/m^2', chk_delta_temperature_mul_heat_transfer_coefficient) &
            , it('2 K * 3 W/(m^2 K) = 6 W/m^2', chk_temperature_mul_heat_transfer_coefficient) &
            , it('6 K / 3 (K m^2)/W = 2 W/m^2', chk_delta_temperature_div_insulance) &
            , it('6 K / 3 W/m^2 = 2 (K m^2)/W', chk_delta_temperature_div_intensity) &
            , it('6 K / 3 m = 2 K/m', chk_delta_temperature_div_length) &
            , it('2 K * 3 J/(K mol) = 6 J/mol', chk_delta_temperature_mul_molar_specific_heat) &
            , it('2 K * 3 J/(K mol) = 6 J/mol', chk_temperature_mul_molar_specific_heat) &
            , it('2 K * 3 J/(kg K) = 6 J/kg', chk_delta_temperature_mul_specific_heat) &
            , it('2 K * 3 J/(kg K) = 6 J/kg', chk_temperature_mul_specific_heat) &
            , it('6 K / 3 K/m = 2 m', chk_delta_temperature_div_temperature_gradient) &
            , it('2 K * 3 W/(m K) = 6 N/s', chk_delta_temperature_mul_thermal_conductivity) &
            , it('2 K * 3 W/(m K) = 6 N/s', chk_temperature_mul_thermal_conductivity) &
            , it('6  / 3 K = 2 1/K', chk_number_div_delta_temperature) &
            , it('2 K * 3 1/K = 6 ', chk_delta_temperature_mul_thermal_expansion_coefficient) &
            , it('2 K * 3 Pa/K = 6 Pa', chk_delta_temperature_mul_volumetric_heat_capacity) &
            , it('2 K * 3 Pa/K = 6 Pa', chk_temperature_mul_volumetric_heat_capacity) &
            , it('2 kg/m^3 * 3 m^2/s = 6 Pa s', chk_density_mul_diffusivity) &
            , it('2 kg/m^3 * 3 J/kg = 6 Pa', chk_density_mul_enthalpy) &
            , it('2 kg/m^3 * 3 mol/kg = 6 mol/m^3', chk_density_mul_inverse_molar_mass) &
            , it('6 kg/m^3 / 3 mol/m^3 = 2 kg/mol', chk_density_div_molar_density) &
            , it('6 kg/m^3 / 3 kg/mol = 2 mol/m^3', chk_density_div_molar_mass) &
            , it('2 kg/m^3 * 3 J/(kg K) = 6 Pa/K', chk_density_mul_specific_heat) &
            , it('2 kg/m^3 * 3 m/s = 6 kg/(m^2 s)', chk_density_mul_speed) &
            , it('2 kg/m^3 * 3 m^3 = 6 kg', chk_density_mul_volume) &
            , it('2 kg/m^3 * 3 m^3/s = 6 kg/s', chk_density_mul_volume_rate) &
            , it('6 m^2/s / 3 m^2 = 2 Hz', chk_diffusivity_div_area) &
            , it('2 m^2/s * 3 kg/m^3 = 6 Pa s', chk_diffusivity_mul_density) &
            , it('2 m^2/s * 3 Pa s = 6 N', chk_diffusivity_mul_dynamic_viscosity) &
            , it('6 m^2/s / 3 J/kg = 2 s', chk_diffusivity_div_enthalpy) &
            , it('2 m^2/s * 3 particles/m^2 = 6 Hz', chk_diffusivity_mul_fluence) &
            , it('2 m^2/s * 3 Hz = 6 J/kg', chk_diffusivity_mul_frequency) &
            , it('6 m^2/s / 3 Hz = 2 m^2', chk_diffusivity_div_frequency) &
            , it('2 m^2/s * 3 m = 6 m^3/s', chk_diffusivity_mul_length) &
            , it('6 m^2/s / 3 m = 2 m/s', chk_diffusivity_div_length) &
            , it('2 m^2/s * 3 kg/s = 6 J', chk_diffusivity_mul_mass_rate) &
            , it('2 m^2/s * 3 Pa = 6 N/s', chk_diffusivity_mul_pressure) &
            , it('6 m^2/s / 3 m/s = 2 m', chk_diffusivity_div_speed) &
            , it('2 m^2/s * 3 s = 6 m^2', chk_diffusivity_mul_time) &
            , it('6 m^2/s / 3 s = 2 J/kg', chk_diffusivity_div_time) &
            , it('2 m^2/s * 3 Pa/K = 6 W/(m K)', chk_diffusivity_mul_volumetric_heat_capacity) &
            , it('2 Pa s * 3 m/s^2 = 6 W/m^2', chk_dynamic_viscosity_mul_acceleration) &
            , it('6 Pa s / 3 kg/m^3 = 2 m^2/s', chk_dynamic_viscosity_div_density) &
            , it('2 Pa s * 3 m^2/s = 6 N', chk_dynamic_viscosity_mul_diffusivity) &
            , it('6 Pa s / 3 m^2/s = 2 kg/m^3', chk_dynamic_viscosity_div_diffusivity) &
            , it('2 Pa s * 3 J/kg = 6 N/s', chk_dynamic_viscosity_mul_enthalpy) &
            , it('2 Pa s * 3 Hz = 6 Pa', chk_dynamic_viscosity_mul_frequency) &
            , it('2 Pa s * 3 m = 6 kg/s', chk_dynamic_viscosity_mul_length) &
            , it('6 Pa s / 3 m = 2 kg/(m^2 s)', chk_dynamic_viscosity_div_length) &
            , it('6 Pa s / 3 kg/(m^2 s) = 2 m', chk_dynamic_viscosity_div_mass_flux) &
            , it('6 Pa s / 3 Pa = 2 s', chk_dynamic_viscosity_div_pressure) &
            , it('2 Pa s * 3 J/(kg K) = 6 W/(m K)', chk_dynamic_viscosity_mul_specific_heat) &
            , it('6 Pa s / 3 s = 2 Pa', chk_dynamic_viscosity_div_time) &
            , it('2 Pa s * 3 m^3/s = 6 J', chk_dynamic_viscosity_mul_volume_rate) &
            , it('6 J / 3 mol = 2 J/mol', chk_energy_div_amount) &
            , it('6 J / 3 mol K = 2 J/(K mol)', chk_energy_div_amount_temperature) &
            , it('6 J / 3 K = 2 J/K', chk_energy_div_delta_temperature) &
            , it('6 J / 3 K = 2 J/K', chk_energy_div_temperature) &
            , it('6 J / 3 m^2/s = 2 kg/s', chk_energy_div_diffusivity) &
            , it('6 J / 3 Pa s = 2 m^3/s', chk_energy_div_dynamic_viscosity) &
            , it('6 J / 3 J/kg = 2 kg', chk_energy_div_enthalpy) &
            , it('6 J / 3 N = 2 m', chk_energy_div_force) &
            , it('2 J * 3 Hz = 6 W', chk_energy_mul_frequency) &
            , it('6 J / 3 J/K = 2 K', chk_energy_div_heat_capacity) &
            , it('6 J / 3 m = 2 N', chk_energy_div_length) &
            , it('6 J / 3 kg = 2 J/kg', chk_energy_div_mass) &
            , it('6 J / 3 kg/s = 2 m^2/s', chk_energy_div_mass_rate) &
            , it('6 J / 3 J/mol = 2 mol', chk_energy_div_molar_enthalpy) &
            , it('6 J / 3 J/(K mol) = 2 mol K', chk_energy_div_molar_specific_heat) &
            , it('6 J / 3 W = 2 s', chk_energy_div_power) &
            , it('6 J / 3 Pa = 2 m^3', chk_energy_div_pressure) &
            , it('2 J * 3 1/K = 6 J/K', chk_energy_mul_thermal_expansion_coefficient) &
            , it('6 J / 3 s = 2 W', chk_energy_div_time) &
            , it('6 J / 3 m^3 = 2 Pa', chk_energy_div_volume) &
            , it('6 J / 3 m^3/s = 2 Pa s', chk_energy_div_volume_rate) &
            , it('6 J/kg / 3 m/s^2 = 2 m', chk_enthalpy_div_acceleration) &
            , it('6 J/kg / 3 K = 2 J/(kg K)', chk_enthalpy_div_delta_temperature) &
            , it('6 J/kg / 3 K = 2 J/(kg K)', chk_enthalpy_div_temperature) &
            , it('2 J/kg * 3 kg/m^3 = 6 Pa', chk_enthalpy_mul_density) &
            , it('6 J/kg / 3 m^2/s = 2 Hz', chk_enthalpy_div_diffusivity) &
            , it('2 J/kg * 3 Pa s = 6 N/s', chk_enthalpy_mul_dynamic_viscosity) &
            , it('6 J/kg / 3 Hz = 2 m^2/s', chk_enthalpy_div_frequency) &
            , it('6 J/kg / 3 mol/kg = 2 J/mol', chk_enthalpy_div_inverse_molar_mass) &
            , it('6 J/kg / 3 m = 2 m/s^2', chk_enthalpy_div_length) &
            , it('2 J/kg * 3 kg = 6 J', chk_enthalpy_mul_mass) &
            , it('2 J/kg * 3 kg/(m^2 s) = 6 W/m^2', chk_enthalpy_mul_mass_flux) &
            , it('2 J/kg * 3 kg/s = 6 W', chk_enthalpy_mul_mass_rate) &
            , it('6 J/kg / 3 J/mol = 2 mol/kg', chk_enthalpy_div_molar_enthalpy) &
            , it('2 J/kg * 3 kg/mol = 6 J/mol', chk_enthalpy_mul_molar_mass) &
            , it('6 J/kg / 3 J/(kg K) = 2 K', chk_enthalpy_div_specific_heat) &
            , it('6 J/kg / 3 m/s = 2 m/s', chk_enthalpy_div_speed) &
            , it('2 J/kg * 3 1/K = 6 J/(kg K)', chk_enthalpy_mul_thermal_expansion_coefficient) &
            , it('2 J/kg * 3 s = 6 m^2/s', chk_enthalpy_mul_time) &
            , it('6  / 3 particles/m^2 = 2 m^2', chk_number_div_fluence) &
            , it('2 particles/m^2 * 3 m^2 = 6 ', chk_fluence_mul_area) &
            , it('2 particles/m^2 * 3 W/K = 6 W/(m^2 K)', chk_fluence_mul_conductance) &
            , it('2 particles/m^2 * 3 m^2/s = 6 Hz', chk_fluence_mul_diffusivity) &
            , it('2 particles/m^2 * 3 N = 6 Pa', chk_fluence_mul_force) &
            , it('2 particles/m^2 * 3 kg/s = 6 kg/(m^2 s)', chk_fluence_mul_mass_rate) &
            , it('2 particles/m^2 * 3 W = 6 W/m^2', chk_fluence_mul_power) &
            , it('2 particles/m^2 * 3 m^3 = 6 m', chk_fluence_mul_volume) &
            , it('2 particles/m^2 * 3 m^3/s = 6 m/s', chk_fluence_mul_volume_rate) &
            , it('6 N / 3 m/s^2 = 2 kg', chk_force_div_acceleration) &
            , it('6 N / 3 m^2 = 2 Pa', chk_force_div_area) &
            , it('6 N / 3 m^2/s = 2 Pa s', chk_force_div_diffusivity) &
            , it('6 N / 3 Pa s = 2 m^2/s', chk_force_div_dynamic_viscosity) &
            , it('2 N * 3 particles/m^2 = 6 Pa', chk_force_mul_fluence) &
            , it('2 N * 3 Hz = 6 N/s', chk_force_mul_frequency) &
            , it('6 N / 3 J/K = 2 K/m', chk_force_div_heat_capacity) &
            , it('2 N * 3 m = 6 J', chk_force_mul_length) &
            , it('6 N / 3 kg = 2 m/s^2', chk_force_div_mass) &
            , it('6 N / 3 kg/(m^2 s) = 2 m^3/s', chk_force_div_mass_flux) &
            , it('6 N / 3 kg/s = 2 m/s', chk_force_div_mass_rate) &
            , it('6 N / 3 Pa = 2 m^2', chk_force_div_pressure) &
            , it('2 N * 3 m/s = 6 W', chk_force_mul_speed) &
            , it('6 N / 3 m/s = 2 kg/s', chk_force_div_speed) &
            , it('6 N / 3 K/m = 2 J/K', chk_force_div_temperature_gradient) &
            , it('6 N / 3 s = 2 N/s', chk_force_div_time) &
            , it('6 N / 3 m^3/s = 2 kg/(m^2 s)', chk_force_div_volume_rate) &
            , it('6 N / 3 N/s = 2 s', chk_force_div_yank) &
            , it('2 Hz * 3 mol = 6 mol/s', chk_frequency_mul_amount) &
            , it('2 Hz * 3 mol K = 6 (mol K)/s', chk_frequency_mul_amount_temperature) &
            , it('2 Hz * 3 m^2 = 6 m^2/s', chk_frequency_mul_area) &
            , it('2 Hz * 3 m^2/s = 6 J/kg', chk_frequency_mul_diffusivity) &
            , it('6 Hz / 3 m^2/s = 2 particles/m^2', chk_frequency_div_diffusivity) &
            , it('2 Hz * 3 Pa s = 6 Pa', chk_frequency_mul_dynamic_viscosity) &
            , it('2 Hz * 3 J = 6 W', chk_frequency_mul_energy) &
            , it('6 Hz / 3 particles/m^2 = 2 m^2/s', chk_frequency_div_fluence) &
            , it('2 Hz * 3 N = 6 N/s', chk_frequency_mul_force) &
            , it('2 Hz * 3 J/K = 6 W/K', chk_frequency_mul_heat_capacity) &
            , it('2 Hz * 3 m = 6 m/s', chk_frequency_mul_length) &
            , it('2 Hz * 3 kg = 6 kg/s', chk_frequency_mul_mass) &
            , it('2 Hz * 3 m/s = 6 m/s^2', chk_frequency_mul_speed) &
            , it('6  / 3 Hz = 2 s', chk_number_div_frequency) &
            , it('2 Hz * 3 s = 6 ', chk_frequency_mul_time) &
            , it('2 Hz * 3 m^3 = 6 m^3/s', chk_frequency_mul_volume) &
            , it('6 J/K / 3 mol = 2 J/(K mol)', chk_heat_capacity_div_amount) &
            , it('6 J/K / 3 W/K = 2 s', chk_heat_capacity_div_conductance) &
            , it('2 J/K * 3 K = 6 J', chk_heat_capacity_mul_delta_temperature) &
            , it('2 J/K * 3 K = 6 J', chk_heat_capacity_mul_temperature) &
            , it('6 J/K / 3 J = 2 1/K', chk_heat_capacity_div_energy) &
            , it('2 J/K * 3 Hz = 6 W/K', chk_heat_capacity_mul_frequency) &
            , it('6 J/K / 3 kg = 2 J/(kg K)', chk_heat_capacity_div_mass) &
            , it('6 J/K / 3 J/(K mol) = 2 mol', chk_heat_capacity_div_molar_specific_heat) &
            , it('6 J/K / 3 J/(kg K) = 2 kg', chk_heat_capacity_div_specific_heat) &
            , it('2 J/K * 3 K/m = 6 N', chk_heat_capacity_mul_temperature_gradient) &
            , it('6 J/K / 3 1/K = 2 J', chk_heat_capacity_div_thermal_expansion_coefficient) &
            , it('6 J/K / 3 s = 2 W/K', chk_heat_capacity_div_time) &
            , it('6 J/K / 3 m^3 = 2 Pa/K', chk_heat_capacity_div_volume) &
            , it('6 J/K / 3 Pa/K = 2 m^3', chk_heat_capacity_div_volumetric_heat_capacity) &
            , it('2 W/(m^2 K) * 3 m^2 = 6 W/K', chk_heat_transfer_coefficient_mul_area) &
            , it('6 W/(m^2 K) / 3 W/K = 2 particles/m^2', chk_heat_transfer_coefficient_div_conductance) &
            , it('2 W/(m^2 K) * 3 K = 6 W/m^2', chk_heat_transfer_coefficient_mul_delta_temperature) &
            , it('2 W/(m^2 K) * 3 K = 6 W/m^2', chk_heat_transfer_coefficient_mul_temperature) &
            , it('6 W/(m^2 K) / 3 particles/m^2 = 2 W/K', chk_heat_transfer_coefficient_div_fluence) &
            , it('6  / 3 W/(m^2 K) = 2 (K m^2)/W', chk_number_div_heat_transfer_coefficient) &
            , it('2 W/(m^2 K) * 3 (K m^2)/W = 6 ', chk_heat_transfer_coefficient_mul_insulance) &
            , it('6 W/(m^2 K) / 3 W/m^2 = 2 1/K', chk_heat_transfer_coefficient_div_intensity) &
            , it('2 W/(m^2 K) * 3 m = 6 W/(m K)', chk_heat_transfer_coefficient_mul_length) &
            , it('6 W/(m^2 K) / 3 kg/(m^2 s) = 2 J/(kg K)', chk_heat_transfer_coefficient_div_mass_flux) &
            , it('6 W/(m^2 K) / 3 J/(kg K) = 2 kg/(m^2 s)', chk_heat_transfer_coefficient_div_specific_heat) &
            , it('6 W/(m^2 K) / 3 m/s = 2 Pa/K', chk_heat_transfer_coefficient_div_speed) &
            , it('6 W/(m^2 K) / 3 1/K = 2 W/m^2', chk_heat_transfer_coefficient_div_thermal_expansion_coefficient) &
            , it('6 W/(m^2 K) / 3 Pa/K = 2 m/s', chk_heat_transfer_coefficient_div_volumetric_heat_capacity) &
            , it('2 (K m^2)/W * 3 W/K = 6 m^2', chk_insulance_mul_conductance) &
            , it('6  / 3 (K m^2)/W = 2 W/(m^2 K)', chk_number_div_insulance) &
            , it('2 (K m^2)/W * 3 W/(m^2 K) = 6 ', chk_insulance_mul_heat_transfer_coefficient) &
            , it('2 (K m^2)/W * 3 W/m^2 = 6 K', chk_insulance_mul_intensity) &
            , it('2 (K m^2)/W * 3 W/(m K) = 6 m', chk_insulance_mul_thermal_conductivity) &
            , it('6 W/m^2 / 3 m/s^2 = 2 Pa s', chk_intensity_div_acceleration) &
            , it('2 W/m^2 * 3 m^2 = 6 W', chk_intensity_mul_area) &
            , it('6 W/m^2 / 3 K = 2 W/(m^2 K)', chk_intensity_div_delta_temperature) &
            , it('6 W/m^2 / 3 K = 2 W/(m^2 K)', chk_intensity_div_temperature) &
            , it('6 W/m^2 / 3 Pa s = 2 m/s^2', chk_intensity_div_dynamic_viscosity) &
            , it('6 W/m^2 / 3 J/kg = 2 kg/(m^2 s)', chk_intensity_div_enthalpy) &
            , it('6 W/m^2 / 3 particles/m^2 = 2 W', chk_intensity_div_fluence) &
            , it('6 W/m^2 / 3 W/(m^2 K) = 2 K', chk_intensity_div_heat_transfer_coefficient) &
            , it('2 W/m^2 * 3 (K m^2)/W = 6 K', chk_intensity_mul_insulance) &
            , it('2 W/m^2 * 3 m = 6 N/s', chk_intensity_mul_length) &
            , it('6 W/m^2 / 3 kg/(m^2 s) = 2 J/kg', chk_intensity_div_mass_flux) &
            , it('6 W/m^2 / 3 W = 2 particles/m^2', chk_intensity_div_power) &
            , it('6 W/m^2 / 3 Pa = 2 m/s', chk_intensity_div_pressure) &
            , it('6 W/m^2 / 3 m/s = 2 Pa', chk_intensity_div_speed) &
            , it('6 W/m^2 / 3 K/m = 2 W/(m K)', chk_intensity_div_temperature_gradient) &
            , it('6 W/m^2 / 3 W/(m K) = 2 K/m', chk_intensity_div_thermal_conductivity) &
            , it('2 W/m^2 * 3 1/K = 6 W/(m^2 K)', chk_intensity_mul_thermal_expansion_coefficient) &
            , it('2 mol/kg * 3 kg/m^3 = 6 mol/m^3', chk_inverse_molar_mass_mul_density) &
            , it('2 mol/kg * 3 kg = 6 mol', chk_inverse_molar_mass_mul_mass) &
            , it('2 mol/kg * 3 kg/s = 6 mol/s', chk_inverse_molar_mass_mul_mass_rate) &
            , it('2 mol/kg * 3 J/mol = 6 J/kg', chk_inverse_molar_mass_mul_molar_enthalpy) &
            , it('6  / 3 mol/kg = 2 kg/mol', chk_number_div_inverse_molar_mass) &
            , it('2 mol/kg * 3 kg/mol = 6 ', chk_inverse_molar_mass_mul_molar_mass) &
            , it('2 mol/kg * 3 J/(K mol) = 6 J/(kg K)', chk_inverse_molar_mass_mul_molar_specific_heat) &
            , it('2 m * 3 m/s^2 = 6 J/kg', chk_length_mul_acceleration) &
            , it('2 m * 3 m^2 = 6 m^3', chk_length_mul_area) &
            , it('2 m * 3 m^2/s = 6 m^3/s', chk_length_mul_diffusivity) &
            , it('2 m * 3 Pa s = 6 kg/s', chk_length_mul_dynamic_viscosity) &
            , it('6 m / 3 particles/m^2 = 2 m^3', chk_length_div_fluence) &
            , it('2 m * 3 N = 6 J', chk_length_mul_force) &
            , it('2 m * 3 Hz = 6 m/s', chk_length_mul_frequency) &
            , it('2 m * 3 W/(m^2 K) = 6 W/(m K)', chk_length_mul_heat_transfer_coefficient) &
            , it('6 m / 3 (K m^2)/W = 2 W/(m K)', chk_length_div_insulance) &
            , it('2 m * 3 W/m^2 = 6 N/s', chk_length_mul_intensity) &
            , it('2 m * 3 m = 6 m^2', chk_length_mul_length) &
            , it('2 m * 3 kg/(m^2 s) = 6 Pa s', chk_length_mul_mass_flux) &
            , it('2 m * 3 m/s = 6 m^2/s', chk_length_mul_speed) &
            , it('6 m / 3 m/s = 2 s', chk_length_div_speed) &
            , it('2 m * 3 K/m = 6 K', chk_length_mul_temperature_gradient) &
            , it('2 m * 3 W/(m K) = 6 W/K', chk_length_mul_thermal_conductivity) &
            , it('6 m / 3 W/(m K) = 2 (K m^2)/W', chk_length_div_thermal_conductivity) &
            , it('6 m / 3 s = 2 m/s', chk_length_div_time) &
            , it('6 m / 3 m^3 = 2 particles/m^2', chk_length_div_volume) &
            , it('2 m * 3 N/s = 6 W', chk_length_mul_yank) &
            , it('2 kg * 3 m/s^2 = 6 N', chk_mass_mul_acceleration) &
            , it('6 kg / 3 mol = 2 kg/mol', chk_mass_div_amount) &
            , it('6 kg / 3 kg/m^3 = 2 m^3', chk_mass_div_density) &
            , it('2 kg * 3 J/kg = 6 J', chk_mass_mul_enthalpy) &
            , it('2 kg * 3 Hz = 6 kg/s', chk_mass_mul_frequency) &
            , it('2 kg * 3 mol/kg = 6 mol', chk_mass_mul_inverse_molar_mass) &
            , it('6 kg / 3 kg/s = 2 s', chk_mass_div_mass_rate) &
            , it('6 kg / 3 kg/mol = 2 mol', chk_mass_div_molar_mass) &
            , it('2 kg * 3 J/(kg K) = 6 J/K', chk_mass_mul_specific_heat) &
            , it('6 kg / 3 s = 2 kg/s', chk_mass_div_time) &
            , it('6 kg / 3 m^3 = 2 kg/m^3', chk_mass_div_volume) &
            , it('2 kg/(m^2 s) * 3 m^2 = 6 kg/s', chk_mass_flux_mul_area) &
            , it('6 kg/(m^2 s) / 3 kg/m^3 = 2 m/s', chk_mass_flux_div_density) &
            , it('2 kg/(m^2 s) * 3 J/kg = 6 W/m^2', chk_mass_flux_mul_enthalpy) &
            , it('6 kg/(m^2 s) / 3 particles/m^2 = 2 kg/s', chk_mass_flux_div_fluence) &
            , it('2 kg/(m^2 s) * 3 m = 6 Pa s', chk_mass_flux_mul_length) &
            , it('6 kg/(m^2 s) / 3 kg/s = 2 particles/m^2', chk_mass_flux_div_mass_rate) &
            , it('2 kg/(m^2 s) * 3 J/(kg K) = 6 W/(m^2 K)', chk_mass_flux_mul_specific_heat) &
            , it('2 kg/(m^2 s) * 3 m/s = 6 Pa', chk_mass_flux_mul_speed) &
            , it('6 kg/(m^2 s) / 3 m/s = 2 kg/m^3', chk_mass_flux_div_speed) &
            , it('2 kg/(m^2 s) * 3 m^3/s = 6 N', chk_mass_flux_mul_volume_rate) &
            , it('2 kg/s * 3 m/s^2 = 6 N/s', chk_mass_rate_mul_acceleration) &
            , it('6 kg/s / 3 mol/s = 2 kg/mol', chk_mass_rate_div_amount_rate) &
            , it('6 kg/s / 3 m^2 = 2 kg/(m^2 s)', chk_mass_rate_div_area) &
            , it('6 kg/s / 3 kg/m^3 = 2 m^3/s', chk_mass_rate_div_density) &
            , it('2 kg/s * 3 m^2/s = 6 J', chk_mass_rate_mul_diffusivity) &
            , it('6 kg/s / 3 Pa s = 2 m', chk_mass_rate_div_dynamic_viscosity) &
            , it('2 kg/s * 3 J/kg = 6 W', chk_mass_rate_mul_enthalpy) &
            , it('2 kg/s * 3 particles/m^2 = 6 kg/(m^2 s)', chk_mass_rate_mul_fluence) &
            , it('6 kg/s / 3 Hz = 2 kg', chk_mass_rate_div_frequency) &
            , it('2 kg/s * 3 mol/kg = 6 mol/s', chk_mass_rate_mul_inverse_molar_mass) &
            , it('6 kg/s / 3 m = 2 Pa s', chk_mass_rate_div_length) &
            , it('6 kg/s / 3 kg = 2 Hz', chk_mass_rate_div_mass) &
            , it('6 kg/s / 3 kg/(m^2 s) = 2 m^2', chk_mass_rate_div_mass_flux) &
            , it('6 kg/s / 3 kg/mol = 2 mol/s', chk_mass_rate_div_molar_mass) &
            , it('2 kg/s * 3 J/(kg K) = 6 W/K', chk_mass_rate_mul_specific_heat) &
            , it('2 kg/s * 3 m/s = 6 N', chk_mass_rate_mul_speed) &
            , it('2 kg/s * 3 s = 6 kg', chk_mass_rate_mul_time) &
            , it('6 kg/s / 3 m^3/s = 2 kg/m^3', chk_mass_rate_div_volume_rate) &
            , it('6 mol/m^3 / 3 kg/m^3 = 2 mol/kg', chk_molar_density_div_density) &
            , it('6 mol/m^3 / 3 mol/kg = 2 kg/m^3', chk_molar_density_div_inverse_molar_mass) &
            , it('2 mol/m^3 * 3 J/mol = 6 Pa', chk_molar_density_mul_molar_enthalpy) &
            , it('2 mol/m^3 * 3 kg/mol = 6 kg/m^3', chk_molar_density_mul_molar_mass) &
            , it('2 mol/m^3 * 3 J/(K mol) = 6 Pa/K', chk_molar_density_mul_molar_specific_heat) &
            , it('2 mol/m^3 * 3 m^3 = 6 mol', chk_molar_density_mul_volume) &
            , it('2 mol/m^3 * 3 m^3/s = 6 mol/s', chk_molar_density_mul_volume_rate) &
            , it('2 J/mol * 3 mol = 6 J', chk_molar_enthalpy_mul_amount) &
            , it('2 J/mol * 3 mol/s = 6 W', chk_molar_enthalpy_mul_amount_rate) &
            , it('6 J/mol / 3 K = 2 J/(K mol)', chk_molar_enthalpy_div_delta_temperature) &
            , it('6 J/mol / 3 K = 2 J/(K mol)', chk_molar_enthalpy_div_temperature) &
            , it('6 J/mol / 3 J/kg = 2 kg/mol', chk_molar_enthalpy_div_enthalpy) &
            , it('2 J/mol * 3 mol/kg = 6 J/kg', chk_molar_enthalpy_mul_inverse_molar_mass) &
            , it('2 J/mol * 3 mol/m^3 = 6 Pa', chk_molar_enthalpy_mul_molar_density) &
            , it('6 J/mol / 3 kg/mol = 2 J/kg', chk_molar_enthalpy_div_molar_mass) &
            , it('6 J/mol / 3 J/(K mol) = 2 K', chk_molar_enthalpy_div_molar_specific_heat) &
            , it('2 J/mol * 3 1/K = 6 J/(K mol)', chk_molar_enthalpy_mul_thermal_expansion_coefficient) &
            , it('2 kg/mol * 3 mol = 6 kg', chk_molar_mass_mul_amount) &
            , it('2 kg/mol * 3 mol/s = 6 kg/s', chk_molar_mass_mul_amount_rate) &
            , it('2 kg/mol * 3 J/kg = 6 J/mol', chk_molar_mass_mul_enthalpy) &
            , it('6  / 3 kg/mol = 2 mol/kg', chk_number_div_molar_mass) &
            , it('2 kg/mol * 3 mol/kg = 6 ', chk_molar_mass_mul_inverse_molar_mass) &
            , it('2 kg/mol * 3 mol/m^3 = 6 kg/m^3', chk_molar_mass_mul_molar_density) &
            , it('2 kg/mol * 3 J/(kg K) = 6 J/(K mol)', chk_molar_mass_mul_specific_heat) &
            , it('2 J/(K mol) * 3 mol = 6 J/K', chk_molar_specific_heat_mul_amount) &
            , it('2 J/(K mol) * 3 mol/s = 6 W/K', chk_molar_specific_heat_mul_amount_rate) &
            , it('2 J/(K mol) * 3 mol K = 6 J', chk_molar_specific_heat_mul_amount_temperature) &
            , it('2 J/(K mol) * 3 (mol K)/s = 6 W', chk_molar_specific_heat_mul_amount_temperature_rate) &
            , it('2 J/(K mol) * 3 K = 6 J/mol', chk_molar_specific_heat_mul_delta_temperature) &
            , it('2 J/(K mol) * 3 K = 6 J/mol', chk_molar_specific_heat_mul_temperature) &
            , it('2 J/(K mol) * 3 mol/kg = 6 J/(kg K)', chk_molar_specific_heat_mul_inverse_molar_mass) &
            , it('2 J/(K mol) * 3 mol/m^3 = 6 Pa/K', chk_molar_specific_heat_mul_molar_density) &
            , it('6 J/(K mol) / 3 J/mol = 2 1/K', chk_molar_specific_heat_div_molar_enthalpy) &
            , it('6 J/(K mol) / 3 kg/mol = 2 J/(kg K)', chk_molar_specific_heat_div_molar_mass) &
            , it('6 J/(K mol) / 3 J/(kg K) = 2 kg/mol', chk_molar_specific_heat_div_specific_heat) &
            , it('6 J/(K mol) / 3 1/K = 2 J/mol', chk_molar_specific_heat_div_thermal_expansion_coefficient) &
            , it('6 W / 3 mol/s = 2 J/mol', chk_power_div_amount_rate) &
            , it('6 W / 3 (mol K)/s = 2 J/(K mol)', chk_power_div_amount_temperature_rate) &
            , it('6 W / 3 m^2 = 2 W/m^2', chk_power_div_area) &
            , it('6 W / 3 W/K = 2 K', chk_power_div_conductance) &
            , it('6 W / 3 K = 2 W/K', chk_power_div_delta_temperature) &
            , it('6 W / 3 K = 2 W/K', chk_power_div_temperature) &
            , it('6 W / 3 J = 2 Hz', chk_power_div_energy) &
            , it('6 W / 3 J/kg = 2 kg/s', chk_power_div_enthalpy) &
            , it('2 W * 3 particles/m^2 = 6 W/m^2', chk_power_mul_fluence) &
            , it('6 W / 3 N = 2 m/s', chk_power_div_force) &
            , it('6 W / 3 Hz = 2 J', chk_power_div_frequency) &
            , it('6 W / 3 W/m^2 = 2 m^2', chk_power_div_intensity) &
            , it('6 W / 3 m = 2 N/s', chk_power_div_length) &
            , it('6 W / 3 kg/s = 2 J/kg', chk_power_div_mass_rate) &
            , it('6 W / 3 J/mol = 2 mol/s', chk_power_div_molar_enthalpy) &
            , it('6 W / 3 J/(K mol) = 2 (mol K)/s', chk_power_div_molar_specific_heat) &
            , it('6 W / 3 Pa = 2 m^3/s', chk_power_div_pressure) &
            , it('6 W / 3 m/s = 2 N', chk_power_div_speed) &
            , it('2 W * 3 1/K = 6 W/K', chk_power_mul_thermal_expansion_coefficient) &
            , it('2 W * 3 s = 6 J', chk_power_mul_time) &
            , it('6 W / 3 m^3/s = 2 Pa', chk_power_div_volume_rate) &
            , it('6 W / 3 N/s = 2 m', chk_power_div_yank) &
            , it('2 Pa * 3 m^2 = 6 N', chk_pressure_mul_area) &
            , it('6 Pa / 3 K = 2 Pa/K', chk_pressure_div_delta_temperature) &
            , it('6 Pa / 3 K = 2 Pa/K', chk_pressure_div_temperature) &
            , it('6 Pa / 3 kg/m^3 = 2 J/kg', chk_pressure_div_density) &
            , it('2 Pa * 3 m^2/s = 6 N/s', chk_pressure_mul_diffusivity) &
            , it('6 Pa / 3 Pa s = 2 Hz', chk_pressure_div_dynamic_viscosity) &
            , it('6 Pa / 3 J/kg = 2 kg/m^3', chk_pressure_div_enthalpy) &
            , it('6 Pa / 3 particles/m^2 = 2 N', chk_pressure_div_fluence) &
            , it('6 Pa / 3 N = 2 particles/m^2', chk_pressure_div_force) &
            , it('6 Pa / 3 Hz = 2 Pa s', chk_pressure_div_frequency) &
            , it('6 Pa / 3 kg/(m^2 s) = 2 m/s', chk_pressure_div_mass_flux) &
            , it('6 Pa / 3 mol/m^3 = 2 J/mol', chk_pressure_div_molar_density) &
            , it('6 Pa / 3 J/mol = 2 mol/m^3', chk_pressure_div_molar_enthalpy) &
            , it('2 Pa * 3 m/s = 6 W/m^2', chk_pressure_mul_speed) &
            , it('6 Pa / 3 m/s = 2 kg/(m^2 s)', chk_pressure_div_speed) &
            , it('2 Pa * 3 1/K = 6 Pa/K', chk_pressure_mul_thermal_expansion_coefficient) &
            , it('2 Pa * 3 s = 6 Pa s', chk_pressure_mul_time) &
            , it('2 Pa * 3 m^3 = 6 J', chk_pressure_mul_volume) &
            , it('2 Pa * 3 m^3/s = 6 W', chk_pressure_mul_volume_rate) &
            , it('6 Pa / 3 Pa/K = 2 K', chk_pressure_div_volumetric_heat_capacity) &
            , it('2 J/(kg K) * 3 K = 6 J/kg', chk_specific_heat_mul_delta_temperature) &
            , it('2 J/(kg K) * 3 K = 6 J/kg', chk_specific_heat_mul_temperature) &
            , it('2 J/(kg K) * 3 kg/m^3 = 6 Pa/K', chk_specific_heat_mul_density) &
            , it('2 J/(kg K) * 3 Pa s = 6 W/(m K)', chk_specific_heat_mul_dynamic_viscosity) &
            , it('6 J/(kg K) / 3 J/kg = 2 1/K', chk_specific_heat_div_enthalpy) &
            , it('6 J/(kg K) / 3 mol/kg = 2 J/(K mol)', chk_specific_heat_div_inverse_molar_mass) &
            , it('2 J/(kg K) * 3 kg = 6 J/K', chk_specific_heat_mul_mass) &
            , it('2 J/(kg K) * 3 kg/(m^2 s) = 6 W/(m^2 K)', chk_specific_heat_mul_mass_flux) &
            , it('2 J/(kg K) * 3 kg/s = 6 W/K', chk_specific_heat_mul_mass_rate) &
            , it('2 J/(kg K) * 3 kg/mol = 6 J/(K mol)', chk_specific_heat_mul_molar_mass) &
            , it('6 J/(kg K) / 3 J/(K mol) = 2 mol/kg', chk_specific_heat_div_molar_specific_heat) &
            , it('2 J/(kg K) * 3 K/m = 6 m/s^2', chk_specific_heat_mul_temperature_gradient) &
            , it('6 J/(kg K) / 3 1/K = 2 J/kg', chk_specific_heat_div_thermal_expansion_coefficient) &
            , it('6 m/s / 3 m/s^2 = 2 s', chk_speed_div_acceleration) &
            , it('2 m/s * 3 m^2 = 6 m^3/s', chk_speed_mul_area) &
            , it('2 m/s * 3 kg/m^3 = 6 kg/(m^2 s)', chk_speed_mul_density) &
            , it('6 m/s / 3 particles/m^2 = 2 m^3/s', chk_speed_div_fluence) &
            , it('2 m/s * 3 N = 6 W', chk_speed_mul_force) &
            , it('2 m/s * 3 Hz = 6 m/s^2', chk_speed_mul_frequency) &
            , it('6 m/s / 3 Hz = 2 m', chk_speed_div_frequency) &
            , it('2 m/s * 3 m = 6 m^2/s', chk_speed_mul_length) &
            , it('6 m/s / 3 m = 2 Hz', chk_speed_div_length) &
            , it('2 m/s * 3 kg/(m^2 s) = 6 Pa', chk_speed_mul_mass_flux) &
            , it('2 m/s * 3 kg/s = 6 N', chk_speed_mul_mass_rate) &
            , it('2 m/s * 3 Pa = 6 W/m^2', chk_speed_mul_pressure) &
            , it('2 m/s * 3 m/s = 6 J/kg', chk_speed_mul_speed) &
            , it('2 m/s * 3 s = 6 m', chk_speed_mul_time) &
            , it('6 m/s / 3 s = 2 m/s^2', chk_speed_div_time) &
            , it('6 m/s / 3 m^3/s = 2 particles/m^2', chk_speed_div_volume_rate) &
            , it('2 m/s * 3 Pa/K = 6 W/(m^2 K)', chk_speed_mul_volumetric_heat_capacity) &
            , it('2 K/m * 3 W/K = 6 N/s', chk_temperature_gradient_mul_conductance) &
            , it('2 K/m * 3 J/K = 6 N', chk_temperature_gradient_mul_heat_capacity) &
            , it('2 K/m * 3 m = 6 K', chk_temperature_gradient_mul_length) &
            , it('2 K/m * 3 J/(kg K) = 6 m/s^2', chk_temperature_gradient_mul_specific_heat) &
            , it('2 K/m * 3 W/(m K) = 6 W/m^2', chk_temperature_gradient_mul_thermal_conductivity) &
            , it('2 W/(m K) * 3 K = 6 N/s', chk_thermal_conductivity_mul_delta_temperature) &
            , it('2 W/(m K) * 3 K = 6 N/s', chk_thermal_conductivity_mul_temperature) &
            , it('6 W/(m K) / 3 m^2/s = 2 Pa/K', chk_thermal_conductivity_div_diffusivity) &
            , it('6 W/(m K) / 3 Pa s = 2 J/(kg K)', chk_thermal_conductivity_div_dynamic_viscosity) &
            , it('6 W/(m K) / 3 W/(m^2 K) = 2 m', chk_thermal_conductivity_div_heat_transfer_coefficient) &
            , it('2 W/(m K) * 3 (K m^2)/W = 6 m', chk_thermal_conductivity_mul_insulance) &
            , it('2 W/(m K) * 3 m = 6 W/K', chk_thermal_conductivity_mul_length) &
            , it('6 W/(m K) / 3 m = 2 W/(m^2 K)', chk_thermal_conductivity_div_length) &
            , it('6 W/(m K) / 3 J/(kg K) = 2 Pa s', chk_thermal_conductivity_div_specific_heat) &
            , it('2 W/(m K) * 3 K/m = 6 W/m^2', chk_thermal_conductivity_mul_temperature_gradient) &
            , it('6 W/(m K) / 3 1/K = 2 N/s', chk_thermal_conductivity_div_thermal_expansion_coefficient) &
            , it('6 W/(m K) / 3 Pa/K = 2 m^2/s', chk_thermal_conductivity_div_volumetric_heat_capacity) &
            , it('6 W/(m K) / 3 N/s = 2 1/K', chk_thermal_conductivity_div_yank) &
            , it('2 1/K * 3 mol K = 6 mol', chk_thermal_expansion_coefficient_mul_amount_temperature) &
            , it('2 1/K * 3 (mol K)/s = 6 mol/s', chk_thermal_expansion_coefficient_mul_amount_temperature_rate) &
            , it('6  / 3 1/K = 2 K', chk_number_div_thermal_expansion_coefficient) &
            , it('2 1/K * 3 K = 6 ', chk_thermal_expansion_coefficient_mul_delta_temperature) &
            , it('2 1/K * 3 J = 6 J/K', chk_thermal_expansion_coefficient_mul_energy) &
            , it('2 1/K * 3 J/kg = 6 J/(kg K)', chk_thermal_expansion_coefficient_mul_enthalpy) &
            , it('2 1/K * 3 W/m^2 = 6 W/(m^2 K)', chk_thermal_expansion_coefficient_mul_intensity) &
            , it('2 1/K * 3 J/mol = 6 J/(K mol)', chk_thermal_expansion_coefficient_mul_molar_enthalpy) &
            , it('2 1/K * 3 W = 6 W/K', chk_thermal_expansion_coefficient_mul_power) &
            , it('2 1/K * 3 Pa = 6 Pa/K', chk_thermal_expansion_coefficient_mul_pressure) &
            , it('2 1/K * 3 N/s = 6 W/(m K)', chk_thermal_expansion_coefficient_mul_yank) &
            , it('2 s * 3 m/s^2 = 6 m/s', chk_time_mul_acceleration) &
            , it('2 s * 3 mol/s = 6 mol', chk_time_mul_amount_rate) &
            , it('2 s * 3 (mol K)/s = 6 mol K', chk_time_mul_amount_temperature_rate) &
            , it('2 s * 3 W/K = 6 J/K', chk_time_mul_conductance) &
            , it('2 s * 3 m^2/s = 6 m^2', chk_time_mul_diffusivity) &
            , it('2 s * 3 J/kg = 6 m^2/s', chk_time_mul_enthalpy) &
            , it('6  / 3 s = 2 Hz', chk_number_div_time) &
            , it('2 s * 3 Hz = 6 ', chk_time_mul_frequency) &
            , it('2 s * 3 kg/s = 6 kg', chk_time_mul_mass_rate) &
            , it('2 s * 3 W = 6 J', chk_time_mul_power) &
            , it('2 s * 3 Pa = 6 Pa s', chk_time_mul_pressure) &
            , it('2 s * 3 m/s = 6 m', chk_time_mul_speed) &
            , it('2 s * 3 m^3/s = 6 m^3', chk_time_mul_volume_rate) &
            , it('2 s * 3 N/s = 6 N', chk_time_mul_yank) &
            , it('6 m^3 / 3 m^2 = 2 m', chk_volume_div_area) &
            , it('2 m^3 * 3 kg/m^3 = 6 kg', chk_volume_mul_density) &
            , it('2 m^3 * 3 particles/m^2 = 6 m', chk_volume_mul_fluence) &
            , it('2 m^3 * 3 Hz = 6 m^3/s', chk_volume_mul_frequency) &
            , it('6 m^3 / 3 m = 2 m^2', chk_volume_div_length) &
            , it('2 m^3 * 3 mol/m^3 = 6 mol', chk_volume_mul_molar_density) &
            , it('2 m^3 * 3 Pa = 6 J', chk_volume_mul_pressure) &
            , it('6 m^3 / 3 s = 2 m^3/s', chk_volume_div_time) &
            , it('6 m^3 / 3 m^3/s = 2 s', chk_volume_div_volume_rate) &
            , it('2 m^3 * 3 Pa/K = 6 J/K', chk_volume_mul_volumetric_heat_capacity) &
            , it('6 m^3/s / 3 m^2 = 2 m/s', chk_volume_rate_div_area) &
            , it('2 m^3/s * 3 kg/m^3 = 6 kg/s', chk_volume_rate_mul_density) &
            , it('6 m^3/s / 3 m^2/s = 2 m', chk_volume_rate_div_diffusivity) &
            , it('2 m^3/s * 3 Pa s = 6 J', chk_volume_rate_mul_dynamic_viscosity) &
            , it('2 m^3/s * 3 particles/m^2 = 6 m/s', chk_volume_rate_mul_fluence) &
            , it('6 m^3/s / 3 Hz = 2 m^3', chk_volume_rate_div_frequency) &
            , it('6 m^3/s / 3 m = 2 m^2/s', chk_volume_rate_div_length) &
            , it('2 m^3/s * 3 kg/(m^2 s) = 6 N', chk_volume_rate_mul_mass_flux) &
            , it('2 m^3/s * 3 mol/m^3 = 6 mol/s', chk_volume_rate_mul_molar_density) &
            , it('2 m^3/s * 3 Pa = 6 W', chk_volume_rate_mul_pressure) &
            , it('6 m^3/s / 3 m/s = 2 m^2', chk_volume_rate_div_speed) &
            , it('2 m^3/s * 3 s = 6 m^3', chk_volume_rate_mul_time) &
            , it('6 m^3/s / 3 m^3 = 2 Hz', chk_volume_rate_div_volume) &
            , it('2 m^3/s * 3 Pa/K = 6 W/K', chk_volume_rate_mul_volumetric_heat_capacity) &
            , it('2 Pa/K * 3 K = 6 Pa', chk_volumetric_heat_capacity_mul_delta_temperature) &
            , it('2 Pa/K * 3 K = 6 Pa', chk_volumetric_heat_capacity_mul_temperature) &
            , it('6 Pa/K / 3 kg/m^3 = 2 J/(kg K)', chk_volumetric_heat_capacity_div_density) &
            , it('2 Pa/K * 3 m^2/s = 6 W/(m K)', chk_volumetric_heat_capacity_mul_diffusivity) &
            , it('6 Pa/K / 3 mol/m^3 = 2 J/(K mol)', chk_volumetric_heat_capacity_div_molar_density) &
            , it('6 Pa/K / 3 J/(K mol) = 2 mol/m^3', chk_volumetric_heat_capacity_div_molar_specific_heat) &
            , it('6 Pa/K / 3 Pa = 2 1/K', chk_volumetric_heat_capacity_div_pressure) &
            , it('6 Pa/K / 3 J/(kg K) = 2 kg/m^3', chk_volumetric_heat_capacity_div_specific_heat) &
            , it('2 Pa/K * 3 m/s = 6 W/(m^2 K)', chk_volumetric_heat_capacity_mul_speed) &
            , it('6 Pa/K / 3 1/K = 2 Pa', chk_volumetric_heat_capacity_div_thermal_expansion_coefficient) &
            , it('2 Pa/K * 3 m^3 = 6 J/K', chk_volumetric_heat_capacity_mul_volume) &
            , it('2 Pa/K * 3 m^3/s = 6 W/K', chk_volumetric_heat_capacity_mul_volume_rate) &
            , it('6 N/s / 3 m/s^2 = 2 kg/s', chk_yank_div_acceleration) &
            , it('6 N/s / 3 W/K = 2 K/m', chk_yank_div_conductance) &
            , it('6 N/s / 3 K = 2 W/(m K)', chk_yank_div_delta_temperature) &
            , it('6 N/s / 3 K = 2 W/(m K)', chk_yank_div_temperature) &
            , it('6 N/s / 3 m^2/s = 2 Pa', chk_yank_div_diffusivity) &
            , it('6 N/s / 3 Pa s = 2 J/kg', chk_yank_div_dynamic_viscosity) &
            , it('6 N/s / 3 J/kg = 2 Pa s', chk_yank_div_enthalpy) &
            , it('6 N/s / 3 N = 2 Hz', chk_yank_div_force) &
            , it('6 N/s / 3 Hz = 2 N', chk_yank_div_frequency) &
            , it('6 N/s / 3 W/m^2 = 2 m', chk_yank_div_intensity) &
            , it('2 N/s * 3 m = 6 W', chk_yank_mul_length) &
            , it('6 N/s / 3 m = 2 W/m^2', chk_yank_div_length) &
            , it('6 N/s / 3 kg/s = 2 m/s^2', chk_yank_div_mass_rate) &
            , it('6 N/s / 3 Pa = 2 m^2/s', chk_yank_div_pressure) &
            , it('6 N/s / 3 K/m = 2 W/K', chk_yank_div_temperature_gradient) &
            , it('6 N/s / 3 W/(m K) = 2 K', chk_yank_div_thermal_conductivity) &
            , it('2 N/s * 3 1/K = 6 W/(m K)', chk_yank_mul_thermal_expansion_coefficient) &
            , it('2 N/s * 3 s = 6 N', chk_yank_mul_time) &
            , it('6 K - 4 K = 2 K', chk_temp_minus_temp) &
            , it('6 K - 4 K = 2 K', chk_temp_minus_delta_temp) &
            , it('6 K + 3 K = 9 K', chk_temp_plus_delta_temp) &
            , it('6 K + 3 K = 9 K', chk_delta_temp_plus_temp) &
            , it('sqrt(9 m^2) = 3 m', chk_sqrt_area) &
            , it('sqrt(9 J/kg) = 3 m/s', chk_sqrt_enthalpy) &
            , it('cbrt(27 m^3) = 3 m', chk_cbrt_volume) &
            ])
    end function

    pure function chk_acceleration_mul_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.meters_per_square_second) * (3.d0.unit.pascal_seconds))
    end function

    pure function chk_acceleration_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.meters_per_square_second) / (3.d0.unit.hertz))
    end function

    pure function chk_acceleration_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.meters_per_square_second) * (3.d0.unit.meters))
    end function

    pure function chk_acceleration_mul_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.meters_per_square_second) * (3.d0.unit.kilograms))
    end function

    pure function chk_acceleration_mul_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.meters_per_square_second) * (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_acceleration_div_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kelvin_per_meter, &
                (6.d0.unit.meters_per_square_second) / (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_acceleration_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.meters_per_square_second) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_acceleration_div_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram_kelvin, &
                (6.d0.unit.meters_per_square_second) / (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_acceleration_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_second, &
                (2.d0.unit.meters_per_square_second) * (3.d0.unit.seconds))
    end function

    pure function chk_amount_div_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.mols) / (3.d0.unit.mols_per_second))
    end function

    pure function chk_amount_div_amount_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0.unit.mols) / (3.d0.unit.mols_kelvin))
    end function

    pure function chk_amount_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin, &
                (2.d0.unit.mols) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_amount_mul_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin, &
                (2.d0.unit.mols) * (3.d0.unit.kelvin))
    end function

    pure function chk_amount_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_second, &
                (2.d0.unit.mols) * (3.d0.unit.hertz))
    end function

    pure function chk_amount_div_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms, &
                (6.d0.unit.mols) / (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_amount_div_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_kilogram, &
                (6.d0.unit.mols) / (3.d0.unit.kilograms))
    end function

    pure function chk_amount_div_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters, &
                (6.d0.unit.mols) / (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_amount_mul_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.mols) * (3.d0.unit.joules_per_mol))
    end function

    pure function chk_amount_mul_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms, &
                (2.d0.unit.mols) * (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_amount_mul_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.mols) * (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_amount_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_kelvin, &
                (6.d0.unit.mols) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_amount_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_second, &
                (6.d0.unit.mols) / (3.d0.unit.seconds))
    end function

    pure function chk_amount_div_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_cubic_meter, &
                (6.d0.unit.mols) / (3.d0.unit.cubic_meters))
    end function

    pure function chk_amount_rate_div_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.mols_per_second) / (3.d0.unit.mols))
    end function

    pure function chk_amount_rate_div_amount_temperature_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0.unit.mols_per_second) / (3.d0.unit.mols_kelvin_per_second))
    end function

    pure function chk_amount_rate_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin_per_second, &
                (2.d0.unit.mols_per_second) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_amount_rate_mul_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin_per_second, &
                (2.d0.unit.mols_per_second) * (3.d0.unit.kelvin))
    end function

    pure function chk_amount_rate_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols, &
                (6.d0.unit.mols_per_second) / (3.d0.unit.hertz))
    end function

    pure function chk_amount_rate_div_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_second, &
                (6.d0.unit.mols_per_second) / (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_amount_rate_div_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_kilogram, &
                (6.d0.unit.mols_per_second) / (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_amount_rate_div_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters_per_second, &
                (6.d0.unit.mols_per_second) / (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_amount_rate_mul_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.mols_per_second) * (3.d0.unit.joules_per_mol))
    end function

    pure function chk_amount_rate_mul_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.mols_per_second) * (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_amount_rate_mul_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.mols_per_second) * (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_amount_rate_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_kelvin_per_second, &
                (6.d0.unit.mols_per_second) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_amount_rate_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols, &
                (2.d0.unit.mols_per_second) * (3.d0.unit.seconds))
    end function

    pure function chk_amount_rate_div_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_cubic_meter, &
                (6.d0.unit.mols_per_second) / (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_amount_temperature_div_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.mols_kelvin) / (3.d0.unit.mols))
    end function

    pure function chk_amount_temperature_div_amount_temperature_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.mols_kelvin) / (3.d0.unit.mols_kelvin_per_second))
    end function

    pure function chk_amount_temperature_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols, &
                (6.d0.unit.mols_kelvin) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_amount_temperature_div_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols, &
                (6.d0.unit.mols_kelvin) / (3.d0.unit.kelvin))
    end function

    pure function chk_amount_temperature_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin_per_second, &
                (2.d0.unit.mols_kelvin) * (3.d0.unit.hertz))
    end function

    pure function chk_amount_temperature_mul_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.mols_kelvin) * (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_amount_temperature_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols, &
                (2.d0.unit.mols_kelvin) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_amount_temperature_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_kelvin_per_second, &
                (6.d0.unit.mols_kelvin) / (3.d0.unit.seconds))
    end function

    pure function chk_amount_temperature_rate_div_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.mols_kelvin_per_second) / (3.d0.unit.mols_per_second))
    end function

    pure function chk_amount_temperature_rate_div_amount_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.mols_kelvin_per_second) / (3.d0.unit.mols_kelvin))
    end function

    pure function chk_amount_temperature_rate_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_second, &
                (6.d0.unit.mols_kelvin_per_second) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_amount_temperature_rate_div_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_second, &
                (6.d0.unit.mols_kelvin_per_second) / (3.d0.unit.kelvin))
    end function

    pure function chk_amount_temperature_rate_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_kelvin, &
                (6.d0.unit.mols_kelvin_per_second) / (3.d0.unit.hertz))
    end function

    pure function chk_amount_temperature_rate_mul_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.mols_kelvin_per_second) * (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_amount_temperature_rate_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_second, &
                (2.d0.unit.mols_kelvin_per_second) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_amount_temperature_rate_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin, &
                (2.d0.unit.mols_kelvin_per_second) * (3.d0.unit.seconds))
    end function

    pure function chk_area_div_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kelvin_square_meters_per_watt, &
                (6.d0.unit.square_meters) / (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_area_div_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.square_meters) / (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_number_div_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.particles_per_square_meter, &
                (6.d0) / (3.d0.unit.square_meters))
    end function

    pure function chk_area_mul_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.square_meters) * (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_area_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters_per_second, &
                (2.d0.unit.square_meters) * (3.d0.unit.hertz))
    end function

    pure function chk_area_mul_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.square_meters) * (3.d0.unit.WATTS_PER_SQUARE_METER_KELVIN))
    end function

    pure function chk_area_div_insulance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_kelvin, &
                (6.d0.unit.square_meters) / (3.d0.unit.kelvin_square_meters_per_watt))
    end function

    pure function chk_area_mul_intensity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.square_meters) * (3.d0.unit.watts_per_square_meter))
    end function

    pure function chk_area_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters, &
                (2.d0.unit.square_meters) * (3.d0.unit.meters))
    end function

    pure function chk_area_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.square_meters) / (3.d0.unit.meters))
    end function

    pure function chk_area_mul_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.square_meters) * (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_area_mul_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.square_meters) * (3.d0.unit.pascals))
    end function

    pure function chk_area_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters_per_second, &
                (2.d0.unit.square_meters) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_area_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters_per_second, &
                (6.d0.unit.square_meters) / (3.d0.unit.seconds))
    end function

    pure function chk_conductance_div_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin_mol, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.mols_per_second))
    end function

    pure function chk_conductance_div_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.square_meters))
    end function

    pure function chk_conductance_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.watts_per_kelvin) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_conductance_mul_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.watts_per_kelvin) * (3.d0.unit.kelvin))
    end function

    pure function chk_conductance_mul_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (2.d0.unit.watts_per_kelvin) * (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_conductance_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.hertz))
    end function

    pure function chk_conductance_div_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.joules_per_kelvin))
    end function

    pure function chk_conductance_div_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.WATTS_PER_SQUARE_METER_KELVIN))
    end function

    pure function chk_conductance_mul_insulance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters, &
                (2.d0.unit.watts_per_kelvin) * (3.d0.unit.kelvin_square_meters_per_watt))
    end function

    pure function chk_conductance_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_meter_kelvin, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.meters))
    end function

    pure function chk_conductance_div_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram_kelvin, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_conductance_div_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_second, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_conductance_div_power() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.watts))
    end function

    pure function chk_conductance_div_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_second, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_conductance_mul_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.watts_per_kelvin) * (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_conductance_div_thermal_conductivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.watts_per_meter_kelvin))
    end function

    pure function chk_conductance_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_conductance_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.watts_per_kelvin) * (3.d0.unit.seconds))
    end function

    pure function chk_conductance_div_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals_per_kelvin, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_conductance_div_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters_per_second, &
                (6.d0.unit.watts_per_kelvin) / (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_delta_temperature_mul_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.mols))
    end function

    pure function chk_temperature_mul_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin, &
                (2.d0.unit.kelvin) * (3.d0.unit.mols))
    end function

    pure function chk_delta_temperature_mul_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin_per_second, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.mols_per_second))
    end function

    pure function chk_temperature_mul_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin_per_second, &
                (2.d0.unit.kelvin) * (3.d0.unit.mols_per_second))
    end function

    pure function chk_delta_temperature_mul_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_temperature_mul_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.kelvin) * (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_delta_temperature_mul_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.joules_per_kelvin))
    end function

    pure function chk_temperature_mul_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.kelvin) * (3.d0.unit.joules_per_kelvin))
    end function

    pure function chk_delta_temperature_mul_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.WATTS_PER_SQUARE_METER_KELVIN))
    end function

    pure function chk_temperature_mul_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.kelvin) * (3.d0.unit.WATTS_PER_SQUARE_METER_KELVIN))
    end function

    pure function chk_delta_temperature_div_insulance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_square_meter, &
                (6.d0.unit.delta_kelvin) / (3.d0.unit.kelvin_square_meters_per_watt))
    end function

    pure function chk_delta_temperature_div_intensity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kelvin_square_meters_per_watt, &
                (6.d0.unit.delta_kelvin) / (3.d0.unit.watts_per_square_meter))
    end function

    pure function chk_delta_temperature_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kelvin_per_meter, &
                (6.d0.unit.delta_kelvin) / (3.d0.unit.meters))
    end function

    pure function chk_delta_temperature_mul_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_mol, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_temperature_mul_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_mol, &
                (2.d0.unit.kelvin) * (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_delta_temperature_mul_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_temperature_mul_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.kelvin) * (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_delta_temperature_div_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.delta_kelvin) / (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_delta_temperature_mul_thermal_conductivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.watts_per_meter_kelvin))
    end function

    pure function chk_temperature_mul_thermal_conductivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.kelvin) * (3.d0.unit.watts_per_meter_kelvin))
    end function

    pure function chk_number_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_delta_temperature_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_delta_temperature_mul_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.delta_kelvin) * (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_temperature_mul_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.kelvin) * (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_density_mul_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascal_seconds, &
                (2.d0.unit.kilograms_per_cubic_meter) * (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_density_mul_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.kilograms_per_cubic_meter) * (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_density_mul_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_cubic_meter, &
                (2.d0.unit.kilograms_per_cubic_meter) * (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_density_div_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_mol, &
                (6.d0.unit.kilograms_per_cubic_meter) / (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_density_div_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_cubic_meter, &
                (6.d0.unit.kilograms_per_cubic_meter) / (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_density_mul_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals_per_kelvin, &
                (2.d0.unit.kilograms_per_cubic_meter) * (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_density_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_square_meter_seconds, &
                (2.d0.unit.kilograms_per_cubic_meter) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_density_mul_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms, &
                (2.d0.unit.kilograms_per_cubic_meter) * (3.d0.unit.cubic_meters))
    end function

    pure function chk_density_mul_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.kilograms_per_cubic_meter) * (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_diffusivity_div_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.square_meters_per_second) / (3.d0.unit.square_meters))
    end function

    pure function chk_diffusivity_mul_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascal_seconds, &
                (2.d0.unit.square_meters_per_second) * (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_diffusivity_mul_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.square_meters_per_second) * (3.d0.unit.pascal_seconds))
    end function

    pure function chk_diffusivity_div_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.square_meters_per_second) / (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_diffusivity_mul_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.hertz, &
                (2.d0.unit.square_meters_per_second) * (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_diffusivity_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.square_meters_per_second) * (3.d0.unit.hertz))
    end function

    pure function chk_diffusivity_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters, &
                (6.d0.unit.square_meters_per_second) / (3.d0.unit.hertz))
    end function

    pure function chk_diffusivity_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters_per_second, &
                (2.d0.unit.square_meters_per_second) * (3.d0.unit.meters))
    end function

    pure function chk_diffusivity_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.square_meters_per_second) / (3.d0.unit.meters))
    end function

    pure function chk_diffusivity_mul_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.square_meters_per_second) * (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_diffusivity_mul_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.square_meters_per_second) * (3.d0.unit.pascals))
    end function

    pure function chk_diffusivity_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.square_meters_per_second) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_diffusivity_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters, &
                (2.d0.unit.square_meters_per_second) * (3.d0.unit.seconds))
    end function

    pure function chk_diffusivity_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram, &
                (6.d0.unit.square_meters_per_second) / (3.d0.unit.seconds))
    end function

    pure function chk_diffusivity_mul_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_meter_kelvin, &
                (2.d0.unit.square_meters_per_second) * (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_dynamic_viscosity_mul_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.pascal_seconds) * (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_dynamic_viscosity_div_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters_per_second, &
                (6.d0.unit.pascal_seconds) / (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_dynamic_viscosity_mul_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.pascal_seconds) * (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_dynamic_viscosity_div_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_cubic_meter, &
                (6.d0.unit.pascal_seconds) / (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_dynamic_viscosity_mul_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.pascal_seconds) * (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_dynamic_viscosity_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.pascal_seconds) * (3.d0.unit.hertz))
    end function

    pure function chk_dynamic_viscosity_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.pascal_seconds) * (3.d0.unit.meters))
    end function

    pure function chk_dynamic_viscosity_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_square_meter_seconds, &
                (6.d0.unit.pascal_seconds) / (3.d0.unit.meters))
    end function

    pure function chk_dynamic_viscosity_div_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.pascal_seconds) / (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_dynamic_viscosity_div_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.pascal_seconds) / (3.d0.unit.pascals))
    end function

    pure function chk_dynamic_viscosity_mul_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_meter_kelvin, &
                (2.d0.unit.pascal_seconds) * (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_dynamic_viscosity_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals, &
                (6.d0.unit.pascal_seconds) / (3.d0.unit.seconds))
    end function

    pure function chk_dynamic_viscosity_mul_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.pascal_seconds) * (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_energy_div_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_mol, &
                (6.d0.unit.joules) / (3.d0.unit.mols))
    end function

    pure function chk_energy_div_amount_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin_mol, &
                (6.d0.unit.joules) / (3.d0.unit.mols_kelvin))
    end function

    pure function chk_energy_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin, &
                (6.d0.unit.joules) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_energy_div_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin, &
                (6.d0.unit.joules) / (3.d0.unit.kelvin))
    end function

    pure function chk_energy_div_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_second, &
                (6.d0.unit.joules) / (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_energy_div_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters_per_second, &
                (6.d0.unit.joules) / (3.d0.unit.pascal_seconds))
    end function

    pure function chk_energy_div_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms, &
                (6.d0.unit.joules) / (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_energy_div_force() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.joules) / (3.d0.unit.newtons))
    end function

    pure function chk_energy_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.joules) * (3.d0.unit.hertz))
    end function

    pure function chk_energy_div_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.joules) / (3.d0.unit.joules_per_kelvin))
    end function

    pure function chk_energy_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.newtons, &
                (6.d0.unit.joules) / (3.d0.unit.meters))
    end function

    pure function chk_energy_div_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram, &
                (6.d0.unit.joules) / (3.d0.unit.kilograms))
    end function

    pure function chk_energy_div_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters_per_second, &
                (6.d0.unit.joules) / (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_energy_div_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols, &
                (6.d0.unit.joules) / (3.d0.unit.joules_per_mol))
    end function

    pure function chk_energy_div_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_kelvin, &
                (6.d0.unit.joules) / (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_energy_div_power() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.joules) / (3.d0.unit.watts))
    end function

    pure function chk_energy_div_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters, &
                (6.d0.unit.joules) / (3.d0.unit.pascals))
    end function

    pure function chk_energy_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.joules) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_energy_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts, &
                (6.d0.unit.joules) / (3.d0.unit.seconds))
    end function

    pure function chk_energy_div_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals, &
                (6.d0.unit.joules) / (3.d0.unit.cubic_meters))
    end function

    pure function chk_energy_div_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascal_seconds, &
                (6.d0.unit.joules) / (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_enthalpy_div_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_enthalpy_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram_kelvin, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_enthalpy_div_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram_kelvin, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.kelvin))
    end function

    pure function chk_enthalpy_mul_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.joules_per_kilogram) * (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_enthalpy_div_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_enthalpy_mul_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.joules_per_kilogram) * (3.d0.unit.pascal_seconds))
    end function

    pure function chk_enthalpy_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters_per_second, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.hertz))
    end function

    pure function chk_enthalpy_div_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_mol, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_enthalpy_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_square_second, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.meters))
    end function

    pure function chk_enthalpy_mul_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.joules_per_kilogram) * (3.d0.unit.kilograms))
    end function

    pure function chk_enthalpy_mul_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.joules_per_kilogram) * (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_enthalpy_mul_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.joules_per_kilogram) * (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_enthalpy_div_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_kilogram, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.joules_per_mol))
    end function

    pure function chk_enthalpy_mul_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_mol, &
                (2.d0.unit.joules_per_kilogram) * (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_enthalpy_div_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_enthalpy_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.joules_per_kilogram) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_enthalpy_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram_kelvin, &
                (2.d0.unit.joules_per_kilogram) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_enthalpy_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters_per_second, &
                (2.d0.unit.joules_per_kilogram) * (3.d0.unit.seconds))
    end function

    pure function chk_number_div_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters, &
                (6.d0) / (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_fluence_mul_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.particles_per_square_meter) * (3.d0.unit.square_meters))
    end function

    pure function chk_fluence_mul_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (2.d0.unit.particles_per_square_meter) * (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_fluence_mul_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.hertz, &
                (2.d0.unit.particles_per_square_meter) * (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_fluence_mul_force() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.particles_per_square_meter) * (3.d0.unit.newtons))
    end function

    pure function chk_fluence_mul_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_square_meter_seconds, &
                (2.d0.unit.particles_per_square_meter) * (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_fluence_mul_power() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.particles_per_square_meter) * (3.d0.unit.watts))
    end function

    pure function chk_fluence_mul_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters, &
                (2.d0.unit.particles_per_square_meter) * (3.d0.unit.cubic_meters))
    end function

    pure function chk_fluence_mul_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_second, &
                (2.d0.unit.particles_per_square_meter) * (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_force_div_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms, &
                (6.d0.unit.newtons) / (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_force_div_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals, &
                (6.d0.unit.newtons) / (3.d0.unit.square_meters))
    end function

    pure function chk_force_div_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascal_seconds, &
                (6.d0.unit.newtons) / (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_force_div_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters_per_second, &
                (6.d0.unit.newtons) / (3.d0.unit.pascal_seconds))
    end function

    pure function chk_force_mul_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.newtons) * (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_force_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.newtons) * (3.d0.unit.hertz))
    end function

    pure function chk_force_div_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kelvin_per_meter, &
                (6.d0.unit.newtons) / (3.d0.unit.joules_per_kelvin))
    end function

    pure function chk_force_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.newtons) * (3.d0.unit.meters))
    end function

    pure function chk_force_div_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_square_second, &
                (6.d0.unit.newtons) / (3.d0.unit.kilograms))
    end function

    pure function chk_force_div_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters_per_second, &
                (6.d0.unit.newtons) / (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_force_div_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.newtons) / (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_force_div_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters, &
                (6.d0.unit.newtons) / (3.d0.unit.pascals))
    end function

    pure function chk_force_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.newtons) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_force_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_second, &
                (6.d0.unit.newtons) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_force_div_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin, &
                (6.d0.unit.newtons) / (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_force_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.newtons_per_second, &
                (6.d0.unit.newtons) / (3.d0.unit.seconds))
    end function

    pure function chk_force_div_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_square_meter_seconds, &
                (6.d0.unit.newtons) / (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_force_div_yank() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.newtons) / (3.d0.unit.newtons_per_second))
    end function

    pure function chk_frequency_mul_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_second, &
                (2.d0.unit.hertz) * (3.d0.unit.mols))
    end function

    pure function chk_frequency_mul_amount_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin_per_second, &
                (2.d0.unit.hertz) * (3.d0.unit.mols_kelvin))
    end function

    pure function chk_frequency_mul_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters_per_second, &
                (2.d0.unit.hertz) * (3.d0.unit.square_meters))
    end function

    pure function chk_frequency_mul_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.hertz) * (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_frequency_div_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.particles_per_square_meter, &
                (6.d0.unit.hertz) / (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_frequency_mul_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.hertz) * (3.d0.unit.pascal_seconds))
    end function

    pure function chk_frequency_mul_energy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.hertz) * (3.d0.unit.joules))
    end function

    pure function chk_frequency_div_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters_per_second, &
                (6.d0.unit.hertz) / (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_frequency_mul_force() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.hertz) * (3.d0.unit.newtons))
    end function

    pure function chk_frequency_mul_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.hertz) * (3.d0.unit.joules_per_kelvin))
    end function

    pure function chk_frequency_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_second, &
                (2.d0.unit.hertz) * (3.d0.unit.meters))
    end function

    pure function chk_frequency_mul_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.hertz) * (3.d0.unit.kilograms))
    end function

    pure function chk_frequency_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_square_second, &
                (2.d0.unit.hertz) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_number_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0) / (3.d0.unit.hertz))
    end function

    pure function chk_frequency_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.hertz) * (3.d0.unit.seconds))
    end function

    pure function chk_frequency_mul_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters_per_second, &
                (2.d0.unit.hertz) * (3.d0.unit.cubic_meters))
    end function

    pure function chk_heat_capacity_div_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin_mol, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.mols))
    end function

    pure function chk_heat_capacity_div_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_heat_capacity_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.joules_per_kelvin) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_heat_capacity_mul_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.joules_per_kelvin) * (3.d0.unit.kelvin))
    end function

    pure function chk_heat_capacity_div_energy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.joules))
    end function

    pure function chk_heat_capacity_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.joules_per_kelvin) * (3.d0.unit.hertz))
    end function

    pure function chk_heat_capacity_div_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram_kelvin, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.kilograms))
    end function

    pure function chk_heat_capacity_div_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_heat_capacity_div_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_heat_capacity_mul_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.joules_per_kelvin) * (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_heat_capacity_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_heat_capacity_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_kelvin, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.seconds))
    end function

    pure function chk_heat_capacity_div_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals_per_kelvin, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.cubic_meters))
    end function

    pure function chk_heat_capacity_div_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters, &
                (6.d0.unit.joules_per_kelvin) / (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_heat_transfer_coefficient_mul_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) * (3.d0.unit.square_meters))
    end function

    pure function chk_heat_transfer_coefficient_div_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.particles_per_square_meter, &
                (6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) / (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_heat_transfer_coefficient_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_heat_transfer_coefficient_mul_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) * (3.d0.unit.kelvin))
    end function

    pure function chk_heat_transfer_coefficient_div_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_kelvin, &
                (6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) / (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_number_div_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kelvin_square_meters_per_watt, &
                (6.d0) / (3.d0.unit.WATTS_PER_SQUARE_METER_KELVIN))
    end function

    pure function chk_heat_transfer_coefficient_mul_insulance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) * (3.d0.unit.kelvin_square_meters_per_watt))
    end function

    pure function chk_heat_transfer_coefficient_div_intensity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) / (3.d0.unit.watts_per_square_meter))
    end function

    pure function chk_heat_transfer_coefficient_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_meter_kelvin, &
                (2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) * (3.d0.unit.meters))
    end function

    pure function chk_heat_transfer_coefficient_div_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram_kelvin, &
                (6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) / (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_heat_transfer_coefficient_div_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_square_meter_seconds, &
                (6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) / (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_heat_transfer_coefficient_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals_per_kelvin, &
                (6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_heat_transfer_coefficient_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_square_meter, &
                (6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_heat_transfer_coefficient_div_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN) / (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_insulance_mul_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters, &
                (2.d0.unit.kelvin_square_meters_per_watt) * (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_number_div_insulance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (6.d0) / (3.d0.unit.kelvin_square_meters_per_watt))
    end function

    pure function chk_insulance_mul_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.kelvin_square_meters_per_watt) * (3.d0.unit.WATTS_PER_SQUARE_METER_KELVIN))
    end function

    pure function chk_insulance_mul_intensity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.delta_kelvin, &
                (2.d0.unit.kelvin_square_meters_per_watt) * (3.d0.unit.watts_per_square_meter))
    end function

    pure function chk_insulance_mul_thermal_conductivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters, &
                (2.d0.unit.kelvin_square_meters_per_watt) * (3.d0.unit.watts_per_meter_kelvin))
    end function

    pure function chk_intensity_div_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascal_seconds, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_intensity_mul_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.watts_per_square_meter) * (3.d0.unit.square_meters))
    end function

    pure function chk_intensity_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_intensity_div_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.kelvin))
    end function

    pure function chk_intensity_div_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_square_second, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.pascal_seconds))
    end function

    pure function chk_intensity_div_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_square_meter_seconds, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_intensity_div_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_intensity_div_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.WATTS_PER_SQUARE_METER_KELVIN))
    end function

    pure function chk_intensity_mul_insulance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.delta_kelvin, &
                (2.d0.unit.watts_per_square_meter) * (3.d0.unit.kelvin_square_meters_per_watt))
    end function

    pure function chk_intensity_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.watts_per_square_meter) * (3.d0.unit.meters))
    end function

    pure function chk_intensity_div_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_intensity_div_power() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.particles_per_square_meter, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.watts))
    end function

    pure function chk_intensity_div_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.pascals))
    end function

    pure function chk_intensity_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_intensity_div_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_meter_kelvin, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_intensity_div_thermal_conductivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kelvin_per_meter, &
                (6.d0.unit.watts_per_square_meter) / (3.d0.unit.watts_per_meter_kelvin))
    end function

    pure function chk_intensity_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (2.d0.unit.watts_per_square_meter) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_inverse_molar_mass_mul_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_cubic_meter, &
                (2.d0.unit.mols_per_kilogram) * (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_inverse_molar_mass_mul_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols, &
                (2.d0.unit.mols_per_kilogram) * (3.d0.unit.kilograms))
    end function

    pure function chk_inverse_molar_mass_mul_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_second, &
                (2.d0.unit.mols_per_kilogram) * (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_inverse_molar_mass_mul_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.mols_per_kilogram) * (3.d0.unit.joules_per_mol))
    end function

    pure function chk_number_div_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_mol, &
                (6.d0) / (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_inverse_molar_mass_mul_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.mols_per_kilogram) * (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_inverse_molar_mass_mul_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram_kelvin, &
                (2.d0.unit.mols_per_kilogram) * (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_length_mul_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.meters) * (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_length_mul_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters, &
                (2.d0.unit.meters) * (3.d0.unit.square_meters))
    end function

    pure function chk_length_mul_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters_per_second, &
                (2.d0.unit.meters) * (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_length_mul_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.meters) * (3.d0.unit.pascal_seconds))
    end function

    pure function chk_length_div_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters, &
                (6.d0.unit.meters) / (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_length_mul_force() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.meters) * (3.d0.unit.newtons))
    end function

    pure function chk_length_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_second, &
                (2.d0.unit.meters) * (3.d0.unit.hertz))
    end function

    pure function chk_length_mul_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_meter_kelvin, &
                (2.d0.unit.meters) * (3.d0.unit.WATTS_PER_SQUARE_METER_KELVIN))
    end function

    pure function chk_length_div_insulance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_meter_kelvin, &
                (6.d0.unit.meters) / (3.d0.unit.kelvin_square_meters_per_watt))
    end function

    pure function chk_length_mul_intensity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.meters) * (3.d0.unit.watts_per_square_meter))
    end function

    pure function chk_length_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters, &
                (2.d0.unit.meters) * (3.d0.unit.meters))
    end function

    pure function chk_length_mul_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascal_seconds, &
                (2.d0.unit.meters) * (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_length_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters_per_second, &
                (2.d0.unit.meters) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_length_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.meters) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_length_mul_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.delta_kelvin, &
                (2.d0.unit.meters) * (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_length_mul_thermal_conductivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.meters) * (3.d0.unit.watts_per_meter_kelvin))
    end function

    pure function chk_length_div_thermal_conductivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kelvin_square_meters_per_watt, &
                (6.d0.unit.meters) / (3.d0.unit.watts_per_meter_kelvin))
    end function

    pure function chk_length_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.meters) / (3.d0.unit.seconds))
    end function

    pure function chk_length_div_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.particles_per_square_meter, &
                (6.d0.unit.meters) / (3.d0.unit.cubic_meters))
    end function

    pure function chk_length_mul_yank() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.meters) * (3.d0.unit.newtons_per_second))
    end function

    pure function chk_mass_mul_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.kilograms) * (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_mass_div_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_mol, &
                (6.d0.unit.kilograms) / (3.d0.unit.mols))
    end function

    pure function chk_mass_div_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters, &
                (6.d0.unit.kilograms) / (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_mass_mul_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.kilograms) * (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_mass_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.kilograms) * (3.d0.unit.hertz))
    end function

    pure function chk_mass_mul_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols, &
                (2.d0.unit.kilograms) * (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_mass_div_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.kilograms) / (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_mass_div_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols, &
                (6.d0.unit.kilograms) / (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_mass_mul_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.kilograms) * (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_mass_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_second, &
                (6.d0.unit.kilograms) / (3.d0.unit.seconds))
    end function

    pure function chk_mass_div_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_cubic_meter, &
                (6.d0.unit.kilograms) / (3.d0.unit.cubic_meters))
    end function

    pure function chk_mass_flux_mul_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.kilograms_per_square_meter_seconds) * (3.d0.unit.square_meters))
    end function

    pure function chk_mass_flux_div_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.kilograms_per_square_meter_seconds) / (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_mass_flux_mul_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.kilograms_per_square_meter_seconds) * (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_mass_flux_div_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_second, &
                (6.d0.unit.kilograms_per_square_meter_seconds) / (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_mass_flux_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascal_seconds, &
                (2.d0.unit.kilograms_per_square_meter_seconds) * (3.d0.unit.meters))
    end function

    pure function chk_mass_flux_div_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.particles_per_square_meter, &
                (6.d0.unit.kilograms_per_square_meter_seconds) / (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_mass_flux_mul_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (2.d0.unit.kilograms_per_square_meter_seconds) * (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_mass_flux_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.kilograms_per_square_meter_seconds) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_mass_flux_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_cubic_meter, &
                (6.d0.unit.kilograms_per_square_meter_seconds) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_mass_flux_mul_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.kilograms_per_square_meter_seconds) * (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_mass_rate_mul_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.kilograms_per_second) * (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_mass_rate_div_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_mol, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.mols_per_second))
    end function

    pure function chk_mass_rate_div_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_square_meter_seconds, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.square_meters))
    end function

    pure function chk_mass_rate_div_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters_per_second, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_mass_rate_mul_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.kilograms_per_second) * (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_mass_rate_div_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.pascal_seconds))
    end function

    pure function chk_mass_rate_mul_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.kilograms_per_second) * (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_mass_rate_mul_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_square_meter_seconds, &
                (2.d0.unit.kilograms_per_second) * (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_mass_rate_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.hertz))
    end function

    pure function chk_mass_rate_mul_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_second, &
                (2.d0.unit.kilograms_per_second) * (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_mass_rate_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascal_seconds, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.meters))
    end function

    pure function chk_mass_rate_div_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.kilograms))
    end function

    pure function chk_mass_rate_div_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_mass_rate_div_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_second, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_mass_rate_mul_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.kilograms_per_second) * (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_mass_rate_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.kilograms_per_second) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_mass_rate_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms, &
                (2.d0.unit.kilograms_per_second) * (3.d0.unit.seconds))
    end function

    pure function chk_mass_rate_div_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_cubic_meter, &
                (6.d0.unit.kilograms_per_second) / (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_molar_density_div_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_kilogram, &
                (6.d0.unit.mols_per_cubic_meter) / (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_molar_density_div_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_cubic_meter, &
                (6.d0.unit.mols_per_cubic_meter) / (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_molar_density_mul_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.mols_per_cubic_meter) * (3.d0.unit.joules_per_mol))
    end function

    pure function chk_molar_density_mul_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_cubic_meter, &
                (2.d0.unit.mols_per_cubic_meter) * (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_molar_density_mul_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals_per_kelvin, &
                (2.d0.unit.mols_per_cubic_meter) * (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_molar_density_mul_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols, &
                (2.d0.unit.mols_per_cubic_meter) * (3.d0.unit.cubic_meters))
    end function

    pure function chk_molar_density_mul_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_second, &
                (2.d0.unit.mols_per_cubic_meter) * (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_molar_enthalpy_mul_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.joules_per_mol) * (3.d0.unit.mols))
    end function

    pure function chk_molar_enthalpy_mul_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.joules_per_mol) * (3.d0.unit.mols_per_second))
    end function

    pure function chk_molar_enthalpy_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin_mol, &
                (6.d0.unit.joules_per_mol) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_molar_enthalpy_div_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin_mol, &
                (6.d0.unit.joules_per_mol) / (3.d0.unit.kelvin))
    end function

    pure function chk_molar_enthalpy_div_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_mol, &
                (6.d0.unit.joules_per_mol) / (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_molar_enthalpy_mul_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.joules_per_mol) * (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_molar_enthalpy_mul_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.joules_per_mol) * (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_molar_enthalpy_div_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram, &
                (6.d0.unit.joules_per_mol) / (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_molar_enthalpy_div_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.joules_per_mol) / (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_molar_enthalpy_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin_mol, &
                (2.d0.unit.joules_per_mol) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_molar_mass_mul_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms, &
                (2.d0.unit.kilograms_per_mol) * (3.d0.unit.mols))
    end function

    pure function chk_molar_mass_mul_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.kilograms_per_mol) * (3.d0.unit.mols_per_second))
    end function

    pure function chk_molar_mass_mul_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_mol, &
                (2.d0.unit.kilograms_per_mol) * (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_number_div_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_kilogram, &
                (6.d0) / (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_molar_mass_mul_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.kilograms_per_mol) * (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_molar_mass_mul_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_cubic_meter, &
                (2.d0.unit.kilograms_per_mol) * (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_molar_mass_mul_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin_mol, &
                (2.d0.unit.kilograms_per_mol) * (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_molar_specific_heat_mul_amount() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.joules_per_kelvin_mol) * (3.d0.unit.mols))
    end function

    pure function chk_molar_specific_heat_mul_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.joules_per_kelvin_mol) * (3.d0.unit.mols_per_second))
    end function

    pure function chk_molar_specific_heat_mul_amount_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.joules_per_kelvin_mol) * (3.d0.unit.mols_kelvin))
    end function

    pure function chk_molar_specific_heat_mul_amount_temperature_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.joules_per_kelvin_mol) * (3.d0.unit.mols_kelvin_per_second))
    end function

    pure function chk_molar_specific_heat_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_mol, &
                (2.d0.unit.joules_per_kelvin_mol) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_molar_specific_heat_mul_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_mol, &
                (2.d0.unit.joules_per_kelvin_mol) * (3.d0.unit.kelvin))
    end function

    pure function chk_molar_specific_heat_mul_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram_kelvin, &
                (2.d0.unit.joules_per_kelvin_mol) * (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_molar_specific_heat_mul_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals_per_kelvin, &
                (2.d0.unit.joules_per_kelvin_mol) * (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_molar_specific_heat_div_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0.unit.joules_per_kelvin_mol) / (3.d0.unit.joules_per_mol))
    end function

    pure function chk_molar_specific_heat_div_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram_kelvin, &
                (6.d0.unit.joules_per_kelvin_mol) / (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_molar_specific_heat_div_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_mol, &
                (6.d0.unit.joules_per_kelvin_mol) / (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_molar_specific_heat_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_mol, &
                (6.d0.unit.joules_per_kelvin_mol) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_power_div_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_mol, &
                (6.d0.unit.watts) / (3.d0.unit.mols_per_second))
    end function

    pure function chk_power_div_amount_temperature_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin_mol, &
                (6.d0.unit.watts) / (3.d0.unit.mols_kelvin_per_second))
    end function

    pure function chk_power_div_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_square_meter, &
                (6.d0.unit.watts) / (3.d0.unit.square_meters))
    end function

    pure function chk_power_div_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.watts) / (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_power_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_kelvin, &
                (6.d0.unit.watts) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_power_div_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_kelvin, &
                (6.d0.unit.watts) / (3.d0.unit.kelvin))
    end function

    pure function chk_power_div_energy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.watts) / (3.d0.unit.joules))
    end function

    pure function chk_power_div_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_second, &
                (6.d0.unit.watts) / (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_power_mul_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.watts) * (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_power_div_force() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.watts) / (3.d0.unit.newtons))
    end function

    pure function chk_power_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules, &
                (6.d0.unit.watts) / (3.d0.unit.hertz))
    end function

    pure function chk_power_div_intensity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters, &
                (6.d0.unit.watts) / (3.d0.unit.watts_per_square_meter))
    end function

    pure function chk_power_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.newtons_per_second, &
                (6.d0.unit.watts) / (3.d0.unit.meters))
    end function

    pure function chk_power_div_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram, &
                (6.d0.unit.watts) / (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_power_div_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_second, &
                (6.d0.unit.watts) / (3.d0.unit.joules_per_mol))
    end function

    pure function chk_power_div_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_kelvin_per_second, &
                (6.d0.unit.watts) / (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_power_div_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters_per_second, &
                (6.d0.unit.watts) / (3.d0.unit.pascals))
    end function

    pure function chk_power_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.newtons, &
                (6.d0.unit.watts) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_power_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.watts) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_power_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.watts) * (3.d0.unit.seconds))
    end function

    pure function chk_power_div_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals, &
                (6.d0.unit.watts) / (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_power_div_yank() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.watts) / (3.d0.unit.newtons_per_second))
    end function

    pure function chk_pressure_mul_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.pascals) * (3.d0.unit.square_meters))
    end function

    pure function chk_pressure_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals_per_kelvin, &
                (6.d0.unit.pascals) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_pressure_div_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals_per_kelvin, &
                (6.d0.unit.pascals) / (3.d0.unit.kelvin))
    end function

    pure function chk_pressure_div_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram, &
                (6.d0.unit.pascals) / (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_pressure_mul_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.pascals) * (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_pressure_div_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.pascals) / (3.d0.unit.pascal_seconds))
    end function

    pure function chk_pressure_div_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_cubic_meter, &
                (6.d0.unit.pascals) / (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_pressure_div_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.newtons, &
                (6.d0.unit.pascals) / (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_pressure_div_force() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.particles_per_square_meter, &
                (6.d0.unit.pascals) / (3.d0.unit.newtons))
    end function

    pure function chk_pressure_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascal_seconds, &
                (6.d0.unit.pascals) / (3.d0.unit.hertz))
    end function

    pure function chk_pressure_div_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.pascals) / (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_pressure_div_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_mol, &
                (6.d0.unit.pascals) / (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_pressure_div_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_cubic_meter, &
                (6.d0.unit.pascals) / (3.d0.unit.joules_per_mol))
    end function

    pure function chk_pressure_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.pascals) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_pressure_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_square_meter_seconds, &
                (6.d0.unit.pascals) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_pressure_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals_per_kelvin, &
                (2.d0.unit.pascals) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_pressure_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascal_seconds, &
                (2.d0.unit.pascals) * (3.d0.unit.seconds))
    end function

    pure function chk_pressure_mul_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.pascals) * (3.d0.unit.cubic_meters))
    end function

    pure function chk_pressure_mul_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.pascals) * (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_pressure_div_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.pascals) / (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_specific_heat_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.joules_per_kilogram_kelvin) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_specific_heat_mul_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.joules_per_kilogram_kelvin) * (3.d0.unit.kelvin))
    end function

    pure function chk_specific_heat_mul_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals_per_kelvin, &
                (2.d0.unit.joules_per_kilogram_kelvin) * (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_specific_heat_mul_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_meter_kelvin, &
                (2.d0.unit.joules_per_kilogram_kelvin) * (3.d0.unit.pascal_seconds))
    end function

    pure function chk_specific_heat_div_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0.unit.joules_per_kilogram_kelvin) / (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_specific_heat_div_inverse_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin_mol, &
                (6.d0.unit.joules_per_kilogram_kelvin) / (3.d0.unit.mols_per_kilogram))
    end function

    pure function chk_specific_heat_mul_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.joules_per_kilogram_kelvin) * (3.d0.unit.kilograms))
    end function

    pure function chk_specific_heat_mul_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (2.d0.unit.joules_per_kilogram_kelvin) * (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_specific_heat_mul_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.joules_per_kilogram_kelvin) * (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_specific_heat_mul_molar_mass() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin_mol, &
                (2.d0.unit.joules_per_kilogram_kelvin) * (3.d0.unit.kilograms_per_mol))
    end function

    pure function chk_specific_heat_div_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_kilogram, &
                (6.d0.unit.joules_per_kilogram_kelvin) / (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_specific_heat_mul_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_square_second, &
                (2.d0.unit.joules_per_kilogram_kelvin) * (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_specific_heat_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram, &
                (6.d0.unit.joules_per_kilogram_kelvin) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_speed_div_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.meters_per_second) / (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_speed_mul_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters_per_second, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.square_meters))
    end function

    pure function chk_speed_mul_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_square_meter_seconds, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_speed_div_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters_per_second, &
                (6.d0.unit.meters_per_second) / (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_speed_mul_force() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.newtons))
    end function

    pure function chk_speed_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_square_second, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.hertz))
    end function

    pure function chk_speed_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.meters_per_second) / (3.d0.unit.hertz))
    end function

    pure function chk_speed_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters_per_second, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.meters))
    end function

    pure function chk_speed_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.meters_per_second) / (3.d0.unit.meters))
    end function

    pure function chk_speed_mul_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_speed_mul_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_speed_mul_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.pascals))
    end function

    pure function chk_speed_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_speed_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.seconds))
    end function

    pure function chk_speed_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_square_second, &
                (6.d0.unit.meters_per_second) / (3.d0.unit.seconds))
    end function

    pure function chk_speed_div_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.particles_per_square_meter, &
                (6.d0.unit.meters_per_second) / (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_speed_mul_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (2.d0.unit.meters_per_second) * (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_temperature_gradient_mul_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.kelvin_per_meter) * (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_temperature_gradient_mul_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.kelvin_per_meter) * (3.d0.unit.joules_per_kelvin))
    end function

    pure function chk_temperature_gradient_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.delta_kelvin, &
                (2.d0.unit.kelvin_per_meter) * (3.d0.unit.meters))
    end function

    pure function chk_temperature_gradient_mul_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_square_second, &
                (2.d0.unit.kelvin_per_meter) * (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_temperature_gradient_mul_thermal_conductivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.kelvin_per_meter) * (3.d0.unit.watts_per_meter_kelvin))
    end function

    pure function chk_thermal_conductivity_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.watts_per_meter_kelvin) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_thermal_conductivity_mul_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons_per_second, &
                (2.d0.unit.watts_per_meter_kelvin) * (3.d0.unit.kelvin))
    end function

    pure function chk_thermal_conductivity_div_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals_per_kelvin, &
                (6.d0.unit.watts_per_meter_kelvin) / (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_thermal_conductivity_div_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram_kelvin, &
                (6.d0.unit.watts_per_meter_kelvin) / (3.d0.unit.pascal_seconds))
    end function

    pure function chk_thermal_conductivity_div_heat_transfer_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.watts_per_meter_kelvin) / (3.d0.unit.WATTS_PER_SQUARE_METER_KELVIN))
    end function

    pure function chk_thermal_conductivity_mul_insulance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters, &
                (2.d0.unit.watts_per_meter_kelvin) * (3.d0.unit.kelvin_square_meters_per_watt))
    end function

    pure function chk_thermal_conductivity_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.watts_per_meter_kelvin) * (3.d0.unit.meters))
    end function

    pure function chk_thermal_conductivity_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (6.d0.unit.watts_per_meter_kelvin) / (3.d0.unit.meters))
    end function

    pure function chk_thermal_conductivity_div_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascal_seconds, &
                (6.d0.unit.watts_per_meter_kelvin) / (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_thermal_conductivity_mul_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_square_meter, &
                (2.d0.unit.watts_per_meter_kelvin) * (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_thermal_conductivity_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.newtons_per_second, &
                (6.d0.unit.watts_per_meter_kelvin) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_thermal_conductivity_div_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters_per_second, &
                (6.d0.unit.watts_per_meter_kelvin) / (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_thermal_conductivity_div_yank() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0.unit.watts_per_meter_kelvin) / (3.d0.unit.newtons_per_second))
    end function

    pure function chk_thermal_expansion_coefficient_mul_amount_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.mols_kelvin))
    end function

    pure function chk_thermal_expansion_coefficient_mul_amount_temperature_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_second, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.mols_kelvin_per_second))
    end function

    pure function chk_number_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_thermal_expansion_coefficient_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_thermal_expansion_coefficient_mul_energy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.joules))
    end function

    pure function chk_thermal_expansion_coefficient_mul_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kilogram_kelvin, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_thermal_expansion_coefficient_mul_intensity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.watts_per_square_meter))
    end function

    pure function chk_thermal_expansion_coefficient_mul_molar_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin_mol, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.joules_per_mol))
    end function

    pure function chk_thermal_expansion_coefficient_mul_power() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.watts))
    end function

    pure function chk_thermal_expansion_coefficient_mul_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals_per_kelvin, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.pascals))
    end function

    pure function chk_thermal_expansion_coefficient_mul_yank() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_meter_kelvin, &
                (2.d0.unit.per_kelvin) * (3.d0.unit.newtons_per_second))
    end function

    pure function chk_time_mul_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_second, &
                (2.d0.unit.seconds) * (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_time_mul_amount_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols, &
                (2.d0.unit.seconds) * (3.d0.unit.mols_per_second))
    end function

    pure function chk_time_mul_amount_temperature_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_kelvin, &
                (2.d0.unit.seconds) * (3.d0.unit.mols_kelvin_per_second))
    end function

    pure function chk_time_mul_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.seconds) * (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_time_mul_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters, &
                (2.d0.unit.seconds) * (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_time_mul_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.square_meters_per_second, &
                (2.d0.unit.seconds) * (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_number_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0) / (3.d0.unit.seconds))
    end function

    pure function chk_time_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0, &
                (2.d0.unit.seconds) * (3.d0.unit.hertz))
    end function

    pure function chk_time_mul_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms, &
                (2.d0.unit.seconds) * (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_time_mul_power() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.seconds) * (3.d0.unit.watts))
    end function

    pure function chk_time_mul_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascal_seconds, &
                (2.d0.unit.seconds) * (3.d0.unit.pascals))
    end function

    pure function chk_time_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters, &
                (2.d0.unit.seconds) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_time_mul_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters, &
                (2.d0.unit.seconds) * (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_time_mul_yank() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.seconds) * (3.d0.unit.newtons_per_second))
    end function

    pure function chk_volume_div_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.cubic_meters) / (3.d0.unit.square_meters))
    end function

    pure function chk_volume_mul_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms, &
                (2.d0.unit.cubic_meters) * (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_volume_mul_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters, &
                (2.d0.unit.cubic_meters) * (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_volume_mul_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters_per_second, &
                (2.d0.unit.cubic_meters) * (3.d0.unit.hertz))
    end function

    pure function chk_volume_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters, &
                (6.d0.unit.cubic_meters) / (3.d0.unit.meters))
    end function

    pure function chk_volume_mul_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols, &
                (2.d0.unit.cubic_meters) * (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_volume_mul_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.cubic_meters) * (3.d0.unit.pascals))
    end function

    pure function chk_volume_div_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters_per_second, &
                (6.d0.unit.cubic_meters) / (3.d0.unit.seconds))
    end function

    pure function chk_volume_div_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.seconds, &
                (6.d0.unit.cubic_meters) / (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_volume_mul_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.cubic_meters) * (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_volume_rate_div_area() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_second, &
                (6.d0.unit.cubic_meters_per_second) / (3.d0.unit.square_meters))
    end function

    pure function chk_volume_rate_mul_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.kilograms_per_second, &
                (2.d0.unit.cubic_meters_per_second) * (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_volume_rate_div_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.cubic_meters_per_second) / (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_volume_rate_mul_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules, &
                (2.d0.unit.cubic_meters_per_second) * (3.d0.unit.pascal_seconds))
    end function

    pure function chk_volume_rate_mul_fluence() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.meters_per_second, &
                (2.d0.unit.cubic_meters_per_second) * (3.d0.unit.particles_per_square_meter))
    end function

    pure function chk_volume_rate_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.cubic_meters, &
                (6.d0.unit.cubic_meters_per_second) / (3.d0.unit.hertz))
    end function

    pure function chk_volume_rate_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters_per_second, &
                (6.d0.unit.cubic_meters_per_second) / (3.d0.unit.meters))
    end function

    pure function chk_volume_rate_mul_mass_flux() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.cubic_meters_per_second) * (3.d0.unit.kilograms_per_square_meter_seconds))
    end function

    pure function chk_volume_rate_mul_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.mols_per_second, &
                (2.d0.unit.cubic_meters_per_second) * (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_volume_rate_mul_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.cubic_meters_per_second) * (3.d0.unit.pascals))
    end function

    pure function chk_volume_rate_div_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters, &
                (6.d0.unit.cubic_meters_per_second) / (3.d0.unit.meters_per_second))
    end function

    pure function chk_volume_rate_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.cubic_meters, &
                (2.d0.unit.cubic_meters_per_second) * (3.d0.unit.seconds))
    end function

    pure function chk_volume_rate_div_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.cubic_meters_per_second) / (3.d0.unit.cubic_meters))
    end function

    pure function chk_volume_rate_mul_volumetric_heat_capacity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.cubic_meters_per_second) * (3.d0.unit.pascals_per_kelvin))
    end function

    pure function chk_volumetric_heat_capacity_mul_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.pascals_per_kelvin) * (3.d0.unit.delta_kelvin))
    end function

    pure function chk_volumetric_heat_capacity_mul_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.pascals, &
                (2.d0.unit.pascals_per_kelvin) * (3.d0.unit.kelvin))
    end function

    pure function chk_volumetric_heat_capacity_div_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram_kelvin, &
                (6.d0.unit.pascals_per_kelvin) / (3.d0.unit.kilograms_per_cubic_meter))
    end function

    pure function chk_volumetric_heat_capacity_mul_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_meter_kelvin, &
                (2.d0.unit.pascals_per_kelvin) * (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_volumetric_heat_capacity_div_molar_density() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kelvin_mol, &
                (6.d0.unit.pascals_per_kelvin) / (3.d0.unit.mols_per_cubic_meter))
    end function

    pure function chk_volumetric_heat_capacity_div_molar_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.mols_per_cubic_meter, &
                (6.d0.unit.pascals_per_kelvin) / (3.d0.unit.joules_per_kelvin_mol))
    end function

    pure function chk_volumetric_heat_capacity_div_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.per_kelvin, &
                (6.d0.unit.pascals_per_kelvin) / (3.d0.unit.pascals))
    end function

    pure function chk_volumetric_heat_capacity_div_specific_heat() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_cubic_meter, &
                (6.d0.unit.pascals_per_kelvin) / (3.d0.unit.joules_per_kilogram_kelvin))
    end function

    pure function chk_volumetric_heat_capacity_mul_speed() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.WATTS_PER_SQUARE_METER_KELVIN, &
                (2.d0.unit.pascals_per_kelvin) * (3.d0.unit.meters_per_second))
    end function

    pure function chk_volumetric_heat_capacity_div_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals, &
                (6.d0.unit.pascals_per_kelvin) / (3.d0.unit.per_kelvin))
    end function

    pure function chk_volumetric_heat_capacity_mul_volume() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.joules_per_kelvin, &
                (2.d0.unit.pascals_per_kelvin) * (3.d0.unit.cubic_meters))
    end function

    pure function chk_volumetric_heat_capacity_mul_volume_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_kelvin, &
                (2.d0.unit.pascals_per_kelvin) * (3.d0.unit.cubic_meters_per_second))
    end function

    pure function chk_yank_div_acceleration() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kilograms_per_second, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.meters_per_square_second))
    end function

    pure function chk_yank_div_conductance() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.kelvin_per_meter, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.watts_per_kelvin))
    end function

    pure function chk_yank_div_delta_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_meter_kelvin, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.delta_kelvin))
    end function

    pure function chk_yank_div_temperature() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_meter_kelvin, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.kelvin))
    end function

    pure function chk_yank_div_diffusivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascals, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.square_meters_per_second))
    end function

    pure function chk_yank_div_dynamic_viscosity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.joules_per_kilogram, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.pascal_seconds))
    end function

    pure function chk_yank_div_enthalpy() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.pascal_seconds, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.joules_per_kilogram))
    end function

    pure function chk_yank_div_force() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.hertz, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.newtons))
    end function

    pure function chk_yank_div_frequency() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.newtons, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.hertz))
    end function

    pure function chk_yank_div_intensity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.watts_per_square_meter))
    end function

    pure function chk_yank_mul_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts, &
                (2.d0.unit.newtons_per_second) * (3.d0.unit.meters))
    end function

    pure function chk_yank_div_length() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_square_meter, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.meters))
    end function

    pure function chk_yank_div_mass_rate() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.meters_per_square_second, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.kilograms_per_second))
    end function

    pure function chk_yank_div_pressure() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.square_meters_per_second, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.pascals))
    end function

    pure function chk_yank_div_temperature_gradient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.watts_per_kelvin, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.kelvin_per_meter))
    end function

    pure function chk_yank_div_thermal_conductivity() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.newtons_per_second) / (3.d0.unit.watts_per_meter_kelvin))
    end function

    pure function chk_yank_mul_thermal_expansion_coefficient() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.watts_per_meter_kelvin, &
                (2.d0.unit.newtons_per_second) * (3.d0.unit.per_kelvin))
    end function

    pure function chk_yank_mul_time() result(result_)
        type(result_t) :: result_
         
        result_ = assert_equals( &
                6.d0.unit.newtons, &
                (2.d0.unit.newtons_per_second) * (3.d0.unit.seconds))
    end function

    pure function chk_temp_minus_temp() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                2.d0.unit.delta_kelvin, &
                (6.d0.unit.kelvin) - (4.d0.unit.kelvin))
    end function
    
    pure function chk_temp_minus_delta_temp() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                2.d0.unit.kelvin, &
                (6.d0.unit.kelvin) - (4.d0.unit.delta_kelvin))
    end function
    
    pure function chk_temp_plus_delta_temp() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                9.d0.unit.kelvin, &
                (6.d0.unit.kelvin) + (3.d0.unit.delta_kelvin))
    end function
    
    pure function chk_delta_temp_plus_temp() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                9.d0.unit.kelvin, &
                (6.d0.unit.delta_kelvin) + (3.d0.unit.kelvin))
    end function
    
    pure function chk_sqrt_area() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                3.d0.unit.meters, &
                sqrt(9.d0.unit.square_meters))
    end function
    
    pure function chk_sqrt_enthalpy() result(result_)
        type(result_t) :: result_
        
        result_ = assert_equals( &
                3.d0.unit.meters_per_second, &
                sqrt(9.d0.unit.joules_per_kilogram))
    end function

    pure function chk_cbrt_volume() result(result_)
        type(result_t) :: result_

        result_ = assert_equals( &
                3.d0.unit.meters, &
                cbrt(27.d0.unit.cubic_meters))
    end function
end module
