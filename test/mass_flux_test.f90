module mass_flux_test
    use double_precision_generator_m, only: DOUBLE_PRECISION_GENERATOR
    use double_precision_pair_generator_m, only: DOUBLE_PRECISION_PAIR_GENERATOR
    use double_precision_pair_input_m, only: double_precision_pair_input_t
    use erloff, only: error_list_t
    use iso_varying_string, only: operator(//)
    use non_zero_double_precision_generator_m, only: &
            NON_ZERO_DOUBLE_PRECISION_GENERATOR
    use non_zero_double_precision_pair_generator_m, only: &
            NON_ZERO_DOUBLE_PRECISION_PAIR_GENERATOR
    use quaff
    use quaff_asserts_m, only: assert_equals, assert_equals_within_relative
    use mass_flux_utilities_m, only: &
            units_input_t, units_pair_input_t, make_units_examples
    use units_examples_m, only: units_examples_t
    use veggies, only: &
            double_precision_input_t, &
            input_t, &
            result_t, &
            test_item_t, &
            test_result_item_t, &
            assert_equals, &
            assert_equals_within_relative, &
            assert_not, &
            assert_that, &
            describe, &
            fail, &
            it

    implicit none
    private
    public :: test_mass_flux
contains
    function test_mass_flux() result(tests)
        type(test_item_t) :: tests

        type(units_examples_t) :: examples

        examples = make_units_examples(provided_mass_flux_units)
        tests = describe( &
                "mass_flux_t", &
                [ it( &
                        "returns the same value given the same units", &
                        examples%units(), &
                        check_round_trip) &
                , it( &
                        "preserves its value converting to and from a string", &
                        examples%units(), &
                        check_to_and_from_string) &
                , it( &
                        "returns an error trying to parse a bad string", &
                        check_bad_string) &
                , it( &
                        "returns an error trying to parse an unknown unit", &
                        check_bad_unit) &
                , it( &
                        "returns an error trying to parse a bad number", &
                        check_bad_number) &
                , it( &
                        "the conversion factors between two units are inverses", &
                        examples%pairs(), &
                        check_conversion_factors_inverse) &
                , it("arrays can be summed", check_sum) &
                , it("can take the absolute value", check_abs) &
                , it("can be negated", DOUBLE_PRECISION_GENERATOR, check_negation) &
                , it( &
                        "adding zero returns the original mass_flux", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_add_zero) &
                , it( &
                        "subtracting zero returns the original mass_flux", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_subtract_zero) &
                , it( &
                        "adding and subtracting the same mass_flux &
                        &returns the original mass_flux", &
                        DOUBLE_PRECISION_PAIR_GENERATOR, &
                        check_add_subtract) &
                , it( &
                        "multiplying by one returns the original mass_flux", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_multiply_by_one) &
                , it( &
                        "multiplying by zero returns zero mass_flux", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_multiply_by_zero) &
                , it( &
                        "dividing by one returns the original mass_flux", &
                        DOUBLE_PRECISION_GENERATOR, &
                        check_divide_by_one) &
                , it( &
                        "dividing by itself returns one", &
                        NON_ZERO_DOUBLE_PRECISION_GENERATOR, &
                        check_divide_by_self) &
                , it( &
                        "multiplying and dividing by the same number returns the original mass_flux", &
                        NON_ZERO_DOUBLE_PRECISION_PAIR_GENERATOR, &
                        check_multiply_divide) &
                , it( &
                        "min and max return the smaller or larger of its argument respectively", &
                        check_min_max) &
                , describe( &
                        "operator(==)", &
                        [ it( &
                                "is true for the same mass_flux", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_with_same_number) &
                        , it( &
                                "is false for different mass_fluxs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_with_different_numbers) &
                        ]) &
                , describe( &
                        "operator(/=)", &
                        [ it( &
                                "is false for the same mass_flux", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_not_equal_with_same_number) &
                        , it( &
                                "is true for different mass_fluxs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_not_equal_with_different_numbers) &
                        ]) &
                , describe( &
                        "%equal(mass_flux, within)", &
                        [ it( &
                                "is true for the same mass_flux even for tiny tolerance", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_within_with_same_number) &
                        , it( &
                                "is true for sufficiently close values", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_within_with_close_numbers) &
                        , it( &
                                "is false for sufficiently different numbers", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_equal_within_with_different_numbers) &
                        ]) &
                , describe( &
                        "operator(>=)", &
                        [ it( &
                                "is true if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_or_equal_with_greater_number) &
                        , it( &
                                "is true if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_or_equal_with_same_numbers) &
                        , it( &
                                "is false if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_or_equal_with_lesser_number) &
                        ]) &
                , describe( &
                        "operator(<=)", &
                        [ it( &
                                "is true if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_or_equal_with_less_number) &
                        , it( &
                                "is true if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_or_equal_with_same_numbers) &
                        , it( &
                                "is false if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_or_equal_with_greater_number) &
                        ]) &
                , describe( &
                        "operator(>)", &
                        [ it( &
                                "is true if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_with_greater_number) &
                        , it( &
                                "is false if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_with_same_numbers) &
                        , it( &
                                "is false if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_greater_than_with_lesser_number) &
                        ]) &
                , describe( &
                        "operator(<)", &
                        [ it( &
                                "is true if the lhs is less than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_with_less_number) &
                        , it( &
                                "is false if the lhs is equal to the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_with_same_numbers) &
                        , it( &
                                "is false if the lhs is greater than the rhs", &
                                DOUBLE_PRECISION_GENERATOR, &
                                check_less_than_with_greater_number) &
                        ]) &
                ])
    end function

    function check_round_trip(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_input_t)
            result_ = check_round_trip_in(input%unit())
        class default
            result_ = fail("Expected to get a units_input_t")
        end select
    end function

    function check_round_trip_in(units) result(result_)
        class(mass_flux_unit_t), intent(in) :: units
        type(result_t) :: result_

        type(test_item_t) :: the_test
        type(test_result_item_t) :: the_result

        the_test = it(to_string(units), DOUBLE_PRECISION_GENERATOR, check_round_trip_)
        the_result = the_test%run()
        result_ = assert_that(the_result%passed(), the_result%verbose_description(.false.))
    contains
        pure function check_round_trip_(input) result(result__)
            class(input_t), intent(in) :: input
            type(result_t) :: result__

            type(mass_flux_t) :: intermediate
            double precision :: original
            real :: lower_precision
            integer :: int_part

            select type (input)
            type is (double_precision_input_t)
                original = input%input()
                intermediate = original.unit.units
                result__ = assert_equals_within_relative( &
                        original, &
                        intermediate.in.units, &
                        1.0d-12)
                lower_precision = real(original)
                intermediate = lower_precision.unit.units
                result__ = result__.and.assert_equals_within_relative( &
                        real(lower_precision, kind=kind(original)), &
                        intermediate.in.units, &
                        1.0d-12)
                int_part = int(original)
                intermediate = int_part.unit.units
                result__ = result__.and.assert_equals_within_relative( &
                        real(int_part, kind=kind(original)), &
                        intermediate.in.units, &
                        1.0d-12)
            class default
                result__ = fail("Expected to get a double_precision_input_t")
            end select
        end function
    end function

    function check_to_and_from_string(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_input_t)
            result_ = check_string_trip(input%unit())
        class default
            result_ = fail("Expected to get an units_input_t")
        end select
    end function

    function check_string_trip(units) result(result_)
        class(mass_flux_unit_t), intent(in) :: units
        type(result_t) :: result_

        type(test_item_t) :: the_test
        type(test_result_item_t) :: the_result

        the_test = it(to_string(units), DOUBLE_PRECISION_GENERATOR, do_check)
        the_result = the_test%run()
        result_ = assert_that(the_result%passed(), the_result%verbose_description(.false.))
    contains
        function do_check(input) result(result__)
            class(input_t), intent(in) :: input
            type(result_t) :: result__

            type(error_list_t) :: errors
            type(mass_flux_t) :: original_mass_flux
            type(fallible_mass_flux_t) :: maybe_mass_flux
            type(mass_flux_t) :: new_mass_flux

            select type (input)
            type is (double_precision_input_t)
                original_mass_flux = input%input().unit.units
                maybe_mass_flux = parse_mass_flux( &
                        to_string(original_mass_flux, units))
                new_mass_flux = maybe_mass_flux%mass_flux()
                errors = maybe_mass_flux%errors()
                result__ = &
                        assert_equals( &
                                original_mass_flux, &
                                new_mass_flux) &
                        .and.assert_not(errors%has_any(), errors%to_string())
            class default
                result__ = fail("Expected to get a double_precision_input_t")
            end select
        end function
    end function

    function check_bad_string() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_mass_flux_t) :: maybe_mass_flux

        maybe_mass_flux = parse_mass_flux("bad")
        errors = maybe_mass_flux%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_bad_unit() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_mass_flux_t) :: maybe_mass_flux

        maybe_mass_flux = parse_mass_flux("1.0 kg/(m^2 s)bad", [kilograms_per_square_meter_seconds])
        errors = maybe_mass_flux%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_bad_number() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_mass_flux_t) :: maybe_mass_flux

        maybe_mass_flux = parse_mass_flux("bad kg/(m^2 s)")
        errors = maybe_mass_flux%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_conversion_factors_inverse(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_pair_input_t)
            result_ = check_conversion_factors_are_inverse(input%first(), input%second_())
        class default
            result_ = fail("Expected to get a units_pair_input_t")
        end select
    end function

    pure function check_conversion_factors_are_inverse( &
            from, to) result(result_)
        class(mass_flux_unit_t), intent(in) :: to
        class(mass_flux_unit_t), intent(in) :: from
        type(result_t) :: result_

        double precision :: factor1
        double precision :: factor2

        factor1 = (1.0d0.unit.from).in.to
        factor2 = (1.0d0.unit.to).in.from
        result_ = assert_equals_within_relative( &
                factor1, &
                1.0d0 / factor2, &
                1.0d-12, &
                to_string(from) // " to " // to_string(to))
    end function

    pure function check_sum() result(result_)
        type(result_t) :: result_

        double precision, parameter :: numbers(*) = [1.0d0, 2.0d0, 3.0d0]

        result_ = assert_equals( &
                sum(numbers).unit.kilograms_per_square_meter_seconds, &
                sum(numbers.unit.kilograms_per_square_meter_seconds))
    end function

    pure function check_abs() result(result_)
        type(result_t) :: result_

        result_ = &
                assert_equals( &
                        abs(1.0d0).unit.kilograms_per_square_meter_seconds, &
                        abs(1.0d0.unit.kilograms_per_square_meter_seconds)) &
                .and.assert_equals( &
                        abs(-1.0d0).unit.kilograms_per_square_meter_seconds, &
                        abs((-1.0d0).unit.kilograms_per_square_meter_seconds))
    end function

    pure function check_negation(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type(input)
        type is (double_precision_input_t)
            result_ = assert_equals( &
                    (-input%input()).unit.kilograms_per_square_meter_seconds, &
                    -(input%input().unit.kilograms_per_square_meter_seconds))
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_add_zero(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux
        type(mass_flux_t) :: zero

        select type(input)
        type is (double_precision_input_t)
            mass_flux = input%input().unit.kilograms_per_square_meter_seconds
            zero = 0.0d0.unit.kilograms_per_square_meter_seconds
            result_ = assert_equals(mass_flux, mass_flux + zero)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_subtract_zero(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux
        type(mass_flux_t) :: zero

        select type(input)
        type is (double_precision_input_t)
            mass_flux = input%input().unit.kilograms_per_square_meter_seconds
            zero = 0.0d0.unit.kilograms_per_square_meter_seconds
            result_ = assert_equals(mass_flux, mass_flux - zero)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_add_subtract(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type(input)
        type is (double_precision_pair_input_t)
            mass_flux1 = input%first().unit.kilograms_per_square_meter_seconds
            mass_flux2 = input%second_().unit.kilograms_per_square_meter_seconds
            result_ = assert_equals_within_relative( &
                    mass_flux1, &
                    (mass_flux1 + mass_flux2) - mass_flux2, &
                    1.0d-8, &
                    "mass_flux1 = " // to_string(mass_flux1) &
                    // ", mass_flux2 = " // to_string(mass_flux2))
        class default
            result_ = fail("Expected a double_precision_pair_input_t")
        end select
    end function

    pure function check_multiply_by_one(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux

        select type(input)
        type is (double_precision_input_t)
            mass_flux = input%input().unit.kilograms_per_square_meter_seconds
            result_ = &
                    assert_equals(mass_flux, mass_flux * 1.0d0) &
                    .and. assert_equals(mass_flux, mass_flux * 1.0)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_multiply_by_zero(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux
        type(mass_flux_t) :: zero

        select type(input)
        type is (double_precision_input_t)
            mass_flux = input%input().unit.kilograms_per_square_meter_seconds
            zero = 0.0d0.unit.kilograms_per_square_meter_seconds
            result_ = &
                    assert_equals(zero, mass_flux * 0.0d0) &
                    .and. assert_equals(zero, mass_flux * 0.0)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_divide_by_one(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux

        select type(input)
        type is (double_precision_input_t)
            mass_flux = input%input().unit.kilograms_per_square_meter_seconds
            result_ = &
                    assert_equals(mass_flux, mass_flux / 1.0d0) &
                    .and. assert_equals(mass_flux, mass_flux / 1.0)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_divide_by_self(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux

        select type(input)
        type is (double_precision_input_t)
            mass_flux = input%input().unit.kilograms_per_square_meter_seconds
            result_ = assert_equals(1.0d0, mass_flux / mass_flux)
        class default
            result_ = fail("Expected a double_precision_input_t")
        end select
    end function

    pure function check_multiply_divide(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux

        select type (input)
        type is (double_precision_pair_input_t)
            mass_flux = input%first().unit.kilograms_per_square_meter_seconds
            result_ = assert_equals( &
                    mass_flux, &
                    (mass_flux * input%second_()) / input%second_())
        class default
            result_ = fail("Expected a double_precision_pair_input_t")
        end select
    end function
    
    pure function check_min_max() result(result_)
        type(result_t) :: result_
        
        type(mass_flux_t) :: smaller, larger

        smaller = 1.d0.unit.kilograms_per_square_meter_seconds
        larger = 2.d0.unit.kilograms_per_square_meter_seconds

        result_ = &
            assert_equals(smaller, min(smaller, larger)) &
            .and.assert_equals(smaller, min(larger, smaller)) &
            .and.assert_equals(larger, max(smaller, larger)) &
            .and.assert_equals(larger, max(larger, smaller))
    end function

    pure function check_equal_with_same_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: the_mass_flux

        select type (input)
        type is (double_precision_input_t)
            the_mass_flux = input%input().unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    the_mass_flux == the_mass_flux, &
                    to_string(the_mass_flux) // " == " // to_string(the_mass_flux))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_with_different_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() + 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_not( &
                    mass_flux1 == mass_flux2, &
                    to_string(mass_flux1) // " == " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_not_equal_with_same_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: the_mass_flux

        select type (input)
        type is (double_precision_input_t)
            the_mass_flux = input%input().unit.kilograms_per_square_meter_seconds
            result_ = assert_not( &
                    the_mass_flux /= the_mass_flux, &
                    to_string(the_mass_flux) // " /= " // to_string(the_mass_flux))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_not_equal_with_different_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() + 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    mass_flux1 /= mass_flux2, &
                    to_string(mass_flux1) // " /= " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_within_with_same_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: the_mass_flux
        type(mass_flux_t) :: tolerance

        select type (input)
        type is (double_precision_input_t)
            the_mass_flux = input%input().unit.kilograms_per_square_meter_seconds
            tolerance = tiny(1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    equal(the_mass_flux, the_mass_flux, within = tolerance), &
                    "equal(" // to_string(the_mass_flux) // ", " &
                        // to_string(the_mass_flux) // ", within = " &
                        // to_string(tolerance) // ")")
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_within_with_close_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2
        type(mass_flux_t) :: tolerance

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() + 0.05d0).unit.kilograms_per_square_meter_seconds
            tolerance = 0.1d0.unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    equal(mass_flux1, mass_flux2, within = tolerance), &
                    "equal(" // to_string(mass_flux1) // ", " &
                        // to_string(mass_flux2) // ", within = " &
                        // to_string(tolerance) // ")")
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_equal_within_with_different_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2
        type(mass_flux_t) :: tolerance

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() + 0.2d0).unit.kilograms_per_square_meter_seconds
            tolerance = 0.1d0.unit.kilograms_per_square_meter_seconds
            result_ = assert_not( &
                    equal(mass_flux1, mass_flux2, within = tolerance), &
                    "equal(" // to_string(mass_flux1) // ", " &
                    // to_string(mass_flux2) // ", within = " &
                    // to_string(tolerance) // ")")
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_or_equal_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit. kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() - 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    mass_flux1 >= mass_flux2, &
                    to_string(mass_flux1) // " >= " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_or_equal_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = input%input().unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    mass_flux1 >= mass_flux2, &
                    to_string(mass_flux1) // " >= " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_or_equal_with_lesser_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() + 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_not( &
                    mass_flux1 >= mass_flux2, &
                    to_string(mass_flux1) // " >= " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_or_equal_with_less_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit. kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() + 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    mass_flux1 <= mass_flux2, &
                    to_string(mass_flux1) // " <= " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_or_equal_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = input%input().unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    mass_flux1 <= mass_flux2, &
                    to_string(mass_flux1) // " <= " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_or_equal_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() - 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_not( &
                    mass_flux1 <= mass_flux2, &
                    to_string(mass_flux1) // " <= " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit. kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() - 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    mass_flux1 > mass_flux2, &
                    to_string(mass_flux1) // " > " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = input%input().unit.kilograms_per_square_meter_seconds
            result_ = assert_not( &
                    mass_flux1 > mass_flux2, &
                    to_string(mass_flux1) // " > " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_greater_than_with_lesser_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() + 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_not( &
                    mass_flux1 > mass_flux2, &
                    to_string(mass_flux1) // " > " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_with_less_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit. kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() + 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_that( &
                    mass_flux1 < mass_flux2, &
                    to_string(mass_flux1) // " < " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_with_same_numbers(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = input%input().unit.kilograms_per_square_meter_seconds
            result_ = assert_not( &
                    mass_flux1 < mass_flux2, &
                    to_string(mass_flux1) // " <=" // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function

    pure function check_less_than_with_greater_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(mass_flux_t) :: mass_flux1
        type(mass_flux_t) :: mass_flux2

        select type (input)
        type is (double_precision_input_t)
            mass_flux1 = input%input().unit.kilograms_per_square_meter_seconds
            mass_flux2 = (input%input() - 1.0d0).unit.kilograms_per_square_meter_seconds
            result_ = assert_not( &
                    mass_flux1 < mass_flux2, &
                    to_string(mass_flux1) // " < " // to_string(mass_flux2))
        class default
            result_ = fail("Expected to get a double_precision_input_t")
        end select
    end function
end module
