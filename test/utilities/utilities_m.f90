module test_utilities_m
    implicit none
    private
    public :: combinations, effectively_zero

    double precision, parameter :: MACHINE_EPSILON = epsilon(1.0d0)
contains
    pure recursive function combinations(num_items) result(num_combinations)
        integer, intent(in) :: num_items
        integer :: num_combinations

        if (num_items <= 1) then
            num_combinations = 0
        else
            num_combinations = num_items - 1 + combinations(num_items - 1)
        end if
    end function

    elemental function effectively_zero(a)
        double precision, intent(in) :: a
        logical :: effectively_zero

        effectively_zero = abs(a) < MACHINE_EPSILON
    end function
end module
